# Sistem Informasi Pegawai

## v0.6.2 - 27 Desember 2020
- delete employee position
- cron job to delete unused files
- employee position: browse employee

## v0.6.1 - 22 Desember 2020
- redesign login page
- ImageUpload placeholder
- refactor employee kid
- refactor employee parent
- refactor employee education
- refactor employee language

## v0.6.0 - 19 Desember 2020
- SKP Pegawai
- Mutasi Pegawai
- Laporan Nominatif Pegawai
- Laporan DUK Pegawai
- Laporan Bezetting
- Laporan Rekapitulasi

## v0.5.1 - 11 Oktober 2020
- kepegawaian: bulk delete
- kepegawaian: hide employee jika status edit
- kepegawaian: tab kepegawaian

## v0.5.0 - 11 Oktober 2020
- kepegawaian seminar
- kepegawaian penugasan
- kepegawaian penghargaan
- kepegawaian diklat
- kepegawaian hukuman
- kepegawaian cuti
- kepegawaian latihan jabatan
- kepegawaian tunjangan

## v0.4.2 - 22 September 2020
- employee detail: kepegawaian pangkat

## v0.4.1 - 22 September 2020
- refactor upload photo user

## v0.4.0 - 15 September 2020
- kepegawaian pangkat
- bulk delete
- refactor file upload

## v0.3.1 - 13 September 2020
- refactor migration seed employee group

## v0.3.0 - 30 Mei 2020
- refactor file path
- kepegawaian jabatan
- refactor file upload
- page component directory
- module resolver
- refactor app navigation
- implement unit test
- employee detail

## v0.2.0 - 21 Mei 2020
- riwayat keluarga pegawai
- riwayat pendidikan pegawai

## v0.1.0 - 07 Mei 2020
- refactor menu
- user: implementasi API
- unit kerja: implementasi API
- redux
- sidepopup validation
- side menu icon
- breadcrumb
- refactor pegawai