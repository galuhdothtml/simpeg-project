DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_rpt_recap_position;

CREATE PROCEDURE `sp_rpt_recap_position`()
BEGIN
  SELECT p.name, COUNT(*) AS employee_total FROM Employees e
  JOIN EmployeePositions ep ON ep.id_employee = e.id
  JOIN Positions p ON p.id = ep.id_position
  WHERE ep.is_active = 1
  GROUP BY p.name;
END ;;
DELIMITER ;