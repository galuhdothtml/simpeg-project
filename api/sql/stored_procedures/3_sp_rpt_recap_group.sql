DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_rpt_recap_group;

CREATE PROCEDURE `sp_rpt_recap_group`()
BEGIN
  SELECT g.name, COUNT(*) AS employee_total FROM Employees e
  JOIN EmployeeLevels el ON el.id_employee = e.id
  JOIN Groups g ON g.id = el.id_group
  WHERE el.is_active = 1
  GROUP BY g.name;
END ;;
DELIMITER ;