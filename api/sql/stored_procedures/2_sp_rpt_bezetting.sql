DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_rpt_bezetting;

CREATE PROCEDURE `sp_rpt_bezetting`()
BEGIN
  SELECT e.fullname, e.birth_place, e.birth_date, e.nip,
  (SELECT el.level FROM EmployeeLevels el JOIN Groups g ON g.id=el.id_group WHERE el.id_employee=e.id AND el.is_active='1' LIMIT 1) AS employee_level,
  (SELECT g.name FROM EmployeeLevels el JOIN Groups g ON g.id=el.id_group WHERE el.id_employee=e.id AND el.is_active='1' LIMIT 1) AS employee_group_level,
  (SELECT p.name FROM EmployeePositions ep JOIN Positions p ON p.id=ep.id_position JOIN Echelons ec ON ec.id=ep.id_echelon WHERE ep.id_employee = e.id AND ep.is_active='1' LIMIT 1) AS employee_position,
  (SELECT degree FROM EmployeeSchools es WHERE es.id_employee=e.id LIMIT 1) AS degree,
  e.employee_status
  FROM Employees e;
END ;;
DELIMITER ;