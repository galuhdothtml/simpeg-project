DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_rpt_recap_work_unit;

CREATE PROCEDURE `sp_rpt_recap_work_unit`()
BEGIN
  SELECT wu.name, COUNT(*) AS employee_total FROM Employees e
  JOIN WorkUnits wu ON wu.id = e.id_work_unit
  GROUP BY wu.name;
END ;;
DELIMITER ;