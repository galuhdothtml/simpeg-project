DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_rpt_nominative_employee;

CREATE PROCEDURE `sp_rpt_nominative_employee`()
BEGIN
  SELECT e.nip, e.fullname, e.birth_place, e.birth_date, r.name as religion_name, e.gender,
  (SELECT el.level FROM EmployeeLevels el JOIN Groups g ON g.id=el.id_group WHERE el.id_employee=e.id AND el.is_active='1' LIMIT 1) AS employee_level,
  (SELECT g.name FROM EmployeeLevels el JOIN Groups g ON g.id=el.id_group WHERE el.id_employee=e.id AND el.is_active='1' LIMIT 1) AS employee_group_level,
  (SELECT el.tmt_level FROM EmployeeLevels el JOIN Groups g ON g.id=el.id_group WHERE el.id_employee=e.id AND el.is_active='1' LIMIT 1) AS employee_tmt_level,
  (SELECT p.name FROM EmployeePositions ep JOIN Positions p ON p.id=ep.id_position JOIN Echelons ec ON ec.id=ep.id_echelon WHERE ep.id_employee = e.id AND ep.is_active='1' LIMIT 1) AS employee_position,
  (SELECT ep.tmt_position_from FROM EmployeePositions ep JOIN Positions p ON p.id=ep.id_position JOIN Echelons ec ON ec.id=ep.id_echelon WHERE ep.id_employee = e.id AND ep.is_active='1' LIMIT 1) AS employee_tmt_position,
  (SELECT ec.name FROM EmployeePositions ep JOIN Positions p ON p.id=ep.id_position JOIN Echelons ec ON ec.id=ep.id_echelon WHERE ep.id_employee = e.id AND ep.is_active='1' LIMIT 1) AS employee_echelon_position,
  (SELECT degree FROM EmployeeSchools es WHERE es.id_employee=e.id LIMIT 1) AS degree,
  (SELECT school_name FROM EmployeeSchools es WHERE es.id_employee=e.id LIMIT 1) AS school_name,
  (SELECT department FROM EmployeeSchools es WHERE es.id_employee=e.id LIMIT 1) AS department,
  (SELECT certificate_date FROM EmployeeSchools es WHERE es.id_employee=e.id LIMIT 1) AS certificate_date,
  e.address, e.phone, e.employee_status
  FROM Employees e JOIN Religions r ON r.id = e.id_religion;
END ;;
DELIMITER ;