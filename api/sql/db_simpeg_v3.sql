-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: db_simpeg_v3
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Menus`
--

DROP TABLE IF EXISTS `Menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Menus`
--

LOCK TABLES `Menus` WRITE;
/*!40000 ALTER TABLE `Menus` DISABLE KEYS */;
INSERT INTO `Menus` VALUES (1,'Pegawai','[{\"id\":\"1.1\",\"title\":\"Dashboard\",\"link\":\"dashboard\"},{\"id\":\"1.2\",\"title\":\"Data Pegawai\",\"link\":\"pegawai\"},{\"id\":\"1.3\",\"title\":\"Data Keluarga\",\"content\":[{\"id\":\"1.3.1\",\"title\":\"Suami Istri\",\"link\":\"keluarga-suami-istri\"},{\"id\":\"1.3.2\",\"title\":\"Anak\",\"link\":\"keluarga-anak\"},{\"id\":\"1.3.3\",\"title\":\"Orang Tua\",\"link\":\"keluarga-orang-tua\"}]},{\"id\":\"1.4\",\"title\":\"Penilaian Pegawai (DP3)\",\"link\":\"dp3\"},{\"id\":\"1.5\",\"title\":\"Transaksi Pensiun\",\"link\":\"transaksi-pensiun\"},{\"id\":\"1.6\",\"title\":\"Riwayat Pegawai\",\"content\":[{\"id\":\"1.6.1\",\"title\":\"Riwayat Pendidikan\",\"link\":\"riwayat-pegawai-pendidikan\"},{\"id\":\"1.6.2\",\"title\":\"Riwayat Jabatan\",\"link\":\"riwayat-pegawai-jabatan\"},{\"id\":\"1.6.3\",\"title\":\"Riwayat Kepangkatan\",\"link\":\"riwayat-pegawai-kepangkatan\"},{\"id\":\"1.6.4\",\"title\":\"Riwayat Hukdis\",\"link\":\"riwayat-pegawai-hukdis\"},{\"id\":\"1.6.5\",\"title\":\"Riwayat Diklat\",\"link\":\"riwayat-pegawai-diklat\"},{\"id\":\"1.6.6\",\"title\":\"Riwayat Organisasi\",\"link\":\"riwayat-pegawai-organisasi\"},{\"id\":\"1.6.7\",\"title\":\"Riwayat Penghargaan\",\"link\":\"riwayat-pegawai-penghargaan\"},{\"id\":\"1.6.8\",\"title\":\"Riwayat Seminar\",\"link\":\"riwayat-pegawai-seminar\"},{\"id\":\"1.6.9\",\"title\":\"Riwayat Penugasan LN\",\"link\":\"riwayat-pegawai-penugasan-ln\"},{\"id\":\"1.6.10\",\"title\":\"Riwayat Bahasa\",\"link\":\"riwayat-pegawai-bahasa\"},{\"id\":\"1.6.11\",\"title\":\"Riwayat Cuti\",\"link\":\"riwayat-pegawai-cuti\"}]}]','2019-05-09 04:41:37','2019-05-09 04:41:37'),(2,'Master','[{\"id\":\"2.1\",\"title\":\"Umum\",\"content\":[{\"id\":\"2.1.1\",\"title\":\"Agama\",\"link\":\"umum-agama\"},{\"id\":\"2.1.2\",\"title\":\"Bentuk Wajah\",\"link\":\"umum-bentuk-wajah\"},{\"id\":\"2.1.3\",\"title\":\"Jenis Diklat\",\"link\":\"umum-jenis-diklat\"},{\"id\":\"2.1.4\",\"title\":\"Jenjang Pendidikan\",\"link\":\"umum-jenjang-pendidikan\"},{\"id\":\"2.1.5\",\"title\":\"Jenis Rambut\",\"link\":\"umum-jenis-rambut\"},{\"id\":\"2.1.6\",\"title\":\"Status Pernikahan\",\"link\":\"umum-status-pernikahan\"}]},{\"id\":\"2.2\",\"title\":\"Instansi\",\"content\":[{\"id\":\"2.2.1\",\"title\":\"Instansi\",\"link\":\"instansi\"},{\"id\":\"2.2.2\",\"title\":\"Unit Kerja\",\"link\":\"instansi-unit-kerja\"}]},{\"id\":\"2.3\",\"title\":\"Tingkat Eselon\",\"link\":\"tingkat-eselon\"},{\"id\":\"2.4\",\"title\":\"Jabatan\",\"content\":[{\"id\":\"2.4.1\",\"title\":\"Jenis Jabatan\",\"link\":\"jabatan-jenis-jabatan\"},{\"id\":\"2.4.2\",\"title\":\"Jabatan\",\"link\":\"jabatan\"},{\"id\":\"2.4.3\",\"title\":\"Jns. Kenaikan Pangkat\",\"link\":\"jabatan-jenis-kenaikan-pangkat\"},{\"id\":\"2.4.4\",\"title\":\"Golongan/Ruang\",\"link\":\"jabatan-golongan-ruang\"}]}]','2019-05-09 04:41:37','2019-05-09 04:41:37'),(3,'Laporan','[{\"id\":\"3.1\",\"title\":\"Rekapitulasi\",\"content\":[{\"id\":\"3.1.1\",\"title\":\"Agama & Jenis Kelamin\",\"link\":\"rekapitulasi-agama-jk\"},{\"id\":\"3.1.2\",\"title\":\"Golongan\",\"link\":\"rekapitulasi-golongan\"}]},{\"id\":\"3.2\",\"title\":\"Laporan\",\"content\":[{\"id\":\"3.2.1\",\"title\":\"Daftar Riwayat Hidup\",\"link\":\"laporan-daftar-riwayat-hidup\"},{\"id\":\"3.2.2\",\"title\":\"Daftar Nominatif\",\"link\":\"laporan-nominatif\"},{\"id\":\"3.2.3\",\"title\":\"Daftar Urut Kepangkatan\",\"link\":\"laporan-daftar-urut-kepangkatan\"},{\"id\":\"3.2.4\",\"title\":\"Daftar Jabatan Fungsional\",\"link\":\"laporan-daftar-jabatan-fungsional\"},{\"id\":\"3.2.5\",\"title\":\"Daftar Pensiun/Non Aktif\",\"link\":\"laporan-daftar-pensiun-non-aktif\"},{\"id\":\"3.2.6\",\"title\":\"Daftar Pegawai Mutasi\",\"link\":\"laporan-daftar-pegawai-mutasi\"},{\"id\":\"3.2.7\",\"title\":\"Pegawai Akan Naik Pangkat\",\"link\":\"laporan-pegawai-akan-naik-pangkat\"}]}]','2019-05-09 04:41:37','2019-05-09 04:41:37'),(4,'Pengaturan','[{\"id\":\"4.1\",\"title\":\"Grup Pengguna\",\"link\":\"grup-pengguna\"},{\"id\":\"4.2\",\"title\":\"Pengguna\",\"link\":\"pengguna\"},{\"id\":\"4.3\",\"title\":\"Ganti Password\",\"link\":\"ganti-password\"}]','2019-05-09 04:41:37','2019-05-09 04:41:37');
/*!40000 ALTER TABLE `Menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SequelizeMeta`
--

DROP TABLE IF EXISTS `SequelizeMeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SequelizeMeta` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SequelizeMeta`
--

LOCK TABLES `SequelizeMeta` WRITE;
/*!40000 ALTER TABLE `SequelizeMeta` DISABLE KEYS */;
INSERT INTO `SequelizeMeta` VALUES ('20190501225518-create-menu.js'),('20190501232653-create-user.js');
/*!40000 ALTER TABLE `SequelizeMeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `passwd` varchar(255) NOT NULL,
  `information` text NOT NULL,
  `last_login` datetime NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (2,'superadmin','$2a$10$jUcNdL/gaFxKmph.IPo5.O5pHLXB535u/mraWnnW5F8S8BGCjoA7e','{\"fullname\":\"Administrator\",\"email\":\"\",\"phone\":\"\",\"jenis_kelamin\":\"\"}','2019-05-09 04:41:38','2019-05-09 04:41:38','2019-05-09 04:41:38');
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-09  4:50:34
