'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmployeePosition = sequelize.define('EmployeePosition', {
    id_employee: DataTypes.INTEGER,
    id_position: DataTypes.INTEGER,
    id_echelon: DataTypes.INTEGER,
    sk_number: DataTypes.STRING,
    sk_date: DataTypes.DATE,
    tmt_position_from: DataTypes.DATE,
    tmt_position_to: DataTypes.DATE,
    is_active: DataTypes.BOOLEAN,
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  EmployeePosition.associate = function(models) {
    // associations can be defined here
  };
  return EmployeePosition;
};