'use strict';
module.exports = (sequelize, DataTypes) => {
  const PromotionType = sequelize.define('PromotionType', {
    name: DataTypes.STRING,
    is_active: { type: DataTypes.BOOLEAN, defaultValue: false },
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  PromotionType.associate = function(models) {
    // associations can be defined here
  };
  return PromotionType;
};