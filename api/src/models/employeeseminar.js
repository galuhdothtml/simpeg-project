'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmployeeSeminar = sequelize.define('EmployeeSeminar', {
    id_employee: DataTypes.INTEGER,
    name: DataTypes.STRING,
    place: DataTypes.STRING,
    organizer: DataTypes.STRING,
    start_date: DataTypes.DATE,
    end_date: DataTypes.DATE,
    charter_number: DataTypes.STRING,
    charter_date: DataTypes.DATE,
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  EmployeeSeminar.associate = function(models) {
    // associations can be defined here
  };
  return EmployeeSeminar;
};