'use strict';
module.exports = (sequelize, DataTypes) => {
  const EducationLevel = sequelize.define('EducationLevel', {
    name: DataTypes.STRING,
    is_active: { type: DataTypes.BOOLEAN, defaultValue: false },
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  EducationLevel.associate = function(models) {
    // associations can be defined here
  };
  return EducationLevel;
};