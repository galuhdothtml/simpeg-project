'use strict';
module.exports = (sequelize, DataTypes) => {
  const FileStorage = sequelize.define('FileStorage', {
    tablename: DataTypes.STRING,
    filename: DataTypes.TEXT,
    id_reference: DataTypes.INTEGER,
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  FileStorage.associate = function(models) {
    // associations can be defined here
  };
  return FileStorage;
};