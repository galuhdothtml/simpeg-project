'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmployeeMutation = sequelize.define('EmployeeMutation', {
    id_employee: DataTypes.INTEGER,
    /*
    1: Masuk
    2: Keluar
    3: Pindah Antar Instansi
    4: Pensiun
    5: Wafat
    6: Kenaikan Pangkat
    */
    mutation_type: DataTypes.CHAR(3),
    sk_number: DataTypes.INTEGER,
    mutation_date: DataTypes.DATE,
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  EmployeeMutation.associate = function(models) {
    // associations can be defined here
  };
  return EmployeeMutation;
};