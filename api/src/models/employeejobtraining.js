'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmployeeJobTraining = sequelize.define('EmployeeJobTraining', {
    id_employee: DataTypes.INTEGER,
    coach_name: DataTypes.STRING,
    year: DataTypes.INTEGER,
    hour_total: DataTypes.INTEGER,
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  EmployeeJobTraining.associate = function(models) {
    // associations can be defined here
  };
  return EmployeeJobTraining;
};