'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmployeeSchool = sequelize.define('EmployeeSchool', {
    id_employee: DataTypes.INTEGER,
    degree: DataTypes.STRING,
    school_name: DataTypes.STRING,
    location: DataTypes.STRING,
    department: DataTypes.STRING,
    certificate_number: DataTypes.STRING,
    certificate_date: DataTypes.DATE,
    headmaster: DataTypes.STRING,
    last_education: { type: DataTypes.BOOLEAN, defaultValue: false },
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  EmployeeSchool.associate = function(models) {
    // associations can be defined here
  };
  return EmployeeSchool;
};