'use strict';

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    username: DataTypes.STRING,
    passwd: DataTypes.STRING,
    email: DataTypes.STRING,
    fullname: DataTypes.STRING,
    role: DataTypes.CHAR(5),
    last_login: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};