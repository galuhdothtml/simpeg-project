'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmployeeLanguage = sequelize.define('EmployeeLanguage', {
    id_employee: DataTypes.INTEGER,
    language_type: DataTypes.STRING,
    language: DataTypes.STRING,
    ability: DataTypes.CHAR(2),
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  EmployeeLanguage.associate = function(models) {
    // associations can be defined here
  };
  return EmployeeLanguage;
};