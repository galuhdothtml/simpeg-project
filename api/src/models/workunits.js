'use strict';
module.exports = (sequelize, DataTypes) => {
  const WorkUnits = sequelize.define('WorkUnits', {
    name: DataTypes.STRING,
    is_active: { type: DataTypes.BOOLEAN, defaultValue: false },
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  WorkUnits.associate = function(models) {
    // associations can be defined here
  };
  return WorkUnits;
};