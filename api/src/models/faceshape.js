'use strict';
module.exports = (sequelize, DataTypes) => {
  const FaceShape = sequelize.define('FaceShape', {
    name: DataTypes.STRING,
    is_active: { type: DataTypes.BOOLEAN, defaultValue: false },
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  FaceShape.associate = function(models) {
    // associations can be defined here
  };
  return FaceShape;
};