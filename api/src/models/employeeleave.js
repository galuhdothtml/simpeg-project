'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmployeeLeave = sequelize.define('EmployeeLeave', {
    id_employee: DataTypes.INTEGER,
    leave_number: DataTypes.STRING,
    leave_letter_date: DataTypes.DATE,
    leave_type: DataTypes.STRING,
    start_date: DataTypes.DATE,
    end_date: DataTypes.DATE,
    length_of_leave: DataTypes.INTEGER,
    provisions: DataTypes.TEXT,
    copies: DataTypes.TEXT,
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  EmployeeLeave.associate = function(models) {
    // associations can be defined here
  };
  return EmployeeLeave;
};