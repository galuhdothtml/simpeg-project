'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmployeePenalty = sequelize.define('EmployeePenalty', {
    id_employee: DataTypes.INTEGER,
    id_penalty_type: DataTypes.INTEGER,
    sk_official_certifier: DataTypes.STRING,
    sk_number: DataTypes.STRING,
    sk_date: DataTypes.DATE,
    penalty_restorer_official: DataTypes.STRING,
    restore_penalty_number: DataTypes.STRING,
    restore_penalty_date: DataTypes.DATE,
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  EmployeePenalty.associate = function(models) {
    // associations can be defined here
  };
  return EmployeePenalty;
};