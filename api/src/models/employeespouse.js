'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmployeeSpouse = sequelize.define('EmployeeSpouse', {
    id_employee: DataTypes.INTEGER,
    nik: DataTypes.STRING,
    fullname: DataTypes.STRING,
    birth_place: DataTypes.STRING,
    birth_date: DataTypes.DATE,
    id_education_level: DataTypes.INTEGER,
    job: { type: DataTypes.STRING, defaultValue: '-' },
    status: DataTypes.CHAR(2),
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  EmployeeSpouse.associate = function(models) {
    // associations can be defined here
  };
  return EmployeeSpouse;
};