'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmployeeLevel = sequelize.define('EmployeeLevel', {
    id_employee: DataTypes.INTEGER,
    id_group: DataTypes.INTEGER,
    level: DataTypes.STRING,
    level_type: DataTypes.STRING,
    tmt_level: DataTypes.DATE,
    sk_official_certifier: DataTypes.STRING,
    sk_number: DataTypes.STRING,
    sk_date: DataTypes.STRING,
    is_active: DataTypes.BOOLEAN,
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  EmployeeLevel.associate = function(models) {
    // associations can be defined here
  };
  return EmployeeLevel;
};