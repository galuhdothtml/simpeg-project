'use strict';
module.exports = (sequelize, DataTypes) => {
  const TrainingType = sequelize.define('TrainingType', {
    name: DataTypes.STRING,
    is_active: { type: DataTypes.BOOLEAN, defaultValue: false },
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  TrainingType.associate = function(models) {
    // associations can be defined here
  };
  return TrainingType;
};