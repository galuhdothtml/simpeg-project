'use strict';
module.exports = (sequelize, DataTypes) => {
  const SkpEmployee = sequelize.define('SkpEmployee', {
    id_employee: DataTypes.INTEGER,
    start_date: DataTypes.DATE,
    end_date: DataTypes.DATE,
    evaluator_name: DataTypes.STRING,
    head_evaluator_name: DataTypes.STRING,
    score: DataTypes.TEXT,
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  SkpEmployee.associate = function(models) {
    // associations can be defined here
  };
  return SkpEmployee;
};