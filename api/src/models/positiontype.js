'use strict';
module.exports = (sequelize, DataTypes) => {
  const PositionType = sequelize.define('PositionType', {
    name: DataTypes.STRING,
    is_active: { type: DataTypes.BOOLEAN, defaultValue: false },
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  PositionType.associate = function(models) {
    // associations can be defined here
  };
  return PositionType;
};