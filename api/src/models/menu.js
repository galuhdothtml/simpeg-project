'use strict';

module.exports = (sequelize, DataTypes) => {
  const Menu = sequelize.define('Menu', {
    title: DataTypes.STRING,
    content: DataTypes.TEXT,
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  Menu.associate = function(models) {
    // associations can be defined here
  };
  return Menu;
};