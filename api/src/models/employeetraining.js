'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmployeeTraining = sequelize.define('EmployeeTraining', {
    id_employee: DataTypes.INTEGER,
    name: DataTypes.STRING,
    duration: DataTypes.INTEGER,
    organizer: DataTypes.STRING,
    place: DataTypes.STRING,
    generation: DataTypes.STRING,
    year: DataTypes.INTEGER,
    sttpp_number: DataTypes.STRING,
    sttpp_date: DataTypes.DATE,
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  EmployeeTraining.associate = function(models) {
    // associations can be defined here
  };
  return EmployeeTraining;
};