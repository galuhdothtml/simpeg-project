'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmployeeAssignment = sequelize.define('EmployeeAssignment', {
    id_employee: DataTypes.INTEGER,
    destination: DataTypes.STRING,
    year: DataTypes.INTEGER,
    duration: DataTypes.INTEGER,
    reason: DataTypes.STRING,
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  EmployeeAssignment.associate = function(models) {
    // associations can be defined here
  };
  return EmployeeAssignment;
};