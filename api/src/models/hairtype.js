'use strict';
module.exports = (sequelize, DataTypes) => {
  const HairType = sequelize.define('HairType', {
    name: DataTypes.STRING,
    is_active: { type: DataTypes.BOOLEAN, defaultValue: false },
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  HairType.associate = function(models) {
    // associations can be defined here
  };
  return HairType;
};