'use strict';
module.exports = (sequelize, DataTypes) => {
  const Employee = sequelize.define('Employee', {
    nip: DataTypes.STRING,
    fullname: DataTypes.STRING,
    birth_place: DataTypes.STRING,
    birth_date: DataTypes.DATE,
    gender: DataTypes.CHAR(2),
    id_religion: DataTypes.INTEGER,
    blood_type: DataTypes.STRING,
    id_marital_status: DataTypes.INTEGER,
    employee_status: DataTypes.CHAR(3),
    id_work_unit: DataTypes.INTEGER,
    promotion_date: DataTypes.DATE,
    salary_increase_date: DataTypes.DATE,
    address: DataTypes.TEXT,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    is_active: { type: DataTypes.BOOLEAN, defaultValue: false },
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  Employee.associate = function(models) {
    // associations can be defined here
  };
  return Employee;
};