'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmployeeAllowance = sequelize.define('EmployeeAllowance', {
    id_employee: DataTypes.INTEGER,
    allowance_number: DataTypes.STRING,
    allowance_date: DataTypes.DATE,
    allowance_type: DataTypes.STRING,
    start_date: DataTypes.DATE,
    marriage_certificate_from: DataTypes.STRING,
    marriage_certificate_number: DataTypes.STRING,
    marriage_certificate_date: DataTypes.DATE,
    birth_certificate_from: DataTypes.STRING,
    birth_certificate_number: DataTypes.STRING,
    birth_certificate_date: DataTypes.DATE,
    copies: DataTypes.TEXT,
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  EmployeeAllowance.associate = function(models) {
    // associations can be defined here
  };
  return EmployeeAllowance;
};