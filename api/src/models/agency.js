'use strict';
module.exports = (sequelize, DataTypes) => {
  const Agency = sequelize.define('Agency', {
    code: DataTypes.STRING,
    raw_code: DataTypes.STRING,
    name: DataTypes.STRING,
    parent_id: DataTypes.INTEGER,
    is_active: { type: DataTypes.BOOLEAN, defaultValue: false },
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  Agency.associate = function(models) {
    // associations can be defined here
  };
  return Agency;
};