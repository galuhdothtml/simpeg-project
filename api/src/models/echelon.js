'use strict';
module.exports = (sequelize, DataTypes) => {
  const EchelonLevel = sequelize.define('Echelon', {
    name: DataTypes.STRING,
    is_active: { type: DataTypes.BOOLEAN, defaultValue: false },
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  EchelonLevel.associate = function(models) {
    // associations can be defined here
  };
  return EchelonLevel;
};