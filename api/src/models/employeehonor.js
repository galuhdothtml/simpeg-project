'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmployeeHonor = sequelize.define('EmployeeHonor', {
    id_employee: DataTypes.INTEGER,
    name: DataTypes.STRING,
    year: DataTypes.INTEGER,
    provider: DataTypes.STRING,
    createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {});
  EmployeeHonor.associate = function(models) {
    // associations can be defined here
  };
  return EmployeeHonor;
};