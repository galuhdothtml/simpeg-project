'use strict';
var moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('WorkUnits', [{
      id: '1',
      name: 'Pemerintah Kabupaten Malang',
      is_active: true,
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
    },{
      id: '2',
      name: 'Sekretariat Daerah Kabupaten Malang',
      is_active: true,
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
    },{
      id: '3',
      name: 'Sekretariat DPRD',
      is_active: true,
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
    },{
      id: '4',
      name: 'Sekretariat Komisi Pemilihan Umum',
      is_active: true,
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
    },{
      id: '5',
      name: 'Inspektorat',
      is_active: true,
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
    },{
      id: '6',
      name: 'Badan Kepegawaian Daerah',
      is_active: true,
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
    },{
      id: '7',
      name: 'Badan Penanggulangan Bencana Daerah',
      is_active: true,
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
    },{
      id: '8',
      name: 'Badan Pelayanan Pajak Daerah',
      is_active: true,
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
    },{
      id: '9',
      name: 'Badan Pengelolaan Keuangan Dan Aset Daerah',
      is_active: true,
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
    },{
      id: '10',
      name: 'Dinas Lingkungan Hidup Dan Kebersihan',
      is_active: true,
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('WorkUnits', null, {});
  }
};
