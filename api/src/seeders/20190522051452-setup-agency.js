'use strict';
var moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Agencies', [{
      id: '1',
      code: '00',
      raw_code: '00',
      name: 'Kejaksaan Agung Republik Indonesia',
      is_active: true,
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
    }, {
      id: '2',
      code: '00.01',
      raw_code: '01',
      name: 'Kejaksaan Tinggi Aceh',
      parent_id: '1',
      is_active: true,
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
    }, {
      id: '3',
      code: '00.01.01',
      raw_code: '01',
      name: 'Kejaksaan Negeri Banda Aceh',
      parent_id: '1#2',
      is_active: true,
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
    }, {
      id: '4',
      code: '00.01.02',
      raw_code: '02',
      name: 'Kejaksaan Negeri Sabang',
      parent_id: '1#2',
      is_active: true,
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
    }, {
      id: '5',
      code: '00.01.03',
      raw_code: '03',
      name: 'Kejaksaan Negeri Sigli',
      parent_id: '1#2',
      is_active: true,
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
    }, {
      id: '6',
      code: '00.01.03.01',
      raw_code: '01',
      name: 'Cabang Kejaksaan Negeri Sigli di Kota Bakti',
      parent_id: '1#2#5',
      is_active: true,
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Agencies', null, {});
  }
};
