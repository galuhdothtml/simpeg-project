'use strict';
var moment = require('moment');
var bcrypt = require('bcrypt');

function hashPassword(val) {
  const saltRounds = 10;
  const salt = bcrypt.genSaltSync(saltRounds);
  const hash = bcrypt.hashSync(val, salt);

  return hash;
}

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [{
      username: 'superadmin',
      passwd: hashPassword('superadmin'),
      fullname: 'Administrator',
      email: 'superadmin@example.com',
      role: '1',
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      last_login: moment().format('YYYY-MM-DD HH:mm:ss'),
    },{
      username: 'tasya',
      passwd: hashPassword('123456'),
      fullname: 'Anastasya Meme',
      email: 'tasya@example.com',
      role: '2',
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      last_login: moment().format('YYYY-MM-DD HH:mm:ss'),
    }], {});
  },

  down: (queryInterface, Sequelize) => {
   return queryInterface.bulkDelete('Users', null, {});
  }
};
