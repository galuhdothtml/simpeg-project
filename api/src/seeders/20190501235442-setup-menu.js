'use strict';
var moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Menus', [{
      id: '1',
      title: 'Pegawai',
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      content: JSON.stringify([
        {
          id: '1.1',
          title: 'Dashboard',
          link: 'dashboard',
          icon: 'fa fa-dashboard',
        }, {
          id: '1.2',
          title: 'Data Pegawai',
          link: 'pegawai',
          icon: 'fa fa-group',
        }, {
          id: '1.3',
          title: 'Riwayat Keluarga',
          icon: 'fa fa-child',
          content: [
            {
              id: '1.3.1',
              title: 'Suami / Istri',
              link: 'keluarga-suami-istri',
            },
            {
              id: '1.3.2',
              title: 'Anak',
              link: 'keluarga-anak',
            },
            {
              id: '1.3.3',
              title: 'Orang Tua',
              link: 'keluarga-orang-tua',
            },
          ]
        }, {
          id: '1.4',
          title: 'Riwayat Pendidikan',
          icon: 'fa fa-graduation-cap',
          content: [
            {
              id: '1.4.1',
              title: 'Sekolah',
              link: 'riwayat-pendidikan-sekolah',
            },
            {
              id: '1.4.2',
              title: 'Bahasa',
              link: 'riwayat-pendidikan-bahasa',
            },
          ]
        }, {
          id: '1.5',
          title: 'Kepegawaian',
          icon: 'fa fa-briefcase',
          content: [
            {
              id: '1.5.1',
              title: 'Jabatan',
              link: 'kepegawaian-jabatan',
            },
            {
              id: '1.5.2',
              title: 'Pangkat',
              link: 'kepegawaian-pangkat',
            },
            {
              id: '1.5.3',
              title: 'Hukuman',
              link: 'kepegawaian-hukuman',
            },
            {
              id: '1.5.4',
              title: 'Diklat',
              link: 'kepegawaian-diklat',
            },
            {
              id: '1.5.5',
              title: 'Penghargaan',
              link: 'kepegawaian-penghargaan',
            },
            {
              id: '1.5.6',
              title: 'Penugasan',
              link: 'kepegawaian-penugasan',
            },
            {
              id: '1.5.7',
              title: 'Seminar',
              link: 'kepegawaian-seminar',
            },
            {
              id: '1.5.8',
              title: 'Cuti',
              link: 'kepegawaian-cuti',
            },
            {
              id: '1.5.9',
              title: 'Latihan Jabatan',
              link: 'kepegawaian-latihan-jabatan',
            },
            {
              id: '1.5.10',
              title: 'Tunjangan',
              link: 'kepegawaian-tunjangan',
            },
          ]
        }, {
          id: '1.6',
          title: 'Mutasi',
          link: 'mutasi',
          icon: 'fa fa-exchange',
        }, {
          id: '1.7',
          title: 'SKP',
          link: 'skp',
          icon: 'fa fa-tasks',
        }
      ])
    }, {
      id: '2',
      title: 'Laporan',
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      content: JSON.stringify([
        {
          id: '3.1',
          title: 'Rekapitulasi',
          icon: 'fa fa-bar-chart',
          content: [
            {
              id: '3.1.1',
              title: 'Golongan',
              link: 'rekapitulasi-golongan',
            },
            {
              id: '3.1.2',
              title: 'Jabatan',
              link: 'rekapitulasi-jabatan',
            },
            {
              id: '3.1.3',
              title: 'Pendidikan',
              link: 'rekapitulasi-pendidikan',
            },
            {
              id: '3.1.4',
              title: 'Unit Kerja',
              link: 'rekapitulasi-unit-kerja',
            },
          ]
        },
        {
          id: '3.2',
          title: 'Laporan',
          icon: 'fa fa-file-text-o',
          content: [
            {
              id: '3.2.1',
              title: 'Nominatif',
              link: 'laporan-nominatif',
            },
            {
              id: '3.2.2',
              title: 'DUK',
              link: 'laporan-duk',
            },
            {
              id: '3.2.3',
              title: 'Bezetting',
              link: 'laporan-bezetting',
            },
            {
              id: '3.2.4',
              title: 'Keadaan Pegawai',
              link: 'laporan-keadaan-pegawai',
            },
            {
              id: '3.2.5',
              title: 'Pensiun',
              link: 'laporan-pensiun',
            },
          ]
        },
        {
          id: '3.3',
          title: 'Laporan KGB',
          link: 'laporan-kgb',
          icon: 'fa fa-file-excel-o',
        }
      ])
    }, {
      id: '3',
      title: 'Pengaturan',
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      content: JSON.stringify([
        {
          id: '4.1',
          title: 'BKD',
          link: 'bkd',
          icon: 'fa fa-building-o',
        },
        {
          id: '4.2',
          title: 'Sekretariat Daerah',
          link: 'sekretariat-daerah',
          icon: 'fa fa-building',
        },
        {
          id: '4.3',
          title: 'Unit Kerja',
          link: 'unit-kerja',
          icon: 'fa fa-table',
        },
        {
          id: '4.4',
          title: 'User',
          link: 'user',
          icon: 'fa fa-user-plus',
        },
        {
          id: '4.5',
          title: 'User Pegawai',
          link: 'user-pegawai',
          icon: 'fa fa-users',
        },
        {
          id: '4.6',
          title: 'Ganti Password',
          link: 'ganti-password',
          icon: 'fa fa-lock',
        },
      ])
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Menus', null, {});
  }
};
