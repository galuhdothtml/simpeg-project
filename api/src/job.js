import rimraf from "rimraf";
import fs from "fs";
import Sequelize from "sequelize";

import { ModelName } from './constants';
import ModelFactory from './utils/model_factory';

const Op = Sequelize.Op;

const path = require('path');

const Queue = require('bull');
const cronJobQueue = new Queue('cron job');

const FileStorage = ModelFactory.createModel(ModelName.FILE_STORAGE);

const JobType = {
    DELETE_UNUSED_FILES: "DELETE_UNUSED_FILES",
}

const unlinkFile = (directory, filesInDatabase, callbackIfDirectory = null) => {
    const files = fs.readdirSync(directory);
    files.forEach(file => {
        const isDirectory = fs.lstatSync(path.resolve(directory, file)).isDirectory();
        if (!isDirectory) {
            const found = filesInDatabase.find(datum => (datum.filepath === `${directory}${file}`));
            if (!found) {
                fs.unlinkSync(`${directory}${file}`);
            }
        } else if (callbackIfDirectory) {
            callbackIfDirectory(file);
        }
    });
}

const unlinkTempFiles = () => {
    const directory = "public/uploads/temp/";
    const files = fs.readdirSync(directory);
    files.forEach(file => {
        const isDirectory = fs.lstatSync(path.resolve(directory, file)).isDirectory();
        if (isDirectory) {
            rimraf.sync(`${directory}${file}`);
        } else {
            fs.unlinkSync(`${directory}${file}`);
        }
    });
}

const deleteUnusedFiles = async (jobDone) => {
    try {
        const directory = "public/uploads/";
        const directoryList = [];

        let results = await FileStorage.findAll({
            where: {
                [Op.or]: [{ tablename: "Employees" }, { tablename: "Users" }],
            }
        });
        let newData = JSON.parse(JSON.stringify(results));
        newData = newData.map((x) => {
            const parseFilename = JSON.parse(x.filename);

            return Object.assign({}, x, { filepath: parseFilename.filepath });
        });

        unlinkFile(directory, newData, (dir) => {
            directoryList.push(dir);
        });

        // delete unused files in employee directory
        results = await FileStorage.findAll({
            where: {
                [Op.and]: [
                    { 
                        tablename: {
                            [Op.ne]: "Employees",  
                        }
                    },
                    { 
                        tablename: {
                            [Op.ne]: "Users",  
                        }
                    },
                ], 
            }
        });
        newData = JSON.parse(JSON.stringify(results));
        newData = newData.map((x) => {
            const parseFilename = JSON.parse(x.filename);

            return Object.assign({}, x, { filepath: parseFilename.filepath });
        });
        directoryList.filter(dir => dir != "temp").forEach((x) => {
            unlinkFile(`${directory}${x}/`, newData);
        });

        unlinkTempFiles();

        jobDone();
    } catch (err) {
        console.error(err);
        jobDone(new Error(err.message));
    }
}

const processJobs = () => {
    cronJobQueue.process(function(job, done){
        const { type } = job.data;
        console.log('JOB-TYPE: ', type);
        if (type === JobType.DELETE_UNUSED_FILES) {
            // should be removed: j7nr09.jpeg
            deleteUnusedFiles(done)
        } else {
            done();
        }
    });
}

const startCronJob = () => {
    cronJobQueue.add({ type: JobType.DELETE_UNUSED_FILES }, { repeat: { cron: '00 1 * * *' } });
    // cronJobQueue.add({ type: JobType.DELETE_UNUSED_FILES });
}

export { processJobs, startCronJob };