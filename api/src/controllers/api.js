import jwt from 'jsonwebtoken';
import Sequelize from "sequelize";
import bcrypt from "bcrypt";
import sharp from "sharp";
import moment from "moment";
import fs from "fs";

import { SECRET_KEY, ModelName } from '../constants';
import ModelFactory from '../utils/model_factory';
import StoredProcedure from '../utils/stored_procedure';
import Db from '../utils/db';
import Util from '../utils';

const SALT_ROUNDS = 10;
const SALT_PASSWD = bcrypt.genSaltSync(SALT_ROUNDS);

const Op = Sequelize.Op;

const Province = ModelFactory.createModel(ModelName.PROVINCE);
const City = ModelFactory.createModel(ModelName.CITY);
const Menu = ModelFactory.createModel(ModelName.MENU);
const User = ModelFactory.createModel(ModelName.USER);
const Religion = ModelFactory.createModel(ModelName.RELIGION);
const Agency = ModelFactory.createModel(ModelName.AGENCY);
const EducationLevel = ModelFactory.createModel(ModelName.EDUCATION_LEVEL);
const Echelon = ModelFactory.createModel(ModelName.ECHELON);
const Employee = ModelFactory.createModel(ModelName.EMPLOYEE);
const FaceShape = ModelFactory.createModel(ModelName.FACE_SHAPE);
const FileStorage = ModelFactory.createModel(ModelName.FILE_STORAGE);
const Group = ModelFactory.createModel(ModelName.GROUP);
const HairType = ModelFactory.createModel(ModelName.HAIR_TYPE);
const MaritalStatus = ModelFactory.createModel(ModelName.MARITAL_STATUS);
const PositionType = ModelFactory.createModel(ModelName.POSITION_TYPE);
const Position = ModelFactory.createModel(ModelName.POSITION);
const PromotionType = ModelFactory.createModel(ModelName.PROMOTION_TYPE);
const TrainingType = ModelFactory.createModel(ModelName.TRAINING_TYPE);
const WorkUnit = ModelFactory.createModel(ModelName.WORK_UNIT);
const EmployeeSpouse = ModelFactory.createModel(ModelName.EMPLOYEE_SPOUSE);
const EmployeeKid = ModelFactory.createModel(ModelName.EMPLOYEE_KID);
const EmployeeParent = ModelFactory.createModel(ModelName.EMPLOYEE_PARENT);
const EmployeeLanguage = ModelFactory.createModel(ModelName.EMPLOYEE_LANGUAGE);
const EmployeeSchool = ModelFactory.createModel(ModelName.EMPLOYEE_SCHOOL);
const EmployeePosition = ModelFactory.createModel(ModelName.EMPLOYEE_POSITION);
const EmployeeLevel = ModelFactory.createModel(ModelName.EMPLOYEE_LEVEL);
const EmployeePenalty = ModelFactory.createModel(ModelName.EMPLOYEE_PENALTY);
const PenaltyType = ModelFactory.createModel(ModelName.PENALTY_TYPE);
const EmployeeTraining = ModelFactory.createModel(ModelName.EMPLOYEE_TRAINING);
const EmployeeHonor = ModelFactory.createModel(ModelName.EMPLOYEE_HONOR);
const EmployeeAssignment = ModelFactory.createModel(ModelName.EMPLOYEE_ASSIGNMENT);
const EmployeeSeminar = ModelFactory.createModel(ModelName.EMPLOYEE_SEMINAR);
const EmployeeLeave = ModelFactory.createModel(ModelName.EMPLOYEE_LEAVE);
const EmployeeJobTraining = ModelFactory.createModel(ModelName.EMPLOYEE_JOB_TRAINING);
const EmployeeAllowance = ModelFactory.createModel(ModelName.EMPLOYEE_ALLOWANCE);
const EmployeeMutation = ModelFactory.createModel(ModelName.EMPLOYEE_MUTATION);
const SkpEmployee = ModelFactory.createModel(ModelName.SKP_EMPLOYEE);

const createFileName = (rawPath) => {
    const path = rawPath.split("/");

    return path[path.length - 1];
}
class Api {
    index = (req, res) => {
        const data = 'Simpeg REST API';

        res.json({ status: true, data });
    }

    uploadFile = (req, res) => {
        try {
            const { data } = req.body;
            let groupDir = "";
            const newData = req.file;

            if (data) {
                const parsedData = JSON.parse(data);
                groupDir = parsedData.groupDir;

                const dir = `public/uploads/temp/${groupDir}`;

                if (!fs.existsSync(dir)){
                    fs.mkdirSync(dir);
                }

                const oldPath = req.file.path;
                const newPath = `${req.file.destination}${groupDir}/${req.file.originalname}`;
                fs.renameSync(oldPath, newPath);

                Object.assign(newData , {
                    destination: `${req.file.destination}${groupDir}/`,
                    path: newPath,
                });
            }

            const api = {
                "status": true,
                "data": newData,
                "msg": 'success',
            }

            res.json(api);
        } catch (err) {
            console.log(err);
        }
    }

    login = async (req, res) => {
        const { username, password } = req.body;
        let isPasswordValid = false;
        let data = {};
        const result = await User.findOne({
            where: { username },
            raw: true
        });

        if (result) {
            isPasswordValid = bcrypt.compareSync(password, result.passwd);
            data = result

            delete data.passwd;
        }

        let api = {
            "status": false,
            "msg": "Username atau password masih salah",
        };

        if (isPasswordValid) {
            api = {
                "status": true,
                "data": data,
                "token": jwt.sign({ data }, SECRET_KEY, { expiresIn: '17520h' }),
            }
        }

        res.json(api);
    }

    appMenu = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };

        const results = await Menu.findAll();
        
        if (results) {
            const data = results.map(x => (Object.assign(x, { content: JSON.parse(x.content) })))
            api = {
                "status": true,
                "data": data,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    getUsers = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, } = req.query;
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idUser } } = Util.decodeToken(token);

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['id', 'ASC']],
        };
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        const filter = {
            id: {
                [Op.not]: idUser,
            }
        }

        if (filterText) {
            Object.assign(filter, {
                fullname: {
                    [Op.regexp]: `(${filterText})`,
                }
            });
        }


        Object.assign(options, {
            where: filter,
        });
        Object.assign(calculateOptions, {
            where: filter,
        });

        const results = await User.findAll(options);
        const calculateAmountOfData = await User.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
        
        if (results) {
            api = {
                "status": true,
                "data": results,
                "page_total": Math.ceil(amountOfData / limit),
                "msg": 'success',
            }
        }

        res.json(api);
    }

    getMyAccount = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const token = Util.getTokenFromRequest(req);
        const { data: { id } } = Util.decodeToken(token);

        const result = await User.findById(id);
        const resFile = await FileStorage.findOne({ 
            where: { 
                id_reference: id,
                tablename: 'Users',
            } 
        });

        if (result) {
            const newResult = JSON.parse(JSON.stringify(result));
            let imageFile = "";
            
            if (resFile) {
                const parsedFileName = JSON.parse(resFile.filename);
                imageFile = `http://localhost:3300/${parsedFileName.filepath.replace("public/", "")}`;
            }

            api = {
                "status": true,
                "data": Object.assign({}, newResult, { imageFile }),
                "msg": 'success',
            }
        }

        res.json(api);
    }

    getUser = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;
        const options = {
            where: {
                id,
            },
        };

        const result = await User.findOne(options);
        
        if (result) {
            let imageFile = "";
            
            const resFile = await FileStorage.findOne({ 
                where: { 
                    id_reference: id,
                    tablename: 'Users',
                } 
            });
            
            if (resFile) {
                const parsedFileName = JSON.parse(resFile.filename);
                imageFile = parsedFileName.filepath;
            }
            
            api = {
                "status": true,
                "data": Object.assign({}, JSON.parse(JSON.stringify(result)), { imageFile }),
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createUser = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const data = req.body;
        const { username, passwd: passwdParam, email, fullname, role } = data;

        const result = await User.create({ username, passwd: bcrypt.hashSync(passwdParam, SALT_PASSWD), email, fullname, role });
        
        if (data.photo) {
            const oldPath = data.photo;
            const newPath = data.photo.replace("temp/", "");
            fs.renameSync(oldPath, newPath);
    
            await FileStorage.create({
                tablename: "Users",
                filename: JSON.stringify({ filepath: newPath, filename: createFileName(newPath) }),
                id_reference: result.id,
            });
        }

        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    updateUser = async (req, res) => {
        const tableName = 'Users';
        const data = req.body;
        const { id, username, passwd, email, fullname, role } = data;

        const result = await User.findById(id);

        let newPasswd = result.passwd;

        if (passwd) {
            newPasswd = bcrypt.hashSync(passwd, SALT_PASSWD);
        }

        const response = await result.update({ username, passwd: newPasswd, email, fullname, role });
        
        await FileStorage.destroy({
            where: {
               id_reference: response.id,
               tablename: tableName,
            }
        });

        if (data.photo) {
            const oldPath = data.photo;
            const newPath = data.photo.replace("temp/", "");
            fs.renameSync(oldPath, newPath);

            await FileStorage.create({
                tablename: tableName,
                filename: JSON.stringify({ filepath: newPath, filename: createFileName(newPath) }),
                id_reference: response.id,
            });
        }

        const api = {
            "status": true,
            "data": response,
            "msg": 'success',
        }

        res.json(api);
    }

    deleteUser = async (req, res) => {
        const { id } = req.body;
        const resFile = await FileStorage.findOne({ 
            where: { 
                id_reference: id,
                tablename: 'Users',
            }
        });
        if (resFile) {
            await FileStorage.destroy({
                where: {
                   id_reference: id,
                   tablename: "Users",
                }
            });
        }

        const data = await User.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    bulkDeleteUser = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        
        const tableName = "Users";

        await FileStorage.destroy({
            where: {
               id_reference: deleteIds,
               tablename: tableName,
            }
        });
        const data = await User.destroy({ where: { id: deleteIds }})
        
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    getEmployees = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        WorkUnit.hasMany(Employee, { foreignKey: 'id_work_unit', sourceKey: 'id' });
        Employee.belongsTo(WorkUnit, { foreignKey: 'id_work_unit', sourceKey: 'id' });    

        const options = {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['id', 'ASC']],
            include: [WorkUnit],
        };
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        const results = await Employee.findAll(options);
        let parsedData = JSON.parse(JSON.stringify(results));
        parsedData = parsedData.map((x) => {
            const retval = Object.assign({}, x, { work_unit_name: x.WorkUnit.name });

            delete retval.WorkUnit;

            return retval;
        });

        const calculateAmountOfData = await Employee.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
        
        const payload = {
            total_record: amountOfData,
            total_page: Math.ceil(amountOfData / limit),
            page: Number.parseInt(page),
        };

        if (results) {
            api = {
                "status": true,
                "data": parsedData,
                "payload": payload,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    getEmployee = async (req, res) => {
        try {
            let api = {
                "status": false,
                "msg": 'error',
            };
            const { id } = req.params;
            
            MaritalStatus.hasMany(Employee, { foreignKey: 'id_marital_status', sourceKey: 'id' });
            Employee.belongsTo(MaritalStatus, { foreignKey: 'id_marital_status', sourceKey: 'id' });
            Religion.hasMany(Employee, { foreignKey: 'id_religion', sourceKey: 'id' });
            Employee.belongsTo(Religion, { foreignKey: 'id_religion', sourceKey: 'id' });
            WorkUnit.hasMany(Employee, { foreignKey: 'id_work_unit', sourceKey: 'id' });
            Employee.belongsTo(WorkUnit, { foreignKey: 'id_work_unit', sourceKey: 'id' });
            const options = {
                include: [Religion, WorkUnit, MaritalStatus],
            };
            const result = await Employee.findById(id, options);
            
            if (result) {
                let imageFile = "";
                
                const resFile = await FileStorage.findOne({ 
                    where: { 
                        id_reference: id,
                        tablename: 'Employees',
                    } 
                });
                
                if (resFile) {
                    const parsedFileName = JSON.parse(resFile.filename);
                    imageFile = parsedFileName.filepath;
                }

                let parsedData = JSON.parse(JSON.stringify(result));
                parsedData = Object.assign({}, parsedData, {
                    religion_name: parsedData.Religion.name,
                    work_unit_name: parsedData.WorkUnit.name,
                    marital_status_name: parsedData.MaritalStatus.name,
                });
                
                delete parsedData.Religion;
                delete parsedData.WorkUnit;
                delete parsedData.MaritalStatus;

                api = {
                    "status": true,
                    "data": Object.assign({}, parsedData, { imageFile }),
                    "msg": 'success',
                }
            }
            
            res.json(api);
        } catch (err) {
            console.log(err);
        }
    }

    getEmployeeRetireDate = async (req, res) => {
        try {
            let api = {
                "status": false,
                "msg": 'not found',
            };
            const { id } = req.params;
            const result = await Employee.findById(id);

            if (result) {
                const retireDate = moment(result.birth_date).add(58, 'years').format('YYYY-MM-DD');
                api = {
                    "status": true,
                    "data": {
                        id_employee: result.id,
                        birth_date: result.birth_date,
                        retire_date: retireDate,
                    },
                    "msg": 'success',
                }
            }

            res.json(api);
        } catch (err) {
            console.log(err);
        }
    }

    createEmployee = async (req, res) => {
        const data = req.body;

        const result = await Employee.create(data);

        if (data.photo) {
            const oldPath = data.photo;
            const newPath = data.photo.replace("temp/", "");
            fs.renameSync(oldPath, newPath);
    
            await FileStorage.create({
                tablename: "Employees",
                filename: JSON.stringify({ filepath: newPath, filename: createFileName(newPath) }),
                id_reference: result.id,
            });
        }

        const api = {
            "status": true,
            "msg": 'success',
        }

        res.json(api); 
    }

    updateEmployee = async (req, res) => {
        const tableName = "Employees";
        const data = req.body;

        const result = await Employee.findById(data.id);
        const response = await result.update(data);
        
        await FileStorage.destroy({
            where: {
               id_reference: response.id,
               tablename: tableName,
            }
        });

        if (data.photo) {
            const oldPath = data.photo;
            const newPath = data.photo.replace("temp/", "");
            fs.renameSync(oldPath, newPath);

            await FileStorage.create({
                tablename: tableName,
                filename: JSON.stringify({ filepath: newPath, filename: createFileName(newPath) }),
                id_reference: response.id,
            });
        }

        const api = {
            "status": true,
            "msg": 'success',
        }

        res.json(api); 
    }

    deleteEmployee = async (req, res) => {
        const { id } = req.body;
        const data = await Employee.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    bulkDeleteEmployee = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);

        const tableName = "Employees";
        await FileStorage.destroy({
            where: {
               id_reference: deleteIds,
               tablename: tableName,
            }
        });

        const data = await Employee.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    getReligions = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['id', 'ASC']],
        };
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        const results = await Religion.findAll(options);
        const calculateAmountOfData = await Religion.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            api = {
                "status": true,
                "data": results,
                "page_total": Math.ceil(amountOfData / limit),
                "msg": 'success',
            }
        }

        res.json(api);
    }

    getReligion = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const result = await Religion.findById(id);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createReligion = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { name } = req.body;

        const result = await Religion.create({ name });
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    updateReligion = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id, name } = req.body;

        const result = await Religion.findById(id);
        const response = await result.update({ name });
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteReligion = async (req, res) => {
        const { id } = req.body;
        const data = await Religion.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    bulkDeleteReligion = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await Religion.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    // riwayat suami/istri
    getEmployeeSpouses = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, idEmployee } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            order: [['id', 'ASC']],
            include: [Employee, EducationLevel],
        };
        if (!idEmployee) {
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        } else {
            Object.assign(options, {
                where: {
                    id_employee: idEmployee,
                },
            });
        }
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        EducationLevel.hasMany(EmployeeSpouse, { foreignKey: 'id_education_level', sourceKey: 'id' });
        EmployeeSpouse.belongsTo(EducationLevel, { foreignKey: 'id_education_level', sourceKey: 'id' });
        Employee.hasMany(EmployeeSpouse, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeSpouse.belongsTo(Employee, { foreignKey: 'id_employee', sourceKey: 'id' });
        
        const results = await EmployeeSpouse.findAll(options);
        const calculateAmountOfData = await EmployeeSpouse.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            const parsedResults = JSON.parse(JSON.stringify(results)).map((x) => {
                const newData = Object.assign({}, x, {
                    employee_name: x.Employee.fullname,
                    education_level_name: x.EducationLevel.name,
                });
    
                delete newData.Employee;
                delete newData.EducationLevel;

                return newData;
            });

            api = {
                "status": true,
                "data": parsedResults,
                "msg": 'success',
            }
            if (!idEmployee) {
                Object.assign(api, {
                    "page_total": Math.ceil(amountOfData / limit),
                });
            }
        }

        res.json(api);
    }

    getEmployeeSpouse = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const options = {
            where: {
                id
            },
            include: [{
                model: Employee,
            }],
        };

        Employee.hasMany(EmployeeSpouse, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeSpouse.belongsTo(Employee,  { foreignKey: 'id_employee', sourceKey: 'id' });
        const result = await EmployeeSpouse.findOne(options);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createEmployeeSpouse = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const payload = req.body;

        const result = await EmployeeSpouse.create(payload);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    updateEmployeeSpouse = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const payload = req.body;

        const result = await EmployeeSpouse.findById(payload.id);

        delete payload.id;

        const response = await result.update(payload);
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteEmployeeSpouse = async (req, res) => {
        const { id } = req.body;
        const data = await EmployeeSpouse.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    bulkDeleteEmployeeSpouse = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await EmployeeSpouse.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    // anak
    getEmployeeKids = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, idEmployee } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            order: [['id', 'ASC']],
            include: [Employee, EducationLevel],
        };
        if (!idEmployee) {
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        } else {
            Object.assign(options, {
                where: {
                    id_employee: idEmployee,
                },
            });
        }
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        EducationLevel.hasMany(EmployeeKid, { foreignKey: 'id_education_level', sourceKey: 'id' });
        EmployeeKid.belongsTo(EducationLevel, { foreignKey: 'id_education_level', sourceKey: 'id' });
        Employee.hasMany(EmployeeKid, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeKid.belongsTo(Employee, { foreignKey: 'id_employee', sourceKey: 'id' });
        
        const results = await EmployeeKid.findAll(options);
        const calculateAmountOfData = await EmployeeKid.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            const parsedResults = JSON.parse(JSON.stringify(results)).map((x) => {
                const newData = Object.assign({}, x, {
                    employee_name: x.Employee.fullname,
                    education_level_name: x.EducationLevel.name,
                });
    
                delete newData.Employee;
                delete newData.EducationLevel;

                return newData;
            });

            api = {
                "status": true,
                "data": parsedResults,
                "msg": 'success',
            }
            if (!idEmployee) {
                Object.assign(api, {
                    "page_total": Math.ceil(amountOfData / limit),
                });
            }
        }

        res.json(api);
    }

    getEmployeeKid = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;
        const options = {
            where: {
                id
            },
            include: [{
                model: Employee,
            }],
        };

        Employee.hasMany(EmployeeKid, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeKid.belongsTo(Employee,  { foreignKey: 'id_employee', sourceKey: 'id' });
        const result = await EmployeeKid.findOne(options);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createEmployeeKid = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const payload = req.body;

        const result = await EmployeeKid.create(payload);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    updateEmployeeKid = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const payload = req.body;

        const result = await EmployeeKid.findById(payload.id);

        delete payload.id;

        const response = await result.update(payload);
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteEmployeeKid = async (req, res) => {
        const { id } = req.body;
        const data = await EmployeeKid.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    bulkDeleteEmployeeKid = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await EmployeeKid.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }


    // skp pegawai
    getSkpEmployees = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, idEmployee } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            order: [['id', 'ASC']],
            include: [Employee],
        };
        if (!idEmployee) {
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        } else {
            Object.assign(options, {
                where: {
                    id_employee: idEmployee,
                },
            });
        }
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        Employee.hasMany(SkpEmployee, { foreignKey: 'id_employee', sourceKey: 'id' });
        SkpEmployee.belongsTo(Employee, { foreignKey: 'id_employee', sourceKey: 'id' });
        
        const results = await SkpEmployee.findAll(options);
        const calculateAmountOfData = await SkpEmployee.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            const parsedResults = JSON.parse(JSON.stringify(results)).map((x) => {
                const newData = Object.assign({}, x, {
                    employee_name: x.Employee.fullname,
                });
    
                delete newData.Employee;

                return newData;
            });

            api = {
                "status": true,
                "data": parsedResults,
                "msg": 'success',
            }
            if (!idEmployee) {
                Object.assign(api, {
                    "page_total": Math.ceil(amountOfData / limit),
                });
            }
        }

        res.json(api);
    }

    getSkpEmployee = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const options = {
            where: {
                id
            },
            include: [Employee],
        };

        Employee.hasMany(SkpEmployee, { foreignKey: 'id_employee', sourceKey: 'id' });
        SkpEmployee.belongsTo(Employee,  { foreignKey: 'id_employee', sourceKey: 'id' });
        const result = await SkpEmployee.findOne(options);
        
        if (result) {
                api = {
                    "status": true,
                    "data": result,
                    "msg": 'success',
                }
        }

        res.json(api);
    }

    createSkpEmployee = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const payload = req.body;

        const result = await SkpEmployee.create(payload);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    updateSkpEmployee = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const payload = req.body;

        const result = await SkpEmployee.findById(payload.id);

        delete payload.id;

        const response = await result.update(payload);
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteSkpEmployee = async (req, res) => {
        const { id } = req.body;
        const data = await SkpEmployee.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    bulkDeleteSkpEmployee = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await SkpEmployee.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    // orang tua
    getEmployeeParents = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, idEmployee } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            order: [['id', 'ASC']],
            include: [Employee, EducationLevel],
        };
        if (!idEmployee) {
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        } else {
            Object.assign(options, {
                where: {
                    id_employee: idEmployee,
                },
            });
        }
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        EducationLevel.hasMany(EmployeeParent, { foreignKey: 'id_education_level', sourceKey: 'id' });
        EmployeeParent.belongsTo(EducationLevel,  { foreignKey: 'id_education_level', sourceKey: 'id' });
        Employee.hasMany(EmployeeParent, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeParent.belongsTo(Employee, { foreignKey: 'id_employee', sourceKey: 'id' });
        
        const results = await EmployeeParent.findAll(options);
        const calculateAmountOfData = await EmployeeParent.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            const parsedResults = JSON.parse(JSON.stringify(results)).map((x) => {
                const newData = Object.assign({}, x, {
                    employee_name: x.Employee.fullname,
                    education_level_name: x.EducationLevel.name,
                });
    
                delete newData.Employee;
                delete newData.EducationLevel;
    
                return newData;
            });

            api = {
                "status": true,
                "data": parsedResults,
                "msg": 'success',
            }

            if (!idEmployee) {
                Object.assign(api, {
                    "page_total": Math.ceil(amountOfData / limit),
                });
            }
        }

        res.json(api);
    }

    getEmployeeParent = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;
        const options = {
            where: {
                id
            },
            include: [{
                model: Employee,
            }],
        };

        Employee.hasMany(EmployeeParent, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeParent.belongsTo(Employee,  { foreignKey: 'id_employee', sourceKey: 'id' });
        const result = await EmployeeParent.findOne(options);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createEmployeeParent = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const payload = req.body;

        const result = await EmployeeParent.create(payload);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    updateEmployeeParent = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const payload = req.body;

        const result = await EmployeeParent.findById(payload.id);

        delete payload.id;

        const response = await result.update(payload);
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteEmployeeParent = async (req, res) => {
        const { id } = req.body;
        const data = await EmployeeParent.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    bulkDeleteEmployeeParent = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await EmployeeParent.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    // sekolah
    getEmployeeSchools = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, idEmployee } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            order: [['id', 'ASC']],
            include: [{
                model: Employee,
            }],
        };
        if (!idEmployee) {
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        } else {
            Object.assign(options, {
                where: {
                    id_employee: idEmployee,
                },
                order: [['last_education', 'DESC']],
            });
        }
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        Employee.hasMany(EmployeeSchool, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeSchool.belongsTo(Employee, { foreignKey: 'id_employee', sourceKey: 'id' });
        
        const results = await EmployeeSchool.findAll(options);
        const calculateAmountOfData = await EmployeeSchool.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            const parsedResults = JSON.parse(JSON.stringify(results)).map((x) => {
                const newData = Object.assign({}, x, {
                    employee_name: x.Employee.fullname,
                });
    
                delete newData.Employee;
    
                return newData;
            });

            api = {
                "status": true,
                "data": parsedResults,
                "msg": 'success',
            }

            if (!idEmployee) {
                Object.assign(api, {
                    "page_total": Math.ceil(amountOfData / limit),
                });
            }
        }

        res.json(api);
    }

    getEmployeeSchool = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;
        const options = {
            where: {
                id
            },
            include: [{
                model: Employee,
            }],
        };

        Employee.hasMany(EmployeeSchool, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeSchool.belongsTo(Employee,  { foreignKey: 'id_employee', sourceKey: 'id' });
        const result = await EmployeeSchool.findOne(options);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createEmployeeSchool = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const payload = req.body;
        const idEmployee = payload.id_employee;

        const options = {
            where: {
                id_employee: idEmployee,
            },
        };
        await EmployeeSchool.update({ last_education: false }, options);

        Object.assign(payload, {
            last_education: true,
        });
        const result = await EmployeeSchool.create(payload);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    updateEmployeeSchool = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const payload = req.body;

        const result = await EmployeeSchool.findById(payload.id);

        delete payload.id;

        const response = await result.update(payload);
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteEmployeeSchool = async (req, res) => {
        const { id } = req.body;
        const data = await EmployeeSchool.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    bulkDeleteEmployeeSchool = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await EmployeeSchool.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    setLastEducationEmployee = async (req, res) => {
        const { id, idEmployee } = req.body;
        let options = {
            where: {
                id_employee: idEmployee,
            },
        };
        await EmployeeSchool.update({ last_education: false }, options);

        options = {
            where: {
                id,
            },
        };
        const result = await EmployeeSchool.findOne(options);
        const response = await result.update({ last_education: true });

        const api = {
            "status": true,
            "data": response,
            "msg": 'success',
        }

        res.json(api);
    }

    // bahasa
    getEmployeeLanguages = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, idEmployee } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            order: [['id', 'ASC']],
            include: [{
                model: Employee,
            }],
        };
        if (!idEmployee) {
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        } else {
            Object.assign(options, {
                where: {
                    id_employee: idEmployee,
                },
            });
        }
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        Employee.hasMany(EmployeeLanguage, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeLanguage.belongsTo(Employee, { foreignKey: 'id_employee', sourceKey: 'id' });
        
        const results = await EmployeeLanguage.findAll(options);
        const calculateAmountOfData = await EmployeeLanguage.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            const parsedResults = JSON.parse(JSON.stringify(results)).map((x) => {
                const newData = Object.assign({}, x, {
                    employee_name: x.Employee.fullname,
                });
    
                delete newData.Employee;
    
                return newData;
            });

            api = {
                "status": true,
                "data": parsedResults,
                "msg": 'success',
            }
            if (!idEmployee) {
                Object.assign(api, {
                    "page_total": Math.ceil(amountOfData / limit),
                });
            }
        }

        res.json(api);
    }

    getEmployeeLanguage = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;
        const options = {
            where: {
                id
            },
            include: [{
                model: Employee,
            }],
        };

        Employee.hasMany(EmployeeLanguage, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeLanguage.belongsTo(Employee,  { foreignKey: 'id_employee', sourceKey: 'id' });
        const result = await EmployeeLanguage.findOne(options);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createEmployeeLanguage = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const payload = req.body;

        const result = await EmployeeLanguage.create(payload);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    updateEmployeeLanguage = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const payload = req.body;

        const result = await EmployeeLanguage.findById(payload.id);

        delete payload.id;

        const response = await result.update(payload);
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteEmployeeLanguage = async (req, res) => {
        const { id } = req.body;
        const data = await EmployeeLanguage.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    bulkDeleteEmployeeLanguage = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await EmployeeLanguage.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    // latihan jabatan pegawai
    getEmployeeJobTrainings = async (req, res) => {
        let files = [];
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, idEmployee } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            order: [['id', 'ASC']],
            include: [Employee],
        };
        if (!idEmployee) {
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        } else {
            Object.assign(options, {
                where: {
                    id_employee: idEmployee,
                },
            });
            const resFiles = await FileStorage.findAll({ 
                where: { 
                    tablename: 'EmployeeJobTrainings',
                } 
            });
            files = JSON.parse(JSON.stringify(resFiles));
        }
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        Employee.hasMany(EmployeeJobTraining, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeJobTraining.belongsTo(Employee, { foreignKey: 'id_employee', sourceKey: 'id' });
        
        const results = await EmployeeJobTraining.findAll(options);
        const calculateAmountOfData = await EmployeeJobTraining.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            const parsedResults = JSON.parse(JSON.stringify(results)).map((x) => {
                let file = null;
                const foundFile = files.find(f => String(f.id_reference) === String(x.id));
                
                if (foundFile) {
                    file = foundFile.filename;
                }

                const newData = Object.assign({}, x, {
                    employee_name: x.Employee.fullname,
                    file,
                });

                delete newData.Employee;
                
                return newData;
            });

            api = {
                "status": true,
                "data": parsedResults,
                "msg": 'success',
            }
            if (!idEmployee) {
                Object.assign(api, {
                    "page_total": Math.ceil(amountOfData / limit),
                });
            }
        }

        res.json(api);
    }

    getEmployeeJobTraining = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const options = {
            where: {
                id
            },
            include: [Employee],
        };

        Employee.hasMany(EmployeeJobTraining, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeJobTraining.belongsTo(Employee,  { foreignKey: 'id_employee', sourceKey: 'id' });
        const result = await EmployeeJobTraining.findOne(options);
        
        if (result) {
                const resFile = await FileStorage.findOne({ 
                    where: { 
                        id_reference: id,
                        tablename: 'EmployeeJobTrainings',
                    } 
                });
                
                api = {
                    "status": true,
                    "data": Object.assign({}, JSON.parse(JSON.stringify(result)), { file: resFile }),
                    "msg": 'success',
                }
        }

        res.json(api);
    }

    createEmployeeJobTraining = async (req, res) => {
        let api = null;
        
        try {
            const data = req.body;

            const result = await EmployeeJobTraining.create(data);
            if (data.file && data.file.fileValue) {
                const dir = `public/uploads/${data.id_employee}`;

                if (!fs.existsSync(dir)){
                    fs.mkdirSync(dir);
                }

                const oldPath = data.file.fileValue;
                const newPath = `${dir}/${data.file.fileName}`;
                fs.renameSync(oldPath, newPath);

                await FileStorage.create({
                    tablename: "EmployeeJobTrainings",
                    filename: JSON.stringify({ filepath: newPath, filename: data.file.fileName, filetype: data.file.fileType }),
                    id_reference: result.id,
                });
            }

            if (result) {
                api = {
                    "status": true,
                    "data": result,
                    "msg": 'success',
                }
            }
        } catch (e) {
            api = {
                "status": false,
                "msg": e,
            }
        }

        res.json(api);
    }

    updateEmployeeJobTraining = async (req, res) => {
        const tableName = "EmployeeJobTrainings";
        let api = {
            "status": false,
            "msg": 'error',
        };
        const data = req.body;
        
        const result = await EmployeeJobTraining.findById(data.id);
        const response = await result.update(data);

        await FileStorage.destroy({
            where: {
                id_reference: response.id,
                tablename: tableName,
            }
        });

        if (data.file && data.file.fileValue) {
            const dir = `public/uploads/${data.id_employee}`;

            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }

            const oldPath = data.file.fileValue;
            const newPath = `${dir}/${data.file.fileName}`;
            fs.renameSync(oldPath, newPath);

            await FileStorage.create({
                tablename: tableName,
                filename: JSON.stringify({ filepath: newPath, filename: data.file.fileName, filetype: data.file.fileType }),
                id_reference: result.id,
            });
        }
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteEmployeeJobTraining = async (req, res) => {
        const { id } = req.body;
        const data = await EmployeeJobTraining.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    bulkDeleteEmployeeJobTraining = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await EmployeeJobTraining.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    // pangkat pegawai
    getEmployeeLevels = async (req, res) => {
        let files = [];
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, idEmployee } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            order: [['id', 'ASC']],
            include: [Employee, Group],
        };
        if (!idEmployee) {
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        } else {
            Object.assign(options, {
                where: {
                    id_employee: idEmployee,
                },
            });
            const resFiles = await FileStorage.findAll({ 
                where: { 
                    tablename: 'EmployeeLevels',
                } 
            });
            files = JSON.parse(JSON.stringify(resFiles));
        }
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        Group.hasMany(EmployeeLevel, { foreignKey: 'id_group', sourceKey: 'id' });
        EmployeeLevel.belongsTo(Group, { foreignKey: 'id_group', sourceKey: 'id' });
        
        Employee.hasMany(EmployeeLevel, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeLevel.belongsTo(Employee, { foreignKey: 'id_employee', sourceKey: 'id' });
        
        const results = await EmployeeLevel.findAll(options);
        const calculateAmountOfData = await EmployeeLevel.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            const parsedResults = JSON.parse(JSON.stringify(results)).map((x) => {
                let file = null;
                const foundFile = files.find(f => String(f.id_reference) === String(x.id));
                
                if (foundFile) {
                    file = foundFile.filename;
                }

                const newData = Object.assign({}, x, {
                    employee_name: x.Employee.fullname,
                    group_name: x.Group.name,
                    file,
                });
                
                delete newData.Group;
                delete newData.Employee;
                
                return newData;
            });

            api = {
                "status": true,
                "data": parsedResults,
                "msg": 'success',
            }
            if (!idEmployee) {
                Object.assign(api, {
                    "page_total": Math.ceil(amountOfData / limit),
                });
            }
        }

        res.json(api);
    }

    getEmployeeLevel = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const options = {
            where: {
                id
            },
            include: [Employee, Group],
        };

        Employee.hasMany(EmployeeLevel, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeLevel.belongsTo(Employee,  { foreignKey: 'id_employee', sourceKey: 'id' });
        Group.hasMany(EmployeeLevel, { foreignKey: 'id_group', sourceKey: 'id' });
        EmployeeLevel.belongsTo(Group,  { foreignKey: 'id_group', sourceKey: 'id' });
        const result = await EmployeeLevel.findOne(options);
        
        if (result) {
                const resFile = await FileStorage.findOne({ 
                    where: { 
                        id_reference: id,
                        tablename: 'EmployeeLevels',
                    } 
                });
                
                api = {
                    "status": true,
                    "data": Object.assign({}, JSON.parse(JSON.stringify(result)), { file: resFile }),
                    "msg": 'success',
                }
        }

        res.json(api);
    }

    createEmployeeLevel = async (req, res) => {
        let api = null;
        
        try {
            const data = req.body;

            Object.assign(data, {
                is_active: true,
            });

            const result = await EmployeeLevel.create(data);
            if (data.file && data.file.fileValue) {
                const dir = `public/uploads/${data.id_employee}`;

                if (!fs.existsSync(dir)){
                    fs.mkdirSync(dir);
                }

                const oldPath = data.file.fileValue;
                const newPath = `${dir}/${data.file.fileName}`;
                fs.renameSync(oldPath, newPath);

                await FileStorage.create({
                    tablename: "EmployeeLevels",
                    filename: JSON.stringify({ filepath: newPath, filename: data.file.fileName, filetype: data.file.fileType }),
                    id_reference: result.id,
                });
            }

            if (result) {
                api = {
                    "status": true,
                    "data": result,
                    "msg": 'success',
                }
            }
        } catch (e) {
            api = {
                "status": false,
                "msg": e,
            }
        }

        res.json(api);
    }

    updateEmployeeLevel = async (req, res) => {
        const tableName = "EmployeeLevels";
        let api = {
            "status": false,
            "msg": 'error',
        };
        const data = req.body;
        
        const result = await EmployeeLevel.findById(data.id);
        const response = await result.update(data);

        await FileStorage.destroy({
            where: {
                id_reference: response.id,
                tablename: tableName,
            }
        });

        if (data.file && data.file.fileValue) {
            const dir = `public/uploads/${data.id_employee}`;

            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }

            const oldPath = data.file.fileValue;
            const newPath = `${dir}/${data.file.fileName}`;
            fs.renameSync(oldPath, newPath);

            await FileStorage.create({
                tablename: tableName,
                filename: JSON.stringify({ filepath: newPath, filename: data.file.fileName, filetype: data.file.fileType }),
                id_reference: result.id,
            });
        }
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteEmployeeLevel = async (req, res) => {
        const { id } = req.body;
        const data = await EmployeeLevel.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    bulkDeleteEmployeeLevel = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await EmployeeLevel.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    setActiveEmployeeLevel = async (req, res) => {
        const { id, idEmployee } = req.body;
        let options = {
            where: {
                id_employee: idEmployee,
            },
        };
        await EmployeeLevel.update({ is_active: false }, options);

        options = {
            where: {
                id,
            },
        };
        const result = await EmployeeLevel.findOne(options);
        const response = await result.update({ is_active: true });

        const api = {
            "status": true,
            "data": response,
            "msg": 'success',
        }

        res.json(api);
    }

    getPenaltyTypes = async (req, res) => {
        const results = await PenaltyType.findAll();

        const api = {
            "status": true,
            "data": results,
            "msg": 'success',
        }

        res.json(api);
    }

    // diklat pegawai
    getEmployeeTrainings = async (req, res) => {
        let files = [];
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, idEmployee } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            order: [['id', 'ASC']],
            include: [Employee],
        };

        if (!idEmployee) {
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        } else {
            Object.assign(options, {
                where: {
                    id_employee: idEmployee,
                },
            });
            const resFiles = await FileStorage.findAll({ 
                where: { 
                    tablename: 'EmployeeTrainings',
                } 
            });
            files = JSON.parse(JSON.stringify(resFiles));
        }
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        Employee.hasMany(EmployeeTraining, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeTraining.belongsTo(Employee, { foreignKey: 'id_employee', sourceKey: 'id' });
        
        const results = await EmployeeTraining.findAll(options);
        const calculateAmountOfData = await EmployeeTraining.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            const parsedResults = JSON.parse(JSON.stringify(results)).map((x) => {
                let file = null;
                const foundFile = files.find(f => String(f.id_reference) === String(x.id));
                
                if (foundFile) {
                    file = foundFile.filename;
                }
                
                const newData = Object.assign({}, x, {
                    employee_name: x.Employee.fullname,
                    file,
                });
                
                delete newData.Employee;
                
                return newData;
            });

            api = {
                "status": true,
                "data": parsedResults,
                "msg": 'success',
            }
            if (!idEmployee) {
                Object.assign(api, {
                    "page_total": Math.ceil(amountOfData / limit),
                });
            }
        }

        res.json(api);
    }

    getEmployeeTraining = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const options = {
            where: {
                id
            },
            include: [Employee],
        };

        Employee.hasMany(EmployeeTraining, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeTraining.belongsTo(Employee,  { foreignKey: 'id_employee', sourceKey: 'id' });
        const result = await EmployeeTraining.findOne(options);
        
        if (result) {
                const resFile = await FileStorage.findOne({ 
                    where: { 
                        id_reference: id,
                        tablename: 'EmployeeTrainings',
                    } 
                });
                
                api = {
                    "status": true,
                    "data": Object.assign({}, JSON.parse(JSON.stringify(result)), { file: resFile }),
                    "msg": 'success',
                }
        }

        res.json(api);
    }

    createEmployeeTraining = async (req, res) => {
        let api = null;
        
        try {
            const data = req.body;

            const result = await EmployeeTraining.create(data);
            if (data.file && data.file.fileValue) {
                const dir = `public/uploads/${data.id_employee}`;

                if (!fs.existsSync(dir)){
                    fs.mkdirSync(dir);
                }

                const oldPath = data.file.fileValue;
                const newPath = `${dir}/${data.file.fileName}`;
                fs.renameSync(oldPath, newPath);

                await FileStorage.create({
                    tablename: "EmployeeTrainings",
                    filename: JSON.stringify({ filepath: newPath, filename: data.file.fileName, filetype: data.file.fileType }),
                    id_reference: result.id,
                });
            }

            if (result) {
                api = {
                    "status": true,
                    "data": result,
                    "msg": 'success',
                }
            }
        } catch (e) {
            api = {
                "status": false,
                "msg": e,
            }
        }

        res.json(api);
    }

    updateEmployeeTraining = async (req, res) => {
        const tableName = "EmployeeTrainings";
        let api = {
            "status": false,
            "msg": 'error',
        };
        const data = req.body;
        
        const result = await EmployeeTraining.findById(data.id);
        const response = await result.update(data);

        await FileStorage.destroy({
            where: {
                id_reference: response.id,
                tablename: tableName,
            }
        });

        if (data.file && data.file.fileValue) {
            const dir = `public/uploads/${data.id_employee}`;

            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }

            const oldPath = data.file.fileValue;
            const newPath = `${dir}/${data.file.fileName}`;
            fs.renameSync(oldPath, newPath);

            await FileStorage.create({
                tablename: tableName,
                filename: JSON.stringify({ filepath: newPath, filename: data.file.fileName, filetype: data.file.fileType }),
                id_reference: result.id,
            });
        }
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteEmployeeTraining = async (req, res) => {
        const { id } = req.body;
        const data = await EmployeeTraining.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    bulkDeleteEmployeeTraining = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await EmployeeTraining.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    // penghargaan pegawai
    getEmployeeHonors = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, idEmployee } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            order: [['id', 'ASC']],
            include: [Employee],
        };
        if (!idEmployee) {
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        } else {
            Object.assign(options, {
                where: {
                    id_employee: idEmployee,
                },
            });
        }
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        Employee.hasMany(EmployeeHonor, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeHonor.belongsTo(Employee, { foreignKey: 'id_employee', sourceKey: 'id' });
        
        const results = await EmployeeHonor.findAll(options);
        const calculateAmountOfData = await EmployeeHonor.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            const parsedResults = JSON.parse(JSON.stringify(results)).map((x) => {
                const newData = Object.assign({}, x, {
                    employee_name: x.Employee.fullname,
                });
                
                delete newData.Employee;
                
                return newData;
            });

            api = {
                "status": true,
                "data": parsedResults,
                "msg": 'success',
            }
            if (!idEmployee) {
                Object.assign(api, {
                    "page_total": Math.ceil(amountOfData / limit),
                });
            }
        }

        res.json(api);
    }

    getEmployeeHonor = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const options = {
            where: {
                id
            },
            include: [Employee],
        };

        Employee.hasMany(EmployeeHonor, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeHonor.belongsTo(Employee,  { foreignKey: 'id_employee', sourceKey: 'id' });
        const result = await EmployeeHonor.findOne(options);
        
        if (result) {
                api = {
                    "status": true,
                    "data": result,
                    "msg": 'success',
                }
        }

        res.json(api);
    }

    createEmployeeHonor = async (req, res) => {
        let api = null;
        
        try {
            const data = req.body;

            const result = await EmployeeHonor.create(data);
            
            if (result) {
                api = {
                    "status": true,
                    "data": result,
                    "msg": 'success',
                }
            }
        } catch (e) {
            api = {
                "status": false,
                "msg": e,
            }
        }

        res.json(api);
    }

    updateEmployeeHonor = async (req, res) => {
        let api = null;
        
        try {
            const data = req.body;

            const result = await EmployeeHonor.findById(data.id);
            const response = await result.update(data);

            if (response) {
                api = {
                    "status": true,
                    "data": response,
                    "msg": 'success',
                }
            }
        } catch (e) {
            api = {
                "status": false,
                "msg": e,
            }
        }

        res.json(api);
    }

    deleteEmployeeHonor = async (req, res) => {
        const { id } = req.body;
        const data = await EmployeeHonor.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    bulkDeleteEmployeeHonor = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await EmployeeHonor.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    // tunjangan pegawai
    getEmployeeAllowances = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, idEmployee } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            order: [['id', 'ASC']],
            include: [Employee],
        };
        if (!idEmployee) {
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        } else {
            Object.assign(options, {
                where: {
                    id_employee: idEmployee,
                },
            });
        }
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        Employee.hasMany(EmployeeAllowance, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeAllowance.belongsTo(Employee, { foreignKey: 'id_employee', sourceKey: 'id' });
        
        const results = await EmployeeAllowance.findAll(options);
        const calculateAmountOfData = await EmployeeAllowance.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            const parsedResults = JSON.parse(JSON.stringify(results)).map((x) => {
                const newData = Object.assign({}, x, {
                    employee_name: x.Employee.fullname,
                });
                
                delete newData.Employee;
                
                return newData;
            });

            api = {
                "status": true,
                "data": parsedResults,
                "msg": 'success',
            }
            if (!idEmployee) {
                Object.assign(api, {
                    "page_total": Math.ceil(amountOfData / limit),
                });
            }
        }

        res.json(api);
    }

    getEmployeeAllowance = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const options = {
            where: {
                id
            },
            include: [Employee],
        };

        Employee.hasMany(EmployeeAllowance, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeAllowance.belongsTo(Employee,  { foreignKey: 'id_employee', sourceKey: 'id' });
        const result = await EmployeeAllowance.findOne(options);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createEmployeeAllowance = async (req, res) => {
        let api = null;
        
        try {
            const data = req.body;

            const result = await EmployeeAllowance.create(data);
            
            if (result) {
                api = {
                    "status": true,
                    "data": result,
                    "msg": 'success',
                }
            }
        } catch (e) {
            api = {
                "status": false,
                "msg": e,
            }
        }

        res.json(api);
    }

    updateEmployeeAllowance = async (req, res) => {
        let api = null;
        
        try {
            const data = req.body;

            const result = await EmployeeAllowance.findById(data.id);
            const response = await result.update(data);

            if (response) {
                api = {
                    "status": true,
                    "data": response,
                    "msg": 'success',
                }
            }
        } catch (e) {
            api = {
                "status": false,
                "msg": e,
            }
        }

        res.json(api);
    }

    deleteEmployeeAllowance = async (req, res) => {
        const { id } = req.body;
        const data = await EmployeeAllowance.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    bulkDeleteEmployeeAllowance = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await EmployeeAllowance.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    // cuti pegawai
    getEmployeeLeaves = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, idEmployee } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            order: [['id', 'ASC']],
            include: [Employee],
        };
        if (!idEmployee) {
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        } else {
            Object.assign(options, {
                where: {
                    id_employee: idEmployee,
                },
            });
        }
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        Employee.hasMany(EmployeeLeave, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeLeave.belongsTo(Employee, { foreignKey: 'id_employee', sourceKey: 'id' });
        
        const results = await EmployeeLeave.findAll(options);
        const calculateAmountOfData = await EmployeeLeave.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            const parsedResults = JSON.parse(JSON.stringify(results)).map((x) => {
                const newData = Object.assign({}, x, {
                    employee_name: x.Employee.fullname,
                });
                
                delete newData.Employee;
                
                return newData;
            });

            api = {
                "status": true,
                "data": parsedResults,
                "msg": 'success',
            }
            if (!idEmployee) {
                Object.assign(api, {
                    "page_total": Math.ceil(amountOfData / limit),
                });
            }
        }

        res.json(api);
    }

    getEmployeeLeave = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const options = {
            where: {
                id
            },
            include: [Employee],
        };

        Employee.hasMany(EmployeeLeave, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeLeave.belongsTo(Employee,  { foreignKey: 'id_employee', sourceKey: 'id' });
        const result = await EmployeeLeave.findOne(options);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createEmployeeLeave = async (req, res) => {
        let api = null;
        
        try {
            const data = req.body;

            const result = await EmployeeLeave.create(data);
            
            if (result) {
                api = {
                    "status": true,
                    "data": result,
                    "msg": 'success',
                }
            }
        } catch (e) {
            api = {
                "status": false,
                "msg": e,
            }
        }

        res.json(api);
    }

    updateEmployeeLeave = async (req, res) => {
        let api = null;
        
        try {
            const data = req.body;

            const result = await EmployeeLeave.findById(data.id);
            const response = await result.update(data);

            if (response) {
                api = {
                    "status": true,
                    "data": response,
                    "msg": 'success',
                }
            }
        } catch (e) {
            api = {
                "status": false,
                "msg": e,
            }
        }

        res.json(api);
    }

    deleteEmployeeLeave = async (req, res) => {
        const { id } = req.body;
        const data = await EmployeeLeave.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    bulkDeleteEmployeeLeave = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await EmployeeLeave.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    // penugasan pegawai
    getEmployeeAssignments = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, idEmployee } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            order: [['id', 'ASC']],
            include: [Employee],
        };
        if (!idEmployee) {
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        } else {
            Object.assign(options, {
                where: {
                    id_employee: idEmployee,
                },
            });
        }
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        Employee.hasMany(EmployeeAssignment, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeAssignment.belongsTo(Employee, { foreignKey: 'id_employee', sourceKey: 'id' });
        
        const results = await EmployeeAssignment.findAll(options);
        const calculateAmountOfData = await EmployeeAssignment.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            const parsedResults = JSON.parse(JSON.stringify(results)).map((x) => {
                const newData = Object.assign({}, x, {
                    employee_name: x.Employee.fullname,
                });
                
                delete newData.Employee;
                
                return newData;
            });

            api = {
                "status": true,
                "data": parsedResults,
                "msg": 'success',
            }
            if (!idEmployee) {
                Object.assign(api, {
                    "page_total": Math.ceil(amountOfData / limit),
                });
            }
        }

        res.json(api);
    }

    getEmployeeAssignment = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const options = {
            where: {
                id
            },
            include: [Employee],
        };

        Employee.hasMany(EmployeeAssignment, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeAssignment.belongsTo(Employee,  { foreignKey: 'id_employee', sourceKey: 'id' });
        const result = await EmployeeAssignment.findOne(options);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createEmployeeAssignment = async (req, res) => {
        let api = null;
        
        try {
            const data = req.body;

            const result = await EmployeeAssignment.create(data);
            
            if (result) {
                api = {
                    "status": true,
                    "data": result,
                    "msg": 'success',
                }
            }
        } catch (e) {
            api = {
                "status": false,
                "msg": e,
            }
        }

        res.json(api);
    }

    updateEmployeeAssignment = async (req, res) => {
        let api = null;
        
        try {
            const data = req.body;

            const result = await EmployeeAssignment.findById(data.id);
            const response = await result.update(data);

            if (response) {
                api = {
                    "status": true,
                    "data": response,
                    "msg": 'success',
                }
            }
        } catch (e) {
            api = {
                "status": false,
                "msg": e,
            }
        }

        res.json(api);
    }

    deleteEmployeeAssignment = async (req, res) => {
        const { id } = req.body;
        const data = await EmployeeAssignment.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    bulkDeleteEmployeeAssignment = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await EmployeeAssignment.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    // hukuman pegawai
    getEmployeePenalties = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, idEmployee } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            order: [['id', 'ASC']],
            include: [Employee, PenaltyType],
        };
        if (!idEmployee) {
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        } else {
            Object.assign(options, {
                where: {
                    id_employee: idEmployee,
                },
            });
        }
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        PenaltyType.hasMany(EmployeePenalty, { foreignKey: 'id_penalty_type', sourceKey: 'id' });
        EmployeePenalty.belongsTo(PenaltyType, { foreignKey: 'id_penalty_type', sourceKey: 'id' });
        
        Employee.hasMany(EmployeePenalty, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeePenalty.belongsTo(Employee, { foreignKey: 'id_employee', sourceKey: 'id' });
        
        const results = await EmployeePenalty.findAll(options);
        const calculateAmountOfData = await EmployeePenalty.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            const parsedResults = JSON.parse(JSON.stringify(results)).map((x) => {
                const newData = Object.assign({}, x, {
                    employee_name: x.Employee.fullname,
                    penalty_type_name: x.PenaltyType.name,
                });
                
                delete newData.PenaltyType;
                delete newData.Employee;
                
                return newData;
            });

            api = {
                "status": true,
                "data": parsedResults,
                "msg": 'success',
            }
            if (!idEmployee) {
                Object.assign(api, {
                    "page_total": Math.ceil(amountOfData / limit),
                });
            }
        }

        res.json(api);
    }

    getEmployeePenalty = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const options = {
            where: {
                id
            },
            include: [Employee, PenaltyType],
        };

        Employee.hasMany(EmployeePenalty, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeePenalty.belongsTo(Employee,  { foreignKey: 'id_employee', sourceKey: 'id' });
        PenaltyType.hasMany(EmployeePenalty, { foreignKey: 'id_penalty_type', sourceKey: 'id' });
        EmployeePenalty.belongsTo(PenaltyType,  { foreignKey: 'id_penalty_type', sourceKey: 'id' });
        const result = await EmployeePenalty.findOne(options);
        
        if (result) {
                api = {
                    "status": true,
                    "data": result,
                    "msg": 'success',
                }
        }

        res.json(api);
    }

    createEmployeePenalty = async (req, res) => {
        let api = null;
        
        try {
            const data = req.body;

            const result = await EmployeePenalty.create(data);
            
            if (result) {
                api = {
                    "status": true,
                    "data": result,
                    "msg": 'success',
                }
            }
        } catch (e) {
            api = {
                "status": false,
                "msg": e,
            }
        }

        res.json(api);
    }

    updateEmployeePenalty = async (req, res) => {
        let api = null;
        
        try {
            const data = req.body;

            const result = await EmployeePenalty.findById(data.id);
            const response = await result.update(data);

            if (response) {
                api = {
                    "status": true,
                    "data": response,
                    "msg": 'success',
                }
            }
        } catch (e) {
            api = {
                "status": false,
                "msg": e,
            }
        }

        res.json(api);
    }

    deleteEmployeePenalty = async (req, res) => {
        const { id } = req.body;
        const data = await EmployeePenalty.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    bulkDeleteEmployeePenalty = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await EmployeePenalty.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    // mutasi pegawai
    getEmployeeMutations = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, idEmployee } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            order: [['id', 'ASC']],
            include: [Employee],
        };
        if (!idEmployee) {
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        } else {
            Object.assign(options, {
                where: {
                    id_employee: idEmployee,
                },
            });
        }
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        Employee.hasMany(EmployeeMutation, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeMutation.belongsTo(Employee, { foreignKey: 'id_employee', sourceKey: 'id' });
        
        const results = await EmployeeMutation.findAll(options);
        const calculateAmountOfData = await EmployeeMutation.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            const parsedResults = JSON.parse(JSON.stringify(results)).map((x) => {
                const newData = Object.assign({}, x, {
                    employee_name: x.Employee.fullname,
                });
                
                delete newData.Employee;
                
                return newData;
            });

            api = {
                "status": true,
                "data": parsedResults,
                "msg": 'success',
            }
            if (!idEmployee) {
                Object.assign(api, {
                    "page_total": Math.ceil(amountOfData / limit),
                });
            }
        }

        res.json(api);
    }

    getEmployeeMutation = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const options = {
            where: {
                id
            },
            include: [Employee],
        };

        Employee.hasMany(EmployeeMutation, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeMutation.belongsTo(Employee,  { foreignKey: 'id_employee', sourceKey: 'id' });
        const result = await EmployeeMutation.findOne(options);
        
        if (result) {
                api = {
                    "status": true,
                    "data": result,
                    "msg": 'success',
                }
        }

        res.json(api);
    }

    createEmployeeMutation = async (req, res) => {
        let api = null;
        
        try {
            const data = req.body;

            const result = await EmployeeMutation.create(data);
            
            if (result) {
                api = {
                    "status": true,
                    "data": result,
                    "msg": 'success',
                }
            }
        } catch (e) {
            api = {
                "status": false,
                "msg": e,
            }
        }

        res.json(api);
    }

    updateEmployeeMutation = async (req, res) => {
        let api = null;
        
        try {
            const data = req.body;

            const result = await EmployeeMutation.findById(data.id);
            const response = await result.update(data);

            if (response) {
                api = {
                    "status": true,
                    "data": response,
                    "msg": 'success',
                }
            }
        } catch (e) {
            api = {
                "status": false,
                "msg": e,
            }
        }

        res.json(api);
    }

    deleteEmployeeMutation = async (req, res) => {
        const { id } = req.body;
        const data = await EmployeeMutation.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    bulkDeleteEmployeeMutation = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await EmployeeMutation.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    // seminar pegawai
    getEmployeeSeminars = async (req, res) => {
        let files = [];
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, idEmployee } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            order: [['id', 'ASC']],
            include: [Employee],
        };
        if (!idEmployee) {
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        } else {
            Object.assign(options, {
                where: {
                    id_employee: idEmployee,
                },
            });
            const resFiles = await FileStorage.findAll({ 
                where: { 
                    tablename: 'EmployeeSeminars',
                } 
            });
            files = JSON.parse(JSON.stringify(resFiles));
        }
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        Employee.hasMany(EmployeeSeminar, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeSeminar.belongsTo(Employee, { foreignKey: 'id_employee', sourceKey: 'id' });
        
        const results = await EmployeeSeminar.findAll(options);
        const calculateAmountOfData = await EmployeeSeminar.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            const parsedResults = JSON.parse(JSON.stringify(results)).map((x) => {
                let file = null;
                const foundFile = files.find(f => String(f.id_reference) === String(x.id));
                
                if (foundFile) {
                    file = foundFile.filename;
                }

                const newData = Object.assign({}, x, {
                    employee_name: x.Employee.fullname,
                    file
                });
                
                delete newData.Employee;
                
                return newData;
            });

            api = {
                "status": true,
                "data": parsedResults,
                "msg": 'success',
            }
            if (!idEmployee) {
                Object.assign(api, {
                    "page_total": Math.ceil(amountOfData / limit),
                });
            }
        }

        res.json(api);
    }

    getEmployeeSeminar = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;
        const options = {
            where: {
                id
            },
            include: [Employee],
        };

        Employee.hasMany(EmployeeSeminar, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeeSeminar.belongsTo(Employee,  { foreignKey: 'id_employee', sourceKey: 'id' });
        const result = await EmployeeSeminar.findOne(options);
        
        if (result) {
                const resFile = await FileStorage.findOne({ 
                    where: { 
                        id_reference: id,
                        tablename: 'EmployeeSeminars',
                    } 
                });
                
                api = {
                    "status": true,
                    "data": Object.assign({}, JSON.parse(JSON.stringify(result)), { file: resFile }),
                    "msg": 'success',
                }
        }

        res.json(api);
    }

    createEmployeeSeminar = async (req, res) => {
        let api = null;
        
        try {
            const data = req.body;

            const result = await EmployeeSeminar.create(data);
            if (data.file && data.file.fileValue) {
                const dir = `public/uploads/${data.id_employee}`;

                if (!fs.existsSync(dir)){
                    fs.mkdirSync(dir);
                }

                const oldPath = data.file.fileValue;
                const newPath = `${dir}/${data.file.fileName}`;
                fs.renameSync(oldPath, newPath);

                await FileStorage.create({
                    tablename: "EmployeeSeminars",
                    filename: JSON.stringify({ filepath: newPath, filename: data.file.fileName, filetype: data.file.fileType }),
                    id_reference: result.id,
                });
            }

            if (result) {
                api = {
                    "status": true,
                    "data": result,
                    "msg": 'success',
                }
            }
        } catch (e) {
            api = {
                "status": false,
                "msg": e,
            }
        }

        res.json(api);
    }

    updateEmployeeSeminar = async (req, res) => {
        const tableName = "EmployeeSeminars";
        let api = {
            "status": false,
            "msg": 'error',
        };
        const data = req.body;
        
        const result = await EmployeeSeminar.findById(data.id);
        const response = await result.update(data);

        await FileStorage.destroy({
            where: {
                id_reference: response.id,
                tablename: tableName,
            }
        });

        if (data.file && data.file.fileValue) {
            const dir = `public/uploads/${data.id_employee}`;

            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }

            const oldPath = data.file.fileValue;
            const newPath = `${dir}/${data.file.fileName}`;
            fs.renameSync(oldPath, newPath);

            await FileStorage.create({
                tablename: tableName,
                filename: JSON.stringify({ filepath: newPath, filename: data.file.fileName, filetype: data.file.fileType }),
                id_reference: result.id,
            });
        }
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteEmployeeSeminar = async (req, res) => {
        const { id } = req.body;
        const data = await EmployeeSeminar.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    bulkDeleteEmployeeSeminar = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);

        const tableName = "EmployeeSeminars";
        await FileStorage.destroy({
            where: {
               id_reference: deleteIds,
               tablename: tableName,
            }
        });

        const data = await EmployeeSeminar.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    // jabatan pegawai
    getEmployeePositions = async (req, res) => {
        let files = [];
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, idEmployee } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            order: [['id', 'ASC']],
            include: [Employee, Position, Echelon],
        };
        if (!idEmployee) {
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        } else {
            Object.assign(options, {
                where: {
                    id_employee: idEmployee,
                },
            });
            const resFiles = await FileStorage.findAll({ 
                where: { 
                    tablename: 'EmployeePositions',
                } 
            });
            files = JSON.parse(JSON.stringify(resFiles));
        }
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        Position.hasMany(EmployeePosition, { foreignKey: 'id_position', sourceKey: 'id' });
        EmployeePosition.belongsTo(Position, { foreignKey: 'id_position', sourceKey: 'id' });
        
        Echelon.hasMany(EmployeePosition, { foreignKey: 'id_echelon', sourceKey: 'id' });
        EmployeePosition.belongsTo(Echelon, { foreignKey: 'id_echelon', sourceKey: 'id' });
        
        Employee.hasMany(EmployeePosition, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeePosition.belongsTo(Employee, { foreignKey: 'id_employee', sourceKey: 'id' });
        
        const results = await EmployeePosition.findAll(options);
        const calculateAmountOfData = await EmployeePosition.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            const parsedResults = JSON.parse(JSON.stringify(results)).map((x) => {
                let file = null;
                const foundFile = files.find(f => String(f.id_reference) === String(x.id));
                
                if (foundFile) {
                    file = foundFile.filename;
                }

                const newData = Object.assign({}, x, {
                    employee_name: x.Employee.fullname,
                    position_name: x.Position.name,
                    echelon_name: x.Echelon.name,
                    file,
                });
                
                delete newData.Position;
                delete newData.Employee;
                delete newData.Echelon;
                
                return newData;
            });

            api = {
                "status": true,
                "data": parsedResults,
                "msg": 'success',
            }
            if (!idEmployee) {
                Object.assign(api, {
                    "page_total": Math.ceil(amountOfData / limit),
                });
            }
        }

        res.json(api);
    }

    getEmployeePosition = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const options = {
            where: {
                id
            },
            include: [{
                model: Employee,
            }],
        };
        Employee.hasMany(EmployeePosition, { foreignKey: 'id_employee', sourceKey: 'id' });
        EmployeePosition.belongsTo(Employee,  { foreignKey: 'id_employee', sourceKey: 'id' });
        const result = await EmployeePosition.findOne(options);
        
        if (result) {
                const resFile = await FileStorage.findOne({ 
                    where: { 
                        id_reference: id,
                        tablename: 'EmployeePositions',
                    } 
                });
                
                api = {
                    "status": true,
                    "data": Object.assign({}, JSON.parse(JSON.stringify(result)), { file: resFile }),
                    "msg": 'success',
                }
        }

        res.json(api);
    }

    createEmployeePosition = async (req, res) => {
        let api = null;
        
        try {
            const data = req.body;

            Object.assign(data, {
                is_active: true,
            });

            const result = await EmployeePosition.create(data);
            if (data.file && data.file.fileValue) {
                const dir = `public/uploads/${data.id_employee}`;

                if (!fs.existsSync(dir)){
                    fs.mkdirSync(dir);
                }

                const oldPath = data.file.fileValue;
                const newPath = `${dir}/${data.file.fileName}`;
                fs.renameSync(oldPath, newPath);

                await FileStorage.create({
                    tablename: "EmployeePositions",
                    filename: JSON.stringify({ filepath: newPath, filename: data.file.fileName, filetype: data.file.fileType }),
                    id_reference: result.id,
                });
            }

            if (result) {
                api = {
                    "status": true,
                    "data": result,
                    "msg": 'success',
                }
            }
        } catch (e) {
            api = {
                "status": false,
                "msg": e,
            }
        }

        res.json(api);
    }

    updateEmployeePosition = async (req, res) => {
        const tableName = "EmployeePositions";
        let api = {
            "status": false,
            "msg": 'error',
        };
        const data = req.body;
        
        const result = await EmployeePosition.findById(data.id);
        const response = await result.update(data);

        await FileStorage.destroy({
            where: {
                id_reference: response.id,
                tablename: tableName,
            }
        });

        if (data.file && data.file.fileValue) {
            const dir = `public/uploads/${data.id_employee}`;

            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }

            const oldPath = data.file.fileValue;
            const newPath = `${dir}/${data.file.fileName}`;
            fs.renameSync(oldPath, newPath);

            await FileStorage.create({
                tablename: tableName,
                filename: JSON.stringify({ filepath: newPath, filename: data.file.fileName, filetype: data.file.fileType }),
                id_reference: result.id,
            });
        }
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteEmployeePosition = async (req, res) => {
        const t = await Db.transaction();

        try {
            const { id } = req.body;

            const data = await EmployeePosition.destroy({
                where: {
                    id
                }
            }, { transaction: t });

            await FileStorage.destroy({
                where: {
                    tablename: "EmployeePositions",
                    id_reference: id,
                }
            }, { transaction: t });

            await t.commit();
        
            const api = {
                "status": true,
                "data": data,
            };
        
            res.json(api);
        } catch (err) {
            await t.rollback();
            console.error(err);
            res.status(400).send({ status: false, message: err.message });
        }
    }

    bulkDeleteEmployeePosition = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);

        const tableName = "EmployeePositions";
        await FileStorage.destroy({
            where: {
               id_reference: deleteIds,
               tablename: tableName,
            }
        });

        const data = await EmployeePosition.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data,
        };
    
        res.json(api);
    }

    setActiveEmployeePosition = async (req, res) => {
        const { id, idEmployee } = req.body;
        let options = {
            where: {
                id_employee: idEmployee,
            },
        };
        await EmployeePosition.update({ is_active: false }, options);

        options = {
            where: {
                id,
            },
        };
        const result = await EmployeePosition.findOne(options);
        const response = await result.update({ is_active: true });

        const api = {
            "status": true,
            "data": response,
            "msg": 'success',
        }

        res.json(api);
    }

    getEchelons = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['id', 'ASC']],
        };
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        const results = await Echelon.findAll(options);
        const calculateAmountOfData = await Echelon.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            api = {
                "status": true,
                "data": results,
                "page_total": Math.ceil(amountOfData / limit),
                "msg": 'success',
            }
        }

        res.json(api);
    }

    getEchelon = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const result = await Echelon.findById(id);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createEchelon = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const payload = req.body;

        const result = await Echelon.create(payload);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    updateEchelon = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const payload = req.body;

        const result = await Echelon.findById(payload.id);

        delete payload.id;

        const response = await result.update(payload);
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteEchelon = async (req, res) => {
        const { id } = req.body;
        const data = await Echelon.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    bulkDeleteEchelon = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await Echelon.destroy({ where: { id: deleteIds }});
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    getFaceShapes = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['id', 'ASC']],
        };
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        const results = await FaceShape.findAll(options);
        const calculateAmountOfData = await FaceShape.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            api = {
                "status": true,
                "data": results,
                "page_total": Math.ceil(amountOfData / limit),
                "msg": 'success',
            }
        }

        res.json(api);
    }

    getFaceShape = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const result = await FaceShape.findById(id);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createFaceShape = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { name } = req.body;

        const result = await FaceShape.create({ name });
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    updateFaceShape = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id, name } = req.body;

        const result = await FaceShape.findById(id);
        const response = await result.update({ name });
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteFaceShape = async (req, res) => {
        const { id } = req.body;
        const data = await FaceShape.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    bulkDeleteFaceShape = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await FaceShape.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    getGroups = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['id', 'ASC']],
        };
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        const results = await Group.findAll(options);
        const calculateAmountOfData = await Group.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        const data = results.map(x => (Object.assign({}, JSON.parse(JSON.stringify(x)), { preview: `${x.roman_numeral}/${x.letter}` })));
        const payload = {
            total_record: amountOfData,
            total_page: Math.ceil(amountOfData / limit),
            page: Number.parseInt(page),
        };

        api = {
            "status": true,
            "data": data,
            "payload": payload,
            "msg": 'success',
        }

        res.json(api);
    }

    getGroup = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const result = await Group.findById(id);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createGroup = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { numeral, roman_numeral, letter, grade } = req.body;

        const result = await Group.create({ numeral, roman_numeral, letter, grade });
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    updateGroup = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id, numeral, roman_numeral, letter, grade } = req.body;

        const result = await Group.findById(id);
        const response = await result.update({ numeral, roman_numeral, letter, grade });
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteGroup = async (req, res) => {
        const { id } = req.body;
        const data = await Group.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    bulkDeleteGroup = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await Group.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    getHairTypes = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['id', 'ASC']],
        };
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        const results = await HairType.findAll(options);
        const calculateAmountOfData = await HairType.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            api = {
                "status": true,
                "data": results,
                "page_total": Math.ceil(amountOfData / limit),
                "msg": 'success',
            }
        }

        res.json(api);
    }

    getHairType = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const result = await HairType.findById(id);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createHairType = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { name } = req.body;

        const result = await HairType.create({ name });
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    updateHairType = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id, name } = req.body;

        const result = await HairType.findById(id);
        const response = await result.update({ name });
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteHairType = async (req, res) => {
        const { id } = req.body;
        const data = await HairType.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    bulkDeleteHairType = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await HairType.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    getMaritalStatuses = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['id', 'ASC']],
        };
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        const results = await MaritalStatus.findAll(options);
        const calculateAmountOfData = await MaritalStatus.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            api = {
                "status": true,
                "data": results,
                "page_total": Math.ceil(amountOfData / limit),
                "msg": 'success',
            }
        }

        res.json(api);
    }

    getMaritalStatus = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const result = await MaritalStatus.findById(id);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createMaritalStatus = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { name } = req.body;

        const result = await MaritalStatus.create({ name });
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    updateMaritalStatus = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id, name } = req.body;

        const result = await MaritalStatus.findById(id);
        const response = await result.update({ name });
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteMaritalStatus = async (req, res) => {
        const { id } = req.body;
        const data = await MaritalStatus.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    bulkDeleteMaritalStatus = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await MaritalStatus.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    getPositionTypes = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['id', 'ASC']],
        };
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        const results = await PositionType.findAll(options);
        const calculateAmountOfData = await PositionType.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            api = {
                "status": true,
                "data": results,
                "page_total": Math.ceil(amountOfData / limit),
                "msg": 'success',
            }
        }

        res.json(api);
    }

    getPositionType = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const result = await PositionType.findById(id);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createPositionType = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { name } = req.body;

        const result = await PositionType.create({ name });
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    updatePositionType = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id, name } = req.body;

        const result = await PositionType.findById(id);
        const response = await result.update({ name });
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deletePositionType = async (req, res) => {
        const { id } = req.body;
        const data = await PositionType.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    bulkDeletePositionType = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await PositionType.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    getPositions = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['id', 'ASC']],
        };
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        const results = await Position.findAll(options);
        const calculateAmountOfData = await Position.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            api = {
                "status": true,
                "data": results,
                "page_total": Math.ceil(amountOfData / limit),
                "msg": 'success',
            }
        }

        res.json(api);
    }

    getPromotionTypes = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['id', 'ASC']],
        };
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        const results = await PromotionType.findAll(options);
        const calculateAmountOfData = await PromotionType.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            api = {
                "status": true,
                "data": results,
                "page_total": Math.ceil(amountOfData / limit),
                "msg": 'success',
            }
        }

        res.json(api);
    }

    getPromotionType = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const result = await PromotionType.findById(id);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createPromotionType = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { name } = req.body;

        const result = await PromotionType.create({ name });
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    updatePromotionType = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id, name } = req.body;

        const result = await PromotionType.findById(id);
        const response = await result.update({ name });
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deletePromotionType = async (req, res) => {
        const { id } = req.body;
        const data = await PromotionType.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    bulkDeletePromotionType = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await PromotionType.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    getTrainingTypes = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['id', 'ASC']],
        };
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        const results = await TrainingType.findAll(options);
        const calculateAmountOfData = await TrainingType.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            api = {
                "status": true,
                "data": results,
                "page_total": Math.ceil(amountOfData / limit),
                "msg": 'success',
            }
        }

        res.json(api);
    }

    getTrainingType = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const result = await TrainingType.findById(id);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createTrainingType = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { name } = req.body;

        const result = await TrainingType.create({ name });
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    updateTrainingType = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id, name } = req.body;

        const result = await TrainingType.findById(id);
        const response = await result.update({ name });
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteTrainingType = async (req, res) => {
        const { id } = req.body;
        const data = await TrainingType.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    bulkDeleteTrainingType = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await TrainingType.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    getWorkUnits = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['id', 'ASC']],
        };
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        const results = await WorkUnit.findAll(options);
        const calculateAmountOfData = await WorkUnit.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            api = {
                "status": true,
                "data": results,
                "page_total": Math.ceil(amountOfData / limit),
                "msg": 'success',
            }
        }

        res.json(api);
    }

    getWorkUnit = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;
        const options = {
            where: {
                id,
            },
        };

        const result = await WorkUnit.findOne(options);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createWorkUnit = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { name } = req.body;

        const result = await WorkUnit.create({ name });
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    updateWorkUnit = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id, name } = req.body;
        
        const result = await WorkUnit.findById(id);
        const response = await result.update({ name });

        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteWorkUnit = async (req, res) => {
        const { id } = req.body;
        const data = await WorkUnit.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    bulkDeleteWorkUnit = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await WorkUnit.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    getAgencies = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, exceptId, orId } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['code', 'ASC']],
        };
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        if (exceptId) {
            const filter = {
                id: {
                    [Op.not]: exceptId,
                }
            }
            
            let { where } = options;
            if (where) {
                where = Object.assign(where, filter);
            } else {
                where = filter;
            }

            Object.assign(options, {
                where,
            });
            Object.assign(calculateOptions, {
                where,
            });
        }

        if (orId) {
            const orOptions = [{
                id: orId
            }];
            if (filterText) {
                orOptions.push({
                    name: {
                        [Op.regexp]: `(${filterText})`,
                    }
                })
            }
            const filter = {
                [Op.or]: orOptions
            }
            Object.assign(options, {
                where: filter,
            });
        }
        const results = await Agency.findAll(options);
        const calculateAmountOfData = await Agency.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            api = {
                "status": true,
                "data": results,
                "page_total": Math.ceil(amountOfData / limit),
                "msg": 'success',
            }
        }

        res.json(api);
    }

    getAgency = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const result = await Agency.findById(id);

        if (result) {
            const splitData = String(result.parent_id).split("#");
            const parsed = JSON.parse(JSON.stringify(result));
            const parentId = splitData[splitData.length - 1];
            const newData = Object.assign({}, parsed, { parent_id: (String(parentId) === "null") ? "" : parentId });
    
            api = {
                "status": true,
                "data": newData,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createAgency = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { code, name, parent_id } = req.body;

        const execStoredProcedure = await StoredProcedure.run('sp_create_agency', [
            `'${code}'`,
            `'${name}'`,
            parent_id,
            1
        ]);
    
        const result = execStoredProcedure.result;
        
        api = {
            "status": (String(result.response_status) === '1'),
            "msg": String(result.response_msg),
        }

        res.json(api);
    }

    updateAgency = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id, code, name, parent_id } = req.body;

        const execStoredProcedure = await StoredProcedure.run('sp_update_agency', [
            id,
            `'${code}'`,
            `'${name}'`,
            parent_id,
            1
        ]);
    
        const result = execStoredProcedure.result;
        
        api = {
            "status": (String(result.response_status) === '1'),
            "msg": String(result.response_msg),
        }

        res.json(api);
    }

    deleteAgency = async (req, res) => {
        const { id } = req.body;
        const data = await Agency.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    bulkDeleteAgency = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await Agency.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    getEducationLevels = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, } = req.query;

        let limit = 5,
        page = 1;

        if (_limit) {
            limit = _limit;
        }
    
        if (_page) {
            page = _page;
        }

        const options = {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['id', 'ASC']],
        };
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        const results = await EducationLevel.findAll(options);
        const calculateAmountOfData = await EducationLevel.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;
    
        if (results) {
            api = {
                "status": true,
                "data": results,
                "page_total": Math.ceil(amountOfData / limit),
                "msg": 'success',
            }
        }

        res.json(api);
    }

    getEducationLevel = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id } = req.params;

        const result = await EducationLevel.findById(id);
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    createEducationLevel = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { name } = req.body;

        const result = await EducationLevel.create({ name });
        
        if (result) {
            api = {
                "status": true,
                "data": result,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    updateEducationLevel = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { id, name } = req.body;

        const result = await EducationLevel.findById(id);
        const response = await result.update({ name });
        
        if (response) {
            api = {
                "status": true,
                "data": response,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    deleteEducationLevel = async (req, res) => {
        const { id } = req.body;
        const data = await EducationLevel.destroy({
            where: {
               id
            }
        });
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    bulkDeleteEducationLevel = async (req, res) => {
        const { ids } = req.body;
        const deleteIds = JSON.parse(ids);
        const data = await EducationLevel.destroy({ where: { id: deleteIds }})
    
        const api = {
            "status": true,
            "data": data
        };
    
        res.json(api);
    }

    getNominativeEmployeeReport = async (req, res) => {
        const execStoredProcedure = await StoredProcedure.run('sp_rpt_nominative_employee', [], true);
    
        const results = execStoredProcedure;
        
        const api = {
            "status": true,
            "data": results,
        };
    
        res.json(api);
    }

    getBezettingReport = async (req, res) => {
        const execStoredProcedure = await StoredProcedure.run('sp_rpt_bezetting', [], true);
    
        const results = execStoredProcedure;
        
        const api = {
            "status": true,
            "data": results,
        };
    
        res.json(api);
    }

    getGroupRecapitulation = async (req, res) => {
        const execStoredProcedure = await StoredProcedure.run('sp_rpt_recap_group', [], true);
    
        const results = execStoredProcedure;
        
        const api = {
            "status": true,
            "data": results,
        };
    
        res.json(api);
    }

    getPositionRecapitulation = async (req, res) => {
        const execStoredProcedure = await StoredProcedure.run('sp_rpt_recap_position', [], true);
    
        const results = execStoredProcedure;
        
        const api = {
            "status": true,
            "data": results,
        };
    
        res.json(api);
    }

    getWorkUnitRecapitulation = async (req, res) => {
        const execStoredProcedure = await StoredProcedure.run('sp_rpt_recap_work_unit', [], true);
    
        const results = execStoredProcedure;
        
        const api = {
            "status": true,
            "data": results,
        };
    
        res.json(api); 
    }

    getProvinces = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, filterText, orId } = req.query;

        let limit = 5;

        if (_limit) {
            limit = _limit;
        }

        const options = {
            limit: Number.parseInt(limit, 10),
            order: [['name', 'ASC']],
        };

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
        }

        if (orId) {
            const orOptions = [{
                id: orId
            }];
            if (filterText) {
                orOptions.push({
                    name: {
                        [Op.regexp]: `(${filterText})`,
                    }
                })
            }
            const filter = {
                [Op.or]: orOptions
            }
            Object.assign(options, {
                where: filter,
            });
        }
        const results = await Province.findAll(options);
    
        if (results) {
            api = {
                "status": true,
                "data": results,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    getCities = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, filterText, idProvince, orId } = req.query;

        let limit = 5;

        if (_limit) {
            limit = _limit;
        }

        const options = {
            limit: Number.parseInt(limit, 10),
            order: [['name', 'ASC']],
        };

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                id_province: idProvince,
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
        }

        if (orId) {
            const orOptions = [{
                id: orId
            }];
            if (filterText) {
                orOptions.push({
                    name: {
                        [Op.regexp]: `(${filterText})`,
                    }
                })
            }
            const filter = {
                [Op.or]: orOptions
            }
            Object.assign(options, {
                where: filter,
            });
        }

        const results = await City.findAll(options);
    
        if (results) {
            api = {
                "status": true,
                "data": results,
                "msg": 'success',
            }
        }

        res.json(api);
    }

}

const api = new Api();

export default api;