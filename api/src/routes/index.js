import express from 'express';
import Api from '../controllers/api';
import multer from "multer";
import fs from "fs";
import mime from "mime";
import Util from '../utils';

const createFilename = () => (Math.random().toString(36).substring(2, 5) + Math.random().toString(36).substring(2, 5));
const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    const dir = `public/uploads/temp`;

    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }
    cb(null, `${dir}/`);
  },
  filename: function(req, file, cb) {
    const fileExtension = mime.getExtension(file.mimetype);
    const filename = createFilename() + "." + fileExtension;
    
    cb(null, filename);
  }
});
const upload = multer({ storage: storage });

const router = express.Router();

router.get('/', Api.index);
router.post('/api/uploadFile', upload.single('photo'), Api.uploadFile);
router.post('/api/login', Api.login);
router.get('/api/appmenu', Api.appMenu);
router.get('/api/provinces', Api.getProvinces);
router.get('/api/cities', Api.getCities);
router.get('/api/employee_retire_date/:id', Api.getEmployeeRetireDate);
router.get('/api/employees', Api.getEmployees);
router.get('/api/employee/:id', Api.getEmployee);
router.post('/api/employee', Api.createEmployee);
router.put('/api/employee', Api.updateEmployee);
router.delete('/api/employee/bulk', Api.bulkDeleteEmployee);
router.delete('/api/employee', Api.deleteEmployee);

router.get('/api/religions', Api.getReligions);
router.get('/api/religion/:id', Api.getReligion);
router.post('/api/religion', Api.createReligion);
router.put('/api/religion', Api.updateReligion);
router.delete('/api/religion/bulk', Api.bulkDeleteReligion);
router.delete('/api/religion', Api.deleteReligion);
router.get('/api/echelons', Api.getEchelons);
router.get('/api/echelon/:id', Api.getEchelon);
router.post('/api/echelon', Api.createEchelon);
router.put('/api/echelon', Api.updateEchelon);
router.delete('/api/echelon/bulk', Api.bulkDeleteEchelon);
router.delete('/api/echelon', Api.deleteEchelon);
router.get('/api/face_shapes', Api.getFaceShapes);
router.get('/api/face_shape/:id', Api.getFaceShape);
router.post('/api/face_shape', Api.createFaceShape);
router.put('/api/face_shape', Api.updateFaceShape);
router.delete('/api/face_shape/bulk', Api.bulkDeleteFaceShape);
router.delete('/api/face_shape', Api.deleteFaceShape);
router.get('/api/groups', Api.getGroups);
router.get('/api/group/:id', Api.getGroup);
router.post('/api/group', Api.createGroup);
router.put('/api/group', Api.updateGroup);
router.delete('/api/group/bulk', Api.bulkDeleteGroup);
router.delete('/api/group', Api.deleteGroup);
router.get('/api/hair_types', Api.getHairTypes);
router.get('/api/hair_type/:id', Api.getHairType);
router.post('/api/hair_type', Api.createHairType);
router.put('/api/hair_type', Api.updateHairType);
router.delete('/api/hair_type/bulk', Api.bulkDeleteHairType);
router.delete('/api/hair_type', Api.deleteHairType);
router.get('/api/my-account', Api.getMyAccount);
router.get('/api/users', Api.getUsers);
router.get('/api/user/:id', Api.getUser);
router.post('/api/user', upload.single('photo'), Api.createUser);
router.put('/api/user', upload.single('photo'), Api.updateUser);
router.delete('/api/user/bulk', Api.bulkDeleteUser);
router.delete('/api/user', Api.deleteUser);
router.get('/api/marital_statuses', Api.getMaritalStatuses);
router.get('/api/marital_status/:id', Api.getMaritalStatus);
router.post('/api/marital_status', Api.createMaritalStatus);
router.put('/api/marital_status', Api.updateMaritalStatus);
router.delete('/api/marital_status/bulk', Api.bulkDeleteMaritalStatus);
router.delete('/api/marital_status', Api.deleteMaritalStatus);
router.get('/api/position_types', Api.getPositionTypes);
router.get('/api/position_type/:id', Api.getPositionType);
router.post('/api/position_type', Api.createPositionType);
router.put('/api/position_type', Api.updatePositionType);
router.delete('/api/position_type/bulk', Api.bulkDeletePositionType);
router.delete('/api/position_type', Api.deletePositionType);
router.get('/api/positions', Api.getPositions);
router.get('/api/promotion_types', Api.getPromotionTypes);
router.get('/api/promotion_type/:id', Api.getPromotionType);
router.post('/api/promotion_type', Api.createPromotionType);
router.put('/api/promotion_type', Api.updatePromotionType);
router.delete('/api/promotion_type/bulk', Api.bulkDeletePromotionType);
router.delete('/api/promotion_type', Api.deletePromotionType);
router.get('/api/training_types', Api.getTrainingTypes);
router.get('/api/training_type/:id', Api.getTrainingType);
router.post('/api/training_type', Api.createTrainingType);
router.put('/api/training_type', Api.updateTrainingType);
router.delete('/api/training_type/bulk', Api.bulkDeleteTrainingType);
router.delete('/api/training_type', Api.deleteTrainingType);

router.get('/api/employee_spouses', Api.getEmployeeSpouses);
router.get('/api/employee_spouse/:id', Api.getEmployeeSpouse);
router.post('/api/employee_spouse', Api.createEmployeeSpouse);
router.put('/api/employee_spouse', Api.updateEmployeeSpouse);
router.delete('/api/employee_spouse/bulk', Api.bulkDeleteEmployeeSpouse);
router.delete('/api/employee_spouse', Api.deleteEmployeeSpouse);

router.get('/api/employee_kids', Api.getEmployeeKids);
router.get('/api/employee_kid/:id', Api.getEmployeeKid);
router.post('/api/employee_kid', Api.createEmployeeKid);
router.put('/api/employee_kid', Api.updateEmployeeKid);
router.delete('/api/employee_kid/bulk', Api.bulkDeleteEmployeeKid);
router.delete('/api/employee_kid', Api.deleteEmployeeKid);

router.get('/api/employee_parents', Api.getEmployeeParents);
router.get('/api/employee_parent/:id', Api.getEmployeeParent);
router.post('/api/employee_parent', Api.createEmployeeParent);
router.put('/api/employee_parent', Api.updateEmployeeParent);
router.delete('/api/employee_parent/bulk', Api.bulkDeleteEmployeeParent);
router.delete('/api/employee_parent', Api.deleteEmployeeParent);

router.get('/api/employee_schools', Api.getEmployeeSchools);
router.get('/api/employee_school/:id', Api.getEmployeeSchool);
router.post('/api/employee_school', Api.createEmployeeSchool);
router.put('/api/employee_school', Api.updateEmployeeSchool);
router.delete('/api/employee_school/bulk', Api.bulkDeleteEmployeeSchool);
router.delete('/api/employee_school', Api.deleteEmployeeSchool);
router.post('/api/set_last_education_employee', Api.setLastEducationEmployee);

router.get('/api/employee_languages', Api.getEmployeeLanguages);
router.get('/api/employee_language/:id', Api.getEmployeeLanguage);
router.post('/api/employee_language', Api.createEmployeeLanguage);
router.put('/api/employee_language', Api.updateEmployeeLanguage);
router.delete('/api/employee_language/bulk', Api.bulkDeleteEmployeeLanguage);
router.delete('/api/employee_language', Api.deleteEmployeeLanguage);

router.get('/api/employee_positions', Api.getEmployeePositions);
router.get('/api/employee_position/:id', Api.getEmployeePosition);
router.post('/api/employee_position', Api.createEmployeePosition);
router.put('/api/employee_position', Api.updateEmployeePosition);
router.delete('/api/employee_position/bulk', Api.bulkDeleteEmployeePosition);
router.delete('/api/employee_position', Api.deleteEmployeePosition);
router.post('/api/set_active_employee_position', Api.setActiveEmployeePosition);

router.get('/api/employee_levels', Api.getEmployeeLevels);
router.get('/api/employee_level/:id', Api.getEmployeeLevel);
router.post('/api/employee_level', Api.createEmployeeLevel);
router.put('/api/employee_level', Api.updateEmployeeLevel);
router.delete('/api/employee_level/bulk', Api.bulkDeleteEmployeeLevel);
router.delete('/api/employee_level', Api.deleteEmployeeLevel);
router.post('/api/set_active_employee_level', Api.setActiveEmployeeLevel);

router.get('/api/employee_penalties', Api.getEmployeePenalties);
router.get('/api/employee_penalty/:id', Api.getEmployeePenalty);
router.post('/api/employee_penalty', Api.createEmployeePenalty);
router.put('/api/employee_penalty', Api.updateEmployeePenalty);
router.delete('/api/employee_penalty/bulk', Api.bulkDeleteEmployeePenalty);
router.delete('/api/employee_penalty', Api.deleteEmployeePenalty);

router.get('/api/employee_mutations', Api.getEmployeeMutations);
router.get('/api/employee_mutation/:id', Api.getEmployeeMutation);
router.post('/api/employee_mutation', Api.createEmployeeMutation);
router.put('/api/employee_mutation', Api.updateEmployeeMutation);
router.delete('/api/employee_mutation/bulk', Api.bulkDeleteEmployeeMutation);
router.delete('/api/employee_mutation', Api.deleteEmployeeMutation);

router.get('/api/skp_employees', Api.getSkpEmployees);
router.get('/api/skp_employee/:id', Api.getSkpEmployee);
router.post('/api/skp_employee', Api.createSkpEmployee);
router.put('/api/skp_employee', Api.updateSkpEmployee);
router.delete('/api/skp_employee/bulk', Api.bulkDeleteSkpEmployee);
router.delete('/api/skp_employee', Api.deleteSkpEmployee);

router.get('/api/penalty_types', Api.getPenaltyTypes);

router.get('/api/nominative_employee_report', Api.getNominativeEmployeeReport);
router.get('/api/bezetting_report', Api.getBezettingReport);
router.get('/api/group_recapitulation', Api.getGroupRecapitulation);
router.get('/api/position_recapitulation', Api.getPositionRecapitulation);
router.get('/api/work_unit_recapitulation', Api.getWorkUnitRecapitulation);

router.get('/api/employee_trainings', Api.getEmployeeTrainings);
router.get('/api/employee_training/:id', Api.getEmployeeTraining);
router.post('/api/employee_training', Api.createEmployeeTraining);
router.put('/api/employee_training', Api.updateEmployeeTraining);
router.delete('/api/employee_training/bulk', Api.bulkDeleteEmployeeTraining);
router.delete('/api/employee_training', Api.deleteEmployeeTraining);

router.get('/api/employee_honors', Api.getEmployeeHonors);
router.get('/api/employee_honor/:id', Api.getEmployeeHonor);
router.post('/api/employee_honor', Api.createEmployeeHonor);
router.put('/api/employee_honor', Api.updateEmployeeHonor);
router.delete('/api/employee_honor/bulk', Api.bulkDeleteEmployeeHonor);
router.delete('/api/employee_honor', Api.deleteEmployeeHonor);

router.get('/api/employee_assignments', Api.getEmployeeAssignments);
router.get('/api/employee_assignment/:id', Api.getEmployeeAssignment);
router.post('/api/employee_assignment', Api.createEmployeeAssignment);
router.put('/api/employee_assignment', Api.updateEmployeeAssignment);
router.delete('/api/employee_assignment/bulk', Api.bulkDeleteEmployeeAssignment);
router.delete('/api/employee_assignment', Api.deleteEmployeeAssignment);

router.get('/api/employee_seminars', Api.getEmployeeSeminars);
router.get('/api/employee_seminar/:id', Api.getEmployeeSeminar);
router.post('/api/employee_seminar', Api.createEmployeeSeminar);
router.put('/api/employee_seminar', Api.updateEmployeeSeminar);
router.delete('/api/employee_seminar/bulk', Api.bulkDeleteEmployeeSeminar);
router.delete('/api/employee_seminar', Api.deleteEmployeeSeminar);

router.get('/api/employee_leaves', Api.getEmployeeLeaves);
router.get('/api/employee_leave/:id', Api.getEmployeeLeave);
router.post('/api/employee_leave', Api.createEmployeeLeave);
router.put('/api/employee_leave', Api.updateEmployeeLeave);
router.delete('/api/employee_leave/bulk', Api.bulkDeleteEmployeeLeave);
router.delete('/api/employee_leave', Api.deleteEmployeeLeave);

router.get('/api/employee_job_trainings', Api.getEmployeeJobTrainings);
router.get('/api/employee_job_training/:id', Api.getEmployeeJobTraining);
router.post('/api/employee_job_training', Api.createEmployeeJobTraining);
router.put('/api/employee_job_training', Api.updateEmployeeJobTraining);
router.delete('/api/employee_job_training/bulk', Api.bulkDeleteEmployeeJobTraining);
router.delete('/api/employee_job_training', Api.deleteEmployeeJobTraining);

router.get('/api/employee_allowances', Api.getEmployeeAllowances);
router.get('/api/employee_allowance/:id', Api.getEmployeeAllowance);
router.post('/api/employee_allowance', Api.createEmployeeAllowance);
router.put('/api/employee_allowance', Api.updateEmployeeAllowance);
router.delete('/api/employee_allowance/bulk', Api.bulkDeleteEmployeeAllowance);
router.delete('/api/employee_allowance', Api.deleteEmployeeAllowance);

router.get('/api/work_units', Api.getWorkUnits);
router.get('/api/work_unit/:id', Api.getWorkUnit);
router.post('/api/work_unit', Api.createWorkUnit);
router.put('/api/work_unit', Api.updateWorkUnit);
router.delete('/api/work_unit/bulk', Api.bulkDeleteWorkUnit);
router.delete('/api/work_unit', Api.deleteWorkUnit);
router.get('/api/agencies', Api.getAgencies);
router.get('/api/agency/:id', Api.getAgency);
router.post('/api/agency', Api.createAgency);
router.put('/api/agency', Api.updateAgency);
router.delete('/api/agency/bulk', Api.bulkDeleteAgency);
router.delete('/api/agency', Api.deleteAgency);
router.get('/api/education_levels', Api.getEducationLevels);
router.get('/api/education_level/:id', Api.getEducationLevel);
router.post('/api/education_level', Api.createEducationLevel);
router.put('/api/education_level', Api.updateEducationLevel);
router.delete('/api/education_level/bulk', Api.bulkDeleteEducationLevel);
router.delete('/api/education_level', Api.deleteEducationLevel);

export default router;