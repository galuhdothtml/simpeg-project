import Sequelize from "sequelize";
import Db from "./db";

const run = (sp_name, values = [], returnArray = false) => {
    var _sql = 'CALL ' + sp_name;
    _sql += '(';

    if (values.length > 0) {
        for (var i = 0; i < values.length; i++) {
            if (i == (values.length - 1)) {
                _sql += values[i];
            } else {
                _sql += values[i] + ', ';
            }
        }
    }

    _sql += ');';

    return Db.query(_sql, { type: Sequelize.QueryTypes.SELECT }).then((res) => {
        if (returnArray) {
            const results = Object.keys(res[0]).map(x => res[0][x]);
            return results;
        }
        return { result: res[0]['0'] }
    });
}

const StoredProcedure = {
    run
}

export default StoredProcedure;