import db from "./db";
import { ModelName } from "../constants";

const createModel = (type) => {
    if(type === ModelName.PROVINCE) {
        return db.import("../models/province");
    } else if(type === ModelName.CITY) {
        return db.import("../models/city");
    } else if(type === ModelName.MENU) {
        return db.import("../models/menu");
    } else if(type === ModelName.USER) {
        return db.import("../models/user");
    } else if(type === ModelName.RELIGION) {
        return db.import("../models/religion");
    } else if(type === ModelName.AGENCY) {
        return db.import("../models/agency");
    } else if(type === ModelName.EMPLOYEE) {
        return db.import("../models/employee");
    } else if(type === ModelName.FILE_STORAGE) {
        return db.import("../models/filestorage");
    } else if(type === ModelName.ECHELON) {
        return db.import("../models/echelon");
    } else if(type === ModelName.EDUCATION_LEVEL) {
        return db.import("../models/educationlevel");
    } else if(type === ModelName.FACE_SHAPE) {
        return db.import("../models/faceshape");
    } else if(type === ModelName.GROUP) {
        return db.import("../models/group");
    } else if(type === ModelName.HAIR_TYPE) {
        return db.import("../models/hairtype");
    } else if(type === ModelName.MARITAL_STATUS) {
        return db.import("../models/maritalstatus");
    } else if(type === ModelName.POSITION_TYPE) {
        return db.import("../models/positiontype");
    } else if(type === ModelName.POSITION) {
        return db.import("../models/position");
    } else if(type === ModelName.PROMOTION_TYPE) {
        return db.import("../models/promotiontype");
    } else if(type === ModelName.TRAINING_TYPE) {
        return db.import("../models/trainingtype");
    } else if(type === ModelName.WORK_UNIT) {
        return db.import("../models/workunits");
    } else if(type === ModelName.EMPLOYEE_SPOUSE) {
        return db.import("../models/employeespouse");
    } else if(type === ModelName.EMPLOYEE_KID) {
        return db.import("../models/employeekid");
    } else if(type === ModelName.EMPLOYEE_PARENT) {
        return db.import("../models/employeeparent");
    } else if(type === ModelName.EMPLOYEE_LANGUAGE) {
        return db.import("../models/employeelanguage");
    } else if(type === ModelName.EMPLOYEE_SCHOOL) {
        return db.import("../models/employeeschool");
    } else if(type === ModelName.EMPLOYEE_POSITION) {
        return db.import("../models/employeeposition");
    } else if(type === ModelName.EMPLOYEE_LEVEL) {
        return db.import("../models/employeelevel");
    } else if(type === ModelName.EMPLOYEE_PENALTY) {
        return db.import("../models/employeepenalty");
    } else if(type === ModelName.PENALTY_TYPE) {
        return db.import("../models/penaltytype");
    } else if(type === ModelName.EMPLOYEE_TRAINING) {
        return db.import("../models/employeetraining");
    } else if(type === ModelName.EMPLOYEE_HONOR) {
        return db.import("../models/employeehonor");
    } else if(type === ModelName.EMPLOYEE_ASSIGNMENT) {
        return db.import("../models/employeeassignment");
    } else if(type === ModelName.EMPLOYEE_SEMINAR) {
        return db.import("../models/employeeseminar");
    } else if(type === ModelName.EMPLOYEE_LEAVE) {
        return db.import("../models/employeeleave");
    } else if(type === ModelName.EMPLOYEE_JOB_TRAINING) {
        return db.import("../models/employeejobtraining");
    } else if(type === ModelName.EMPLOYEE_ALLOWANCE) {
        return db.import("../models/employeeallowance");
    } else if(type === ModelName.EMPLOYEE_MUTATION) {
        return db.import("../models/employeemutation");
    } else if(type === ModelName.SKP_EMPLOYEE) {
        return db.import("../models/skpemployee");
    }

    return db;
}

const ModelFactory = {
    createModel
}

export default ModelFactory;