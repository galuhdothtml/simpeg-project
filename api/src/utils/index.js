import moment from 'moment';
import jwt from 'jsonwebtoken';
import { SECRET_KEY } from '../constants';

const formatModelDateTime = (val, DataTypes) => {
    return Object.assign({}, val, {
        createdAt: {
            type: DataTypes.DATE,
            get: function () {
                return moment.utc(this.getDataValue('createdAt')).format('YYYY-MM-DD HH:mm:ss')
            }
        },
        updatedAt: {
            type: DataTypes.DATE,
            get: function () {
                return moment.utc(this.getDataValue('updatedAt')).format('YYYY-MM-DD HH:mm:ss')
            }
        },
    })
}

const getTokenFromRequest = (req) => {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        return req.headers.authorization.split(' ')[1];
    }

    return '';
}

const decodeToken = (token) => {
    return jwt.verify(token, SECRET_KEY);
}

const Util = {
    formatModelDateTime,
    decodeToken,
    getTokenFromRequest,
}

export default Util;