'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('EmployeeAllowances', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_employee: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      allowance_number: {
        allowNull: false,
        type: Sequelize.STRING
      },
      allowance_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      allowance_type: {
        allowNull: false,
        type: Sequelize.STRING
      },
      start_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      marriage_certificate_from: {
        allowNull: false,
        type: Sequelize.STRING
      },
      marriage_certificate_number: {
        allowNull: false,
        type: Sequelize.STRING
      },
      marriage_certificate_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      birth_certificate_from: {
        allowNull: false,
        type: Sequelize.STRING
      },
      birth_certificate_number: {
        allowNull: false,
        type: Sequelize.STRING
      },
      birth_certificate_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      copies: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('EmployeeAllowances');
  }
};