'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('EmployeeLeaves', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_employee: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      leave_number: {
        allowNull: false,
        type: Sequelize.STRING
      },
      leave_letter_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      leave_type: {
        allowNull: false,
        type: Sequelize.STRING
      },
      start_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      end_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      length_of_leave: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      provisions: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      copies: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('EmployeeLeaves');
  }
};