'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Employees', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nip: {
        allowNull: false,
        type: Sequelize.STRING
      },
      fullname: {
        allowNull: false,
        type: Sequelize.STRING
      },
      birth_place: {
        allowNull: false,
        type: Sequelize.STRING
      },
      birth_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      gender: {
        allowNull: false,
        type: Sequelize.CHAR(2)
      },
      id_religion: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      blood_type: {
        type: Sequelize.CHAR(5)
      },
      id_marital_status: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      employee_status: {
        allowNull: false,
        type: Sequelize.CHAR(3)
      },
      id_work_unit: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      promotion_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      salary_increase_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      address: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      email: {
        type: Sequelize.STRING
      },
      phone: {
        allowNull: false,
        type: Sequelize.STRING
      },
      is_active: {
        allowNull: false,
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Employees');
  }
};