'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('EmployeeLevels', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_employee: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      id_group: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      level: {
        allowNull: false,
        type: Sequelize.STRING
      },
      level_type: {
        allowNull: false,
        type: Sequelize.STRING
      },
      tmt_level: {
        allowNull: false,
        type: Sequelize.DATE
      },
      sk_official_certifier: {
        allowNull: false,
        type: Sequelize.STRING
      },
      sk_number: {
        allowNull: false,
        type: Sequelize.STRING
      },
      sk_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      is_active: {
        allowNull: false,
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('EmployeeLevels');
  }
};