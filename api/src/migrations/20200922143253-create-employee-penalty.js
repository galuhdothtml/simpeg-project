'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('EmployeePenalties', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_employee: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      id_penalty_type: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      sk_official_certifier: {
        allowNull: false,
        type: Sequelize.STRING
      },
      sk_number: {
        allowNull: false,
        type: Sequelize.STRING
      },
      sk_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      penalty_restorer_official: {
        allowNull: false,
        type: Sequelize.STRING
      },
      restore_penalty_number: {
        allowNull: false,
        type: Sequelize.STRING
      },
      restore_penalty_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('EmployeePenalties');
  }
};