'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('EmployeePositions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_employee: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      id_position: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      id_echelon: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      sk_number: {
        allowNull: false,
        type: Sequelize.STRING
      },
      sk_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      tmt_position_from: {
        allowNull: false,
        type: Sequelize.DATE
      },
      tmt_position_to: {
        allowNull: false,
        type: Sequelize.DATE
      },
      is_active: {
        allowNull: false,
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('EmployeePositions');
  }
};