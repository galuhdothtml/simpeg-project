/* eslint prop-types: 0 */
import _ from "lodash";
import { API_BASE_URL } from "../constants";

const getToken = () => (localStorage.getItem("usertoken"));

const setToken = (_token) => {
  localStorage.setItem("usertoken", _token); // eslint-disable-line no-undef
};

const removeToken = () => {
  localStorage.removeItem("usertoken"); // eslint-disable-line no-undef
};

const getBasePath = () => {
  let currentLink = String(window.location.pathname).substring(1);

  if (currentLink.split("/").length > 0) {
    [currentLink] = currentLink.split("/");
  }

  return currentLink;
};

const findHeaderMenuIdByLink = (currentLink, menu) => {
  const exist = menu.find((x) => {
    const found = x.content.find((fc) => {
      if (fc.link && fc.link === currentLink) {
        return true;
      }

      if (fc.content) {
        return _.find(fc.content, ["link", currentLink]);
      }

      return false;
    });

    return found;
  });

  if (exist) {
    const { id } = exist;

    return String(id);
  }

  return "";
};

const findSideMenuChildrenId = (sideMenu, currentSideMenuId, currentLink) => {
  let found = _.find(sideMenu, ["id", currentSideMenuId]);

  if (found) {
    const { content } = found;

    if (content) {
      const sideMenuChildren = content;
      found = _.find(sideMenuChildren, ["link", currentLink]);

      if (found) {
        return found.id;
      }
    }
  }

  return "";
};

const findSideMenuIdByLink = (currentLink, rawMenu, headerMenuId) => {
  let found = _.find(rawMenu, ["id", Number(headerMenuId)]);

  const sideMenu = found ? found.content : [];
  found = sideMenu.find((x) => {
    if (x.content) {
      return _.find(x.content, ["link", currentLink]);
    }

    return x.link === currentLink;
  });
  let retval = {
    noContent: "",
    withContent: "",
  };

  if (found) {
    const { content } = found;
    if (content) {
      retval = {
        noContent: "",
        withContent: found.id,
      };
    } else {
      retval = {
        noContent: found.id,
        withContent: "",
      };
    }
  }

  return retval;
};

const createSideMenuData = (headerMenuId, rawMenu) => {
  const found = _.find(rawMenu, ["id", Number(headerMenuId)]);
  const sideMenu = found ? found.content : [];

  return sideMenu;
};

const createAjax = (url, extendParam) => ({
  headers: {
    Authorization: `Bearer ${getToken()}`,
  },
  url: `${API_BASE_URL}${url}`,
  dataType: "json",
  data: (params) => {
    const query = {
      filterText: params.term,
      page: 1,
      limit: 5,
    };

    if (extendParam) {
      Object.assign(query, extendParam);
    }

    return query;
  },
  processResults: res => ({
    results: res.data.map(x => ({ id: x.id, text: x.name })),
  }),
});

const createPathPreview = (rawPath) => {
  if (rawPath) {
    const path = rawPath.replace("public/", `${API_BASE_URL}/`);

    return path;
  }

  return rawPath;
};

const parseSkpScores = (data) => {
  let total = 0;
  let avg = 0;
  let quality = "Kurang Baik";

  Object.keys(data).forEach((k) => {
    total += parseFloat(data[k], 10);
  });

  if (total > 0) {
    avg = parseFloat(total / Object.keys(data).length).toFixed(2);
  }

  if (avg >= 85) {
    quality = "Sangat Baik";
  } else if (avg >= 70) {
    quality = "Baik";
  } else if (avg >= 60) {
    quality = "Cukup Baik";
  }

  return { total, avg, quality };
};

const catchError = (e) => {
  let message = "Unknown error";

  if (typeof e === "string") message = e;
  if (Object.prototype.hasOwnProperty.call(e, "message")) ({ message } = e);
  if (Object.prototype.hasOwnProperty.call(e, "error")) ({ error: message } = e);
  return message;
};

const Util = {
  getToken,
  setToken,
  removeToken,
  getBasePath,
  findHeaderMenuIdByLink,
  findSideMenuChildrenId,
  findSideMenuIdByLink,
  createSideMenuData,
  createAjax,
  createPathPreview,
  parseSkpScores,
  catchError,
};

export default Util;
