import { ActionTypes } from "../constants";
import * as api from "../data";

const requestMyAccount = () => ({
  type: ActionTypes.REQUEST_MY_ACCOUNT,
});

const receiveMyAccount = data => ({
  type: ActionTypes.RECEIVE_MY_ACCOUNT,
  data,
});

// eslint-disable-next-line import/prefer-default-export
export const fetchMyAccount = () => (dispatch) => {
  dispatch(requestMyAccount());

  const promise = new Promise(((resolve, reject) => {
    api.getMyAccount().then((res) => {
      if (res.status) {
        dispatch(receiveMyAccount(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestAppMenu = () => ({
  type: ActionTypes.REQUEST_APP_MENU,
});

const receiveAppMenu = data => ({
  type: ActionTypes.RECEIVE_APP_MENU,
  data,
});

// eslint-disable-next-line import/prefer-default-export
export const fetchAppMenu = () => (dispatch) => {
  dispatch(requestAppMenu());

  const promise = new Promise(((resolve, reject) => {
    api.getAppMenu().then((res) => {
      if (res.status) {
        dispatch(receiveAppMenu(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestEmployeeDetail = () => ({
  type: ActionTypes.REQUEST_EMPLOYEE_DETAIL,
});

const receiveEmployeeDetail = data => ({
  type: ActionTypes.RECEIVE_EMPLOYEE_DETAIL,
  data,
});

// eslint-disable-next-line import/prefer-default-export
export const fetchEmployeeDetail = id => (dispatch) => {
  dispatch(requestEmployeeDetail());

  const promise = new Promise(((resolve, reject) => {
    api.getEmployee(id).then((res) => {
      if (res.status) {
        dispatch(receiveEmployeeDetail(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestEmployeeSpouse = () => ({
  type: ActionTypes.REQUEST_EMPLOYEE_SPOUSE,
});

const receiveEmployeeSpouse = data => ({
  type: ActionTypes.RECEIVE_EMPLOYEE_SPOUSE,
  data,
});

// eslint-disable-next-line import/prefer-default-export
export const fetchEmployeeSpouse = payload => (dispatch) => {
  dispatch(requestEmployeeSpouse());

  const promise = new Promise(((resolve, reject) => {
    api.getEmployeeSpouses(payload).then((res) => {
      if (res.status) {
        dispatch(receiveEmployeeSpouse(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestEmployeeKid = () => ({
  type: ActionTypes.REQUEST_EMPLOYEE_KID,
});

const receiveEmployeeKid = data => ({
  type: ActionTypes.RECEIVE_EMPLOYEE_KID,
  data,
});

// eslint-disable-next-line import/prefer-default-export
export const fetchEmployeeKid = payload => (dispatch) => {
  dispatch(requestEmployeeKid());

  const promise = new Promise(((resolve, reject) => {
    api.getEmployeeKids(payload).then((res) => {
      if (res.status) {
        dispatch(receiveEmployeeKid(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestEmployeeParent = () => ({
  type: ActionTypes.REQUEST_EMPLOYEE_PARENT,
});

const receiveEmployeeParent = data => ({
  type: ActionTypes.RECEIVE_EMPLOYEE_PARENT,
  data,
});

// eslint-disable-next-line import/prefer-default-export
export const fetchEmployeeParent = payload => (dispatch) => {
  dispatch(requestEmployeeParent());

  const promise = new Promise(((resolve, reject) => {
    api.getEmployeeParents(payload).then((res) => {
      if (res.status) {
        dispatch(receiveEmployeeParent(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestEmployeeSchool = () => ({
  type: ActionTypes.REQUEST_EMPLOYEE_SCHOOL,
});

const receiveEmployeeSchool = data => ({
  type: ActionTypes.RECEIVE_EMPLOYEE_SCHOOL,
  data,
});

// eslint-disable-next-line import/prefer-default-export
export const fetchEmployeeSchool = payload => (dispatch) => {
  dispatch(requestEmployeeSchool());

  const promise = new Promise(((resolve, reject) => {
    api.getEmployeeSchools(payload).then((res) => {
      if (res.status) {
        dispatch(receiveEmployeeSchool(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestEmployeeLanguage = () => ({
  type: ActionTypes.REQUEST_EMPLOYEE_LANGUAGE,
});

const receiveEmployeeLanguage = data => ({
  type: ActionTypes.RECEIVE_EMPLOYEE_LANGUAGE,
  data,
});

// eslint-disable-next-line import/prefer-default-export
export const fetchEmployeeLanguage = payload => (dispatch) => {
  dispatch(requestEmployeeLanguage());

  const promise = new Promise(((resolve, reject) => {
    api.getEmployeeLanguages(payload).then((res) => {
      if (res.status) {
        dispatch(receiveEmployeeLanguage(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestEmployeePositions = () => ({
  type: ActionTypes.REQUEST_EMPLOYEE_POSITIONS,
});

const receiveEmployeePositions = data => ({
  type: ActionTypes.RECEIVE_EMPLOYEE_POSITIONS,
  data,
});

// eslint-disable-next-line import/prefer-default-export
export const fetchEmployeePositions = payload => (dispatch) => {
  dispatch(requestEmployeePositions());

  const promise = new Promise(((resolve, reject) => {
    api.getEmployeePositions(payload).then((res) => {
      if (res.status) {
        dispatch(receiveEmployeePositions(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestEmployeeRetireDate = () => ({
  type: ActionTypes.REQUEST_EMPLOYEE_RETIRE_DATE,
});

const receiveEmployeeRetireDate = data => ({
  type: ActionTypes.RECEIVE_EMPLOYEE_RETIRE_DATE,
  data,
});

export const fetchEmployeeRetireDate = id => (dispatch) => {
  dispatch(requestEmployeeRetireDate());

  const promise = new Promise(((resolve, reject) => {
    api.getEmployeeRetireDate(id).then((res) => {
      if (res.status) {
        dispatch(receiveEmployeeRetireDate(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestEmployeeLevels = () => ({
  type: ActionTypes.REQUEST_EMPLOYEE_LEVELS,
});

const receiveEmployeeLevels = data => ({
  type: ActionTypes.RECEIVE_EMPLOYEE_LEVELS,
  data,
});

export const fetchEmployeeLevels = payload => (dispatch) => {
  dispatch(requestEmployeeLevels());

  const promise = new Promise(((resolve, reject) => {
    api.getEmployeeLevels(payload).then((res) => {
      if (res.status) {
        dispatch(receiveEmployeeLevels(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestEmployeePenalties = () => ({
  type: ActionTypes.REQUEST_EMPLOYEE_PENALTIES,
});

const receiveEmployeePenalties = data => ({
  type: ActionTypes.RECEIVE_EMPLOYEE_PENALTIES,
  data,
});

export const fetchEmployeePenalties = payload => (dispatch) => {
  dispatch(requestEmployeePenalties());

  const promise = new Promise(((resolve, reject) => {
    api.getEmployeePenalties(payload).then((res) => {
      if (res.status) {
        dispatch(receiveEmployeePenalties(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestEmployeeTrainings = () => ({
  type: ActionTypes.REQUEST_EMPLOYEE_TRAININGS,
});

const receiveEmployeeTrainings = data => ({
  type: ActionTypes.RECEIVE_EMPLOYEE_TRAININGS,
  data,
});

export const fetchEmployeeTrainings = payload => (dispatch) => {
  dispatch(requestEmployeeTrainings());

  const promise = new Promise(((resolve, reject) => {
    api.getEmployeeTrainings(payload).then((res) => {
      if (res.status) {
        dispatch(receiveEmployeeTrainings(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestEmployeeHonors = () => ({
  type: ActionTypes.REQUEST_EMPLOYEE_HONORS,
});

const receiveEmployeeHonors = data => ({
  type: ActionTypes.RECEIVE_EMPLOYEE_HONORS,
  data,
});

export const fetchEmployeeHonors = payload => (dispatch) => {
  dispatch(requestEmployeeHonors());

  const promise = new Promise(((resolve, reject) => {
    api.getEmployeeHonors(payload).then((res) => {
      if (res.status) {
        dispatch(receiveEmployeeHonors(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestEmployeeAssignments = () => ({
  type: ActionTypes.REQUEST_EMPLOYEE_ASSIGNMENTS,
});

const receiveEmployeeAssignments = data => ({
  type: ActionTypes.RECEIVE_EMPLOYEE_ASSIGNMENTS,
  data,
});

export const fetchEmployeeAssignments = payload => (dispatch) => {
  dispatch(requestEmployeeAssignments());

  const promise = new Promise(((resolve, reject) => {
    api.getEmployeeAssignments(payload).then((res) => {
      if (res.status) {
        dispatch(receiveEmployeeAssignments(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestEmployeeSeminars = () => ({
  type: ActionTypes.REQUEST_EMPLOYEE_SEMINARS,
});

const receiveEmployeeSeminars = data => ({
  type: ActionTypes.RECEIVE_EMPLOYEE_SEMINARS,
  data,
});

export const fetchEmployeeSeminars = payload => (dispatch) => {
  dispatch(requestEmployeeSeminars());

  const promise = new Promise(((resolve, reject) => {
    api.getEmployeeSeminars(payload).then((res) => {
      if (res.status) {
        dispatch(receiveEmployeeSeminars(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestEmployeeLeaves = () => ({
  type: ActionTypes.REQUEST_EMPLOYEE_LEAVES,
});

const receiveEmployeeLeaves = data => ({
  type: ActionTypes.RECEIVE_EMPLOYEE_LEAVES,
  data,
});

export const fetchEmployeeLeaves = payload => (dispatch) => {
  dispatch(requestEmployeeLeaves());

  const promise = new Promise(((resolve, reject) => {
    api.getEmployeeLeaves(payload).then((res) => {
      if (res.status) {
        dispatch(receiveEmployeeLeaves(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestEmployeeJobTrainings = () => ({
  type: ActionTypes.REQUEST_EMPLOYEE_JOB_TRAININGS,
});

const receiveEmployeeJobTrainings = data => ({
  type: ActionTypes.RECEIVE_EMPLOYEE_JOB_TRAININGS,
  data,
});

export const fetchEmployeeJobTrainings = payload => (dispatch) => {
  dispatch(requestEmployeeJobTrainings());

  const promise = new Promise(((resolve, reject) => {
    api.getEmployeeJobTrainings(payload).then((res) => {
      if (res.status) {
        dispatch(receiveEmployeeJobTrainings(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestEmployeeAllowances = () => ({
  type: ActionTypes.REQUEST_EMPLOYEE_ALLOWANCES,
});

const receiveEmployeeAllowances = data => ({
  type: ActionTypes.RECEIVE_EMPLOYEE_ALLOWANCES,
  data,
});

export const fetchEmployeeAllowances = payload => (dispatch) => {
  dispatch(requestEmployeeAllowances());

  const promise = new Promise(((resolve, reject) => {
    api.getEmployeeAllowances(payload).then((res) => {
      if (res.status) {
        dispatch(receiveEmployeeAllowances(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestEmployeeMutations = () => ({
  type: ActionTypes.REQUEST_EMPLOYEE_MUTATIONS,
});

const receiveEmployeeMutations = data => ({
  type: ActionTypes.RECEIVE_EMPLOYEE_MUTATIONS,
  data,
});

export const fetchEmployeeMutations = payload => (dispatch) => {
  dispatch(requestEmployeeMutations());

  const promise = new Promise(((resolve, reject) => {
    api.getEmployeeMutations(payload).then((res) => {
      if (res.status) {
        dispatch(receiveEmployeeMutations(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};

const requestSkpEmployees = () => ({
  type: ActionTypes.REQUEST_SKP_EMPLOYEES,
});

const receiveSkpEmployees = data => ({
  type: ActionTypes.RECEIVE_SKP_EMPLOYEES,
  data,
});

export const fetchSkpEmployees = payload => (dispatch) => {
  dispatch(requestSkpEmployees());

  const promise = new Promise(((resolve, reject) => {
    api.getSkpEmployees(payload).then((res) => {
      if (res.status) {
        dispatch(receiveSkpEmployees(res.data));
        resolve(true);
      }
    }).catch((err) => {
      reject(err);
    });
  }));

  return promise;
};
