import { ActionTypes } from "../constants";

// eslint-disable-next-line import/prefer-default-export
export const setActiveHeaderId = value => ({
  type: ActionTypes.SET_ACTIVE_HEADER_ID,
  value,
});
