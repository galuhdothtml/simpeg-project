/* eslint prop-types: 0 */
import request from "superagent";
import { API_BASE_URL } from "../constants";
import Util from "../utils";

const fetch = (url, param = null, method = "GET", withFile = null) => new Promise((resolve, reject) => {
  let req = null;

  if (method === "GET") {
    req = request
      .get(`${API_BASE_URL}${url}`);
  } else if (method === "POST") {
    req = request
      .post(`${API_BASE_URL}${url}`);
  } else if (method === "PUT") {
    req = request
      .put(`${API_BASE_URL}${url}`);
  } else if (method === "DELETE") {
    req = request
      .delete(`${API_BASE_URL}${url}`);
  }

  if (Util.getToken() && Util.getToken().trim().length > 0) {
    req = req.set("authorization", `Bearer ${Util.getToken()}`);
  }

  if (method === "GET") {
    req = req.query(param);
  } else if (withFile) {
    req = req.attach(Object.keys(withFile)[0], Object.values(withFile)[0]);
    if (param) {
      req = req.field({ data: JSON.stringify(param) });
    }
  } else {
    req = req.send(param);
  }

  req.end((err, res) => {
    if (err) {
      let errMsg = res.statusText;
      const responseBody = res.body;

      if (responseBody) {
        if (typeof responseBody === "string") {
          errMsg = responseBody;
        }

        if (typeof responseBody === "object") {
          if (Object.hasOwnProperty.call(responseBody, "msg")) {
            errMsg = responseBody.msg;
          }
          if (Object.hasOwnProperty.call(responseBody, "message")) {
            errMsg = responseBody.message;
          }
        }
      }

      reject(errMsg);
    }

    if (Object.prototype.hasOwnProperty.call(res.body, "token")) {
      Util.setToken(res.body.token);
    }

    resolve(res.body);
  });
});

const ApiClient = {
  fetch,
};

export default ApiClient;
