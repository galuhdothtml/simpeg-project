/* eslint prop-types: 0 */
import ApiClient from "./api_client";

export const getAppMenu = payload => ApiClient.fetch("/api/appmenu", payload);
export const getProvinces = payload => ApiClient.fetch("/api/provinces", payload);
export const getCities = () => ApiClient.fetch("/api/cities");
export const getPenaltyTypes = () => ApiClient.fetch("/api/penalty_types");
export const login = payload => ApiClient.fetch("/api/login", payload, "POST");
export const uploadFile = (withFile, groupDir = null) => ApiClient.fetch("/api/uploadFile", groupDir ? { groupDir } : null, "POST", withFile);
export const getEmployeeRetireDate = id => ApiClient.fetch(`/api/employee_retire_date/${id}`);
export const getEmployees = payload => ApiClient.fetch("/api/employees", payload);
export const getEmployee = id => ApiClient.fetch(`/api/employee/${id}`);
export const createEmployee = payload => ApiClient.fetch("/api/employee", payload, "POST");
export const updateEmployee = payload => ApiClient.fetch("/api/employee", payload, "PUT");
export const deleteEmployee = payload => ApiClient.fetch("/api/employee", payload, "DELETE");
export const bulkDeleteEmployee = payload => ApiClient.fetch("/api/employee/bulk", payload, "DELETE");

export const getReligions = payload => ApiClient.fetch("/api/religions", payload);
export const getReligion = id => ApiClient.fetch(`/api/religion/${id}`);
export const createReligion = payload => ApiClient.fetch("/api/religion", payload, "POST");
export const updateReligion = payload => ApiClient.fetch("/api/religion", payload, "PUT");
export const deleteReligion = payload => ApiClient.fetch("/api/religion", payload, "DELETE");
export const bulkDeleteReligion = payload => ApiClient.fetch("/api/religion/bulk", payload, "DELETE");

export const getEmployeeSpouses = payload => ApiClient.fetch("/api/employee_spouses", payload);
export const getEmployeeSpouse = id => ApiClient.fetch(`/api/employee_spouse/${id}`);
export const createEmployeeSpouse = payload => ApiClient.fetch("/api/employee_spouse", payload, "POST");
export const updateEmployeeSpouse = payload => ApiClient.fetch("/api/employee_spouse", payload, "PUT");
export const deleteEmployeeSpouse = payload => ApiClient.fetch("/api/employee_spouse", payload, "DELETE");
export const bulkDeleteEmployeeSpouse = payload => ApiClient.fetch("/api/employee_spouse/bulk", payload, "DELETE");

export const getEmployeeKids = payload => ApiClient.fetch("/api/employee_kids", payload);
export const getEmployeeKid = id => ApiClient.fetch(`/api/employee_kid/${id}`);
export const createEmployeeKid = payload => ApiClient.fetch("/api/employee_kid", payload, "POST");
export const updateEmployeeKid = payload => ApiClient.fetch("/api/employee_kid", payload, "PUT");
export const deleteEmployeeKid = payload => ApiClient.fetch("/api/employee_kid", payload, "DELETE");
export const bulkDeleteEmployeeKid = payload => ApiClient.fetch("/api/employee_kid/bulk", payload, "DELETE");

export const getEmployeeParents = payload => ApiClient.fetch("/api/employee_parents", payload);
export const getEmployeeParent = id => ApiClient.fetch(`/api/employee_parent/${id}`);
export const createEmployeeParent = payload => ApiClient.fetch("/api/employee_parent", payload, "POST");
export const updateEmployeeParent = payload => ApiClient.fetch("/api/employee_parent", payload, "PUT");
export const deleteEmployeeParent = payload => ApiClient.fetch("/api/employee_parent", payload, "DELETE");
export const bulkDeleteEmployeeParent = payload => ApiClient.fetch("/api/employee_parent/bulk", payload, "DELETE");

export const getEmployeeSchools = payload => ApiClient.fetch("/api/employee_schools", payload);
export const getEmployeeSchool = id => ApiClient.fetch(`/api/employee_school/${id}`);
export const createEmployeeSchool = payload => ApiClient.fetch("/api/employee_school", payload, "POST");
export const updateEmployeeSchool = payload => ApiClient.fetch("/api/employee_school", payload, "PUT");
export const deleteEmployeeSchool = payload => ApiClient.fetch("/api/employee_school", payload, "DELETE");
export const bulkDeleteEmployeeSchool = payload => ApiClient.fetch("/api/employee_school/bulk", payload, "DELETE");
export const setLastEducationEmployee = payload => ApiClient.fetch("/api/set_last_education_employee", payload, "POST");

export const getEmployeeLanguages = payload => ApiClient.fetch("/api/employee_languages", payload);
export const getEmployeeLanguage = id => ApiClient.fetch(`/api/employee_language/${id}`);
export const createEmployeeLanguage = payload => ApiClient.fetch("/api/employee_language", payload, "POST");
export const updateEmployeeLanguage = payload => ApiClient.fetch("/api/employee_language", payload, "PUT");
export const deleteEmployeeLanguage = payload => ApiClient.fetch("/api/employee_language", payload, "DELETE");
export const bulkDeleteEmployeeLanguage = payload => ApiClient.fetch("/api/employee_language/bulk", payload, "DELETE");

export const getEmployeePositions = payload => ApiClient.fetch("/api/employee_positions", payload);
export const getEmployeePosition = id => ApiClient.fetch(`/api/employee_position/${id}`);
export const createEmployeePosition = payload => ApiClient.fetch("/api/employee_position", payload, "POST");
export const updateEmployeePosition = payload => ApiClient.fetch("/api/employee_position", payload, "PUT");
export const deleteEmployeePosition = payload => ApiClient.fetch("/api/employee_position", payload, "DELETE");
export const bulkDeleteEmployeePosition = payload => ApiClient.fetch("/api/employee_position/bulk", payload, "DELETE");
export const setActiveEmployeePosition = payload => ApiClient.fetch("/api/set_active_employee_position", payload, "POST");

export const getEmployeeLevels = payload => ApiClient.fetch("/api/employee_levels", payload);
export const getEmployeeLevel = id => ApiClient.fetch(`/api/employee_level/${id}`);
export const createEmployeeLevel = payload => ApiClient.fetch("/api/employee_level", payload, "POST");
export const updateEmployeeLevel = payload => ApiClient.fetch("/api/employee_level", payload, "PUT");
export const deleteEmployeeLevel = payload => ApiClient.fetch("/api/employee_level", payload, "DELETE");
export const bulkDeleteEmployeeLevel = payload => ApiClient.fetch("/api/employee_level/bulk", payload, "DELETE");
export const setActiveEmployeeLevel = payload => ApiClient.fetch("/api/set_active_employee_level", payload, "POST");

export const getEmployeePenalties = payload => ApiClient.fetch("/api/employee_penalties", payload);
export const getEmployeePenalty = id => ApiClient.fetch(`/api/employee_penalty/${id}`);
export const createEmployeePenalty = payload => ApiClient.fetch("/api/employee_penalty", payload, "POST");
export const updateEmployeePenalty = payload => ApiClient.fetch("/api/employee_penalty", payload, "PUT");
export const deleteEmployeePenalty = payload => ApiClient.fetch("/api/employee_penalty", payload, "DELETE");
export const bulkDeleteEmployeePenalty = payload => ApiClient.fetch("/api/employee_penalty/bulk", payload, "DELETE");

export const getEmployeeMutations = payload => ApiClient.fetch("/api/employee_mutations", payload);
export const getEmployeeMutation = id => ApiClient.fetch(`/api/employee_mutation/${id}`);
export const createEmployeeMutation = payload => ApiClient.fetch("/api/employee_mutation", payload, "POST");
export const updateEmployeeMutation = payload => ApiClient.fetch("/api/employee_mutation", payload, "PUT");
export const deleteEmployeeMutation = payload => ApiClient.fetch("/api/employee_mutation", payload, "DELETE");
export const bulkDeleteEmployeeMutation = payload => ApiClient.fetch("/api/employee_mutation/bulk", payload, "DELETE");

export const getEmployeeHonors = payload => ApiClient.fetch("/api/employee_honors", payload);
export const getEmployeeHonor = id => ApiClient.fetch(`/api/employee_honor/${id}`);
export const createEmployeeHonor = payload => ApiClient.fetch("/api/employee_honor", payload, "POST");
export const updateEmployeeHonor = payload => ApiClient.fetch("/api/employee_honor", payload, "PUT");
export const deleteEmployeeHonor = payload => ApiClient.fetch("/api/employee_honor", payload, "DELETE");
export const bulkDeleteEmployeeHonor = payload => ApiClient.fetch("/api/employee_honor/bulk", payload, "DELETE");

export const getEmployeeAssignments = payload => ApiClient.fetch("/api/employee_assignments", payload);
export const getEmployeeAssignment = id => ApiClient.fetch(`/api/employee_assignment/${id}`);
export const createEmployeeAssignment = payload => ApiClient.fetch("/api/employee_assignment", payload, "POST");
export const updateEmployeeAssignment = payload => ApiClient.fetch("/api/employee_assignment", payload, "PUT");
export const deleteEmployeeAssignment = payload => ApiClient.fetch("/api/employee_assignment", payload, "DELETE");
export const bulkDeleteEmployeeAssignment = payload => ApiClient.fetch("/api/employee_assignment/bulk", payload, "DELETE");

export const getEmployeeTrainings = payload => ApiClient.fetch("/api/employee_trainings", payload);
export const getEmployeeTraining = id => ApiClient.fetch(`/api/employee_training/${id}`);
export const createEmployeeTraining = payload => ApiClient.fetch("/api/employee_training", payload, "POST");
export const updateEmployeeTraining = payload => ApiClient.fetch("/api/employee_training", payload, "PUT");
export const deleteEmployeeTraining = payload => ApiClient.fetch("/api/employee_training", payload, "DELETE");
export const bulkDeleteEmployeeTraining = payload => ApiClient.fetch("/api/employee_training/bulk", payload, "DELETE");

export const getEmployeeSeminars = payload => ApiClient.fetch("/api/employee_seminars", payload);
export const getEmployeeSeminar = id => ApiClient.fetch(`/api/employee_seminar/${id}`);
export const createEmployeeSeminar = payload => ApiClient.fetch("/api/employee_seminar", payload, "POST");
export const updateEmployeeSeminar = payload => ApiClient.fetch("/api/employee_seminar", payload, "PUT");
export const deleteEmployeeSeminar = payload => ApiClient.fetch("/api/employee_seminar", payload, "DELETE");
export const bulkDeleteEmployeeSeminar = payload => ApiClient.fetch("/api/employee_seminar/bulk", payload, "DELETE");

export const getEmployeeLeaves = payload => ApiClient.fetch("/api/employee_leaves", payload);
export const getEmployeeLeave = id => ApiClient.fetch(`/api/employee_leave/${id}`);
export const createEmployeeLeave = payload => ApiClient.fetch("/api/employee_leave", payload, "POST");
export const updateEmployeeLeave = payload => ApiClient.fetch("/api/employee_leave", payload, "PUT");
export const deleteEmployeeLeave = payload => ApiClient.fetch("/api/employee_leave", payload, "DELETE");
export const bulkDeleteEmployeeLeave = payload => ApiClient.fetch("/api/employee_leave/bulk", payload, "DELETE");

export const getEmployeeJobTrainings = payload => ApiClient.fetch("/api/employee_job_trainings", payload);
export const getEmployeeJobTraining = id => ApiClient.fetch(`/api/employee_job_training/${id}`);
export const createEmployeeJobTraining = payload => ApiClient.fetch("/api/employee_job_training", payload, "POST");
export const updateEmployeeJobTraining = payload => ApiClient.fetch("/api/employee_job_training", payload, "PUT");
export const deleteEmployeeJobTraining = payload => ApiClient.fetch("/api/employee_job_training", payload, "DELETE");
export const bulkDeleteEmployeeJobTraining = payload => ApiClient.fetch("/api/employee_job_training/bulk", payload, "DELETE");

export const getEmployeeAllowances = payload => ApiClient.fetch("/api/employee_allowances", payload);
export const getEmployeeAllowance = id => ApiClient.fetch(`/api/employee_allowance/${id}`);
export const createEmployeeAllowance = payload => ApiClient.fetch("/api/employee_allowance", payload, "POST");
export const updateEmployeeAllowance = payload => ApiClient.fetch("/api/employee_allowance", payload, "PUT");
export const deleteEmployeeAllowance = payload => ApiClient.fetch("/api/employee_allowance", payload, "DELETE");
export const bulkDeleteEmployeeAllowance = payload => ApiClient.fetch("/api/employee_allowance/bulk", payload, "DELETE");

export const getSkpEmployees = payload => ApiClient.fetch("/api/skp_employees", payload);
export const getSkpEmployee = id => ApiClient.fetch(`/api/skp_employee/${id}`);
export const createSkpEmployee = payload => ApiClient.fetch("/api/skp_employee", payload, "POST");
export const updateSkpEmployee = payload => ApiClient.fetch("/api/skp_employee", payload, "PUT");
export const deleteSkpEmployee = payload => ApiClient.fetch("/api/skp_employee", payload, "DELETE");
export const bulkDeleteSkpEmployee = payload => ApiClient.fetch("/api/skp_employee/bulk", payload, "DELETE");

export const getFaceShapes = payload => ApiClient.fetch("/api/face_shapes", payload);
export const getFaceShape = id => ApiClient.fetch(`/api/face_shape/${id}`);
export const createFaceShape = payload => ApiClient.fetch("/api/face_shape", payload, "POST");
export const updateFaceShape = payload => ApiClient.fetch("/api/face_shape", payload, "PUT");
export const deleteFaceShape = payload => ApiClient.fetch("/api/face_shape", payload, "DELETE");
export const bulkDeleteFaceShape = payload => ApiClient.fetch("/api/face_shape/bulk", payload, "DELETE");
export const getTrainingTypes = payload => ApiClient.fetch("/api/training_types", payload);
export const getTrainingType = id => ApiClient.fetch(`/api/training_type/${id}`);
export const createTrainingType = payload => ApiClient.fetch("/api/training_type", payload, "POST");
export const updateTrainingType = payload => ApiClient.fetch("/api/training_type", payload, "PUT");
export const deleteTrainingType = payload => ApiClient.fetch("/api/training_type", payload, "DELETE");
export const bulkDeleteTrainingType = payload => ApiClient.fetch("/api/training_type/bulk", payload, "DELETE");
export const getEducationLevels = payload => ApiClient.fetch("/api/education_levels", payload);
export const getEducationLevel = id => ApiClient.fetch(`/api/education_level/${id}`);
export const createEducationLevel = payload => ApiClient.fetch("/api/education_level", payload, "POST");
export const updateEducationLevel = payload => ApiClient.fetch("/api/education_level", payload, "PUT");
export const deleteEducationLevel = payload => ApiClient.fetch("/api/education_level", payload, "DELETE");
export const bulkDeleteEducationLevel = payload => ApiClient.fetch("/api/education_level/bulk", payload, "DELETE");
export const getHairTypes = payload => ApiClient.fetch("/api/hair_types", payload);
export const getHairType = id => ApiClient.fetch(`/api/hair_type/${id}`);
export const createHairType = payload => ApiClient.fetch("/api/hair_type", payload, "POST");
export const updateHairType = payload => ApiClient.fetch("/api/hair_type", payload, "PUT");
export const deleteHairType = payload => ApiClient.fetch("/api/hair_type", payload, "DELETE");
export const bulkDeleteHairType = payload => ApiClient.fetch("/api/hair_type/bulk", payload, "DELETE");
export const getMaritalStatuses = payload => ApiClient.fetch("/api/marital_statuses", payload);
export const getMaritalStatus = id => ApiClient.fetch(`/api/marital_status/${id}`);
export const createMaritalStatus = payload => ApiClient.fetch("/api/marital_status", payload, "POST");
export const updateMaritalStatus = payload => ApiClient.fetch("/api/marital_status", payload, "PUT");
export const deleteMaritalStatus = payload => ApiClient.fetch("/api/marital_status", payload, "DELETE");
export const bulkDeleteMaritalStatus = payload => ApiClient.fetch("/api/marital_status/bulk", payload, "DELETE");
export const getAgencies = payload => ApiClient.fetch("/api/agencies", payload);
export const getAgency = id => ApiClient.fetch(`/api/agency/${id}`);
export const createAgency = payload => ApiClient.fetch("/api/agency", payload, "POST");
export const updateAgency = payload => ApiClient.fetch("/api/agency", payload, "PUT");
export const deleteAgency = payload => ApiClient.fetch("/api/agency", payload, "DELETE");
export const bulkDeleteAgency = payload => ApiClient.fetch("/api/agency/bulk", payload, "DELETE");
export const getMyAccount = () => ApiClient.fetch("/api/my-account");
export const getUsers = payload => ApiClient.fetch("/api/users", payload);
export const getUser = id => ApiClient.fetch(`/api/user/${id}`);
export const createUser = payload => ApiClient.fetch("/api/user", payload, "POST");
export const updateUser = payload => ApiClient.fetch("/api/user", payload, "PUT");
export const deleteUser = payload => ApiClient.fetch("/api/user", payload, "DELETE");
export const bulkDeleteUser = payload => ApiClient.fetch("/api/user/bulk", payload, "DELETE");
export const getWorkUnits = payload => ApiClient.fetch("/api/work_units", payload);
export const getWorkUnit = id => ApiClient.fetch(`/api/work_unit/${id}`);
export const createWorkUnit = payload => ApiClient.fetch("/api/work_unit", payload, "POST");
export const updateWorkUnit = payload => ApiClient.fetch("/api/work_unit", payload, "PUT");
export const deleteWorkUnit = payload => ApiClient.fetch("/api/work_unit", payload, "DELETE");
export const bulkDeleteWorkUnit = payload => ApiClient.fetch("/api/work_unit/bulk", payload, "DELETE");
export const getEchelons = payload => ApiClient.fetch("/api/echelons", payload);
export const getEchelon = id => ApiClient.fetch(`/api/echelon/${id}`);
export const createEchelon = payload => ApiClient.fetch("/api/echelon", payload, "POST");
export const updateEchelon = payload => ApiClient.fetch("/api/echelon", payload, "PUT");
export const deleteEchelon = payload => ApiClient.fetch("/api/echelon", payload, "DELETE");
export const bulkDeleteEchelon = payload => ApiClient.fetch("/api/echelon/bulk", payload, "DELETE");
export const getPositionTypes = payload => ApiClient.fetch("/api/position_types", payload);
export const getPositionType = id => ApiClient.fetch(`/api/position_type/${id}`);
export const createPositionType = payload => ApiClient.fetch("/api/position_type", payload, "POST");
export const updatePositionType = payload => ApiClient.fetch("/api/position_type", payload, "PUT");
export const deletePositionType = payload => ApiClient.fetch("/api/position_type", payload, "DELETE");
export const bulkDeletePositionType = payload => ApiClient.fetch("/api/position_type/bulk", payload, "DELETE");
export const getPositions = payload => ApiClient.fetch("/api/positions", payload);
export const getPromotionTypes = payload => ApiClient.fetch("/api/promotion_types", payload);
export const getPromotionType = id => ApiClient.fetch(`/api/promotion_type/${id}`);
export const createPromotionType = payload => ApiClient.fetch("/api/promotion_type", payload, "POST");
export const updatePromotionType = payload => ApiClient.fetch("/api/promotion_type", payload, "PUT");
export const deletePromotionType = payload => ApiClient.fetch("/api/promotion_type", payload, "DELETE");
export const bulkDeletePromotionType = payload => ApiClient.fetch("/api/promotion_type/bulk", payload, "DELETE");
export const getGroups = payload => ApiClient.fetch("/api/groups", payload);
export const getGroup = id => ApiClient.fetch(`/api/group/${id}`);
export const createGroup = payload => ApiClient.fetch("/api/group", payload, "POST");
export const updateGroup = payload => ApiClient.fetch("/api/group", payload, "PUT");
export const deleteGroup = payload => ApiClient.fetch("/api/group", payload, "DELETE");
export const bulkDeleteGroup = payload => ApiClient.fetch("/api/group/bulk", payload, "DELETE");

export const getEmployeeNominativeReport = () => ApiClient.fetch("/api/nominative_employee_report");
export const getBezettingReport = () => ApiClient.fetch("/api/bezetting_report");
export const getGroupRecapitulation = () => ApiClient.fetch("/api/group_recapitulation");
export const getPositionRecapitulation = () => ApiClient.fetch("/api/position_recapitulation");
export const getWorkUnitRecapitulation = () => ApiClient.fetch("/api/work_unit_recapitulation");
