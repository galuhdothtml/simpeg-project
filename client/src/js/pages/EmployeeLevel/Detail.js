import React from "react";
import update from "immutability-helper";
import PropTypes from "prop-types";
import { FieldFeedbacks, FieldFeedback } from "react-form-with-constraints";
import FormValidation from "~/components/form/FormValidation";
import InputText from "~/components/form/InputText";
import Select from "~/components/form/Select";
import DatePicker from "~/components/form/DatePicker";
import FileUpload from "~/components/form/FileUpload";
import BrowseData from "~/components/form/BrowseData";
import CoreHoC from "../CoreHoC";
import {
  getEmployees, createEmployeeLevel, updateEmployeeLevel, getEmployeeLevel, getGroups,
} from "../../data";

const employeeColumns = [
  {
    accessor: "fullname",
    Header: "Nama Pegawai",
    width: "100%",
  },
];
const groupColumns = [
  {
    accessor: "name",
    Header: "Nama Golongan",
    width: "100%",
  },
];
class EmployeeSchoolDetail extends React.Component {
  static propTypes = {
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    assignButtons: () => { },
    showNotification: () => { },
  }

  initialButtonActions = [
    {
      id: "1",
      type: null,
      content: (
        <span>
          Batal
        </span>
      ),
      action: () => this.gotoBasePath(),
      isDisabled: false,
    },
    {
      id: "2",
      type: "primary",
      content: (
        <span>
          Simpan
        </span>
      ),
      action: () => this.saveDataHandler(),
      isDisabled: false,
    },
  ];

  constructor(props) {
    super(props);

    this.state = {
      id: "",
      isFormSubmitted: false,
      type: "create",
      form: {
        employee: {
          id: "",
          name: "",
        },
        group: {
          id: "",
          name: "",
        },
        level: "",
        level_type: "",
        sk_official_certifier: "",
        sk_number: "",
        tmt_level: moment().format("YYYY-MM-DD"),
        sk_date: moment().format("YYYY-MM-DD"),
      },
      groupDir: {
        dirName: "",
        required: true,
        requiredMsg: "Pilih pegawai terlebih dahulu",
      },
      fileValue: null,
      footerButtons: this.initialButtonActions,
    };
  }

  componentWillMount = () => {
    this.setupData();
  }

  setupData = async () => {
    const { match: { params } } = this.props;

    if (params.type === "edit") {
      await this.setupDetailData(params.id);
    } else {
      this.setupBreadcrumbs("Tambah Data");
    }
  }

  setupBreadcrumbs = (text) => {
    const { assignBreadcrumbs } = this.props;

    assignBreadcrumbs([
      {
        label: "App", link: "#",
      },
      {
        label: "Pegawai", link: "#",
      },
      {
        label: "Kepegawaian", link: "#",
      },
      {
        label: "Pangkat", link: "/kepegawaian-pangkat",
      },
      text,
    ]);
  }

  setupDetailData = async (id) => {
    const res = await getEmployeeLevel(id);

    if (res.status) {
      const { data } = res;
      let fileValue = null;

      if (data.file) {
        const parsedFile = JSON.parse(data.file.filename);
        fileValue = {
          fileValue: parsedFile.filepath,
          fileType: parsedFile.filetype,
          fileName: parsedFile.filename,
        };
      }

      const newState = {
        id: data.id,
        type: "edit",
        form: {
          employee: {
            id: data.Employee.id,
            name: data.Employee.fullname,
          },
          group: {
            id: data.Group.id,
            name: data.Group.name,
          },
          level: data.level,
          level_type: data.level_type,
          sk_official_certifier: data.sk_official_certifier,
          sk_number: data.sk_number,
          tmt_level: moment(data.tmt_level).format("YYYY-MM-DD"),
          sk_date: moment(data.sk_date).format("YYYY-MM-DD"),
        },
        groupDir: {
          dirName: String(data.Employee.id),
          required: true,
          requiredMsg: "Pilih pegawai terlebih dahulu",
        },
        fileValue,
      };

      this.setupBreadcrumbs(data.Employee.fullname);
      this.setState(newState);
    }
  }

  onFetchEmployee = async (state) => {
    const payload = {
      limit: 10,
      page: state.page,
      filterText: state.filterText,
      sorted: JSON.stringify({ id: state.fieldToSort, desc: state.sortType === "desc" }),
    };
    const res = await getEmployees(payload);

    return res;
  }

  onFetchGroup = async (state) => {
    const payload = {
      limit: 10,
      page: state.page,
      filterText: state.filterText,
      sorted: JSON.stringify({ id: state.fieldToSort, desc: state.sortType === "desc" }),
    };
    const res = await getGroups(payload);

    return res;
  }

  changeValueHandler = async (type, val, e) => {
    const { form } = this.state;

    const { isFormSubmitted } = this.state;

    if (isFormSubmitted && e) {
      await this.onInputChangeValidate(e);
    }

    const newValue = update(form, {
      [type]: { $set: val },
    });

    this.setState({ form: newValue });
  }

  onInputChangeValidate = ({ target }) => {
    this.form.validateInput(target);

    if (this.inputTimeout) {
      clearTimeout(this.inputTimeout);
    }

    this.inputTimeout = setTimeout(() => {
      if (this.form.simpleValidateForm()) {
        this.updateButtonsState();
      } else {
        this.updateButtonsState(false, true);
      }
    }, 300);
  }

  updateButtonsState = (isBtnCancelDisabled = false, isBtnSaveDisabled = false) => {
    const footerButtons = update(this.initialButtonActions, {
      0: { isDisabled: { $set: isBtnCancelDisabled } },
      1: { isDisabled: { $set: isBtnSaveDisabled } },
    });

    this.setState({ footerButtons });
  }

  changeFileValueHandler = (val) => {
    this.setState({ fileValue: val });
  }

  changeGroupDir = (val) => {
    const { groupDir } = this.state;
    const newValue = update(groupDir, {
      dirName: { $set: String(val) },
    });

    this.setState({ groupDir: newValue });
  }

  changeEmployeeHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      employee: { $set: { id: val.id, name: val.fullname } },
    });

    this.setState({ form: newValue }, () => {
      this.validateEmployee();
      this.changeGroupDir(val.id);
    });
  }

  validateEmployee = async () => {
    const { isFormSubmitted } = this.state;

    if (isFormSubmitted) {
      await this.onInputChangeValidate({ target: this.employeeHidden });
    }
  }

  changeGroupHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      group: { $set: { id: val.id, name: val.name } },
    });

    this.setState({ form: newValue }, () => {
      this.validateGroup();
    });
  }

  validateGroup = async () => {
    const { isFormSubmitted } = this.state;

    if (isFormSubmitted) {
      await this.onInputChangeValidate({ target: this.groupHidden });
    }
  }

  employeeConditionalRender = (type) => {
    const { form } = this.state;
    if (type === "edit") {
      return (
        <div>

        <div className="row mb-sm">
          <div className="col-sm-4">
            <div className="form-group position-relative mb-0">
              <BrowseData
                label="Golongan"
                placeholder="Pilih Golongan"
                columns={groupColumns}
                value={form.group}
                changeEvent={this.changeGroupHandler}
                onFetch={this.onFetchGroup}
              />
              <input
                ref={(c) => { this.groupHidden = c; }}
                className="hide-for-validation"
                name="groupHidden"
                type="text"
                value={form.group.id}
                onChange={() => { }}
                required
              />
            </div>
            <FieldFeedbacks for="groupHidden">
              <FieldFeedback when="valueMissing">Golongan wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <InputText
              label="Pangkat"
              changeEvent={(val, e) => this.changeValueHandler("level", val, e)}
              value={String(form.level)}
              name="level"
              required
            />
            <FieldFeedbacks for="level">
              <FieldFeedback when="valueMissing">Pangkat wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <InputText
              label="Jenis Pangkat"
              changeEvent={(val, e) => this.changeValueHandler("level_type", val, e)}
              value={String(form.level_type)}
              name="level_type"
              required
            />
            <FieldFeedbacks for="level">
              <FieldFeedback when="valueMissing">Pangkat wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>

      </div>
      );
    }

    return (
      <div>

        <div className="row mb-sm">
          <div className="col-sm-6">
            <div className="form-group position-relative mb-0">
              <BrowseData
                label="Pegawai"
                placeholder="Pilih Pegawai"
                columns={employeeColumns}
                value={form.employee}
                changeEvent={this.changeEmployeeHandler}
                onFetch={this.onFetchEmployee}
              />
              <input
                ref={(c) => { this.employeeHidden = c; }}
                className="hide-for-validation"
                name="employeeHidden"
                type="text"
                value={form.employee.id}
                onChange={() => { }}
                required
              />
            </div>
            <FieldFeedbacks for="employeeHidden">
              <FieldFeedback when="valueMissing">Pegawai wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-6">
            <InputText
              label="Pangkat"
              changeEvent={(val, e) => this.changeValueHandler("level", val, e)}
              value={String(form.level)}
              name="level"
              required
            />
            <FieldFeedbacks for="level">
              <FieldFeedback when="valueMissing">Pangkat wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>

        <div className="row mb-sm">
          <div className="col-sm-6">
            <div className="form-group position-relative mb-0">
              <BrowseData
                label="Golongan"
                placeholder="Pilih Golongan"
                columns={groupColumns}
                value={form.group}
                changeEvent={this.changeGroupHandler}
                onFetch={this.onFetchGroup}
              />
              <input
                ref={(c) => { this.groupHidden = c; }}
                className="hide-for-validation"
                name="groupHidden"
                type="text"
                value={form.group.id}
                onChange={() => { }}
                required
              />
            </div>
            <FieldFeedbacks for="groupHidden">
              <FieldFeedback when="valueMissing">Golongan wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-6">
            <InputText
              label="Jenis Pangkat"
              changeEvent={(val, e) => this.changeValueHandler("level_type", val, e)}
              value={String(form.level_type)}
              name="level_type"
              required
            />
            <FieldFeedbacks for="level">
              <FieldFeedback when="valueMissing">Pangkat wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>

      </div>
    );
  }

  formComponent = () => {
    const {
      form, type,
      fileValue, groupDir,
    } = this.state;
    return (
      <FormValidation ref={(c) => { this.form = c; }}>
        {this.employeeConditionalRender(type)}
        <div className="row mb-sm">
          <div className="col-sm-6">
            <DatePicker
              label="TMT Pangkat"
              id="tmt-level"
              changeEvent={val => this.changeValueHandler("tmt_level", val)}
              value={form.tmt_level}
            />
          </div>
          <div className="col-sm-6">
            <InputText
              label="Pejabat Pengesah SK"
              changeEvent={(val, e) => this.changeValueHandler("sk_official_certifier", val, e)}
              value={String(form.sk_official_certifier)}
              name="sk_official_certifier"
              required
            />
            <FieldFeedbacks for="sk_official_certifier">
              <FieldFeedback when="valueMissing">Pejabat pengesah SK wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
        <div className="row mb-sm">
          <div className="col-sm-3">
            <InputText
              label="Nomor SK"
              changeEvent={(val, e) => this.changeValueHandler("sk_number", val, e)}
              value={String(form.sk_number)}
              name="sk_number"
              required
            />
            <FieldFeedbacks for="sk_number">
              <FieldFeedback when="valueMissing">Nomor SK wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-3">
            <DatePicker
              label="Tanggal SK"
              id="sk-date"
              changeEvent={val => this.changeValueHandler("sk_date", val)}
              value={form.sk_date}
            />
          </div>
          <div className="col-sm-6">
            <div style={{ marginBottom: "5px" }}>( maximum file size : 5MB; format: .png, .jpg, .jpeg )</div>
            <FileUpload
              label="File SK Pangkat"
              changeEvent={this.changeFileValueHandler}
              value={fileValue}
              groupDir={groupDir}
            />
          </div>
        </div>
      </FormValidation>
    );
  }

  gotoBasePath = () => {
    const { history } = this.props;
    history.push("/kepegawaian-pangkat");
  }

  saveDataHandler = async () => {
    const {
      form, type, id,
      fileValue,
    } = this.state;
    this.updateButtonsState(true, true);

    const isFormValid = await this.form.validateForm();

    if (isFormValid) {
      const payload = Object.assign({}, form, {
        id_employee: form.employee.id,
        id_group: form.group.id,
        file: fileValue,
      });
      delete payload.employee;
      delete payload.group;

      if (type === "create") {
        const res = await createEmployeeLevel(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      } else {
        Object.assign(payload, { id });
        const res = await updateEmployeeLevel(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      }
    } else {
      this.updateButtonsState(false, true);
    }

    this.setState({
      isFormSubmitted: true,
    });
  }

  render = () => {
    const {
      footerButtons, type,
    } = this.state;

    return (
      <div style={{ width: "80%", position: "relative", margin: "0px auto" }}>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-12">
                <h3 className="box-title with-title">{type === "create" ? "Tambah" : "Edit"} Kepegawaian Pangkat</h3>
              </div>
            </div>
          </div>
          <div className="box-body">
            {this.formComponent()}
          </div>
          <div className="box-footer text-right">
            <div>
              {footerButtons.map((x, i) => (<button key={x.id} style={(i === footerButtons.length - 1) ? {} : { marginRight: "10px" }} type="button" className={`btn ${x.type ? `btn-${x.type}` : ""}`} onClick={!x.isDisabled ? x.action : () => { }} disabled={x.isDisabled}>{x.content}</button>))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CoreHoC(EmployeeSchoolDetail);
