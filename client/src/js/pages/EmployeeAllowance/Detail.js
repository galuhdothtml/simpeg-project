import React from "react";
import update from "immutability-helper";
import PropTypes from "prop-types";
import { FieldFeedbacks, FieldFeedback } from "react-form-with-constraints";
import FormValidation from "~/components/form/FormValidation";
import InputText from "~/components/form/InputText";
import Select from "~/components/form/Select";
import DatePicker from "~/components/form/DatePicker";
import BrowseData from "~/components/form/BrowseData";
import CoreHoC from "../CoreHoC";
import {
  getEmployees, createEmployeeAllowance, updateEmployeeAllowance, getEmployeeAllowance,
} from "../../data";
import Copies from "../EmployeeLeave/components/Copies";

const employeeColumns = [
  {
    accessor: "fullname",
    Header: "Nama Pegawai",
    width: "100%",
  },
];
const copiesInvalid = (val) => {
  const parsedValue = JSON.parse(val);

  if (parsedValue.length > 0) {
    return parsedValue.some(x => (String(x).length === 0));
  }

  return false;
};
class EmployeeAllowanceDetail extends React.Component {
  static propTypes = {
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    assignButtons: () => { },
    showNotification: () => { },
  }

  initialButtonActions = [
    {
      id: "1",
      type: null,
      content: (
        <span>
          Batal
        </span>
      ),
      action: () => this.gotoBasePath(),
      isDisabled: false,
    },
    {
      id: "2",
      type: "primary",
      content: (
        <span>
          Simpan
        </span>
      ),
      action: () => this.saveDataHandler(),
      isDisabled: false,
    },
  ];

  constructor(props) {
    super(props);

    this.state = {
      id: "",
      isFormSubmitted: false,
      type: "create",
      penaltyTypeList: [],
      form: {
        employee: {
          id: "",
          name: "",
        },
        allowance_number: "",
        allowance_date: moment().format("YYYY-MM-DD"),
        allowance_type: "",
        start_date: moment().format("YYYY-MM-DD"),
        marriage_certificate_from: "",
        marriage_certificate_number: "",
        marriage_certificate_date: moment().format("YYYY-MM-DD"),
        birth_certificate_from: "",
        birth_certificate_number: "",
        birth_certificate_date: moment().format("YYYY-MM-DD"),
        copies: [""],
      },
      footerButtons: this.initialButtonActions,
    };
  }

  componentWillMount = () => {
    this.setupData();
  }

  setupData = async () => {
    const { match: { params } } = this.props;

    if (params.type === "edit") {
      await this.setupDetailData(params.id);
    } else {
      this.setupBreadcrumbs("Tambah Data");
    }
  }

  setupBreadcrumbs = (text) => {
    const { assignBreadcrumbs } = this.props;

    assignBreadcrumbs([
      {
        label: "App", link: "#",
      },
      {
        label: "Pegawai", link: "#",
      },
      {
        label: "Kepegawaian", link: "#",
      },
      {
        label: "Tunjangan", link: "/kepegawaian-tunjangan",
      },
      text,
    ]);
  }

  setupDetailData = async (id) => {
    const res = await getEmployeeAllowance(id);

    if (res.status) {
      const { data } = res;

      const newState = {
        id: data.id,
        type: "edit",
        form: {
          employee: {
            id: data.Employee.id,
            name: data.Employee.fullname,
          },
          allowance_number: data.allowance_number,
          allowance_date: moment(data.allowance_date).format("YYYY-MM-DD"),
          allowance_type: data.allowance_type,
          start_date: moment(data.start_date).format("YYYY-MM-DD"),
          marriage_certificate_from: data.marriage_certificate_from,
          marriage_certificate_number: data.marriage_certificate_number,
          marriage_certificate_date: moment(data.marriage_certificate_date).format("YYYY-MM-DD"),
          birth_certificate_from: data.birth_certificate_from,
          birth_certificate_number: data.birth_certificate_number,
          birth_certificate_date: moment(data.birth_certificate_date).format("YYYY-MM-DD"),
          copies: JSON.parse(data.copies),
        },
      };

      this.setupBreadcrumbs(data.Employee.fullname);
      this.setState(newState);
    }
  }

  onFetchEmployee = async (state) => {
    const payload = {
      limit: 10,
      page: state.page,
      filterText: state.filterText,
      sorted: JSON.stringify({ id: state.fieldToSort, desc: state.sortType === "desc" }),
    };
    const res = await getEmployees(payload);

    return res;
  }

  changeValueHandler = async (type, val, e) => {
    const { form } = this.state;

    const { isFormSubmitted } = this.state;

    if (isFormSubmitted && e) {
      await this.onInputChangeValidate(e);
    }

    const newValue = update(form, {
      [type]: { $set: val },
    });

    this.setState({ form: newValue }, () => {
      if (type === "copies") {
        this.validateCopies();
      }
    });
  }

  onInputChangeValidate = ({ target }) => {
    this.form.validateInput(target);

    if (this.inputTimeout) {
      clearTimeout(this.inputTimeout);
    }

    this.inputTimeout = setTimeout(() => {
      if (this.form.simpleValidateForm()) {
        this.updateButtonsState();
      } else {
        this.updateButtonsState(false, true);
      }
    }, 300);
  }

  updateButtonsState = (isBtnCancelDisabled = false, isBtnSaveDisabled = false) => {
    const footerButtons = update(this.initialButtonActions, {
      0: { isDisabled: { $set: isBtnCancelDisabled } },
      1: { isDisabled: { $set: isBtnSaveDisabled } },
    });

    this.setState({ footerButtons });
  }

  changeEmployeeHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      employee: { $set: { id: val.id, name: val.fullname } },
    });

    this.setState({ form: newValue }, () => {
      this.validateEmployee();
    });
  }

  validateEmployee = async () => {
    const { isFormSubmitted } = this.state;

    if (isFormSubmitted) {
      await this.onInputChangeValidate({ target: this.employeeHidden });
    }
  }

  validateCopies = async () => {
    const { isFormSubmitted } = this.state;

    if (isFormSubmitted) {
      await this.onInputChangeValidate({ target: this.copiesHidden });
    }
  }

  employeeConditionalRender = (type) => {
    const { form } = this.state;

    if (type === "edit") {
      return (
        <div className="row mb-sm">
          <div className="col-sm-12">
            <InputText
              label="Jenis Tunjangan"
              changeEvent={(val, e) => this.changeValueHandler("allowance_type", val, e)}
              value={String(form.allowance_type)}
              name="allowance_type"
              required
            />
            <FieldFeedbacks for="allowance_type">
              <FieldFeedback when="valueMissing">Jenis Tunjangan wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
      );
    }

    return (
      <div className="row mb-sm">
        <div className="col-sm-6">
          <div className="form-group position-relative mb-0">
            <BrowseData
              label="Pegawai"
              placeholder="Pilih Pegawai"
              columns={employeeColumns}
              value={form.employee}
              changeEvent={this.changeEmployeeHandler}
              onFetch={this.onFetchEmployee}
            />
            <input
              ref={(c) => { this.employeeHidden = c; }}
              className="hide-for-validation"
              name="employeeHidden"
              type="text"
              value={form.employee.id}
              onChange={() => { }}
              required
            />
          </div>
          <FieldFeedbacks for="employeeHidden">
            <FieldFeedback when="valueMissing">Pegawai wajib diisi</FieldFeedback>
          </FieldFeedbacks>
        </div>
        <div className="col-sm-6">
          <InputText
            label="Jenis Tunjangan"
            changeEvent={(val, e) => this.changeValueHandler("allowance_type", val, e)}
            value={String(form.allowance_type)}
            name="allowance_type"
            required
          />
          <FieldFeedbacks for="allowance_type">
            <FieldFeedback when="valueMissing">Jenis Tunjangan wajib diisi</FieldFeedback>
          </FieldFeedbacks>
        </div>
      </div>
    );
  }

  formComponent = () => {
    const {
      form, type,
    } = this.state;
    return (
      <div>
        <div className="row mb-sm">
          <div className="col-sm-6">
            <InputText
              label="Nomor Tunjangan"
              changeEvent={(val, e) => this.changeValueHandler("allowance_number", val, e)}
              value={String(form.allowance_number)}
              name="allowance_number"
              required
            />
            <FieldFeedbacks for="allowance_number">
              <FieldFeedback when="valueMissing">Nomor Tunjangan wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-6">
            <DatePicker
              label="Tanggal Tunjangan"
              id="allowance-date"
              changeEvent={val => this.changeValueHandler("allowance_date", val)}
              value={form.allowance_date}
            />
          </div>
        </div>

        {this.employeeConditionalRender(type)}

        <div className="row mb-sm">
          <div className="col-sm-12">
            <DatePicker
              label="Terhitung Mulai"
              id="start-date"
              changeEvent={val => this.changeValueHandler("start_date", val)}
              value={form.start_date}
            />
          </div>
        </div>

        <div className="row mb-sm">
          <div className="col-sm-12">
            <InputText
              label="Akta Perkawinan Dari"
              changeEvent={(val, e) => this.changeValueHandler("marriage_certificate_from", val, e)}
              value={String(form.marriage_certificate_from)}
              name="marriage_certificate_from"
              required
            />
            <FieldFeedbacks for="marriage_certificate_from">
              <FieldFeedback when="valueMissing">Akta Perkawinan Dari wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>

        <div className="row mb-sm">
          <div className="col-sm-6">
            <InputText
              label="Nomor Akta Perkawinan"
              changeEvent={(val, e) => this.changeValueHandler("marriage_certificate_number", val, e)}
              value={String(form.marriage_certificate_number)}
              name="marriage_certificate_number"
              required
            />
            <FieldFeedbacks for="marriage_certificate_number">
              <FieldFeedback when="valueMissing">Nomor Akta Perkawinan wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-6">
            <DatePicker
              label="Tanggal Akta Perkawinan"
              id="marriage-certificate-date"
              changeEvent={val => this.changeValueHandler("marriage_certificate_date", val)}
              value={form.marriage_certificate_date}
            />
          </div>
        </div>

        <div className="row mb-sm">
          <div className="col-sm-12">
            <InputText
              label="Akta Kelahiran Dari"
              changeEvent={(val, e) => this.changeValueHandler("birth_certificate_from", val, e)}
              value={String(form.birth_certificate_from)}
              name="birth_certificate_from"
              required
            />
            <FieldFeedbacks for="birth_certificate_from">
              <FieldFeedback when="valueMissing">Akta Kelahiran Dari wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>

        <div className="row mb-sm">
          <div className="col-sm-6">
            <InputText
              label="Nomor Akta Kelahiran"
              changeEvent={(val, e) => this.changeValueHandler("birth_certificate_number", val, e)}
              value={String(form.birth_certificate_number)}
              name="birth_certificate_number"
              required
            />
            <FieldFeedbacks for="birth_certificate_number">
              <FieldFeedback when="valueMissing">Nomor Akta Kelahiran wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-6">
            <DatePicker
              label="Tanggal Akta Kelahiran"
              id="birth-certificate-date"
              changeEvent={val => this.changeValueHandler("birth_certificate_date", val)}
              value={form.birth_certificate_date}
            />
          </div>
        </div>

      </div>
    );
  }

  gotoBasePath = () => {
    const { history } = this.props;
    history.push("/kepegawaian-tunjangan");
  }

  saveDataHandler = async () => {
    const {
      form, type, id,
    } = this.state;

    this.updateButtonsState(true, true);

    const isFormValid = await this.form.validateForm();

    if (isFormValid) {
      const payload = Object.assign({}, form, {
        id_employee: form.employee.id,
        copies: JSON.stringify(form.copies),
      });
      delete payload.employee;

      if (type === "create") {
        const res = await createEmployeeAllowance(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      } else {
        Object.assign(payload, { id });
        const res = await updateEmployeeAllowance(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      }
    } else {
      this.updateButtonsState(false, true);
    }

    this.setState({
      isFormSubmitted: true,
    });
  }

  render = () => {
    const {
      footerButtons, type, form,
    } = this.state;

    return (
      <div style={{ width: "100%", position: "relative", margin: "0px auto" }}>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-12">
                <h3 className="box-title with-title">{type === "create" ? "Tambah" : "Edit"} Kepegawaian Tunjangan</h3>
              </div>
            </div>
          </div>
          <div className="box-body">
            <FormValidation ref={(c) => { this.form = c; }}>
              <div className="row">
                <div className="col-sm-7">
                  {this.formComponent()}
                </div>
                <div className="col-sm-5">
                  <div className="form-group position-relative mb-0">
                    <Copies
                      data={form.copies}
                      changeEvent={val => this.changeValueHandler("copies", val)}
                    />
                    <input
                      ref={(c) => { this.copiesHidden = c; }}
                      className="hide-for-validation"
                      name="copiesHidden"
                      type="text"
                      value={JSON.stringify(form.copies)}
                      onChange={() => { }}
                    />
                  </div>
                  <FieldFeedbacks for="copiesHidden">
                    <FieldFeedback when={val => copiesInvalid(val)}>Mohon lengkapi tembusan</FieldFeedback>
                  </FieldFeedbacks>
                </div>
              </div>
            </FormValidation>
          </div>
          <div className="box-footer text-right">
            <div>
              {footerButtons.map((x, i) => (<button key={x.id} style={(i === footerButtons.length - 1) ? {} : { marginRight: "10px" }} type="button" className={`btn ${x.type ? `btn-${x.type}` : ""}`} onClick={!x.isDisabled ? x.action : () => { }} disabled={x.isDisabled}>{x.content}</button>))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CoreHoC(EmployeeAllowanceDetail);
