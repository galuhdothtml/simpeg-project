import React from "react";
import { FieldFeedbacks, FieldFeedback } from "react-form-with-constraints";
import InputText from "~/components/form/InputText";

export default class SidebarForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = { ...props };
  }

  getFormData = () => this.state

  changeValueHandler = async (type, val, e) => {
    if (e) {
      const { validateInput } = this.props;
      const { target } = e;

      validateInput(target);
    }

    const payload = {
      [type]: val,
    };

    this.setState({
      ...payload,
    });
  }

  render() {
    const { name } = this.state;

    return (
      <React.Fragment>
        <div className="row">
          <div className="col-sm-12">
            <InputText
              name="name"
              label="Nama Unit Kerja"
              changeEvent={(val, e) => this.changeValueHandler("name", val, e)}
              value={name}
              required
            />
            <FieldFeedbacks for="name">
              <FieldFeedback when="valueMissing">* Nama wajib diisi.</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
