import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import RawTable from "~/components/form/Table";
import withCheckbox from "~/components/form/Table/withCheckbox";
import withEditDelete from "~/components/form/Table/withEditDelete";
import SidePopup from "~/components/form/SidePopup";
import InputText from "~/components/form/InputText";
import Select from "~/components/form/Select";
import CoreHoC from "../CoreHoC";
import {
  getWorkUnits,
  getWorkUnit,
  createWorkUnit,
  updateWorkUnit,
  deleteWorkUnit,
  bulkDeleteWorkUnit,
} from "../../data";
import SidebarForm from "./SidebarForm";

const Table = withCheckbox(withEditDelete(RawTable));
const columns = [
  {
    Header: "ID",
    accessor: "id",
    resizable: false,
    width: 60,
  },
  {
    Header: "Unit Kerja",
    accessor: "name",
    resizable: false,
  },
];
class WorkUnit extends React.Component {
  static propTypes = {
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    assignButtons: () => { },
    showNotification: () => { },
  }

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      type: "",
      form: {
        id: "",
        name: "",
      },
      deleteIds: [],
      filterText: "",
      limitValue: "5",
      limitData: [
        { id: "5", name: "5" },
        { id: "10", name: "10" },
        { id: "20", name: "20" },
        { id: "50", name: "50" },
        { id: "100", name: "100" },
      ],
    };
  }

  componentWillMount = () => {
    const { assignButtons, assignBreadcrumbs } = this.props;

    assignButtons([{
      id: "1", title: "Tambah Data", icon: "fa fa-plus-square", clickEvent: () => this.callCreateHandler(),
    }]);

    assignBreadcrumbs([
      {
        label: "App", link: "#",
      },
      {
        label: "Pengaturan", link: "#",
      },
      "Unit Kerja",
    ]);
  }

  editDeleteHandler = async (id, type) => {
    const { showNotification } = this.props;

    if (type === "edit") {
      const res = await getWorkUnit(id);
      if (res.status) {
        const { data } = res;

        this.setState({
          type: "edit",
          form: {
            id: data.id,
            name: data.name,
          },
        }, () => {
          this.sidePopup.showPopup();
        });
      }
    } else {
      const { deleteIds } = this.state;
      const res = await deleteWorkUnit({ id });
      if (res.status) {
        this.setState({ deleteIds: deleteIds.filter(x => (String(x) !== String(id))) });
        showNotification({
          type: "success",
          msg: "Berhasil menghapus data!",
        });
        this.table.refreshData();
      }
    }
  }

  callCreateHandler = () => {
    this.setState({
      type: "create",
      form: {
        id: "",
        name: "",
      },
    }, () => {
      this.sidePopup.showPopup();
    });
  }

  saveHandler = async () => {
    const { showNotification } = this.props;
    const { type } = this.state;
    const form = this.sideBarForm.getFormData();

    if (type === "create") {
      await createWorkUnit({ name: form.name });
      showNotification({
        type: "success",
        msg: "Berhasil menambah data!",
      });
    } else {
      await updateWorkUnit({ id: form.id, name: form.name });
      showNotification({
        type: "success",
        msg: "Berhasil mengubah data!",
      });
    }

    this.sidePopup.hidePopup();
    this.table.refreshData();
  }

  bulkDeleteHandler = async () => {
    const { showNotification } = this.props;

    try {
      const { deleteIds } = this.state;
      await bulkDeleteWorkUnit({ ids: JSON.stringify(deleteIds) });
      this.table.refreshData();
      this.setState({ deleteIds: [] });
      showNotification({
        type: "success",
        msg: "Berhasil menghapus data!",
      });
    } catch (err) {
      console.log(err);
    }
  }

  fetchData = async (state) => {
    const { filterText, limitValue } = this.state;
    const payload = {
      page: state.page + 1,
      limit: limitValue,
    };

    if (state.sorted) {
      Object.assign(payload, {
        sorted: JSON.stringify(state.sorted),
      });
    }

    if (filterText && filterText.length > 0) {
      Object.assign(payload, {
        filterText,
      });
    }

    const res = await getWorkUnits(payload);

    if (res.status) {
      const newData = res.data.map(x => (Object.assign({}, x, { updatedAt: moment(x.updatedAt).format("DD MMM YYYY HH:mm:ss") })));

      return { data: newData, pageTotal: res.page_total };
    }

    return { data: [], pageTotal: 0 };
  }

  checkedHandler = (values) => {
    this.setState({ deleteIds: values }, () => {
      this.table.refreshData();
    });
  }

  changeFilterTextHandler = (val) => {
    this.setState({ filterText: val });

    clearTimeout(this.filterIdle);
    this.filterIdle = setTimeout(() => {
      this.table.resetCurrentPage(() => {
        this.setState({ filterText: val }, () => {
          this.table.refreshData();
        });
      });
    }, 500);
  }

  changeLimitValueHandler = (val) => {
    this.table.resetCurrentPage(() => {
      this.setState({ limitValue: val }, () => {
        this.table.refreshData();
      });
    });
  }

  render = () => {
    const {
      type,
      form,
      deleteIds,
      filterText,
      limitData,
      limitValue,
    } = this.state;

    return (
      <div>
        <div className="box box-solid">
        <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-8">
                <h3 className="box-title with-title">Master Data Unit Kerja</h3>
              </div>
              <div className="col-sm-4">
                <div className="row">
                  <div className="col-sm-9">
                    <InputText
                      placeholder="Pencarian ..."
                      changeEvent={this.changeFilterTextHandler}
                      value={filterText}
                    />
                  </div>
                  <div className="col-sm-3 pl-0">
                    <Select
                      data={limitData}
                      value={limitValue}
                      changeEvent={this.changeLimitValueHandler}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="box-body">
            <Table
              ref={(c) => { this.table = c; }}
              columns={columns}
              onFetch={this.fetchData}
              checkedValues={deleteIds}
              checkedEvent={this.checkedHandler}
              showPagination={true}
              editDeleteEvent={this.editDeleteHandler}
            />
          </div>
        </div>
        <SidePopup
          ref={(c) => { this.sidePopup = c; }}
          title={type === "create" ? "Tambah Data" : "Edit Data"}
          onSave={this.saveHandler}
          render={({ show, validateInput }) => {
            if (show) {
              return (
                <SidebarForm
                  {...form}
                  ref={(c) => { this.sideBarForm = c; }}
                  validateInput={validateInput}
                />
              );
            }

            return null;
          }}
        />
        {(deleteIds.length > 0) && <div className="bulk-delete-wrapper">
          <div className="row">
            <div className="col-sm-6">
              <h5 className="text-muted"><b>{deleteIds.length} DATA AKAN DIHAPUS ?</b></h5>
            </div>
            <div className="col-sm-6 text-right">
              <button style={{ width: "100px" }} type="button" className="btn btn-danger btn-md btn-flat" onClick={this.bulkDeleteHandler}><i className="fa fa-trash-o" /> Hapus</button>
            </div>
          </div>
        </div>
        }
      </div>
    );
  }
}

export default CoreHoC(WorkUnit);
