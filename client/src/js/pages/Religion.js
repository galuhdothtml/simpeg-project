import React from "react";
import PropTypes from "prop-types";
import update from "immutability-helper";
import moment from "moment";
import RawTable from "~/components/form/Table";
import withEditDelete from "~/components/form/Table/withEditDelete";
import withCheckbox from "~/components/form/Table/withCheckbox";
import SidePopup from "~/components/form/SidePopup";
import InputText from "~/components/form/InputText";
import Select from "~/components/form/Select";
import CoreHoC from "./CoreHoC";
import {
  getReligions, getReligion, createReligion, updateReligion, deleteReligion, bulkDeleteReligion,
} from "../data";

const errorContent = (type, val) => {
  let retval = null;

  if (type === "name") {
    if (String(val).trim().length === 0) {
      retval = "* Agama wajib diisi";
    }
  }

  return retval;
};
const Table = withCheckbox(withEditDelete(RawTable));
const columns = [
  {
    Header: "Kode",
    accessor: "id",
    resizable: false,
    width: 60,
  },
  {
    Header: "Agama",
    accessor: "name",
  },
  {
    Header: "Update Terakhir",
    accessor: "updatedAt",
    width: 180,
  },
];
class Religion extends React.Component {
  static propTypes = {
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    assignButtons: () => { },
    showNotification: () => { },
  }

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      deleteIds: [],
      showPopup: false,
      type: "",
      form: {
        id: "",
        name: "",
      },
      errorMsg: {
        name: "",
      },
      isValidated: false,
      filterText: "",
      limitValue: "5",
      limitData: [
        { id: "5", name: "5" },
        { id: "10", name: "10" },
        { id: "20", name: "20" },
        { id: "50", name: "50" },
        { id: "100", name: "100" },
      ],
    };
  }

  componentWillMount = () => {
    const { assignButtons } = this.props;

    assignButtons([{
      id: "1", title: "Tambah Data", icon: "fa fa-plus-square", clickEvent: () => this.callCreateHandler(),
    }]);
  }

  checkedHandler = (values) => {
    this.setState({ deleteIds: values }, () => {
      this.table.refreshData();
    });
  }

  bulkDeleteHandler = async () => {
    const { showNotification } = this.props;

    try {
      const { deleteIds } = this.state;
      await bulkDeleteReligion({ ids: JSON.stringify(deleteIds) });
      this.table.refreshData();
      this.setState({ deleteIds: [] });
      showNotification({
        type: "success",
        msg: "Berhasil menghapus data!",
      });
    } catch (err) {
      console.log(err);
    }
  }

  editDeleteHandler = async (id, type) => {
    const { showNotification } = this.props;
    if (type === "edit") {
      const res = await getReligion(id);
      if (res.status) {
        const { data } = res;
        this.setState({
          type: "edit",
          form: {
            id: data.id,
            name: data.name,
          },
          errorMsg: {
            name: "",
          },
          isValidated: false,
        }, () => {
          this.showPopup();
        });
      }
    } else {
      const { deleteIds } = this.state;
      const res = await deleteReligion({ id });
      if (res.status) {
        this.setState({ deleteIds: deleteIds.filter(x => (String(x) !== String(id))) });
        showNotification({
          type: "success",
          msg: "Berhasil menghapus data!",
        });
        this.table.refreshData();
      }
    }
  }

  callCreateHandler = () => {
    this.setState({
      type: "create",
      form: {
        id: "",
        name: "",
      },
      errorMsg: {
        name: "",
      },
      isValidated: false,
    }, () => {
      this.showPopup();
    });
  }

  saveHandler = async (hide) => {
    const { showNotification } = this.props;
    const { form, type } = this.state;
    const error = this.bulkCreateErrorMessage();
    const isValid = !error.isError;

    this.setState({ isValidated: true, errorMsg: error.msg });

    if (isValid) {
      if (type === "create") {
        await createReligion(form);
        showNotification({
          type: "success",
          msg: "Berhasil menambah data!",
        });
      } else {
        await updateReligion(form);
        showNotification({
          type: "success",
          msg: "Berhasil mengubah data!",
        });
      }

      this.table.refreshData();
      hide();
    }
  }

  showPopup = () => {
    this.setState({ showPopup: true });
  }

  hidePopup = () => {
    this.setState({ showPopup: false });
  }

  fetchData = async (state) => {
    const { filterText, limitValue } = this.state;
    const payload = {
      page: state.page + 1,
      limit: limitValue,
    };

    if (state.sorted) {
      Object.assign(payload, {
        sorted: JSON.stringify(state.sorted),
      });
    }

    if (filterText && filterText.length > 0) {
      Object.assign(payload, {
        filterText,
      });
    }

    const res = await getReligions(payload);

    if (res.status) {
      const newData = res.data.map(x => (Object.assign({}, x, { updatedAt: moment(x.updatedAt).format("DD MMM YYYY HH:mm:ss") })));

      return { data: newData, pageTotal: res.page_total };
    }

    return { data: [], pageTotal: 0 };
  }

  changeFilterTextHandler = (val) => {
    this.setState({ filterText: val });

    clearTimeout(this.filterIdle);
    this.filterIdle = setTimeout(() => {
      this.table.resetCurrentPage(() => {
        this.setState({ filterText: val }, () => {
          this.table.refreshData();
        });
      });
    }, 500);
  }

  changeLimitValueHandler = (val) => {
    this.table.resetCurrentPage(() => {
      this.setState({ limitValue: val }, () => {
        this.table.refreshData();
      });
    });
  }

  changeValueHandler = (type, val) => {
    const { form } = this.state;
    const newValue = update(form, {
      [type]: { $set: val },
    });

    const errorMsg = this.createErrorMessage(type, val);
    this.setState({ form: newValue, errorMsg });
  }

  bulkCreateErrorMessage = () => {
    const { errorMsg, form } = this.state;
    const keys = Object.keys(errorMsg);
    let newErrorMsg = errorMsg;
    let isError = false;

    keys.forEach((x) => {
      let error = "";
      const ERROR_TEXT = errorContent(x, form[x]);

      if (ERROR_TEXT) {
        error = ERROR_TEXT;
        isError = true;
      }

      newErrorMsg = update(newErrorMsg, {
        [x]: { $set: error },
      });
    });

    return { msg: newErrorMsg, isError };
  }

  createErrorMessage = (type, val) => {
    const { errorMsg, isValidated } = this.state;
    let error = "";

    if (!isValidated) {
      return errorMsg;
    }

    const ERROR_TEXT = errorContent(type, val);

    if (ERROR_TEXT) {
      error = ERROR_TEXT;
    }

    const newValue = update(errorMsg, {
      [type]: { $set: error },
    });

    return newValue;
  }

  render = () => {
    const {
      showPopup,
      form,
      type,
      errorMsg,
      deleteIds,
      filterText,
      limitValue,
      limitData,
    } = this.state;

    return (
      <div>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-8">
                <h3 className="box-title with-title">Master Data Agama</h3>
              </div>
              <div className="col-sm-4">
                <div className="row">
                  <div className="col-sm-9">
                    <InputText
                      placeholder="Pencarian ..."
                      changeEvent={this.changeFilterTextHandler}
                      value={filterText}
                    />
                  </div>
                  <div className="col-sm-3 pl-0">
                    <Select
                      data={limitData}
                      value={limitValue}
                      changeEvent={this.changeLimitValueHandler}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="box-body">
            <Table
              ref={(c) => { this.table = c; }}
              columns={columns}
              onFetch={this.fetchData}
              showPagination={true}
              checkedValues={deleteIds}
              checkedEvent={this.checkedHandler}
              editDeleteEvent={this.editDeleteHandler}
            />
          </div>
        </div>
        {showPopup && <SidePopup
          onHide={this.hidePopup}
          title={type === "create" ? "Tambah Data" : "Edit Data"}
          saveEvent={this.saveHandler}
        >
          <div className="row">
            <div className="col-sm-12">
              <InputText
                label="Agama *"
                changeEvent={val => this.changeValueHandler("name", val)}
                value={form.name}
                errorText={errorMsg.name}
              />
            </div>
          </div>
        </SidePopup>}
        {(deleteIds.length > 0) && <div className="bulk-delete-wrapper">
          <div className="row">
            <div className="col-sm-6">
              <h5 className="text-muted"><b>{deleteIds.length} DATA AKAN DIHAPUS ?</b></h5>
            </div>
            <div className="col-sm-6 text-right">
              <button style={{ width: "100px" }} type="button" className="btn btn-danger btn-md btn-flat" onClick={this.bulkDeleteHandler}><i className="fa fa-trash-o" /> Hapus</button>
            </div>
          </div>
        </div>
        }
      </div>
    );
  }
}

export default CoreHoC(Religion);
