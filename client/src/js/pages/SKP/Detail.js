import React from "react";
import update from "immutability-helper";
import PropTypes from "prop-types";
import { FieldFeedbacks, FieldFeedback } from "react-form-with-constraints";
import FormValidation from "~/components/form/FormValidation";
import InputText from "~/components/form/InputText";
import Select from "~/components/form/Select";
import DatePicker from "~/components/form/DatePicker";
import BrowseData from "~/components/form/BrowseData";
import CoreHoC from "../CoreHoC";
import {
  getEmployees, createSkpEmployee, updateSkpEmployee, getSkpEmployee,
} from "../../data";

const employeeColumns = [
  {
    accessor: "fullname",
    Header: "Nama Pegawai",
    width: "100%",
  },
];
class SkpEmployeeDetail extends React.Component {
  static propTypes = {
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    assignButtons: () => { },
    showNotification: () => { },
  }

  initialButtonActions = [
    {
      id: "1",
      type: null,
      content: (
        <span>
          Batal
        </span>
      ),
      action: () => this.gotoBasePath(),
      isDisabled: false,
    },
    {
      id: "2",
      type: "primary",
      content: (
        <span>
          Simpan
        </span>
      ),
      action: () => this.saveDataHandler(),
      isDisabled: false,
    },
  ];

  constructor(props) {
    super(props);

    this.state = {
      id: "",
      isFormSubmitted: false,
      type: "create",
      penaltyTypeList: [],
      form: {
        employee: {
          id: "",
          name: "",
        },
        start_date: moment().format("YYYY-MM-DD"),
        end_date: moment().format("YYYY-MM-DD"),
        evaluator_name: "",
        head_evaluator_name: "",
        scores: {
          service_orientation: "",
          integrity: "",
          commitment: "",
          discipline: "",
          cooperation: "",
          leadership: "",
        },
      },
      footerButtons: this.initialButtonActions,
    };
  }

  componentWillMount = () => {
    this.setupData();
  }

  setupData = async () => {
    const { match: { params } } = this.props;

    if (params.type === "edit") {
      await this.setupDetailData(params.id);
    } else {
      this.setupBreadcrumbs("Tambah Data");
    }
  }

  setupBreadcrumbs = (text) => {
    const { assignBreadcrumbs } = this.props;

    assignBreadcrumbs([
      {
        label: "App", link: "#",
      },
      {
        label: "Pegawai", link: "#",
      },
      {
        label: "SKP", link: "/skp",
      },
      text,
    ]);
  }

  setupDetailData = async (id) => {
    const res = await getSkpEmployee(id);

    if (res.status) {
      const { data } = res;
      const scores = JSON.parse(data.score);

      const newState = {
        id: data.id,
        type: "edit",
        form: {
          employee: {
            id: data.Employee.id,
            name: data.Employee.fullname,
          },
          start_date: moment(data.start_date).format("YYYY-MM-DD"),
          end_date: moment(data.end_date).format("YYYY-MM-DD"),
          evaluator_name: data.evaluator_name,
          head_evaluator_name: data.head_evaluator_name,
          scores,
        },
      };

      this.setupBreadcrumbs(data.Employee.fullname);
      this.setState(newState);
    }
  }

  onFetchEmployee = async (state) => {
    const payload = {
      limit: 10,
      page: state.page,
      filterText: state.filterText,
      sorted: JSON.stringify({ id: state.fieldToSort, desc: state.sortType === "desc" }),
    };
    const res = await getEmployees(payload);

    return res;
  }

  changeValueHandler = async (type, val, e) => {
    const { form } = this.state;

    const { isFormSubmitted } = this.state;

    if (isFormSubmitted && e) {
      await this.onInputChangeValidate(e);
    }

    const newValue = update(form, {
      [type]: { $set: val },
    });

    this.setState({ form: newValue });
  }

  changeScoresHandler = async (type, val, e) => {
    const { form } = this.state;

    const { isFormSubmitted } = this.state;

    if (isFormSubmitted && e) {
      await this.onInputChangeValidate(e);
    }

    const newValue = update(form, {
      scores: {
        [type]: { $set: val },
      },
    });

    this.setState({ form: newValue });
  }

  onInputChangeValidate = ({ target }) => {
    this.form.validateInput(target);

    if (this.inputTimeout) {
      clearTimeout(this.inputTimeout);
    }

    this.inputTimeout = setTimeout(() => {
      if (this.form.simpleValidateForm()) {
        this.updateButtonsState();
      } else {
        this.updateButtonsState(false, true);
      }
    }, 300);
  }

  updateButtonsState = (isBtnCancelDisabled = false, isBtnSaveDisabled = false) => {
    const footerButtons = update(this.initialButtonActions, {
      0: { isDisabled: { $set: isBtnCancelDisabled } },
      1: { isDisabled: { $set: isBtnSaveDisabled } },
    });

    this.setState({ footerButtons });
  }

  changeEmployeeHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      employee: { $set: { id: val.id, name: val.fullname } },
    });

    this.setState({ form: newValue }, () => {
      this.validateEmployee();
    });
  }

  validateEmployee = async () => {
    const { isFormSubmitted } = this.state;

    if (isFormSubmitted) {
      await this.onInputChangeValidate({ target: this.employeeHidden });
    }
  }

  employeeConditionalRender = (type) => {
    const { form } = this.state;

    if (type === "edit") {
      return (
        <div className="row mb-sm">
          <div className="col-sm-6">
            <DatePicker
              label="Periode Penilaian Dari"
              id="start-date"
              changeEvent={val => this.changeValueHandler("start_date", val)}
              value={form.start_date}
            />
          </div>
          <div className="col-sm-6">
            <DatePicker
              label="Periode Penilaian Sampai"
              id="end-date"
              changeEvent={val => this.changeValueHandler("end_date", val)}
              value={form.end_date}
            />
          </div>
        </div>
      );
    }

    return (
      <div className="row mb-sm">
          <div className="col-sm-4">
            <div className="form-group position-relative mb-0">
              <BrowseData
                label="Pegawai"
                placeholder="Pilih Pegawai"
                columns={employeeColumns}
                value={form.employee}
                changeEvent={this.changeEmployeeHandler}
                onFetch={this.onFetchEmployee}
              />
              <input
                ref={(c) => { this.employeeHidden = c; }}
                className="hide-for-validation"
                name="employeeHidden"
                type="text"
                value={form.employee.id}
                onChange={() => { }}
                required
              />
            </div>
            <FieldFeedbacks for="employeeHidden">
              <FieldFeedback when="valueMissing">Pegawai wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <DatePicker
              label="Periode Penilaian Dari"
              id="start-date"
              changeEvent={val => this.changeValueHandler("start_date", val)}
              value={form.start_date}
            />
          </div>
          <div className="col-sm-4">
            <DatePicker
              label="Periode Penilaian Sampai"
              id="end-date"
              changeEvent={val => this.changeValueHandler("end_date", val)}
              value={form.end_date}
            />
          </div>
        </div>
    );
  }

  formComponent = () => {
    const {
      form, type,
    } = this.state;
    return (
      <FormValidation ref={(c) => { this.form = c; }}>
        {this.employeeConditionalRender(type)}
        <div className="row mb-sm">
          <div className="col-sm-6">
            <InputText
              label="Nama Pejabat Penilai"
              changeEvent={(val, e) => this.changeValueHandler("evaluator_name", val, e)}
              value={String(form.evaluator_name)}
              name="evaluator_name"
              required
            />
            <FieldFeedbacks for="evaluator_name">
              <FieldFeedback when="valueMissing">Nama Pejabat Penilai wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-6">
            <InputText
              label="Nama Atasan Pejabat Penilai"
              changeEvent={(val, e) => this.changeValueHandler("head_evaluator_name", val, e)}
              value={String(form.head_evaluator_name)}
              name="head_evaluator_name"
              required
            />
            <FieldFeedbacks for="head_evaluator_name">
              <FieldFeedback when="valueMissing">Nama Atasan Pejabat Penilai wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>

        <div className="row mb-sm">
          <div className="col-sm-6">
            <InputText
              label="Nilai Orientasi Pelayanan"
              changeEvent={(val, e) => this.changeScoresHandler("service_orientation", val, e)}
              value={String(form.scores.service_orientation)}
              name="scores_service_orientation"
              required
            />
            <FieldFeedbacks for="scores_service_orientation">
              <FieldFeedback when="valueMissing">Nilai Orientasi Pelayanan wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-6">
            <InputText
              label="Nilai Integritas"
              changeEvent={(val, e) => this.changeScoresHandler("integrity", val, e)}
              value={String(form.scores.integrity)}
              name="scores_integrity"
              required
            />
            <FieldFeedbacks for="scores_integrity">
              <FieldFeedback when="valueMissing">Nilai Integritas wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>

        <div className="row mb-sm">
          <div className="col-sm-6">
            <InputText
              label="Nilai Komitmen"
              changeEvent={(val, e) => this.changeScoresHandler("commitment", val, e)}
              value={String(form.scores.commitment)}
              name="scores_commitment"
              required
            />
            <FieldFeedbacks for="scores_commitment">
              <FieldFeedback when="valueMissing">Nilai Komitmen wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-6">
            <InputText
              label="Nilai Disiplin"
              changeEvent={(val, e) => this.changeScoresHandler("discipline", val, e)}
              value={String(form.scores.discipline)}
              name="scores_discipline"
              required
            />
            <FieldFeedbacks for="scores_discipline">
              <FieldFeedback when="valueMissing">Nilai Disiplin wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>

        <div className="row mb-sm">
          <div className="col-sm-6">
            <InputText
              label="Nilai Kerjasama"
              changeEvent={(val, e) => this.changeScoresHandler("cooperation", val, e)}
              value={String(form.scores.cooperation)}
              name="scores_cooperation"
              required
            />
            <FieldFeedbacks for="scores_cooperation">
              <FieldFeedback when="valueMissing">Nilai Kerjasama wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-6">
            <InputText
              label="Nilai Kepemimpinan"
              changeEvent={(val, e) => this.changeScoresHandler("leadership", val, e)}
              value={String(form.scores.leadership)}
              name="scores_leadership"
              required
            />
            <FieldFeedbacks for="scores_leadership">
              <FieldFeedback when="valueMissing">Nilai Kepemimpinan wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
      </FormValidation>
    );
  }

  gotoBasePath = () => {
    const { history } = this.props;
    history.push("/skp");
  }

  saveDataHandler = async () => {
    const {
      form, type, id,
    } = this.state;
    this.updateButtonsState(true, true);

    const isFormValid = await this.form.validateForm();

    if (isFormValid) {
      const payload = Object.assign({}, form, {
        id_employee: form.employee.id,
        score: JSON.stringify(form.scores),
      });
      delete payload.employee;

      if (type === "create") {
        const res = await createSkpEmployee(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      } else {
        Object.assign(payload, { id });
        const res = await updateSkpEmployee(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      }
    } else {
      this.updateButtonsState(false, true);
    }

    this.setState({
      isFormSubmitted: true,
    });
  }

  render = () => {
    const {
      footerButtons, type,
    } = this.state;

    return (
      <div style={{ width: "80%", position: "relative", margin: "0px auto" }}>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-12">
                <h3 className="box-title with-title">{type === "create" ? "Tambah" : "Edit"} SKP</h3>
              </div>
            </div>
          </div>
          <div className="box-body">
            {this.formComponent()}
          </div>
          <div className="box-footer text-right">
            <div>
              {footerButtons.map((x, i) => (<button key={x.id} style={(i === footerButtons.length - 1) ? {} : { marginRight: "10px" }} type="button" className={`btn ${x.type ? `btn-${x.type}` : ""}`} onClick={!x.isDisabled ? x.action : () => { }} disabled={x.isDisabled}>{x.content}</button>))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CoreHoC(SkpEmployeeDetail);
