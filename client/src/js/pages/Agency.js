import React from "react";
import PropTypes from "prop-types";
import update from "immutability-helper";
import moment from "moment";
import RawTable from "~/components/form/Table";
import withEditDelete from "~/components/form/Table/withEditDelete";
import withCheckbox from "~/components/form/Table/withCheckbox";
import SidePopup from "~/components/form/SidePopup";
import InputText from "~/components/form/InputText";
import Select2 from "~/components/form/Select2";
import Select from "~/components/form/Select";
import CoreHoC from "./CoreHoC";
import {
  getAgencies, getAgency, createAgency, updateAgency, deleteAgency, bulkDeleteAgency,
} from "../data";

const Table = withCheckbox(withEditDelete(RawTable));

const columns = [
  {
    Header: "Kode",
    accessor: "code",
    resizable: false,
    width: 150,
  },
  {
    Header: "Instansi",
    accessor: "name",
    resizable: false,
  },
];
class Agency extends React.Component {
  static propTypes = {
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    assignButtons: () => { },
    showNotification: () => { },
  }

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      showPopup: false,
      type: "",
      form: {
        id: "",
        parent_id: "",
        code: "",
        name: "",
      },
      errorMsg: {
        code: "",
        name: "",
      },
      deleteIds: [],
      filterText: "",
      limitValue: "5",
      limitData: [
        { id: "5", name: "5" },
        { id: "10", name: "10" },
        { id: "20", name: "20" },
        { id: "50", name: "50" },
        { id: "100", name: "100" },
      ],
    };
  }

  componentWillMount = () => {
    const { assignButtons } = this.props;

    assignButtons([{
      id: "1", title: "Tambah Data", icon: "fa fa-plus-square", clickEvent: () => this.callCreateHandler(),
    }]);
  }

  checkedHandler = (values) => {
    this.setState({ deleteIds: values }, () => {
      this.table.refreshData();
    });
  }

  changeFilterTextHandler = (val) => {
    this.setState({ filterText: val });

    clearTimeout(this.filterIdle);
    this.filterIdle = setTimeout(() => {
      this.table.resetCurrentPage(() => {
        this.setState({ filterText: val }, () => {
          this.table.refreshData();
        });
      });
    }, 500);
  }

  changeLimitValueHandler = (val) => {
    this.table.resetCurrentPage(() => {
      this.setState({ limitValue: val }, () => {
        this.table.refreshData();
      });
    });
  }

  editDeleteHandler = async (id, type) => {
    const { showNotification } = this.props;

    if (type === "edit") {
      const res = await getAgency(id);
      if (res.status) {
        const { data } = res;
        const parentId = data.parent_id ? String(data.parent_id) : "";
        let defaultParentText = "";

        if (parentId !== "") {
          const parentResponse = await getAgency(parentId);

          if (parentResponse.status) {
            defaultParentText = parentResponse.data.name;
          }
        }

        this.setState({
          type: "edit",
          form: {
            id: data.id,
            parent_id: parentId,
            code: data.raw_code,
            name: data.name,
          },
          errorMsg: {
            code: "",
            name: "",
          },
          isValidated: false,
          defaultParentText,
        }, () => {
          this.showPopup();
        });
      }
    } else {
      const { deleteIds } = this.state;
      const res = await deleteAgency({ id });
      if (res.status) {
        this.setState({ deleteIds: deleteIds.filter(x => (String(x) !== String(id))) });
        showNotification({
          type: "success",
          msg: "Berhasil menghapus data!",
        });
        this.table.refreshData();
      }
    }
  }

  bulkDeleteHandler = async () => {
    const { showNotification } = this.props;

    try {
      const { deleteIds } = this.state;
      await bulkDeleteAgency({ ids: JSON.stringify(deleteIds) });
      this.table.refreshData();
      this.setState({ deleteIds: [] });
      showNotification({
        type: "success",
        msg: "Berhasil menghapus data!",
      });
    } catch (err) {
      console.log(err);
    }
  }

  callCreateHandler = () => {
    this.setState({
      type: "create",
      form: {
        id: "",
        parent_id: "",
        code: "",
        name: "",
      },
      errorMsg: {
        code: "",
        name: "",
      },
      isValidated: false,
      defaultParentText: "",
    }, () => {
      this.showPopup();
    });
  }

  saveHandler = async (hide) => {
    const { showNotification } = this.props;
    const { form, type } = this.state;
    const error = this.bulkCreateErrorMessage();
    const isValid = !error.isError;

    this.setState({ isValidated: true, errorMsg: error.msg });

    if (isValid) {
      if (type === "create") {
        let payload = form;

        if (String(form.parent_id) === "") {
          const newData = update(payload, {
            parent_id: { $set: null },
          });

          payload = newData;
        }

        const res = await createAgency(payload);
        if (res.status) {
          showNotification({
            type: "success",
            msg: "Berhasil menambah data!",
          });
          this.table.refreshData();
          hide();
        } else if (res.msg === "raw_code_exist") {
          showNotification({
            type: "error",
            msg: "Kode sudah digunakan!",
          });
        } else {
          showNotification({
            type: "error",
            msg: "Gagal menambah data!",
          });
        }
      } else {
        let payload = form;

        if (String(form.parent_id) === "") {
          const newData = update(payload, {
            parent_id: { $set: null },
          });

          payload = newData;
        }

        const res = await updateAgency(payload);
        if (res.status) {
          showNotification({
            type: "success",
            msg: "Berhasil menambah data!",
          });
          this.table.refreshData();
          hide();
        } else if (res.msg === "raw_code_exist") {
          showNotification({
            type: "error",
            msg: "Kode sudah digunakan!",
          });
        } else {
          showNotification({
            type: "error",
            msg: "Gagal menambah data!",
          });
        }
      }
    }
  }

  showPopup = () => {
    this.setState({ showPopup: true });
  }

  hidePopup = () => {
    this.setState({ showPopup: false });
  }

  fetchData = async (state) => {
    const { filterText, limitValue } = this.state;
    const payload = {
      page: state.page + 1,
      limit: limitValue,
    };

    if (state.sorted) {
      Object.assign(payload, {
        sorted: JSON.stringify(state.sorted),
      });
    }

    if (filterText && filterText.length > 0) {
      Object.assign(payload, {
        filterText,
      });
    }

    const res = await getAgencies(payload);

    if (res.status) {
      const newData = res.data.map(x => (Object.assign({}, x, { updatedAt: moment(x.updatedAt).format("DD MMM YYYY HH:mm:ss") })));

      return { data: newData, pageTotal: res.page_total };
    }

    return { data: [], pageTotal: 0 };
  }

  changeValueHandler = (type, val) => {
    const { form } = this.state;
    const newValue = update(form, {
      [type]: { $set: val },
    });

    const errorMsg = this.createErrorMessage(type, val);
    this.setState({ form: newValue, errorMsg });
  }

  bulkCreateErrorMessage = () => {
    const { errorMsg, form } = this.state;
    const keys = Object.keys(errorMsg);
    let newErrorMsg = errorMsg;
    let isError = false;

    keys.forEach((x) => {
      let error = "";

      if (x === "code") {
        if (String(form[x]).trim().length === 0) {
          error = "* Kode wajib diisi";
          isError = true;
        }
      } else if (x === "name") {
        if (String(form[x]).trim().length === 0) {
          error = "* Nama Instansi wajib diisi";
          isError = true;
        }
      }

      newErrorMsg = update(newErrorMsg, {
        [x]: { $set: error },
      });
    });

    return { msg: newErrorMsg, isError };
  }

  createErrorMessage = (type, val) => {
    const { errorMsg, isValidated } = this.state;
    let error = "";

    if (!isValidated) {
      return errorMsg;
    }

    if (type === "code") {
      if (String(val).trim().length === 0) {
        error = "* Kode wajib diisi";
      }
    }

    if (type === "name") {
      if (String(val).trim().length === 0) {
        error = "* Nama Instansi wajib diisi";
      }
    }

    const newValue = update(errorMsg, {
      [type]: { $set: error },
    });

    return newValue;
  }

  render = () => {
    const {
      showPopup, form, type, errorMsg, deleteIds, filterText, limitData, limitValue,
    } = this.state;

    return (
      <div>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-8">
                <h3 className="box-title with-title">Master Data Instansi</h3>
              </div>
              <div className="col-sm-4">
                <div className="row">
                  <div className="col-sm-9">
                    <InputText
                      placeholder="Pencarian ..."
                      changeEvent={this.changeFilterTextHandler}
                      value={filterText}
                    />
                  </div>
                  <div className="col-sm-3 pl-0">
                    <Select
                      data={limitData}
                      value={limitValue}
                      changeEvent={this.changeLimitValueHandler}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="box-body">
            <Table
              ref={(c) => { this.table = c; }}
              columns={columns}
              onFetch={this.fetchData}
              checkedValues={deleteIds}
              checkedEvent={this.checkedHandler}
              showPagination={true}
              editDeleteEvent={this.editDeleteHandler}
            />
          </div>
        </div>
        {showPopup && <SidePopup
          onHide={this.hidePopup}
          title={type === "create" ? "Tambah Data" : "Edit Data"}
          saveEvent={this.saveHandler}
        >
          <div className="row mb-sm">
            <div className="col-sm-12">
              <Select2
                label="Instansi Induk"
                url="/api/agencies"
                additionalParam={{ exceptId: form.id }}
                placeholder="- Pilih Instansi Induk -"
                value={form.parent_id}
                changeEvent={val => this.changeValueHandler("parent_id", val)}
              />
            </div>
          </div>
          <div className="row mb-sm">
            <div className="col-sm-12">
              <InputText
                label="Kode *"
                changeEvent={val => this.changeValueHandler("code", val)}
                value={form.code}
                errorText={errorMsg.code}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12">
              <InputText
                label="Nama Instansi *"
                changeEvent={val => this.changeValueHandler("name", val)}
                value={form.name}
                errorText={errorMsg.name}
              />
            </div>
          </div>
        </SidePopup>}
        {(deleteIds.length > 0) && <div className="bulk-delete-wrapper">
          <div className="row">
            <div className="col-sm-6">
              <h5 className="text-muted"><b>{deleteIds.length} DATA AKAN DIHAPUS ?</b></h5>
            </div>
            <div className="col-sm-6 text-right">
              <button style={{ width: "100px" }} type="button" className="btn btn-danger btn-md btn-flat" onClick={this.bulkDeleteHandler}><i className="fa fa-trash-o" /> Hapus</button>
            </div>
          </div>
        </div>
        }
      </div>
    );
  }
}

export default CoreHoC(Agency);
