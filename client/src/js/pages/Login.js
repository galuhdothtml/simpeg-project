/* eslint prop-types: 0 */
import React from "react";
import InputText from "~/components/form/InputText";
import InputCheckbox from "~/components/form/InputCheckbox";
import BackgroundImage from "../images/bg-login.jpg";
import Logo from "../images/logo.png";
import {
  login,
} from "../data";
import Util from "../utils";

const mt8 = {
  marginTop: "7px",
};
class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "superadmin",
      password: "superadmin",
      rememberPassword: false,
      errorText: "",
      isLoading: false,
    };
  }

  componentWillUnmount = () => {
    clearTimeout(this.clearNotificationTimeout);
  }

  setLoading = (isLoading) => {
    this.setState({ isLoading });
  }

  changeValueHandler = (type, val) => {
    this.setState({ [type]: val });
  }

  login = async (e) => {
    const { email, password } = this.state;
    e.preventDefault();

    clearTimeout(this.clearNotificationTimeout);
    this.setState({ errorText: "" });

    const payload = {
      username: email,
      password,
    };

    this.setLoading(true);

    const res = await login(payload);

    if (res.status) {
      Util.setToken(res.token);
      location.href = "/dashboard";
    } else {
      this.setLoading(false);
      this.showErrorNotification(res.msg);
    }
  }

  showErrorNotification = (val) => {
    this.setState({ errorText: val });
    this.clearNotificationTimeout = setTimeout(() => {
      this.setState({ errorText: "" });
    }, 5000);
  }

  renderErrorNotification = () => {
    const { errorText } = this.state;

    if (errorText) {
      return (
        <div className="mb-md">
          <div className="form-group hidden" />
          <span className="error">{errorText}</span>
        </div>
      );
    }

    return null;
  }

  renderButton = () => {
    const { isLoading } = this.state;

    if (isLoading) {
      return (
        <button type="button" className="btn btn-primary btn-block btn-flat disabled" disabled>Tunggu ...</button>
      );
    }

    return (
      <button
        type="submit"
        className="btn btn-primary btn-block btn-flat">Masuk</button>
    );
  }

  render() {
    const { email, password, rememberPassword } = this.state;

    return (
      <div className="login-page" style={{ backgroundImage: `url(${BackgroundImage})`, backgroundSize: "cover" }}>
        <div className="login-box">
          <div className="login-box-body">
            <div className="login-box-body__left">
              <img src={Logo} />
              <h4>SISTEM INFORMASI MANAJEMEN KEPEGAWAIAN</h4>
              <div className="login-box-body__contact">
                <h5>HUBUNGI KAMI</h5>
                <div>
                  <a href="#"><i className="fa fa-phone" /></a>
                  <a href="#"><i className="fa fa-envelope" /></a>
                  <a href="#"><i className="fa fa-facebook" /></a>
                  <a href="#"><i className="fa fa-twitter" /></a>
                </div>
              </div>
            </div>
            <div className="login-box-body__right">
              <form onSubmit={this.login} style={{
                position: "relative",
                top: "1em",
              }}>
                <div className="row mb-md">
                  <div className="col-xs-12">
                    <InputText
                      placeholder="Email"
                      changeEvent={val => this.changeValueHandler("email", val)}
                      value={email}
                    />
                  </div>
                </div>
                <div className="row mb-md">
                  <div className="col-xs-12">
                    <InputText
                      placeholder="Password"
                      changeEvent={val => this.changeValueHandler("password", val)}
                      value={password}
                    />
                  </div>
                </div>
                {this.renderErrorNotification()}
                <div className="row mb-md">
                  <div className="col-xs-12">
                    {this.renderButton()}
                  </div>
                </div>
                <div className="text-center">
                    <p className="mb-0">Lupa Kata Kunci / Password? <a href="#" className="text-danger">Reset Password</a></p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
