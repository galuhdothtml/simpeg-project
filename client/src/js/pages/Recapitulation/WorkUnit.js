/* eslint prop-types: 0 */
import React from "react";
import CoreHoC from "../CoreHoC";
import {
  getWorkUnitRecapitulation,
} from "../../data";

class WorkUnitRecapitulation extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
    };
  }

  componentWillMount = () => {
    const { assignButtons, assignBreadcrumbs } = this.props;

    assignButtons([{
      id: "1", title: "Print", icon: "fa fa-print", clickEvent: () => this.onPrint(),
    }]);

    assignBreadcrumbs([
      {
        label: "App", link: "#",
      },
      {
        label: "Laporan", link: "#",
      },
      {
        label: "Rekapitulasi", link: "#",
      },
      "Unit Kerja",
    ]);
    this.fetchData();
  }

  onPrint = () => {
    //
  }

  fetchData = async () => {
    const res = await getWorkUnitRecapitulation();
    this.setState({ data: res.data });
  }

  renderTableBody = () => {
    const { data } = this.state;
    const retval = data.map((x, i) => (
      <tr key={i}>
        <td>{i + 1}</td>
        <td>{x.name}</td>
        <td>{x.employee_total}</td>
      </tr>
    ));

    return <tbody>{retval}</tbody>;
  }

  renderTable = () => (
    <table className="table table-bordered">
      <thead>
        <tr>
          <th>No</th>
          <th>Unit Kerja</th>
          <th>Jumlah Pegawai</th>
        </tr>
      </thead>
      {this.renderTableBody()}
    </table>
  )

  render() {
    return (
      <div>
        <div className="box box-solid">
          <div className="box-header with-border">
            <h3 className="box-title with-title">Rekapitulasi Pegawai Berdasarkan Unit Kerja</h3>
          </div>
          <div className="box-body">
            {this.renderTable()}
          </div>
        </div>
      </div>
    );
  }
}

export default CoreHoC(WorkUnitRecapitulation);
