import React from "react";
import update from "immutability-helper";
import PropTypes from "prop-types";
import { FieldFeedbacks, FieldFeedback } from "react-form-with-constraints";
import FormValidation from "~/components/form/FormValidation";
import InputText from "~/components/form/InputText";
import Select from "~/components/form/Select";
import DatePicker from "~/components/form/DatePicker";
import BrowseData from "~/components/form/BrowseData";
import CoreHoC from "../CoreHoC";
import {
  getEmployees, createEmployeePenalty, updateEmployeePenalty, getEmployeePenalty, getPenaltyTypes,
} from "../../data";

const employeeColumns = [
  {
    accessor: "fullname",
    Header: "Nama Pegawai",
    width: "100%",
  },
];
class EmployeePenaltyDetail extends React.Component {
  static propTypes = {
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    assignButtons: () => { },
    showNotification: () => { },
  }

  initialButtonActions = [
    {
      id: "1",
      type: null,
      content: (
        <span>
          Batal
        </span>
      ),
      action: () => this.gotoBasePath(),
      isDisabled: false,
    },
    {
      id: "2",
      type: "primary",
      content: (
        <span>
          Simpan
        </span>
      ),
      action: () => this.saveDataHandler(),
      isDisabled: false,
    },
  ];

  constructor(props) {
    super(props);

    this.state = {
      id: "",
      isFormSubmitted: false,
      type: "create",
      penaltyTypeList: [],
      form: {
        employee: {
          id: "",
          name: "",
        },
        id_penalty_type: "",
        sk_official_certifier: "",
        sk_number: "",
        sk_date: moment().format("YYYY-MM-DD"),
        penalty_restorer_official: "",
        restore_penalty_number: "",
        restore_penalty_date: moment().format("YYYY-MM-DD"),
      },
      footerButtons: this.initialButtonActions,
    };
  }

  componentWillMount = () => {
    this.setupData();
    this.setupPenaltyTypeList();
  }

  setupData = async () => {
    const { match: { params } } = this.props;

    if (params.type === "edit") {
      await this.setupDetailData(params.id);
    } else {
      this.setupBreadcrumbs("Tambah Data");
    }
  }

  setupBreadcrumbs = (text) => {
    const { assignBreadcrumbs } = this.props;

    assignBreadcrumbs([
      {
        label: "App", link: "#",
      },
      {
        label: "Pegawai", link: "#",
      },
      {
        label: "Kepegawaian", link: "#",
      },
      {
        label: "Hukuman", link: "/kepegawaian-hukuman",
      },
      text,
    ]);
  }

  setupDetailData = async (id) => {
    const res = await getEmployeePenalty(id);

    if (res.status) {
      const { data } = res;

      const newState = {
        id: data.id,
        type: "edit",
        form: {
          employee: {
            id: data.Employee.id,
            name: data.Employee.fullname,
          },
          id_penalty_type: data.id_penalty_type,
          sk_official_certifier: data.sk_official_certifier,
          sk_number: data.sk_number,
          sk_date: moment(data.sk_date).format("YYYY-MM-DD"),
          penalty_restorer_official: data.penalty_restorer_official,
          restore_penalty_number: data.restore_penalty_number,
          restore_penalty_date: moment(data.restore_penalty_date).format("YYYY-MM-DD"),
        },
      };

      this.setupBreadcrumbs(data.Employee.fullname);
      this.setState(newState);
    }
  }

  onFetchEmployee = async (state) => {
    const payload = {
      limit: 10,
      page: state.page,
      filterText: state.filterText,
      sorted: JSON.stringify({ id: state.fieldToSort, desc: state.sortType === "desc" }),
    };
    const res = await getEmployees(payload);

    return res;
  }

  setupPenaltyTypeList = async () => {
    const res = await getPenaltyTypes();
    const penaltyTypeList = [{
      id: "",
      name: "- Pilih Jenis Hukuman -",
    }, ...res.data];

    this.setState({ penaltyTypeList });
  }

  changeValueHandler = async (type, val, e) => {
    const { form } = this.state;

    const { isFormSubmitted } = this.state;

    if (isFormSubmitted && e) {
      await this.onInputChangeValidate(e);
    }

    const newValue = update(form, {
      [type]: { $set: val },
    });

    this.setState({ form: newValue });
  }

  onInputChangeValidate = ({ target }) => {
    this.form.validateInput(target);

    if (this.inputTimeout) {
      clearTimeout(this.inputTimeout);
    }

    this.inputTimeout = setTimeout(() => {
      if (this.form.simpleValidateForm()) {
        this.updateButtonsState();
      } else {
        this.updateButtonsState(false, true);
      }
    }, 300);
  }

  updateButtonsState = (isBtnCancelDisabled = false, isBtnSaveDisabled = false) => {
    const footerButtons = update(this.initialButtonActions, {
      0: { isDisabled: { $set: isBtnCancelDisabled } },
      1: { isDisabled: { $set: isBtnSaveDisabled } },
    });

    this.setState({ footerButtons });
  }

  changeEmployeeHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      employee: { $set: { id: val.id, name: val.fullname } },
    });

    this.setState({ form: newValue }, () => {
      this.validateEmployee();
    });
  }

  validateEmployee = async () => {
    const { isFormSubmitted } = this.state;

    if (isFormSubmitted) {
      await this.onInputChangeValidate({ target: this.employeeHidden });
    }
  }

  employeeConditionalRender = (type) => {
    const { penaltyTypeList, form } = this.state;

    if (type === "edit") {
      return (
        <div className="row mb-sm">
          <div className="col-sm-12">
            <Select
                label="Jenis Hukuman"
                name="id_penalty_type"
                data={penaltyTypeList}
                value={form.id_penalty_type}
                changeEvent={(val, e) => this.changeValueHandler("id_penalty_type", val, e)}
                required
            />
            <FieldFeedbacks for="id_penalty_type">
              <FieldFeedback when="valueMissing">Jenis hukuman wajib dipilih</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
      );
    }

    return (
      <div className="row mb-sm">
          <div className="col-sm-6">
            <div className="form-group position-relative mb-0">
              <BrowseData
                label="Pegawai"
                placeholder="Pilih Pegawai"
                columns={employeeColumns}
                value={form.employee}
                changeEvent={this.changeEmployeeHandler}
                onFetch={this.onFetchEmployee}
              />
              <input
                ref={(c) => { this.employeeHidden = c; }}
                className="hide-for-validation"
                name="employeeHidden"
                type="text"
                value={form.employee.id}
                onChange={() => { }}
                required
              />
            </div>
            <FieldFeedbacks for="employeeHidden">
              <FieldFeedback when="valueMissing">Pegawai wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-6">
            <Select
                label="Jenis Hukuman"
                name="id_penalty_type"
                data={penaltyTypeList}
                value={form.id_penalty_type}
                changeEvent={(val, e) => this.changeValueHandler("id_penalty_type", val, e)}
                required
            />
            <FieldFeedbacks for="id_penalty_type">
              <FieldFeedback when="valueMissing">Jenis hukuman wajib dipilih</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
    );
  }

  formComponent = () => {
    const {
      form, type,
    } = this.state;
    return (
      <FormValidation ref={(c) => { this.form = c; }}>
        {this.employeeConditionalRender(type)}
        <div className="row mb-sm">
          <div className="col-sm-4">
            <InputText
              label="Pejabat Pengesah SK Hukuman"
              changeEvent={(val, e) => this.changeValueHandler("sk_official_certifier", val, e)}
              value={String(form.sk_official_certifier)}
              name="sk_official_certifier"
              required
            />
            <FieldFeedbacks for="sk_official_certifier">
              <FieldFeedback when="valueMissing">Pejabat Pengesah SK Hukuman wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <InputText
              label="Nomor SK"
              changeEvent={(val, e) => this.changeValueHandler("sk_number", val, e)}
              value={String(form.sk_number)}
              name="sk_number"
              required
            />
            <FieldFeedbacks for="sk_number">
              <FieldFeedback when="valueMissing">Nomor SK wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <DatePicker
              label="Tanggal Pengesahan SK"
              id="sk-date"
              changeEvent={val => this.changeValueHandler("sk_date", val)}
              value={form.sk_date}
            />
          </div>
        </div>

        <div className="row mb-sm">
          <div className="col-sm-4">
            <InputText
              label="Pejabat Pemulih Hukuman"
              changeEvent={(val, e) => this.changeValueHandler("penalty_restorer_official", val, e)}
              value={String(form.penalty_restorer_official)}
              name="penalty_restorer_official"
              required
            />
            <FieldFeedbacks for="penalty_restorer_official">
              <FieldFeedback when="valueMissing">Pejabat Pemulih Hukuman wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <InputText
              label="Nomor Pemulihan Hukuman"
              changeEvent={(val, e) => this.changeValueHandler("restore_penalty_number", val, e)}
              value={String(form.restore_penalty_number)}
              name="restore_penalty_number"
              required
            />
            <FieldFeedbacks for="restore_penalty_number">
              <FieldFeedback when="valueMissing">Nomor Pemulihan Hukuman wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <DatePicker
              label="Tanggal Pemulihan Hukuman"
              id="restore-penalty-date"
              changeEvent={val => this.changeValueHandler("restore_penalty_date", val)}
              value={form.restore_penalty_date}
            />
          </div>
        </div>
      </FormValidation>
    );
  }

  gotoBasePath = () => {
    const { history } = this.props;
    history.push("/kepegawaian-hukuman");
  }

  saveDataHandler = async () => {
    const {
      form, type, id,
    } = this.state;
    this.updateButtonsState(true, true);

    const isFormValid = await this.form.validateForm();

    if (isFormValid) {
      const payload = Object.assign({}, form, {
        id_employee: form.employee.id,
      });
      delete payload.employee;

      if (type === "create") {
        const res = await createEmployeePenalty(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      } else {
        Object.assign(payload, { id });
        const res = await updateEmployeePenalty(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      }
    } else {
      this.updateButtonsState(false, true);
    }

    this.setState({
      isFormSubmitted: true,
    });
  }

  render = () => {
    const {
      footerButtons, type,
    } = this.state;

    return (
      <div style={{ width: "80%", position: "relative", margin: "0px auto" }}>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-12">
                <h3 className="box-title with-title">{type === "create" ? "Tambah" : "Edit"} Kepegawaian Hukuman</h3>
              </div>
            </div>
          </div>
          <div className="box-body">
            {this.formComponent()}
          </div>
          <div className="box-footer text-right">
            <div>
              {footerButtons.map((x, i) => (<button key={x.id} style={(i === footerButtons.length - 1) ? {} : { marginRight: "10px" }} type="button" className={`btn ${x.type ? `btn-${x.type}` : ""}`} onClick={!x.isDisabled ? x.action : () => { }} disabled={x.isDisabled}>{x.content}</button>))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CoreHoC(EmployeePenaltyDetail);
