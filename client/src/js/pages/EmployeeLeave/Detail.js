import React from "react";
import update from "immutability-helper";
import PropTypes from "prop-types";
import { FieldFeedbacks, FieldFeedback } from "react-form-with-constraints";
import FormValidation from "~/components/form/FormValidation";
import InputText from "~/components/form/InputText";
import Select from "~/components/form/Select";
import DatePicker from "~/components/form/DatePicker";
import BrowseData from "~/components/form/BrowseData";
import CoreHoC from "../CoreHoC";
import {
  getEmployees, createEmployeeLeave, updateEmployeeLeave, getEmployeeLeave,
} from "../../data";
import Provisions from "./components/Provisions";
import Copies from "./components/Copies";

const employeeColumns = [
  {
    accessor: "fullname",
    Header: "Nama Pegawai",
    width: "100%",
  },
];
const provisionsInvalid = (val) => {
  const parsedValue = JSON.parse(val);

  if (parsedValue.length > 0) {
    return parsedValue.some((x) => ((String(x.point).length === 0) || (String(x.note).length === 0)))
  }

  return false;
}
const copiesInvalid = (val) => {
  const parsedValue = JSON.parse(val);

  if (parsedValue.length > 0) {
    return parsedValue.some((x) => (String(x).length === 0));
  }

  return false;
}
class EmployeeLeaveDetail extends React.Component {
  static propTypes = {
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    assignButtons: () => { },
    showNotification: () => { },
  }

  initialButtonActions = [
    {
      id: "1",
      type: null,
      content: (
        <span>
          Batal
        </span>
      ),
      action: () => this.gotoBasePath(),
      isDisabled: false,
    },
    {
      id: "2",
      type: "primary",
      content: (
        <span>
          Simpan
        </span>
      ),
      action: () => this.saveDataHandler(),
      isDisabled: false,
    },
  ];

  constructor(props) {
    super(props);

    this.state = {
      id: "",
      isFormSubmitted: false,
      type: "create",
      penaltyTypeList: [],
      form: {
        employee: {
          id: "",
          name: "",
        },
        leave_number: "",
        leave_letter_date: moment().format("YYYY-MM-DD"),
        leave_type: "",
        start_date: moment().format("YYYY-MM-DD"),
        end_date: moment().format("YYYY-MM-DD"),
        length_of_leave: "",
        provisions: [
          {
            point: 'a.',
            note: '',
          },
        ],
        copies: [""],
      },
      footerButtons: this.initialButtonActions,
    };
  }

  componentWillMount = () => {
    this.setupData();
  }

  setupData = async () => {
    const { match: { params } } = this.props;

    if (params.type === "edit") {
      await this.setupDetailData(params.id);
    } else {
      this.setupBreadcrumbs("Tambah Data");
    }
  }

  setupBreadcrumbs = (text) => {
    const { assignBreadcrumbs } = this.props;

    assignBreadcrumbs([
      {
        label: "App", link: "#",
      },
      {
        label: "Pegawai", link: "#",
      },
      {
        label: "Kepegawaian", link: "#",
      },
      {
        label: "Cuti", link: "/kepegawaian-cuti",
      },
      text,
    ]);
  }

  setupDetailData = async (id) => {
    const res = await getEmployeeLeave(id);

    if (res.status) {
      const { data } = res;

      const newState = {
        id: data.id,
        type: "edit",
        form: {
          employee: {
            id: data.Employee.id,
            name: data.Employee.fullname,
          },
          leave_number: data.leave_number,
          leave_letter_date: moment(data.leave_letter_date).format("YYYY-MM-DD"),
          leave_type: data.leave_type,
          start_date: moment(data.start_date).format("YYYY-MM-DD"),
          end_date: moment(data.end_date).format("YYYY-MM-DD"),
          length_of_leave: data.length_of_leave,
          provisions: JSON.parse(data.provisions),
          copies: JSON.parse(data.copies),
        },
      };

      this.setupBreadcrumbs(data.Employee.fullname);
      this.setState(newState);
    }
  }

  onFetchEmployee = async (state) => {
    const payload = {
      limit: 10,
      page: state.page,
      filterText: state.filterText,
      sorted: JSON.stringify({ id: state.fieldToSort, desc: state.sortType === "desc" }),
    };
    const res = await getEmployees(payload);

    return res;
  }

  changeValueHandler = async (type, val, e) => {
    const { form } = this.state;

    const { isFormSubmitted } = this.state;

    if (isFormSubmitted && e) {
      await this.onInputChangeValidate(e);
    }

    const newValue = update(form, {
      [type]: { $set: val },
    });

    this.setState({ form: newValue }, () => {
      if (type === "provisions") {
        this.validateProvisions();
      } else if (type === "copies") {
        this.validateCopies();
      }
    });
  }

  onInputChangeValidate = ({ target }) => {
    this.form.validateInput(target);

    if (this.inputTimeout) {
      clearTimeout(this.inputTimeout);
    }

    this.inputTimeout = setTimeout(() => {
      if (this.form.simpleValidateForm()) {
        this.updateButtonsState();
      } else {
        this.updateButtonsState(false, true);
      }
    }, 300);
  }

  updateButtonsState = (isBtnCancelDisabled = false, isBtnSaveDisabled = false) => {
    const footerButtons = update(this.initialButtonActions, {
      0: { isDisabled: { $set: isBtnCancelDisabled } },
      1: { isDisabled: { $set: isBtnSaveDisabled } },
    });

    this.setState({ footerButtons });
  }

  changeEmployeeHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      employee: { $set: { id: val.id, name: val.fullname } },
    });

    this.setState({ form: newValue }, () => {
      this.validateEmployee();
    });
  }

  validateEmployee = async () => {
    const { isFormSubmitted } = this.state;

    if (isFormSubmitted) {
      await this.onInputChangeValidate({ target: this.employeeHidden });
    }
  }

  validateProvisions = async () => {
    const { isFormSubmitted } = this.state;

    if (isFormSubmitted) {
      await this.onInputChangeValidate({ target: this.provisionsHidden });
    }
  }

  validateCopies = async () => {
    const { isFormSubmitted } = this.state;

    if (isFormSubmitted) {
      await this.onInputChangeValidate({ target: this.copiesHidden });
    }
  }

  employeeConditionalRender = (type) => {
    const { form } = this.state;

    if (type === "edit") {
      return (
        <div className="row mb-sm">
          <div className="col-sm-12">
            <InputText
              label="Jenis Cuti"
              changeEvent={(val, e) => this.changeValueHandler("leave_type", val, e)}
              value={String(form.leave_type)}
              name="leave_type"
              required
            />
            <FieldFeedbacks for="leave_type">
              <FieldFeedback when="valueMissing">Jenis cuti wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
      );
    }

    return (
      <div className="row mb-sm">
        <div className="col-sm-6">
          <div className="form-group position-relative mb-0">
            <BrowseData
              label="Pegawai"
              placeholder="Pilih Pegawai"
              columns={employeeColumns}
              value={form.employee}
              changeEvent={this.changeEmployeeHandler}
              onFetch={this.onFetchEmployee}
            />
            <input
              ref={(c) => { this.employeeHidden = c; }}
              className="hide-for-validation"
              name="employeeHidden"
              type="text"
              value={form.employee.id}
              onChange={() => { }}
              required
            />
          </div>
          <FieldFeedbacks for="employeeHidden">
            <FieldFeedback when="valueMissing">Pegawai wajib diisi</FieldFeedback>
          </FieldFeedbacks>
        </div>
        <div className="col-sm-6">
          <InputText
            label="Jenis Cuti"
            changeEvent={(val, e) => this.changeValueHandler("leave_type", val, e)}
            value={String(form.leave_type)}
            name="leave_type"
            required
          />
          <FieldFeedbacks for="leave_type">
            <FieldFeedback when="valueMissing">Jenis cuti wajib diisi</FieldFeedback>
          </FieldFeedbacks>
        </div>
      </div>
    );
  }

  formComponent = () => {
    const {
      form, type,
    } = this.state;
    return (
      <div>
        <div className="row mb-sm">
          <div className="col-sm-6">
            <InputText
              label="Nomor Surat Cuti"
              changeEvent={(val, e) => this.changeValueHandler("leave_number", val, e)}
              value={String(form.leave_number)}
              name="leave_number"
              required
            />
            <FieldFeedbacks for="leave_number">
              <FieldFeedback when="valueMissing">Nomor Surat Cuti wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-6">
            <DatePicker
              label="Tanggal Surat Cuti"
              id="leave-letter-date"
              changeEvent={val => this.changeValueHandler("leave_letter_date", val)}
              value={form.leave_letter_date}
            />
          </div>
        </div>

        {this.employeeConditionalRender(type)}

        <div className="row mb-sm">
          <div className="col-sm-6">
            <DatePicker
              label="Tanggal Mulai Cuti"
              id="start-date"
              changeEvent={val => this.changeValueHandler("start_date", val)}
              value={form.start_date}
            />
          </div>
          <div className="col-sm-6">
            <DatePicker
              label="Tanggal Selesai Cuti"
              id="end-date"
              changeEvent={val => this.changeValueHandler("end_date", val)}
              value={form.end_date}
            />
          </div>
        </div>

        <div className="row mb-sm">
          <div className="col-sm-12">
            <div className="form-group position-relative mb-0">
              <Provisions
                data={form.provisions}
                changeEvent={val => this.changeValueHandler("provisions", val)}
              />
              <input
                ref={(c) => { this.provisionsHidden = c; }}
                className="hide-for-validation"
                name="provisionsHidden"
                type="text"
                value={JSON.stringify(form.provisions)}
                onChange={() => { }}
              />
            </div>
            <FieldFeedbacks for="provisionsHidden">
              <FieldFeedback when={(val) => provisionsInvalid(val)}>Mohon lengkapi ketentuan</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
      </div>
    );
  }

  gotoBasePath = () => {
    const { history } = this.props;
    history.push("/kepegawaian-cuti");
  }

  saveDataHandler = async () => {
    const {
      form, type, id,
    } = this.state;

    this.updateButtonsState(true, true);

    const isFormValid = await this.form.validateForm();

    if (isFormValid) {
      const lengthLeave = moment(form.end_date, "YYYY-MM-DD").diff(moment(form.start_date, "YYYY-MM-DD"), 'days');
      const payload = Object.assign({}, form, {
        id_employee: form.employee.id,
        provisions: JSON.stringify(form.provisions),
        copies: JSON.stringify(form.copies),
        length_of_leave: parseInt(lengthLeave),
      });
      delete payload.employee;

      if (type === "create") {
        const res = await createEmployeeLeave(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      } else {
        Object.assign(payload, { id });
        const res = await updateEmployeeLeave(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      }
    } else {
      this.updateButtonsState(false, true);
    }

    this.setState({
      isFormSubmitted: true,
    });
  }

  render = () => {
    const {
      footerButtons, type, form,
    } = this.state;

    return (
      <div style={{ width: "100%", position: "relative", margin: "0px auto" }}>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-12">
                <h3 className="box-title with-title">{type === "create" ? "Tambah" : "Edit"} Kepegawaian Cuti</h3>
              </div>
            </div>
          </div>
          <div className="box-body">
            <FormValidation ref={(c) => { this.form = c; }}>
              <div className="row">
                <div className="col-sm-7">
                  {this.formComponent()}
                </div>
                <div className="col-sm-5">
                  <div className="form-group position-relative mb-0">
                    <Copies
                      data={form.copies}
                      changeEvent={val => this.changeValueHandler("copies", val)}
                    />
                    <input
                      ref={(c) => { this.copiesHidden = c; }}
                      className="hide-for-validation"
                      name="copiesHidden"
                      type="text"
                      value={JSON.stringify(form.copies)}
                      onChange={() => { }}
                    />
                  </div>
                  <FieldFeedbacks for="copiesHidden">
                    <FieldFeedback when={(val) => copiesInvalid(val)}>Mohon lengkapi tembusan</FieldFeedback>
                  </FieldFeedbacks>
                </div>
              </div>
            </FormValidation>
          </div>
          <div className="box-footer text-right">
            <div>
              {footerButtons.map((x, i) => (<button key={x.id} style={(i === footerButtons.length - 1) ? {} : { marginRight: "10px" }} type="button" className={`btn ${x.type ? `btn-${x.type}` : ""}`} onClick={!x.isDisabled ? x.action : () => { }} disabled={x.isDisabled}>{x.content}</button>))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CoreHoC(EmployeeLeaveDetail);
