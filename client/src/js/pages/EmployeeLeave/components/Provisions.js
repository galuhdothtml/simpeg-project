/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import update from "immutability-helper";
import InputText from "~/components/form/InputText";

import "./styles.scss";

class Provisions extends React.Component {
    static propTypes = {
      data: PropTypes.arrayOf(PropTypes.shape({
        point: PropTypes.string,
        note: PropTypes.string,
      })).isRequired,
      changeEvent: PropTypes.func,
    }

    static defaultProps = {
      changeEvent: () => { },
    }

    changeValueHandler = async (type, val, index) => {
      const { data, changeEvent } = this.props;

      const newValue = update(data, {
        [index]: {
          [type]: { $set: val },
        },
      });

      changeEvent(newValue);
    }

    deleteRowHandler = (index) => {
      const { data, changeEvent } = this.props;
      const newData = data.filter((x, i) => (i !== index));

      changeEvent(newData);
    }

    addRowHandler = () => {
      const { data, changeEvent } = this.props;
      const newData = [...data, {
        point: `${(data.length + 10).toString(36)}.`,
        note: "",
      }];

      changeEvent(newData);
    }

    renderRows = () => {
      const { data } = this.props;
      let retval = [
        (
          <div className="employee-leave-empty-placeholder">
            Tidak ada ketentuan
          </div>
        ),
      ];

      if (data.length > 0) {
        retval = data.map((x, i) => (
              <div className="form-group" key={i}>
                  <label>{`Ketentuan ${(i + 10).toString(36).toUpperCase()}`}</label>
                  <div className="row">
                      <div className="col-sm-2">
                          <InputText
                              changeEvent={(val) => { this.changeValueHandler("point", val, i); }}
                              value={String(x.point)}
                          />
                      </div>
                      <div className="col-sm-10">
                          <div style={{
                            display: "inline-block",
                            width: "93%",
                            marginRight: "2%",
                          }}>
                              <InputText
                                  changeEvent={(val) => { this.changeValueHandler("note", val, i); }}
                                  value={String(x.note)}
                              />
                          </div>
                          <div style={{
                            display: "inline-block",
                            width: "5%",
                          }}>
                              <button type="button" className="btn-delete-row" onClick={() => this.deleteRowHandler(i)}><i className="fa fa-times-circle" /></button>
                          </div>
                      </div>
                  </div>
              </div>
        ));
      }

      return retval;
    }

    render() {
      return (
            <div>
                <p className="lead mb-xs">Ketentuan: </p>
                <div>
                    {this.renderRows()}
                </div>
                <button onClick={this.addRowHandler} type="button" className="btn btn-default btn-block"><i className="fa fa-plus-square-o" />Tambah Ketentuan</button>
            </div>
      );
    }
}

export default Provisions;
