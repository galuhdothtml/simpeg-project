/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const style = {
  contentWrapper: {
    minHeight: `${(window.innerHeight - 52)}px`,
  },
};
const CoreHoC = (WrappedComponent) => {
  class InnerHoC extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        title: "Halaman Dashboard",
        buttons: [],
        breadcrumbData: [],
      };
    }

    assignButtons = (buttons) => {
      this.setState({ buttons });
    }

    assignBreadcrumbs = (breadcrumbData) => {
      this.setState({ breadcrumbData });
    }

    showNotification = ({ type, msg }) => {
      if (type === "success") {
        toast.success(msg);
      } else if (type === "error") {
        toast.error(msg);
      }
    }

    setupBreadcrumb = () => {
      const { breadcrumbData } = this.state;

      if (breadcrumbData.length > 0) {
        const breadcrumbs = breadcrumbData.map((x) => {
          const isObject = typeof x === "object";

          if (isObject) {
            return (<li key={x.label}><a href={x.link}>{x.label}</a></li>);
          }

          return (<li className="active" key={x}>{x}</li>);
        });

        return (
          <ol className="breadcrumb">
            {breadcrumbs}
          </ol>
        );
      }

      return null;
    }

    render() {
      const { buttons } = this.state;

      return (
        <div className="content-wrapper" style={style.contentWrapper}>
          <section className="content-header" style={buttons.length > 0 ? { minHeight: "47px" } : {}}>
            <div className="component-wrapper">
              <div className="item"></div>
              {buttons.map(x => (<div key={x.id} className="item">
                <button className="btn btn-primary" type="button" onClick={x.clickEvent}><i className={x.icon} /> {x.title}</button>
              </div>))}
            </div>
            {this.setupBreadcrumb()}
          </section>
          <section className="content">
            <WrappedComponent
              {...this.props} showNotification={this.showNotification}
              assignButtons={this.assignButtons}
              assignBreadcrumbs={this.assignBreadcrumbs}
            />
          </section>
          <ToastContainer autoClose={3000} />
        </div>
      );
    }
  }

  const mapStateToProps = state => ({
    appMenu: state.appMenu,
  });

  return connect(mapStateToProps)(InnerHoC);
};

export default CoreHoC;
