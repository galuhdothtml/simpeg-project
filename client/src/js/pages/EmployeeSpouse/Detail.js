import React from "react";
import update from "immutability-helper";
import PropTypes from "prop-types";
import { FieldFeedbacks, FieldFeedback } from "react-form-with-constraints";
import FormValidation from "~/components/form/FormValidation";
import InputText from "~/components/form/InputText";
import Select from "~/components/form/Select";
import DatePicker from "~/components/form/DatePicker";
import BrowseData from "~/components/form/BrowseData";
import CoreHoC from "../CoreHoC";
import {
  getEmployees, getEducationLevels, createEmployeeSpouse, updateEmployeeSpouse, getEmployeeSpouse,
} from "../../data";

const employeeColumns = [
  {
    accessor: "fullname",
    Header: "Nama Pegawai",
    width: "100%",
  },
];
class EmployeeSpouseDetail extends React.Component {
  static propTypes = {
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    assignButtons: () => { },
    showNotification: () => { },
  }

  initialButtonActions = [
    {
      id: "1",
      type: null,
      content: (
        <span>
          Batal
        </span>
      ),
      action: () => this.gotoBasePath(),
      isDisabled: false,
    },
    {
      id: "2",
      type: "primary",
      content: (
        <span>
          Simpan
        </span>
      ),
      action: () => this.saveDataHandler(),
      isDisabled: false,
    },
  ];

  constructor(props) {
    super(props);

    this.state = {
      id: "",
      isFormSubmitted: false,
      type: "create",
      form: {
        employee: {
          id: "",
          name: "",
        },
        nik: "",
        fullname: "",
        birth_place: "",
        birth_date: moment().subtract(18, "years").format("YYYY-MM-DD"),
        id_education_level: "",
        job: "",
        status: "",
      },
      educationLevelList: [],
      statusList: [
        {
          id: "", name: "- Pilih Status Hubungan -",
        },
        {
          id: "SU", name: "Suami",
        },
        {
          id: "IS", name: "Istri",
        },
      ],
      footerButtons: this.initialButtonActions,
    };
  }

  componentWillMount = () => {
    this.setupData();
  }

  setupData = async () => {
    const { match: { params } } = this.props;

    await this.fetchDropdownData();

    if (params.type === "edit") {
      await this.setupDetailData(params.id);
    } else {
      this.setupBreadcrumbs("Tambah Data");
    }
  }

  setupBreadcrumbs = (text) => {
    const { assignBreadcrumbs } = this.props;

    assignBreadcrumbs([
      {
        label: "App", link: "#",
      },
      {
        label: "Pegawai", link: "#",
      },
      {
        label: "Riwayat Keluarga", link: "#",
      },
      {
        label: "Suami / Istri", link: "/keluarga-suami-istri",
      },
      text,
    ]);
  }

  setupDetailData = async (id) => {
    const res = await getEmployeeSpouse(id);

    if (res.status) {
      const { data } = res;
      const newState = {
        id: data.id,
        type: "edit",
        form: {
          employee: {
            id: data.Employee.id,
            name: data.Employee.fullname,
          },
          nik: data.nik,
          fullname: data.fullname,
          birth_place: data.birth_place,
          birth_date: moment(data.birth_date, "YYYY-MM-DD").format("YYYY-MM-DD"),
          id_education_level: data.id_education_level,
          job: data.job,
          status: data.status,
        },
      };

      this.setupBreadcrumbs(data.Employee.fullname);
      this.setState(newState);
    }
  }

  fetchDropdownData = async () => {
    await this.fetchEducationLevels();
  }

  fetchEducationLevels = async () => {
    const res = await getEducationLevels({ limit: 15 });

    if (res.status) {
      const { data } = res;
      const newData = [{ id: "", name: "- Pilih Pendidikan -" }, ...data.map(x => ({ id: x.id, name: x.name }))];

      this.setState({ educationLevelList: newData });
    }
  }

  changeValueHandler = async (type, val, e) => {
    const { form, isFormSubmitted } = this.state;

    if (isFormSubmitted && e) {
      await this.onInputChangeValidate(e);
    }

    const newValue = update(form, {
      [type]: { $set: val },
    });

    this.setState({ form: newValue });
  }

  validateEmployee = async () => {
    const { isFormSubmitted } = this.state;

    if (isFormSubmitted) {
      await this.onInputChangeValidate({ target: this.employeeHidden });
    }
  }

  changeEmployeeHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      employee: { $set: { id: val.id, name: val.fullname } },
    });

    this.setState({ form: newValue }, () => {
      this.validateEmployee();
    });
  }

  onInputChangeValidate = ({ target }) => {
    this.form.validateInput(target);

    if (this.inputTimeout) {
      clearTimeout(this.inputTimeout);
    }

    this.inputTimeout = setTimeout(() => {
      if (this.form.simpleValidateForm()) {
        this.updateButtonsState();
      } else {
        this.updateButtonsState(false, true);
      }
    }, 300);
  }

  updateButtonsState = (isBtnCancelDisabled = false, isBtnSaveDisabled = false) => {
    const footerButtons = update(this.initialButtonActions, {
      0: { isDisabled: { $set: isBtnCancelDisabled } },
      1: { isDisabled: { $set: isBtnSaveDisabled } },
    });

    this.setState({ footerButtons });
  }

  onFetchEmployee = async (state) => {
    const payload = {
      limit: 10,
      page: state.page,
      filterText: state.filterText,
      sorted: JSON.stringify({ id: state.fieldToSort, desc: state.sortType === "desc" }),
    };
    const res = await getEmployees(payload);

    return res;
  }

  renderChooseEmployee = () => {
    const { form, type } = this.state;

    if (type === "create") {
      return (
        <div className="row mb-sm">
            <div className="col-sm-6">
              <div className="form-group position-relative mb-0">
                <BrowseData
                  label="Pegawai"
                  placeholder="Pilih Pegawai"
                  columns={employeeColumns}
                  value={form.employee}
                  changeEvent={this.changeEmployeeHandler}
                  onFetch={this.onFetchEmployee}
                />
                <input
                  ref={(c) => { this.employeeHidden = c; }}
                  className="hide-for-validation"
                  name="employeeHidden"
                  type="text"
                  value={form.employee.id}
                  onChange={() => { }}
                  required
                />
              </div>
              <FieldFeedbacks for="employeeHidden">
                <FieldFeedback when="valueMissing">Pegawai wajib diisi</FieldFeedback>
              </FieldFeedbacks>
            </div>
            <div className="col-sm-6">
              <InputText
                label="Nama Lengkap *"
                changeEvent={(val, e) => this.changeValueHandler("fullname", val, e)}
                value={String(form.fullname)}
                name="fullname"
                required
              />
              <FieldFeedbacks for="fullname">
                <FieldFeedback when="valueMissing">Nama lengkap wajib diisi</FieldFeedback>
              </FieldFeedbacks>
            </div>
          </div>
      );
    }

    return (
      <div className="row mb-sm">
        <div className="col-sm-12">
          <InputText
            label="Nama Lengkap *"
            changeEvent={(val, e) => this.changeValueHandler("fullname", val, e)}
            value={String(form.fullname)}
            name="fullname"
            required
          />
          <FieldFeedbacks for="fullname">
            <FieldFeedback when="valueMissing">Nama lengkap wajib diisi</FieldFeedback>
          </FieldFeedbacks>
        </div>
      </div>
    );
  }

  formComponent = () => {
    const {
      form, educationLevelList, statusList,
    } = this.state;
    return (
      <FormValidation ref={(c) => { this.form = c; }}>
        {this.renderChooseEmployee()}
        <div className="row mb-sm">
          <div className="col-sm-4">
            <InputText
              label="NIK *"
              changeEvent={(val, e) => this.changeValueHandler("nik", val, e)}
              value={String(form.nik)}
              name="nik"
              required
            />
            <FieldFeedbacks for="nik">
              <FieldFeedback when="valueMissing">NIK wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <InputText
              label="Tempat Lahir"
              changeEvent={(val, e) => this.changeValueHandler("birth_place", val, e)}
              value={String(form.birth_place)}
              name="birth_place"
              required
            />
            <FieldFeedbacks for="birth_place">
              <FieldFeedback when="valueMissing">Tempat lahir wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <DatePicker
              label="Tanggal Lahir"
              id="birth-date"
              changeEvent={val => this.changeValueHandler("birth_date", val)}
              value={form.birth_date}
            />
          </div>
        </div>
        <div className="row mb-sm">
          <div className="col-sm-4">
            <Select
              label="Pendidikan"
              data={educationLevelList}
              value={form.id_education_level}
              changeEvent={(val, e) => this.changeValueHandler("id_education_level", val, e)}
              name="id_education_level"
              required
            />
            <FieldFeedbacks for="id_education_level">
              <FieldFeedback when="valueMissing">Pendidikan wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <InputText
              label="Pekerjaan"
              changeEvent={(val, e) => this.changeValueHandler("job", val, e)}
              value={String(form.job)}
              name="job"
            />
          </div>
          <div className="col-sm-4">
            <Select
              label="Status Hubungan"
              data={statusList}
              value={form.status}
              changeEvent={(val, e) => this.changeValueHandler("status", val, e)}
              name="status"
              required
            />
            <FieldFeedbacks for="status">
              <FieldFeedback when="valueMissing">Status hubungan wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
      </FormValidation>
    );
  }

  gotoBasePath = () => {
    const { history } = this.props;
    history.push("/keluarga-suami-istri");
  }

  saveDataHandler = async () => {
    const {
      form, type, id,
    } = this.state;
    this.updateButtonsState(true, true);

    const isFormValid = await this.form.validateForm();

    if (isFormValid) {
      const payload = Object.assign({}, form, {
        id_employee: form.employee.id,
      });
      delete payload.employee;

      if (type === "create") {
        const res = await createEmployeeSpouse(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      } else {
        Object.assign(payload, { id });
        const res = await updateEmployeeSpouse(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      }
    } else {
      this.updateButtonsState(false, true);
    }

    this.setState({
      isFormSubmitted: true,
    });
  }

  render = () => {
    const {
      footerButtons, type,
    } = this.state;

    return (
      <div style={{ width: "80%", position: "relative", margin: "0px auto" }}>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-12">
                <h3 className="box-title with-title">{type === "create" ? "Tambah" : "Edit"} Data Suami/Istri</h3>
              </div>
            </div>
          </div>
          <div className="box-body">
            {this.formComponent()}
          </div>
          <div className="box-footer text-right">
            <div>
              {footerButtons.map((x, i) => (<button key={x.id} style={(i === footerButtons.length - 1) ? {} : { marginRight: "10px" }} type="button" className={`btn ${x.type ? `btn-${x.type}` : ""}`} onClick={!x.isDisabled ? x.action : () => { }} disabled={x.isDisabled}>{x.content}</button>))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CoreHoC(EmployeeSpouseDetail);
