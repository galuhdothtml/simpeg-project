import React from "react";
import update from "immutability-helper";
import PropTypes from "prop-types";
import { FieldFeedbacks, FieldFeedback } from "react-form-with-constraints";
import FormValidation from "~/components/form/FormValidation";
import InputText from "~/components/form/InputText";
import Select from "~/components/form/Select";
import DatePicker from "~/components/form/DatePicker";
import BrowseData from "~/components/form/BrowseData";
import CoreHoC from "../CoreHoC";
import {
  getEmployees, createEmployeeMutation, updateEmployeeMutation, getEmployeeMutation,
} from "../../data";

const mutationTypeList = [
  {
    id: "",
    name: "- Pilih Jenis Mutasi -",
  },
  {
    id: "1",
    name: "Masuk",
  },
  {
    id: "2",
    name: "Keluar",
  },
  {
    id: "3",
    name: "Pindah Antar Instansi",
  },
  {
    id: "4",
    name: "Pensiun",
  },
  {
    id: "5",
    name: "Wafat",
  },
  {
    id: "6",
    name: "Kenaikan Pangkat",
  },
];
const employeeColumns = [
  {
    accessor: "fullname",
    Header: "Nama Pegawai",
    width: "100%",
  },
];
class EmployeeMutationDetail extends React.Component {
  static propTypes = {
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    assignButtons: () => { },
    showNotification: () => { },
  }

  initialButtonActions = [
    {
      id: "1",
      type: null,
      content: (
        <span>
          Batal
        </span>
      ),
      action: () => this.gotoBasePath(),
      isDisabled: false,
    },
    {
      id: "2",
      type: "primary",
      content: (
        <span>
          Simpan
        </span>
      ),
      action: () => this.saveDataHandler(),
      isDisabled: false,
    },
  ];

  constructor(props) {
    super(props);

    this.state = {
      id: "",
      isFormSubmitted: false,
      type: "create",
      penaltyTypeList: [],
      form: {
        employee: {
          id: "",
          name: "",
        },
        mutation_type: "",
        sk_number: "",
        mutation_date: moment().format("YYYY-MM-DD"),
      },
      footerButtons: this.initialButtonActions,
    };
  }

  componentWillMount = () => {
    this.setupData();
  }

  setupData = async () => {
    const { match: { params } } = this.props;

    if (params.type === "edit") {
      await this.setupDetailData(params.id);
    } else {
      this.setupBreadcrumbs("Tambah Data");
    }
  }

  setupBreadcrumbs = (text) => {
    const { assignBreadcrumbs } = this.props;

    assignBreadcrumbs([
      {
        label: "App", link: "#",
      },
      {
        label: "Pegawai", link: "#",
      },
      {
        label: "Mutasi", link: "/mutasi",
      },
      text,
    ]);
  }

  setupDetailData = async (id) => {
    const res = await getEmployeeMutation(id);

    if (res.status) {
      const { data } = res;

      const newState = {
        id: data.id,
        type: "edit",
        form: {
          employee: {
            id: data.Employee.id,
            name: data.Employee.fullname,
          },
          mutation_type: data.mutation_type,
          sk_number: data.sk_number,
          mutation_date: moment(data.mutation_date).format("YYYY-MM-DD"),
        },
      };

      this.setupBreadcrumbs(data.Employee.fullname);
      this.setState(newState);
    }
  }

  onFetchEmployee = async (state) => {
    const payload = {
      limit: 10,
      page: state.page,
      filterText: state.filterText,
      sorted: JSON.stringify({ id: state.fieldToSort, desc: state.sortType === "desc" }),
    };
    const res = await getEmployees(payload);

    return res;
  }

  changeValueHandler = async (type, val, e) => {
    const { form } = this.state;

    const { isFormSubmitted } = this.state;

    if (isFormSubmitted && e) {
      await this.onInputChangeValidate(e);
    }

    const newValue = update(form, {
      [type]: { $set: val },
    });

    this.setState({ form: newValue });
  }

  onInputChangeValidate = ({ target }) => {
    this.form.validateInput(target);

    if (this.inputTimeout) {
      clearTimeout(this.inputTimeout);
    }

    this.inputTimeout = setTimeout(() => {
      if (this.form.simpleValidateForm()) {
        this.updateButtonsState();
      } else {
        this.updateButtonsState(false, true);
      }
    }, 300);
  }

  updateButtonsState = (isBtnCancelDisabled = false, isBtnSaveDisabled = false) => {
    const footerButtons = update(this.initialButtonActions, {
      0: { isDisabled: { $set: isBtnCancelDisabled } },
      1: { isDisabled: { $set: isBtnSaveDisabled } },
    });

    this.setState({ footerButtons });
  }

  changeEmployeeHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      employee: { $set: { id: val.id, name: val.fullname } },
    });

    this.setState({ form: newValue }, () => {
      this.validateEmployee();
    });
  }

  validateEmployee = async () => {
    const { isFormSubmitted } = this.state;

    if (isFormSubmitted) {
      await this.onInputChangeValidate({ target: this.employeeHidden });
    }
  }

  employeeConditionalRender = (type) => {
    if (type === "edit") {
      return this.formEdit();
    }

    return this.formCreate();
  }

  formEdit = () => {
    const {
      form,
    } = this.state;
    return (
      <FormValidation ref={(c) => { this.form = c; }}>
        <div className="row mb-sm">
          <div className="col-sm-4">
            <Select
                label="Jenis Mutasi"
                name="mutation_type"
                data={mutationTypeList}
                value={form.mutation_type}
                changeEvent={(val, e) => this.changeValueHandler("mutation_type", val, e)}
                required
            />
            <FieldFeedbacks for="mutation_type">
              <FieldFeedback when="valueMissing">Jenis mutasi wajib dipilih</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <InputText
              label="Nomor SK"
              changeEvent={(val, e) => this.changeValueHandler("sk_number", val, e)}
              value={String(form.sk_number)}
              name="sk_number"
              required
            />
            <FieldFeedbacks for="sk_number">
              <FieldFeedback when="valueMissing">Nomor SK wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <DatePicker
              label="Tanggal Mutasi"
              id="mutation-date"
              changeEvent={val => this.changeValueHandler("mutation_date", val)}
              value={form.mutation_date}
            />
          </div>
        </div>
      </FormValidation>
    );
  }

  formCreate = () => {
    const {
      form,
    } = this.state;
    return (
      <FormValidation ref={(c) => { this.form = c; }}>
        <div className="row mb-sm">
          <div className="col-sm-6">
            <div className="form-group position-relative mb-0">
              <BrowseData
                label="Pegawai"
                placeholder="Pilih Pegawai"
                columns={employeeColumns}
                value={form.employee}
                changeEvent={this.changeEmployeeHandler}
                onFetch={this.onFetchEmployee}
              />
              <input
                ref={(c) => { this.employeeHidden = c; }}
                className="hide-for-validation"
                name="employeeHidden"
                type="text"
                value={form.employee.id}
                onChange={() => { }}
                required
              />
            </div>
            <FieldFeedbacks for="employeeHidden">
              <FieldFeedback when="valueMissing">Pegawai wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-6">
            <Select
                label="Jenis Mutasi"
                name="mutation_type"
                data={mutationTypeList}
                value={form.mutation_type}
                changeEvent={(val, e) => this.changeValueHandler("mutation_type", val, e)}
                required
            />
            <FieldFeedbacks for="mutation_type">
              <FieldFeedback when="valueMissing">Jenis mutasi wajib dipilih</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
        <div className="row mb-sm">
          <div className="col-sm-6">
            <InputText
              label="Nomor SK"
              changeEvent={(val, e) => this.changeValueHandler("sk_number", val, e)}
              value={String(form.sk_number)}
              name="sk_number"
              required
            />
            <FieldFeedbacks for="sk_number">
              <FieldFeedback when="valueMissing">Nomor SK wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-6">
            <DatePicker
              label="Tanggal Mutasi"
              id="mutation-date"
              changeEvent={val => this.changeValueHandler("mutation_date", val)}
              value={form.mutation_date}
            />
          </div>
        </div>

      </FormValidation>
    );
  }

  gotoBasePath = () => {
    const { history } = this.props;
    history.push("/mutasi");
  }

  saveDataHandler = async () => {
    const {
      form, type, id,
    } = this.state;
    this.updateButtonsState(true, true);

    const isFormValid = await this.form.validateForm();

    if (isFormValid) {
      const payload = Object.assign({}, form, {
        id_employee: form.employee.id,
      });
      delete payload.employee;

      if (type === "create") {
        const res = await createEmployeeMutation(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      } else {
        Object.assign(payload, { id });
        const res = await updateEmployeeMutation(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      }
    } else {
      this.updateButtonsState(false, true);
    }

    this.setState({
      isFormSubmitted: true,
    });
  }

  render = () => {
    const {
      footerButtons, type,
    } = this.state;

    return (
      <div style={{ width: "80%", position: "relative", margin: "0px auto" }}>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-12">
                <h3 className="box-title with-title">{type === "create" ? "Tambah" : "Edit"} Mutasi Pegawai</h3>
              </div>
            </div>
          </div>
          <div className="box-body">
            {this.employeeConditionalRender(type)}
          </div>
          <div className="box-footer text-right">
            <div>
              {footerButtons.map((x, i) => (<button key={x.id} style={(i === footerButtons.length - 1) ? {} : { marginRight: "10px" }} type="button" className={`btn ${x.type ? `btn-${x.type}` : ""}`} onClick={!x.isDisabled ? x.action : () => { }} disabled={x.isDisabled}>{x.content}</button>))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CoreHoC(EmployeeMutationDetail);
