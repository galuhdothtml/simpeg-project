import React from "react";
import update from "immutability-helper";
import PropTypes from "prop-types";
import { FieldFeedbacks, FieldFeedback } from "react-form-with-constraints";
import FormValidation from "~/components/form/FormValidation";
import InputText from "~/components/form/InputText";
import Select from "~/components/form/Select";
import DatePicker from "~/components/form/DatePicker";
import FileUpload from "~/components/form/FileUpload";
import BrowseData from "~/components/form/BrowseData";
import CoreHoC from "../CoreHoC";
import {
  getEmployees, createEmployeeTraining, updateEmployeeTraining, getEmployeeTraining,
} from "../../data";

const employeeColumns = [
  {
    accessor: "fullname",
    Header: "Nama Pegawai",
    width: "100%",
  },
];
class EmployeeTrainingDetail extends React.Component {
  static propTypes = {
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    assignButtons: () => { },
    showNotification: () => { },
  }

  initialButtonActions = [
    {
      id: "1",
      type: null,
      content: (
        <span>
          Batal
        </span>
      ),
      action: () => this.gotoBasePath(),
      isDisabled: false,
    },
    {
      id: "2",
      type: "primary",
      content: (
        <span>
          Simpan
        </span>
      ),
      action: () => this.saveDataHandler(),
      isDisabled: false,
    },
  ];

  constructor(props) {
    super(props);

    this.state = {
      id: "",
      isFormSubmitted: false,
      type: "create",
      form: {
        employee: {
          id: "",
          name: "",
        },
        name: "",
        duration: "",
        organizer: "",
        place: "",
        generation: "",
        year: "",
        sttpp_number: "",
        sttpp_date: moment().format("YYYY-MM-DD"),
      },
      groupDir: {
        dirName: "",
        required: true,
        requiredMsg: "Pilih pegawai terlebih dahulu",
      },
      fileValue: null,
      footerButtons: this.initialButtonActions,
    };
  }

  componentWillMount = () => {
    this.setupData();
  }

  setupData = async () => {
    const { match: { params } } = this.props;

    if (params.type === "edit") {
      await this.setupDetailData(params.id);
    } else {
      this.setupBreadcrumbs("Tambah Data");
    }
  }

  setupBreadcrumbs = (text) => {
    const { assignBreadcrumbs } = this.props;

    assignBreadcrumbs([
      {
        label: "App", link: "#",
      },
      {
        label: "Pegawai", link: "#",
      },
      {
        label: "Kepegawaian", link: "#",
      },
      {
        label: "Diklat", link: "/kepegawaian-diklat",
      },
      text,
    ]);
  }

  setupDetailData = async (id) => {
    const res = await getEmployeeTraining(id);

    if (res.status) {
      const { data } = res;
      let fileValue = null;

      if (data.file) {
        const parsedFile = JSON.parse(data.file.filename);
        fileValue = {
          fileValue: parsedFile.filepath,
          fileType: parsedFile.filetype,
          fileName: parsedFile.filename,
        };
      }

      const newState = {
        id: data.id,
        type: "edit",
        form: {
          employee: {
            id: data.Employee.id,
            name: data.Employee.fullname,
          },
          name: data.name,
          duration: data.duration,
          organizer: data.organizer,
          place: data.place,
          generation: data.generation,
          year: data.year,
          sttpp_number: data.sttpp_number,
          sttpp_date: moment(data.sttpp_date).format("YYYY-MM-DD"),
        },
        groupDir: {
          dirName: String(data.Employee.id),
          required: true,
          requiredMsg: "Pilih pegawai terlebih dahulu",
        },
        fileValue,
      };

      this.setupBreadcrumbs(data.Employee.fullname);
      this.setState(newState);
    }
  }

  onFetchEmployee = async (state) => {
    const payload = {
      limit: 10,
      page: state.page,
      filterText: state.filterText,
      sorted: JSON.stringify({ id: state.fieldToSort, desc: state.sortType === "desc" }),
    };
    const res = await getEmployees(payload);

    return res;
  }

  changeValueHandler = async (type, val, e) => {
    const { form } = this.state;

    const { isFormSubmitted } = this.state;

    if (isFormSubmitted && e) {
      await this.onInputChangeValidate(e);
    }

    const newValue = update(form, {
      [type]: { $set: val },
    });

    this.setState({ form: newValue });
  }

  onInputChangeValidate = ({ target }) => {
    this.form.validateInput(target);

    if (this.inputTimeout) {
      clearTimeout(this.inputTimeout);
    }

    this.inputTimeout = setTimeout(() => {
      if (this.form.simpleValidateForm()) {
        this.updateButtonsState();
      } else {
        this.updateButtonsState(false, true);
      }
    }, 300);
  }

  updateButtonsState = (isBtnCancelDisabled = false, isBtnSaveDisabled = false) => {
    const footerButtons = update(this.initialButtonActions, {
      0: { isDisabled: { $set: isBtnCancelDisabled } },
      1: { isDisabled: { $set: isBtnSaveDisabled } },
    });

    this.setState({ footerButtons });
  }

  changeFileValueHandler = (val) => {
    this.setState({ fileValue: val });
  }

  changeGroupDir = (val) => {
    const { groupDir } = this.state;
    const newValue = update(groupDir, {
      dirName: { $set: String(val) },
    });

    this.setState({ groupDir: newValue });
  }

  changeEmployeeHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      employee: { $set: { id: val.id, name: val.fullname } },
    });

    this.setState({ form: newValue }, () => {
      this.validateEmployee();
      this.changeGroupDir(val.id);
    });
  }

  validateEmployee = async () => {
    const { isFormSubmitted } = this.state;

    if (isFormSubmitted) {
      await this.onInputChangeValidate({ target: this.employeeHidden });
    }
  }

  employeeConditionalRender = (type) => {
    const { form } = this.state;

    if (type === "edit") {
      return (
        <div className="row mb-sm">
          <div className="col-sm-6">
            <InputText
              label="Nama Diklat"
              changeEvent={(val, e) => this.changeValueHandler("name", val, e)}
              value={String(form.name)}
              name="name"
              required
            />
            <FieldFeedbacks for="name">
              <FieldFeedback when="valueMissing">Nama diklat wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-6">
            <InputText
              label="Jumlah Jam"
              changeEvent={(val, e) => this.changeValueHandler("duration", val, e)}
              value={String(form.duration)}
              name="duration"
              required
            />
            <FieldFeedbacks for="duration">
              <FieldFeedback when="valueMissing">Jumlah jam wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
      );
    }

    return (
      <div className="row mb-sm">
          <div className="col-sm-4">
            <div className="form-group position-relative mb-0">
              <BrowseData
                label="Pegawai"
                placeholder="Pilih Pegawai"
                columns={employeeColumns}
                value={form.employee}
                changeEvent={this.changeEmployeeHandler}
                onFetch={this.onFetchEmployee}
              />
              <input
                ref={(c) => { this.employeeHidden = c; }}
                className="hide-for-validation"
                name="employeeHidden"
                type="text"
                value={form.employee.id}
                onChange={() => { }}
                required
              />
            </div>
            <FieldFeedbacks for="employeeHidden">
              <FieldFeedback when="valueMissing">Pegawai wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <InputText
              label="Nama Diklat"
              changeEvent={(val, e) => this.changeValueHandler("name", val, e)}
              value={String(form.name)}
              name="name"
              required
            />
            <FieldFeedbacks for="name">
              <FieldFeedback when="valueMissing">Nama diklat wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <InputText
              label="Jumlah Jam"
              changeEvent={(val, e) => this.changeValueHandler("duration", val, e)}
              value={String(form.duration)}
              name="duration"
              required
            />
            <FieldFeedbacks for="duration">
              <FieldFeedback when="valueMissing">Jumlah jam wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
    );
  }

  formComponent = () => {
    const {
      form, type,
      fileValue, groupDir,
    } = this.state;
    return (
      <FormValidation ref={(c) => { this.form = c; }}>
        {this.employeeConditionalRender(type)}
        <div className="row mb-sm">
          <div className="col-sm-6">
            <InputText
              label="Penyelenggara"
              changeEvent={(val, e) => this.changeValueHandler("organizer", val, e)}
              value={String(form.organizer)}
              name="organizer"
              required
            />
            <FieldFeedbacks for="organizer">
              <FieldFeedback when="valueMissing">Penyelenggara wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-6">
            <InputText
              label="Tempat"
              changeEvent={(val, e) => this.changeValueHandler("place", val, e)}
              value={String(form.place)}
              name="place"
              required
            />
            <FieldFeedbacks for="place">
              <FieldFeedback when="valueMissing">Tempat wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>

        <div className="row mb-sm">
          <div className="col-sm-6">
            <InputText
              label="Angkatan"
              changeEvent={(val, e) => this.changeValueHandler("generation", val, e)}
              value={String(form.generation)}
              name="generation"
              required
            />
            <FieldFeedbacks for="generation">
              <FieldFeedback when="valueMissing">Angkatan wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-6">
            <InputText
              label="Tahun"
              changeEvent={(val, e) => this.changeValueHandler("year", val, e)}
              value={String(form.year)}
              name="year"
              required
            />
            <FieldFeedbacks for="year">
              <FieldFeedback when="valueMissing">Tahun wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>


        <div className="row mb-sm">
          <div className="col-sm-3">
            <InputText
              label="Nomor STTPP"
              changeEvent={(val, e) => this.changeValueHandler("sttpp_number", val, e)}
              value={String(form.sttpp_number)}
              name="sttpp_number"
              required
            />
            <FieldFeedbacks for="sttpp_number">
              <FieldFeedback when="valueMissing">Nomor STTPP wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-3">
            <DatePicker
              label="Tanggal STTPP"
              id="sttpp-date"
              changeEvent={val => this.changeValueHandler("sttpp_date", val)}
              value={form.sttpp_date}
            />
          </div>
          <div className="col-sm-6">
            <div style={{ marginBottom: "5px" }}>( maximum file size : 5MB; format: .png, .jpg, .jpeg )</div>
            <FileUpload
              label="Sertifikat"
              changeEvent={this.changeFileValueHandler}
              value={fileValue}
              groupDir={groupDir}
            />
          </div>
        </div>
      </FormValidation>
    );
  }

  gotoBasePath = () => {
    const { history } = this.props;
    history.push("/kepegawaian-diklat");
  }

  saveDataHandler = async () => {
    const {
      form, type, id,
      fileValue,
    } = this.state;
    this.updateButtonsState(true, true);

    const isFormValid = await this.form.validateForm();

    if (isFormValid) {
      const payload = Object.assign({}, form, {
        id_employee: form.employee.id,
        file: fileValue,
      });
      delete payload.employee;

      if (type === "create") {
        const res = await createEmployeeTraining(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      } else {
        Object.assign(payload, { id });
        const res = await updateEmployeeTraining(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      }
    } else {
      this.updateButtonsState(false, true);
    }

    this.setState({
      isFormSubmitted: true,
    });
  }

  render = () => {
    const {
      footerButtons, type,
    } = this.state;

    return (
      <div style={{ width: "80%", position: "relative", margin: "0px auto" }}>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-12">
                <h3 className="box-title with-title">{type === "create" ? "Tambah" : "Edit"} Kepegawaian Diklat</h3>
              </div>
            </div>
          </div>
          <div className="box-body">
            {this.formComponent()}
          </div>
          <div className="box-footer text-right">
            <div>
              {footerButtons.map((x, i) => (<button key={x.id} style={(i === footerButtons.length - 1) ? {} : { marginRight: "10px" }} type="button" className={`btn ${x.type ? `btn-${x.type}` : ""}`} onClick={!x.isDisabled ? x.action : () => { }} disabled={x.isDisabled}>{x.content}</button>))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CoreHoC(EmployeeTrainingDetail);
