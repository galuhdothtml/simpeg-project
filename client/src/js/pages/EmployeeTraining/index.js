import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import RawTable from "~/components/form/Table";
import withEditDelete from "~/components/form/Table/withEditDelete";
import withCheckbox from "~/components/form/Table/withCheckbox";
import InputText from "~/components/form/InputText";
import Select from "~/components/form/Select";
import BulkDeleteButton from "~/components/BulkDeleteButton";
import CoreHoC from "../CoreHoC";
import {
  getEmployeeTrainings, bulkDeleteEmployeeTraining,
} from "../../data";

const Table = withCheckbox(withEditDelete(RawTable));
const columns = [
  {
    Header: "Pegawai",
    accessor: "employee_name",
    resizable: false,
  },
  {
    Header: "Nama Diklat",
    accessor: "name",
    resizable: false,
  },
];
class EmployeeTraining extends React.Component {
  static propTypes = {
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    assignButtons: () => { },
    showNotification: () => { },
  }

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      deleteIds: [],
      filterText: "",
      limitValue: "5",
      limitData: [
        { id: "5", name: "5" },
        { id: "10", name: "10" },
        { id: "20", name: "20" },
        { id: "50", name: "50" },
        { id: "100", name: "100" },
      ],
    };
  }

  componentWillMount = () => {
    const { assignButtons, assignBreadcrumbs } = this.props;

    assignButtons([{
      id: "1", title: "Tambah Data", icon: "fa fa-plus-square", clickEvent: () => this.callCreateHandler(),
    }]);

    assignBreadcrumbs([
      {
        label: "App", link: "#",
      },
      {
        label: "Pegawai", link: "#",
      },
      {
        label: "Kepegawaian", link: "#",
      },
      "Diklat",
    ]);
  }

  checkedHandler = (values) => {
    this.setState({ deleteIds: values }, () => {
      this.table.refreshData();
    });
  }

  bulkDeleteHandler = async () => {
    const { showNotification } = this.props;

    try {
      const { deleteIds } = this.state;
      await bulkDeleteEmployeeTraining({ ids: JSON.stringify(deleteIds) });
      this.table.refreshData();
      this.setState({ deleteIds: [] });
      showNotification({
        type: "success",
        msg: "Berhasil menghapus data!",
      });
    } catch (err) {
      console.log(err);
    }
  }

  editDeleteHandler = (id) => {
    const { history } = this.props;

    history.push(`/kepegawaian-diklat/detail/edit/${id}`);
  }

  callCreateHandler = () => {
    const { history } = this.props;

    history.push("/kepegawaian-diklat/detail/create");
  }

  fetchData = async (state) => {
    const { filterText, limitValue } = this.state;
    const payload = {
      page: state.page + 1,
      limit: limitValue,
    };

    if (state.sorted) {
      Object.assign(payload, {
        sorted: JSON.stringify(state.sorted),
      });
    }

    if (filterText && filterText.length > 0) {
      Object.assign(payload, {
        filterText,
      });
    }

    const res = await getEmployeeTrainings(payload);

    if (res.status) {
      const newData = res.data.map(x => (Object.assign({}, x, {
        updatedAt: moment(x.updatedAt).format("DD MMM YYYY HH:mm:ss"),
      })));

      return { data: newData, pageTotal: res.page_total };
    }

    return { data: [], pageTotal: 0 };
  }

  changeFilterTextHandler = (val) => {
    this.setState({ filterText: val });

    clearTimeout(this.filterIdle);
    this.filterIdle = setTimeout(() => {
      this.table.resetCurrentPage(() => {
        this.setState({ filterText: val }, () => {
          this.table.refreshData();
        });
      });
    }, 500);
  }

  changeLimitValueHandler = (val) => {
    this.table.resetCurrentPage(() => {
      this.setState({ limitValue: val }, () => {
        this.table.refreshData();
      });
    });
  }

  render = () => {
    const {
      deleteIds,
      filterText,
      limitValue,
      limitData,
    } = this.state;

    return (
      <div>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-8">
                <h3 className="box-title with-title">Kepegawaian Diklat</h3>
              </div>
              <div className="col-sm-4">
                <div className="row">
                  <div className="col-sm-9">
                    <InputText
                      placeholder="Pencarian ..."
                      changeEvent={this.changeFilterTextHandler}
                      value={filterText}
                    />
                  </div>
                  <div className="col-sm-3 pl-0">
                    <Select
                      data={limitData}
                      value={limitValue}
                      changeEvent={this.changeLimitValueHandler}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="box-body">
            <Table
              ref={(c) => { this.table = c; }}
              columns={columns}
              onFetch={this.fetchData}
              showPagination={true}
              checkedValues={deleteIds}
              checkedEvent={this.checkedHandler}
              editDeleteEvent={this.editDeleteHandler}
            />
          </div>
        </div>
        <BulkDeleteButton value={deleteIds} clickEvent={this.bulkDeleteHandler} />
      </div>
    );
  }
}

export default CoreHoC(EmployeeTraining);
