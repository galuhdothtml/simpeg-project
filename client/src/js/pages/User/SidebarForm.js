import React from "react";
import { FieldFeedbacks, FieldFeedback } from "react-form-with-constraints";
import InputText from "~/components/form/InputText";
import Select from "~/components/form/Select";
import ImageUpload from "~/components/form/ImageUpload";

const roleData = [
  {
    id: "1",
    name: "Superadmin",
  },
  {
    id: "2",
    name: "Admin",
  },
];
export default class SidebarForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = { ...props };
  }

  getFormData = () => this.state

  changeValueHandler = async (type, val, e) => {
    if (e) {
      const { validateInput } = this.props;
      const { target } = e;

      validateInput(target);
    }

    const payload = {
      [type]: val,
    };

    this.setState({
      ...payload,
    });
  }

  render() {
    const {
      username, passwd, email, fullname, role,
      fileImage, type,
    } = this.state;
    const isCreate = (type === "create");

    return (
      <React.Fragment>
        <div className="row mb-sm">
          <div className="col-sm-12">
            <InputText
              name="username"
              label="Username"
              changeEvent={(val, e) => this.changeValueHandler("username", val, e)}
              value={username}
              required
            />
            <FieldFeedbacks for="username">
              <FieldFeedback when="valueMissing">* Username wajib diisi.</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
        <div className="row mb-sm">
          <div className="col-sm-12">
            <InputText
              name="fullname"
              label="Nama Lengkap"
              changeEvent={(val, e) => this.changeValueHandler("fullname", val, e)}
              value={fullname}
              required
            />
            <FieldFeedbacks for="fullname">
              <FieldFeedback when="valueMissing">* Nama lengkap wajib diisi.</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
        <div className="row mb-sm">
          <div className="col-sm-12">
            <InputText
              name="email"
              label="Email"
              changeEvent={(val, e) => this.changeValueHandler("email", val, e)}
              value={email}
              required
            />
            <FieldFeedbacks for="email">
              <FieldFeedback when="valueMissing">* Email wajib diisi.</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
        <div className="row mb-sm">
          <div className="col-sm-12">
            <Select
              label="Role"
              data={roleData}
              value={role}
              changeEvent={(val, e) => this.changeValueHandler("role", val, e)}
            />
          </div>
        </div>
        <div className="row mb-sm">
          <div className="col-sm-12">
            <InputText
              name="passwd"
              label="Password"
              changeEvent={(val, e) => this.changeValueHandler("passwd", val, e)}
              value={passwd}
              required={isCreate}
            />
            { isCreate && (
              <FieldFeedbacks for="passwd">
                <FieldFeedback when="valueMissing">* Password wajib diisi.</FieldFeedback>
              </FieldFeedbacks>
            )}
          </div>
        </div>
        <div className="row mb-sm">
          <div className="col-sm-12">
            <div>
              <label style={{ fontWeight: "normal" }}>Foto Profil</label>
            </div>
            <ImageUpload value={fileImage} changeEvent={val => this.changeValueHandler("fileImage", val)} />
          </div>
        </div>
      </React.Fragment>
    );
  }
}
