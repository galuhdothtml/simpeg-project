import React from "react";
import update from "immutability-helper";
import PropTypes from "prop-types";
import { FieldFeedbacks, FieldFeedback } from "react-form-with-constraints";
import FormValidation from "~/components/form/FormValidation";
import InputText from "~/components/form/InputText";
import Select from "~/components/form/Select";
import DatePicker from "~/components/form/DatePicker";
import FileUpload from "~/components/form/FileUpload";
import BrowseData from "~/components/form/BrowseData";
import CoreHoC from "../CoreHoC";
import {
  getEmployees, createEmployeePosition, updateEmployeePosition, getEmployeePosition, getPositions, getEchelons,
} from "../../data";

const employeeColumns = [
  {
    accessor: "fullname",
    Header: "Nama Pegawai",
    width: "100%",
  },
];
class EmployeeSchoolDetail extends React.Component {
  static propTypes = {
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    assignButtons: () => { },
    showNotification: () => { },
  }

  initialButtonActions = [
    {
      id: "1",
      type: null,
      content: (
        <span>
          Batal
        </span>
      ),
      action: () => this.gotoBasePath(),
      isDisabled: false,
    },
    {
      id: "2",
      type: "primary",
      content: (
        <span>
          Simpan
        </span>
      ),
      action: () => this.saveDataHandler(),
      isDisabled: false,
    },
  ];

  constructor(props) {
    super(props);

    this.state = {
      id: "",
      isFormSubmitted: false,
      type: "create",
      form: {
        employee: {
          id: "",
          name: "",
        },
        id_position: "",
        id_echelon: "",
        sk_number: "",
        sk_date: moment().format("YYYY-MM-DD"),
        tmt_position_from: moment().format("YYYY-MM-DD"),
        tmt_position_to: moment().format("YYYY-MM-DD"),
      },
      fileValue: null,
      groupDir: {
        dirName: "",
        required: true,
        requiredMsg: "Pilih pegawai terlebih dahulu",
      },
      positionList: [],
      echelonList: [],
      footerButtons: this.initialButtonActions,
    };
  }

  componentWillMount = () => {
    this.setupData();
  }

  setupData = async () => {
    const { match: { params } } = this.props;

    await this.fetchEchelons();
    await this.fetchPositions();

    if (params.type === "edit") {
      await this.setupDetailData(params.id);
    } else {
      this.setupBreadcrumbs("Tambah Data");
    }
  }

  setupBreadcrumbs = (text) => {
    const { assignBreadcrumbs } = this.props;

    assignBreadcrumbs([
      {
        label: "App", link: "#",
      },
      {
        label: "Pegawai", link: "#",
      },
      {
        label: "Kepegawaian", link: "#",
      },
      {
        label: "Jabatan", link: "/kepegawaian-jabatan",
      },
      text,
    ]);
  }

  setupDetailData = async (id) => {
    const res = await getEmployeePosition(id);

    if (res.status) {
      const { data } = res;
      let fileValue = null;

      if (data.file) {
        const parsedFile = JSON.parse(data.file.filename);
        fileValue = {
          fileValue: parsedFile.filepath,
          fileType: parsedFile.filetype,
          fileName: parsedFile.filename,
        };
      }

      const newState = {
        id: data.id,
        type: "edit",
        form: {
          employee: {
            id: data.Employee.id,
            name: data.Employee.fullname,
          },
          id_position: String(data.id_position),
          id_echelon: String(data.id_echelon),
          sk_number: data.sk_number,
          sk_date: moment(data.sk_date).format("YYYY-MM-DD"),
          tmt_position_from: moment(data.tmt_position_from).format("YYYY-MM-DD"),
          tmt_position_to: moment(data.tmt_position_to).format("YYYY-MM-DD"),
        },
        groupDir: {
          dirName: String(data.id_employee),
          required: true,
          requiredMsg: "Pilih pegawai terlebih dahulu",
        },
        fileValue,
      };

      this.setupBreadcrumbs(data.Employee.fullname);
      this.setState(newState);
    }
  }

  fetchPositions = async () => {
    const res = await getPositions();

    if (res.status) {
      const { data } = res;
      const newData = [{ id: "", name: "- Pilih Jabatan -" }, ...data.map(x => ({ id: x.id, name: x.name }))];

      this.setState({ positionList: newData });
    }
  }

  fetchEchelons = async () => {
    const res = await getEchelons();

    if (res.status) {
      const { data } = res;
      const newData = [{ id: "", name: "- Pilih Eselon -" }, ...data.map(x => ({ id: x.id, name: x.name }))];

      this.setState({ echelonList: newData });
    }
  }

  changeValueHandler = async (type, val, e) => {
    const { form } = this.state;

    const { isFormSubmitted } = this.state;

    if (isFormSubmitted && e) {
      await this.onInputChangeValidate(e);
    }

    const newValue = update(form, {
      [type]: { $set: val },
    });

    this.setState({ form: newValue }, () => {
      if (type === "id_employee") {
        this.changeGroupDir(val);
      }
    });
  }

  changeGroupDir = (val) => {
    const { groupDir } = this.state;
    const newValue = update(groupDir, {
      dirName: { $set: val },
    });

    this.setState({ groupDir: newValue });
  }

  onInputChangeValidate = ({ target }) => {
    this.form.validateInput(target);

    if (this.inputTimeout) {
      clearTimeout(this.inputTimeout);
    }

    this.inputTimeout = setTimeout(() => {
      if (this.form.simpleValidateForm()) {
        this.updateButtonsState();
      } else {
        this.updateButtonsState(false, true);
      }
    }, 300);
  }

  updateButtonsState = (isBtnCancelDisabled = false, isBtnSaveDisabled = false) => {
    const footerButtons = update(this.initialButtonActions, {
      0: { isDisabled: { $set: isBtnCancelDisabled } },
      1: { isDisabled: { $set: isBtnSaveDisabled } },
    });

    this.setState({ footerButtons });
  }

  changeFileValueHandler = (val) => {
    this.setState({ fileValue: val });
  }

  validateEmployee = async () => {
    const { isFormSubmitted } = this.state;

    if (isFormSubmitted) {
      await this.onInputChangeValidate({ target: this.employeeHidden });
    }
  }

  changeEmployeeHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      employee: { $set: { id: val.id, name: val.fullname } },
    });

    this.setState({ form: newValue }, () => {
      this.validateEmployee();
    });
  }

  onFetchEmployee = async (state) => {
    const payload = {
      limit: 10,
      page: state.page,
      filterText: state.filterText,
      sorted: JSON.stringify({ id: state.fieldToSort, desc: state.sortType === "desc" }),
    };
    const res = await getEmployees(payload);

    return res;
  }

  formComponent = () => {
    const {
      form,
      type, fileValue, positionList, echelonList,
      groupDir,
    } = this.state;
    return (
      <FormValidation ref={(c) => { this.form = c; }}>
        <div className="row mb-sm">
          <div className="col-sm-6">
            {type === "create" && (
              <div className="row mb-sm">
                <div className="col-sm-12">
                  <div className="form-group position-relative mb-0">
                    <BrowseData
                      label="Pegawai"
                      placeholder="Pilih Pegawai"
                      columns={employeeColumns}
                      value={form.employee}
                      changeEvent={this.changeEmployeeHandler}
                      onFetch={this.onFetchEmployee}
                    />
                    <input
                      ref={(c) => { this.employeeHidden = c; }}
                      className="hide-for-validation"
                      name="employeeHidden"
                      type="text"
                      value={form.employee.id}
                      onChange={() => { }}
                      required
                    />
                  </div>
                  <FieldFeedbacks for="employeeHidden">
                    <FieldFeedback when="valueMissing">Pegawai wajib diisi</FieldFeedback>
                  </FieldFeedbacks>
                </div>
              </div>
            )}
            <div className="row mb-sm">
              <div className="col-sm-6">
                <Select
                  label="Jabatan"
                  data={positionList}
                  value={form.id_position}
                  changeEvent={(val, e) => this.changeValueHandler("id_position", val, e)}
                  name="id_position"
                  required
                />
                <FieldFeedbacks for="id_position">
                  <FieldFeedback when="valueMissing">Jabatan wajib diisi</FieldFeedback>
                </FieldFeedbacks>
              </div>
              <div className="col-sm-6">
                <Select
                  label="Eselon"
                  data={echelonList}
                  value={form.id_echelon}
                  changeEvent={(val, e) => this.changeValueHandler("id_echelon", val, e)}
                  name="id_echelon"
                  required
                />
                <FieldFeedbacks for="id_echelon">
                  <FieldFeedback when="valueMissing">Eselon wajib diisi</FieldFeedback>
                </FieldFeedbacks>
              </div>
            </div>
          </div>
          <div className="col-sm-6">
            <div className="row mb-sm">
              <div className="col-sm-12">
                <div style={{ marginBottom: "5px" }}>( maximum file size : 5MB; format: .png, .jpg, .jpeg )</div>
                <FileUpload
                  label="File SK Jabatan"
                  changeEvent={this.changeFileValueHandler}
                  value={fileValue}
                  groupDir={groupDir}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="row mb-sm">
          <div className="col-sm-3">
            <InputText
              label="Nomor SK"
              changeEvent={(val, e) => this.changeValueHandler("sk_number", val, e)}
              value={String(form.sk_number)}
              name="sk_number"
              required
            />
            <FieldFeedbacks for="sk_number">
              <FieldFeedback when="valueMissing">Nomor SK wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-3">
            <DatePicker
              label="Tanggal SK"
              id="sk-date"
              changeEvent={val => this.changeValueHandler("sk_date", val)}
              value={form.sk_date}
            />
          </div>
          <div className="col-sm-3">
            <DatePicker
              label="TMT Jabatan (dari)"
              id="tmt-position-from"
              changeEvent={val => this.changeValueHandler("tmt_position_from", val)}
              value={form.tmt_position_from}
            />
          </div>
          <div className="col-sm-3">
            <DatePicker
              label="TMT Jabatan (sampai)"
              id="tmt-position-to"
              changeEvent={val => this.changeValueHandler("tmt_position_to", val)}
              value={form.tmt_position_to}
            />
          </div>
        </div>
      </FormValidation>
    );
  }

  gotoBasePath = () => {
    const { history } = this.props;
    history.push("/kepegawaian-jabatan");
  }

  saveDataHandler = async () => {
    const {
      form, type, id,
      fileValue,
    } = this.state;
    this.updateButtonsState(true, true);

    const isFormValid = await this.form.validateForm();

    if (isFormValid) {
      const payload = Object.assign({}, form, {
        id_employee: form.employee.id,
      });
      delete payload.employee;

      if (type === "create") {
        const res = await createEmployeePosition(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      } else {
        Object.assign(payload, { id });
        const res = await updateEmployeePosition(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      }
    } else {
      this.updateButtonsState(false, true);
    }

    this.setState({
      isFormSubmitted: true,
    });
  }

  render = () => {
    const {
      footerButtons, type,
    } = this.state;

    return (
      <div style={{ width: "80%", position: "relative", margin: "0px auto" }}>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-12">
                <h3 className="box-title with-title">{type === "create" ? "Tambah" : "Edit"} Kepegawaian Jabatan</h3>
              </div>
            </div>
          </div>
          <div className="box-body">
            {this.formComponent()}
          </div>
          <div className="box-footer text-right">
            <div>
              {footerButtons.map((x, i) => (<button key={x.id} style={(i === footerButtons.length - 1) ? {} : { marginRight: "10px" }} type="button" className={`btn ${x.type ? `btn-${x.type}` : ""}`} onClick={!x.isDisabled ? x.action : () => { }} disabled={x.isDisabled}>{x.content}</button>))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CoreHoC(EmployeeSchoolDetail);
