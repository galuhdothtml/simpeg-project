/* eslint prop-types: 0 */
import React from "react";
import CoreHoC from "../CoreHoC";
import {
  getBezettingReport,
} from "../../data";
import { formatDate } from "../EmployeeDetail/View/Staffing/helper";

class Bezetting extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
    };
  }

  componentWillMount = () => {
    const { assignButtons, assignBreadcrumbs } = this.props;

    assignButtons([{
      id: "1", title: "Print", icon: "fa fa-print", clickEvent: () => this.onPrint(),
    }]);

    assignBreadcrumbs([
      {
        label: "App", link: "#",
      },
      {
        label: "Laporan", link: "#",
      },
      {
        label: "Laporan", link: "#",
      },
      "Bezetting",
    ]);
    this.fetchData();
  }

  onPrint = () => {
    //
  }

  fetchData = async () => {
    const res = await getBezettingReport();
    this.setState({ data: res.data });
  }

  getAge = (birthDate) => {
    const a = moment();
    const b = moment(birthDate);
    const age = moment.duration(a.diff(b));
    const years = age.years();

    return `${years} tahun`;
  }

  renderTableBody = () => {
    const { data } = this.state;
    const retval = data.map((x, i) => (
      <tr key={i}>
        <td>{i + 1}</td>
        <td>{x.fullname}<br />{x.birth_place}, {formatDate(x.birth_date)}</td>
        <td>{x.nip}</td>
        <td>{x.employee_level}<br />{x.employee_group_level}</td>
        <td>{x.employee_position}</td>
        <td>{x.degree}</td>
        <td>{this.getAge(x.birth_date)}</td>
        <td>{x.employee_status}</td>
      </tr>
    ));

    return <tbody>{retval}</tbody>;
  }

  renderTable = () => (
    <table className="table table-bordered">
      <thead>
        <tr>
          <th>No</th>
          <th>NAMA<br />TEMPAT TANGGAL LAHIR</th>
          <th>NIP</th>
          <th>PANGKAT<br />GOL/RUANG</th>
          <th>JABATAN</th>
          <th>PENDIDIKAN<br />TERAKHIR</th>
          <th>UMUR (THN)</th>
          <th>KET</th>
        </tr>
        <tr>
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
          <th>5</th>
          <th>6</th>
          <th>7</th>
          <th>8</th>
        </tr>
      </thead>
      {this.renderTableBody()}
    </table>
  )

  render() {
    return (
      <div>
        <div className="box box-solid">
          <div className="box-header with-border">
            <h5 align="center">DAFTAR BEZETTING PNS</h5>
            <h6 align="center">PEMERINTAH KABUPATEN CILACAP PERIODE BULAN 12 TAHUN 2020</h6>
          </div>
          <div className="box-body">
            {this.renderTable()}
          </div>
        </div>
      </div>
    );
  }
}

export default CoreHoC(Bezetting);
