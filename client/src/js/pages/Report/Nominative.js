/* eslint prop-types: 0 */
import React from "react";
import CoreHoC from "../CoreHoC";
import {
  getEmployeeNominativeReport,
} from "../../data";
import { formatDate } from "../EmployeeDetail/View/Staffing/helper";

class Nominative extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
    };
  }

  componentWillMount = () => {
    const { assignButtons, assignBreadcrumbs } = this.props;

    assignButtons([{
      id: "1", title: "Print", icon: "fa fa-print", clickEvent: () => this.onPrint(),
    }]);

    assignBreadcrumbs([
      {
        label: "App", link: "#",
      },
      {
        label: "Laporan", link: "#",
      },
      {
        label: "Laporan", link: "#",
      },
      "Nominatif",
    ]);
    this.fetchData();
  }

  onPrint = () => {
    //
  }

  fetchData = async () => {
    const res = await getEmployeeNominativeReport();
    this.setState({ data: res.data });
  }

  renderTableBody = () => {
    const { data } = this.state;
    const retval = data.map((x, i) => (
      <tr>
        <td>{i + 1}</td>
        <td colSpan="2">{x.fullname}<br /><br />{x.birth_place}, {formatDate(x.birth_date)}<br />{x.nip}<br />{x.religion_name}</td>
        <td>{x.gender === "L" ? "Laki - Laki" : "Perempuan"}</td>
        <td>{x.employee_level}<br />{x.employee_group_level}</td>
        <td>{formatDate(x.employee_tmt_level)}</td>
        <td>{x.employee_position}</td>
        <td>{formatDate(x.employee_tmt_position)}</td>
        <td>{x.employee_echelon_position}</td>
        <td>{x.degree}<br />{x.school_name}<br />{x.department}<br />{formatDate(x.certificate_date)}</td>
      <td>{x.address} <br /><br />{x.phone}</td>
      <td>{x.employee_status}</td>
      </tr>
    ));

    return <tbody>{retval}</tbody>;
  }

  renderTable = () => (
    <table className="table table-bordered">
      <thead>
        <tr>
          <th rowSpan="2">NO</th>
          <th colSpan="2">NAMA</th>
          <th rowSpan="2">JNS KELAMIN</th>
          <th colSpan="2">PKT TERAKHIR</th>
          <th colSpan="2">JABATAN</th>
          <th rowSpan="2">ESL</th>
          <th rowSpan="2">PEND / JURUSAN / T.LULUS</th>
          <th rowSpan="2">ALAMAT &amp; NO. TELP</th>
          <th rowSpan="2">KET</th>
        </tr>
        <tr>
          <th colSpan="2">TTL / NIP / AGAMA</th>
          <th>GOL/RUANG</th>
          <th>TMT</th>
          <th>NAMA</th>
          <th>TMT</th>
        </tr>
        <tr>
          <th>1</th>
          <th colSpan="2">2</th>
          <th>3</th>
          <th>4</th>
          <th>5</th>
          <th>6</th>
          <th>7</th>
          <th>8</th>
          <th>9</th>
          <th>10</th>
          <th>11</th>
        </tr>
      </thead>
      {this.renderTableBody()}
    </table>
  )

  render() {
    return (
      <div>
        <div className="box box-solid">
          <div className="box-header with-border">
            <h5 align="center">DAFTAR NOMINATIF PEGAWAI NEGERI SIPIL</h5>
            <h6 align="center">PER 26 NOVEMBER 2020</h6>
            <h6>PEMERINTAH KABUPATEN CILACAP</h6>
          </div>
          <div className="box-body">
            {this.renderTable()}
          </div>
        </div>
      </div>
    );
  }
}

export default CoreHoC(Nominative);
