import React from "react";
import PropTypes from "prop-types";
import CoreHoC from "../CoreHoC";

class BKD extends React.Component {
  static propTypes = {
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    assignButtons: () => { },
    showNotification: () => { },
  }

  constructor(props) {
    super(props);

    this.state = {
      data: {
        city: "Cilacap",
        address: "Jl. Raya Pesanggrahan XII No. 201 Telp. 768621,768622 Fax. 768621",
        leader: "Rajasa Hatmoko, SH",
      },
    };
  }

  componentWillMount = () => {
    const { assignButtons, assignBreadcrumbs } = this.props;

    assignButtons([{
      id: "1", title: "Setup", icon: "fa fa-gear", clickEvent: () => this.callCreateHandler(),
    }]);

    assignBreadcrumbs([
      {
        label: "App", link: "#",
      },
      {
        label: "Pengaturan", link: "#",
      },
      "BKD",
    ]);
  }

  callCreateHandler = () => {
    //
  }

  render = () => {
    const {
      data,
    } = this.state;

    return (
      <div style={{ width: "80%", position: "relative", margin: "0px auto" }}>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-12">
                <h3 className="box-title with-title">Badan Kepegawaian Daerah</h3>
              </div>
            </div>
          </div>
          <div className="box-body">
            <div className="row">
                <div className="col-sm-3 text-right">
                    <label>Kabupaten / Kota</label>
                </div>
                <div className="col-sm-9">
                    {data.city}
                </div>
            </div>
            <div className="row">
                <div className="col-sm-3 text-right">
                    <label>Alamat</label>
                </div>
                <div className="col-sm-9">
                    {data.address}
                </div>
            </div>
            <div className="row">
                <div className="col-sm-3 text-right">
                    <label>Kepala</label>
                </div>
                <div className="col-sm-9">
                    {data.leader}
                </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CoreHoC(BKD);
