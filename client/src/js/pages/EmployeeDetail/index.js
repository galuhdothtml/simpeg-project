/* eslint prop-types: 0 */
import React from "react";
import View from "./View";
import Edit from "./Edit";

const EmployeeDetail = (props) => {
  const { match: { params } } = props;

  if (params.type === "view") {
    return (<View {...props} />)
  }

  return (<Edit {...props} />);
};

export default EmployeeDetail;
