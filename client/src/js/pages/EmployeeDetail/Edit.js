/* eslint prop-types: 0 */
import React from "react";
import update from "immutability-helper";
import { FieldFeedbacks, FieldFeedback } from "react-form-with-constraints";
import FormValidation from "~/components/form/FormValidation";
import InputText from "~/components/form/InputText";
import ImageUpload from "~/components/form/ImageUpload";
import DatePicker from "~/components/form/DatePicker";
import Select from "~/components/form/Select";
import {
  getReligions, getMaritalStatuses, getWorkUnits, getEmployee,
  createEmployee, updateEmployee,
} from "../../data";
import CoreHoC from "../CoreHoC";

class EmployeeDetail extends React.Component {
  initialButtonActions = [
    {
      id: "1",
      type: null,
      content: (
        <span>
          Batal
        </span>
      ),
      action: () => this.gotoBasePath(),
      isDisabled: false,
    },
    {
      id: "2",
      type: "primary",
      content: (
        <span>
          Simpan
        </span>
      ),
      action: () => this.saveDataHandler(),
      isDisabled: false,
    },
  ];

  constructor(props) {
    super(props);

    this.state = {
      isFormSubmitted: false,
      fileImage: "",
      id: "",
      type: "create",
      form: {
        nip: "",
        birth_place: "",
        birth_date: moment().subtract(18, "years").format("YYYY-MM-DD"),
        fullname: "",
        id_religion: "",
        gender: "",
        blood_type: "",
        id_marital_status: "",
        employee_status: "",
        id_work_unit: "",
        promotion_date: moment().format("YYYY-MM-DD"),
        salary_increase_date: moment().format("YYYY-MM-DD"),
        address: "",
        phone: "",
        email: "",
      },
      workUnitList: [],
      religionList: [],
      maritalStatusList: [],
      genderList: [
        {
          id: "", name: "- Pilih Jenis Kelamin -",
        },
        {
          id: "L", name: "Laki - Laki",
        },
        {
          id: "P", name: "Perempuan",
        },
      ],
      employeeStatusList: [
        {
          id: "", name: "- Pilih Status Kepegawaian -",
        },
        {
          id: "PNS", name: "Pegawai Negeri Sipil",
        },
        {
          id: "PTT", name: "Pegawai Tidak Tetap",
        },
      ],
      footerButtons: this.initialButtonActions,
    };
  }

  componentWillMount = () => {
    const { match: { params }, assignBreadcrumbs } = this.props;

    this.fetchDropdownData();

    const breadcrumbsData = [
      {
        label: "App", link: "#",
      },
      {
        label: "Pegawai", link: "#",
      },
      {
        label: "Data Pegawai", link: "/pegawai",
      },
    ];

    if (params.type === "edit") {
      breadcrumbsData.push("Edit Pegawai");
      this.setupEmployeeData(params.id);
    } else {
      breadcrumbsData.push("Tambah Pegawai");
      this.setState({ type: "create" });
    }

    assignBreadcrumbs(breadcrumbsData);
  }

  gotoBasePath = () => {
    const { history } = this.props;
    history.push("/pegawai");
  }

  updateButtonsState = (isBtnCancelDisabled = false, isBtnSaveDisabled = false) => {
    const footerButtons = update(this.initialButtonActions, {
      0: { isDisabled: { $set: isBtnCancelDisabled } },
      1: { isDisabled: { $set: isBtnSaveDisabled } },
    });

    this.setState({ footerButtons });
  }

  fetchDropdownData = () => {
    this.fetchReligions();
    this.fetchMaritalStatuses();
    this.fetchWorkUnits();
  }

  fetchReligions = async () => {
    const res = await getReligions();

    if (res.status) {
      const { data } = res;
      const newData = [{ id: "", name: "- Pilih Agama -" }, ...data.map(x => ({ id: x.id, name: x.name }))];

      this.setState({ religionList: newData });
    }
  }

  fetchMaritalStatuses = async () => {
    const res = await getMaritalStatuses();

    if (res.status) {
      const { data } = res;
      const newData = [{ id: "", name: "- Pilih Status Pernikahan -" }, ...data.map(x => ({ id: x.id, name: x.name }))];

      this.setState({ maritalStatusList: newData });
    }
  }

  fetchWorkUnits = async () => {
    const res = await getWorkUnits();

    if (res.status) {
      const { data } = res;
      const newData = [{ id: "", name: "- Pilih Unit Kerja -" }, ...data.map(x => ({ id: x.id, name: x.name }))];

      this.setState({ workUnitList: newData });
    }
  }

  setupEmployeeData = async (id) => {
    const res = await getEmployee(id);

    if (res.status) {
      const { data } = res;
      let newFileImage = "";

      if (data.imageFile) {
        newFileImage = data.imageFile;
      }

      const newState = {
        id: data.id,
        type: "edit",
        form: {
          nip: data.nip,
          birth_place: data.birth_place,
          birth_date: moment(data.birth_date, "YYYY-MM-DD").format("YYYY-MM-DD"),
          fullname: data.fullname,
          id_religion: data.id_religion,
          gender: data.gender,
          blood_type: data.blood_type,
          id_marital_status: data.id_marital_status,
          employee_status: data.employee_status,
          id_work_unit: data.id_work_unit,
          promotion_date: moment(data.promotion_date, "YYYY-MM-DD").format("YYYY-MM-DD"),
          salary_increase_date: moment(data.salary_increase_date, "YYYY-MM-DD").format("YYYY-MM-DD"),
          address: data.address,
          phone: data.phone,
          email: data.email,
        },
        fileImage: newFileImage,
      };

      this.setState(newState);
    }
  }

  changeValueHandler = async (type, val, e) => {
    const { form } = this.state;

    const { isFormSubmitted } = this.state;

    if (isFormSubmitted && e) {
      await this.onInputChangeValidate(e);
    }

    const newValue = update(form, {
      [type]: { $set: val },
    });

    this.setState({ form: newValue });
  }

  changeImageHandler = (val) => {
    this.setState({ fileImage: val });
  }

  saveDataHandler = async () => {
    const {
      form, type, fileImage, id,
    } = this.state;
    this.updateButtonsState(true, true);

    const isFormValid = await this.form.validateForm();

    if (isFormValid) {
      const payload = Object.assign({}, form, { photo: fileImage });

      if (type === "create") {
        const res = await createEmployee(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      } else {
        Object.assign(payload, { id });
        const res = await updateEmployee(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      }
    } else {
      this.updateButtonsState(false, true);
    }

    this.setState({
      isFormSubmitted: true,
    });
  }

  onInputChangeValidate = ({ target }) => {
    this.form.validateInput(target);

    if (this.inputTimeout) {
      clearTimeout(this.inputTimeout);
    }

    this.inputTimeout = setTimeout(() => {
      if (this.form.simpleValidateForm()) {
        this.updateButtonsState();
      } else {
        this.updateButtonsState(false, true);
      }
    }, 300);
  }

  formComponent = () => {
    const {
      form, religionList, genderList,
      maritalStatusList, employeeStatusList,
      workUnitList,
    } = this.state;
    return (
      <FormValidation ref={(c) => { this.form = c; }}>
        <div className="row mb-sm">
          <div className="col-sm-4">
            <InputText
              label="NIP *"
              changeEvent={(val, e) => this.changeValueHandler("nip", val, e)}
              value={String(form.nip)}
              name="nip"
              required
            />
            <FieldFeedbacks for="nip">
              <FieldFeedback when="valueMissing">NIP wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <InputText
              label="Tempat Lahir"
              changeEvent={(val, e) => this.changeValueHandler("birth_place", val, e)}
              value={String(form.birth_place)}
              name="birth_place"
              required
            />
            <FieldFeedbacks for="birth_place">
              <FieldFeedback when="valueMissing">Tempat lahir wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <DatePicker
              label="Tanggal Lahir"
              id="birth-date"
              changeEvent={val => this.changeValueHandler("birth_date", val)}
              value={form.birth_date}
            />
          </div>
        </div>
        <div className="row mb-sm">
          <div className="col-sm-12">
            <InputText
              label="Nama Lengkap *"
              changeEvent={(val, e) => this.changeValueHandler("fullname", val, e)}
              value={String(form.fullname)}
              name="fullname"
              required
            />
            <FieldFeedbacks for="fullname">
              <FieldFeedback when="valueMissing">Nama lengkap wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
        <div className="row mb-sm">
          <div className="col-sm-4">
            <Select
              label="Agama"
              data={religionList}
              value={form.id_religion}
              changeEvent={(val, e) => this.changeValueHandler("id_religion", val, e)}
              name="id_religion"
              required
            />
            <FieldFeedbacks for="id_religion">
              <FieldFeedback when="valueMissing">Agama wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <Select
              label="Jenis Kelamin"
              data={genderList}
              value={form.gender}
              changeEvent={(val, e) => this.changeValueHandler("gender", val, e)}
              name="gender"
              required
            />
            <FieldFeedbacks for="gender">
              <FieldFeedback when="valueMissing">Jenis kelamin wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <InputText
              label="Golongan Darah"
              changeEvent={val => this.changeValueHandler("blood_type", val)}
              value={String(form.blood_type)}
            />
          </div>
        </div>
        <div className="row mb-sm">
          <div className="col-sm-6">
            <DatePicker
              label="Tanggal Kenaikan Pangkat"
              id="promotion-date"
              changeEvent={val => this.changeValueHandler("promotion_date", val)}
              value={form.promotion_date}
            />
          </div>
          <div className="col-sm-6">
            <DatePicker
              label="Tanggal Kenaikan Gaji"
              id="salary-increase-date"
              changeEvent={val => this.changeValueHandler("salary_increase_date", val)}
              value={form.salary_increase_date}
            />
          </div>
        </div>
        <div className="row mb-sm">
          <div className="col-sm-4">
            <Select
              label="Status Pernikahan"
              data={maritalStatusList}
              value={form.id_marital_status}
              changeEvent={(val, e) => this.changeValueHandler("id_marital_status", val, e)}
              name="id_marital_status"
              required
            />
            <FieldFeedbacks for="id_marital_status">
              <FieldFeedback when="valueMissing">Status pernikahan wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <Select
              label="Status Kepegawaian"
              data={employeeStatusList}
              value={form.employee_status}
              changeEvent={(val, e) => this.changeValueHandler("employee_status", val, e)}
              name="employee_status"
              required
            />
            <FieldFeedbacks for="employee_status">
              <FieldFeedback when="valueMissing">Status kepegawaian wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <Select
              label="Unit Kerja"
              data={workUnitList}
              value={form.id_work_unit}
              changeEvent={(val, e) => this.changeValueHandler("id_work_unit", val, e)}
              name="id_work_unit"
              required
            />
            <FieldFeedbacks for="id_work_unit">
              <FieldFeedback when="valueMissing">Unit kerja wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
        <div className="row mb-sm">
          <div className="col-sm-5">
            <InputText
              label="Alamat"
              changeEvent={(val, e) => this.changeValueHandler("address", val, e)}
              value={String(form.address)}
              name="address"
              required
            />
            <FieldFeedbacks for="address">
              <FieldFeedback when="valueMissing">Alamat wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <InputText
              label="Email"
              changeEvent={val => this.changeValueHandler("email", val)}
              value={String(form.email)}
            />
          </div>
          <div className="col-sm-3">
            <InputText
              label="No. Telp"
              changeEvent={(val, e) => this.changeValueHandler("phone", val, e)}
              value={String(form.phone)}
              name="phone"
              required
            />
            <FieldFeedbacks for="phone">
              <FieldFeedback when="valueMissing">Telepon wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
      </FormValidation>
    );
  }

  render() {
    // const { match: { params } } = this.props;
    const { fileImage, type, footerButtons } = this.state;

    return (
      <div>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-12">
                <h3 className="box-title">{type === "create" ? "Tambah" : "Edit"} Pegawai</h3>
              </div>
            </div>
          </div>
          <div className="box-body">
            <div className="row mb-0">
              <div className="col-sm-9">
                {this.formComponent()}
              </div>
              <div className="col-sm-3 text-center">
                <ImageUpload value={fileImage} changeEvent={this.changeImageHandler} />
              </div>
            </div>
          </div>
          <div className="box-footer text-right">
            <div>
              {footerButtons.map((x, i) => (<button key={x.id} style={(i === footerButtons.length - 1) ? {} : { marginRight: "10px" }} type="button" className={`btn ${x.type ? `btn-${x.type}` : ""}`} onClick={!x.isDisabled ? x.action : () => { }} disabled={x.isDisabled}>{x.content}</button>))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CoreHoC(EmployeeDetail);
