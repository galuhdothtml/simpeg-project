/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import { formatDate } from "./Staffing/helper";
import Util from "../../../utils";

class SKP extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      skpData: [],
    };
  }

  componentDidMount = () => {
    const { apiRedux } = this.props;

    this.setupData(apiRedux.data);
  }

  setupData = (data) => {
    const newData = data.map((x) => {
      const { total, avg, quality } = Util.parseSkpScores(JSON.parse(x.score));

      return Object.assign({}, x, {
        total,
        avg,
        quality,
      });
    });

    this.setState({ skpData: newData });
  }

  detailButton = () => (
    <div>
      <a href="#" target="_blank">Detail</a>
    </div>
  )

  editHandler = (id) => {
    const { history } = this.props;

    history.push(`/skp/detail/edit/${id}`);
  }

  tableBodyComponent = (data) => {
    let retval = [];

    retval = data.map((x, i) => (
      <tr key={x.id}>
        <td>{i + 1}</td>
        <td>{formatDate(x.start_date)}</td>
        <td>{formatDate(x.end_date)}</td>
        <td>{x.evaluator_name}</td>
        <td>{x.head_evaluator_name}</td>
        <td>{x.total}</td>
        <td>{x.avg}</td>
        <td>{x.quality}</td>
        <td style={{ width: "15%" }} className="text-center">
          <div className="btn-group">
            <button type="button" className="btn btn-default btn-xs" onClick={() => this.editHandler(x.id)}><i className="fa fa-edit" />{" "}Edit</button>
           <button type="button" className="btn btn-default btn-xs"><i className="fa fa-trash-o" />{" "}Hapus</button>
          </div>
        </td>
        <td>{this.detailButton()}</td>
      </tr>
    ));

    return (<tbody>{retval}</tbody>);
  }

  render() {
    const { skpData } = this.state;

    return (
      <table className="table table-bordered">
      <thead>
        <tr>
          <th rowSpan="2">No<br />&nbsp;</th>
          <th colSpan="2">Periode Penilaian</th>
          <th colSpan="2">Penilai</th>
          <th rowSpan="2">N Total<br />&nbsp;</th>
          <th rowSpan="2">Rata<sup>2</sup><br />&nbsp;</th>
          <th rowSpan="2">Mutu<br />&nbsp;</th>
          <th width="15%" rowSpan="2"><center><i className="fa fa-code fa-lg"></i><br />&nbsp;</center></th>
          <th width="6%" rowSpan="2">View<br />&nbsp;</th>
        </tr>
        <tr>
          <th>Awal</th>
          <th>Akhir</th>
          <th>Pejabat</th>
          <th>Atasan Pejabat</th>
        </tr>
      </thead>
      {this.tableBodyComponent(skpData)}
    </table>
    );
  }
}

const mapStateToProps = state => ({
  apiRedux: state.apiRedux.skpEmployees,
});

const reduxComponent = connect(mapStateToProps)(SKP);


export default reduxComponent;
