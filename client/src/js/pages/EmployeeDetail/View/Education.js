/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import { formatDate } from "./Staffing/helper";
import {
  setLastEducationEmployee,
} from "../../../data";

class Education extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: props.apiRedux.data,
    };
  }

  componentWillReceiveProps = (nextProps) => {
    if (!nextProps.apiRedux.isFetching) {
      this.setState({ data: nextProps.apiRedux.data });
    }
  }

  fetchingData = (param) => {
    const {
      fetchEmployeeSchool: fetchData,
    } = this.props;

    fetchData(param);
  }

  setLastEducation = async (val) => {
    await setLastEducationEmployee({ id: val.id, idEmployee: val.id_employee });

    this.fetchingData({ idEmployee: val.id_employee });
  }

  tableBodyComponent = (data) => {
    let retval = [
      (
        <tr key="empty-row">
          <td colSpan="9" className="text-center">Tidak ada data</td>
        </tr>
      ),
    ];

    if (data.length > 0) {
      retval = data.map(x => (
        <tr key={x.id}>
          <td>{x.degree}</td>
          <td>{x.school_name}</td>
          <td>{x.location}</td>
          <td>{x.department}</td>
          <td>{x.certificate_number}<br />{formatDate(x.certificate_date)}</td>
          <td>{x.headmaster}</td>
          <td>{x.last_education ? "Akhir" : "-"}</td>
          <td className="text-center"><button type="button" className="btn btn-default btn-xs" onClick={() => { this.setLastEducation(x); }}>Setup</button></td>
          <td style={{ width: "15%" }} className="text-center">
            <div className="btn-group">
             <button type="button" className="btn btn-default btn-xs" onClick={() => this.editHandler(x.id)}><i className="fa fa-edit" />{" "}Edit</button>
             <button type="button" className="btn btn-default btn-xs"><i className="fa fa-trash-o" />{" "}Hapus</button>
            </div>
          </td>
        </tr>
      ));
    }

    return (<tbody>{retval}</tbody>);
  }

  editHandler = (id) => {
    const { history } = this.props;

    history.push(`/keluarga-suami-istri/detail/edit/${id}`);
  }

  render() {
    const { data } = this.state;

    return (
      <table className="table table-bordered table-th-valign-middle">
        <thead>
          <tr>
            <th>Tingkat</th>
            <th>Nama Sekolah</th>
            <th>Lokasi</th>
            <th>Jurusan</th>
            <th>No. Ijazah<br />Tgl. Ijazah</th>
            <th>Kepala / Rektor</th>
            <th>Status Pend.</th>
            <th className="text-center">Set Akhir</th>
            <th width="10%"></th>
          </tr>
        </thead>
        {this.tableBodyComponent(data)}
      </table>
    );
  }
}

const mapStateToProps = state => ({
  apiRedux: state.apiRedux.employeeSchool,
});

const reduxComponent = connect(mapStateToProps)(Education);

export default reduxComponent;
