/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";

class Parent extends React.Component {
  tableBodyComponent = (data) => {
    let retval = [];

    retval = data.map(x => (
      <tr key={x.id}>
        <td>{x.nik}</td>
        <td>{x.fullname}</td>
        <td>{`${x.birth_place}, ${moment(x.birth_date).format("DD MMMM YYYY")}`}</td>
        <td>{x.education_level_name}</td>
        <td>{x.job}</td>
        <td>{x.status === "A" ? "Ayah" : "Ibu"}</td>
        <td style={{ width: "15%" }} className="text-center">
          <div className="btn-group">
            <button type="button" className="btn btn-default btn-xs" onClick={() => this.editHandler(x.id)}><i className="fa fa-edit" />{" "}Edit</button>
           <button type="button" className="btn btn-default btn-xs"><i className="fa fa-trash-o" />{" "}Hapus</button>
          </div>
        </td>
      </tr>
    ));

    return (<tbody>{retval}</tbody>);
  }

  editHandler = (id) => {
    const { history } = this.props;

    history.push(`/keluarga-orang-tua/detail/edit/${id}`);
  }

  render() {
    const { apiRedux } = this.props;

    if (apiRedux.isFetching) {
      return (<div>Loading ...</div>);
    }

    if (apiRedux.data === null) {
      return (<div>Data is empty</div>);
    }

    return (
      <table className="table table-bordered">
        <thead>
          <tr>
            <th>NIK</th>
            <th>Nama</th>
            <th>TTL</th>
            <th>Pendidikan</th>
            <th>Pekerjaan</th>
            <th>Hubungan</th>
            <th width="10%"></th>
          </tr>
        </thead>
        {this.tableBodyComponent(apiRedux.data)}
      </table>
    );
  }
}

const mapStateToProps = state => ({
  apiRedux: state.apiRedux.employeeParent,
});

const reduxComponent = connect(mapStateToProps)(Parent);

export default reduxComponent;
