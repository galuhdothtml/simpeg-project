/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";

class Language extends React.Component {
  tableBodyComponent = (data) => {
    let retval = [];

    retval = data.map(x => (
      <tr key={x.id}>
        <td>{x.language_type}</td>
        <td>{x.language}</td>
        <td>{x.ability === "A" ? "Aktif" : "Pasif"}</td>
        <td style={{ width: "15%" }} className="text-center">
          <div className="btn-group">
            <button type="button" className="btn btn-default btn-xs" onClick={() => this.editHandler(x.id)}><i className="fa fa-edit" />{" "}Edit</button>
           <button type="button" className="btn btn-default btn-xs"><i className="fa fa-trash-o" />{" "}Hapus</button>
          </div>
        </td>
      </tr>
    ));

    return (<tbody>{retval}</tbody>);
  }

  editHandler = (id) => {
    const { history } = this.props;

    history.push(`/keluarga-anak/detail/edit/${id}`);
  }

  render() {
    const { apiRedux } = this.props;

    if (apiRedux.isFetching) {
      return (<div>Loading ...</div>);
    }

    if (apiRedux.data === null) {
      return (<div>Data is empty</div>);
    }

    return (
      <table className="table table-bordered">
        <thead>
          <tr>
            <th>Jenis Bahasa</th>
            <th>Bahasa</th>
            <th>Kemampuan Bicara</th>
            <th width="15%"></th>
          </tr>
        </thead>
        {this.tableBodyComponent(apiRedux.data)}
      </table>
    );
  }
}

const mapStateToProps = state => ({
  apiRedux: state.apiRedux.employeeLanguage,
});

const reduxComponent = connect(mapStateToProps)(Language);

export default reduxComponent;
