/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import CoreHoC from "../../CoreHoC";
import Profile from "./Profile";
import Parent from "./Parent";
import Kid from "./Kid";
import Spouse from "./Spouse";
import Education from "./Education";
import Language from "./Language";
import SKP from "./SKP";
import KGB from "./KGB";
import Staffing from "./Staffing";
import * as apiActions from "../../../actions/apiActions";

const tabData = [
  {
    id: "1",
    title: "Profile",
  },
  {
    id: "2",
    title: "Suami Istri",
  },
  {
    id: "3",
    title: "Anak",
  },
  {
    id: "4",
    title: "Orang Tua",
  },
  {
    id: "5",
    title: "Pendidikan",
  },
  {
    id: "6",
    title: "Bahasa",
  },
  {
    id: "7",
    title: "SKP",
  },
  {
    id: "8",
    title: "KGB",
  },
  {
    id: "9",
    title: "Kepegawaian",
  },
];
const TabPosition = {
  PROFILE: "1",
  SPOUSE: "2",
  KID: "3",
  PARENT: "4",
  EDUCATION: "5",
  LANGUAGE: "6",
  SKP: "7",
  KGB: "8",
  STAFFING: "9",
};
class EmployeeDetail extends React.Component {
  initialButtonActions = [
    {
      id: "1",
      type: null,
      content: (
        <span>
          Batal
        </span>
      ),
      action: () => this.gotoBasePath(),
      isDisabled: false,
    },
    {
      id: "2",
      type: "primary",
      content: (
        <span>
          Simpan
        </span>
      ),
      action: () => this.saveDataHandler(),
      isDisabled: false,
    },
  ];

  constructor(props) {
    super(props);

    this.state = {
      footerButtons: this.initialButtonActions,
      currentTab: "1",
    };
  }

  componentWillMount = () => {
    const { assignBreadcrumbs } = this.props;

    const breadcrumbsData = [
      {
        label: "App", link: "#",
      },
      {
        label: "Pegawai", link: "#",
      },
      {
        label: "Data Pegawai", link: "/pegawai",
      },
    ];

    breadcrumbsData.push("Detail Pegawai");

    assignBreadcrumbs(breadcrumbsData);
  }

  componentDidMount = () => {
    this.fetchData();
  }

  fetchData = () => {
    const {
      match: { params },
      fetchEmployeeDetail,
      fetchEmployeeSpouse,
      fetchEmployeeKid,
      fetchEmployeeParent,
      fetchEmployeeSchool,
      fetchEmployeeLanguage,
      fetchSkpEmployees,
    } = this.props;

    const employeeParam = { idEmployee: params.id };
    fetchEmployeeDetail(params.id);
    fetchEmployeeSpouse(employeeParam);
    fetchEmployeeKid(employeeParam);
    fetchEmployeeParent(employeeParam);
    fetchEmployeeSchool(employeeParam);
    fetchEmployeeLanguage(employeeParam);
    fetchSkpEmployees(employeeParam);
    this.fetchStaffing(employeeParam);
  }

  fetchStaffing = (param) => {
    const {
      fetchEmployeePositions,
      fetchEmployeeRetireDate,
      fetchEmployeeLevels,
      fetchEmployeePenalties,
      fetchEmployeeTrainings,
      fetchEmployeeHonors,
      fetchEmployeeAssignments,
      fetchEmployeeSeminars,
      fetchEmployeeLeaves,
      fetchEmployeeJobTrainings,
      fetchEmployeeAllowances,
      fetchEmployeeMutations,
    } = this.props;

    fetchEmployeeLevels(param);
    fetchEmployeePositions(param);
    fetchEmployeeRetireDate(param.idEmployee);
    fetchEmployeePenalties(param);
    fetchEmployeeTrainings(param);
    fetchEmployeeHonors(param);
    fetchEmployeeAssignments(param);
    fetchEmployeeSeminars(param);
    fetchEmployeeLeaves(param);
    fetchEmployeeJobTrainings(param);
    fetchEmployeeAllowances(param);
    fetchEmployeeMutations(param);
  }

  tabClickHandler = (e, id) => {
    e.preventDefault();

    this.setState({ currentTab: id });
  }

  tabComponent = () => {
    const { currentTab } = this.state;

    return (
      <div className="nav-tabs-custom">
        <ul className="nav nav-tabs">
          { tabData.map(x => (<li key={x.id} className={currentTab === x.id ? "active" : ""}><a href="#" onClick={e => this.tabClickHandler(e, x.id)}>{x.title}</a></li>)) }
        </ul>
        <div className="tab-content">
          {this.tabBody(currentTab)}
        </div>
      </div>
    );
  }

  tabBody = (currentTab) => {
    const { props } = this;
    let retval = null;

    switch (currentTab) {
      case TabPosition.PROFILE:
        retval = (<Profile {...props} />);
        break;
      case TabPosition.SPOUSE:
        retval = (<Spouse {...props} />);
        break;
      case TabPosition.KID:
        retval = (<Kid {...props} />);
        break;
      case TabPosition.PARENT:
        retval = (<Parent {...props} />);
        break;
      case TabPosition.EDUCATION:
        retval = (<Education {...props} />);
        break;
      case TabPosition.LANGUAGE:
        retval = (<Language {...props} />);
        break;
      case TabPosition.SKP:
        retval = (<SKP {...props} />);
        break;
      case TabPosition.KGB:
        retval = (<KGB {...props} />);
        break;
      case TabPosition.STAFFING:
        retval = (<Staffing {...props} />);
        break;
      default:
        retval = (<div />);
    }

    return retval;
  }

  createTitle = (props) => {
    let retval = "Detail Pegawai";
    const { apiRedux } = props;

    if (apiRedux.isFetching) {
      retval = "Loading...";
    } else if (apiRedux.data) {
      retval = `${apiRedux.data.fullname} | NIP: ${apiRedux.data.nip}`;
    }

    return retval;
  }

  render() {
    return (
      <div>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-12">
                <h3 className="box-title">{this.createTitle(this.props)}</h3>
              </div>
            </div>
          </div>
          <div className="box-body">
            <div className="row mb-0">
              <div className="col-sm-12">
                {this.tabComponent()}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  apiRedux: state.apiRedux.employeeDetail,
});

const mapDispatchToProps = dispatch => ({
  fetchEmployeeDetail: payload => dispatch(apiActions.fetchEmployeeDetail(payload)),
  fetchEmployeeSpouse: payload => dispatch(apiActions.fetchEmployeeSpouse(payload)),
  fetchSkpEmployees: payload => dispatch(apiActions.fetchSkpEmployees(payload)),
  fetchEmployeeKid: payload => dispatch(apiActions.fetchEmployeeKid(payload)),
  fetchEmployeeParent: payload => dispatch(apiActions.fetchEmployeeParent(payload)),
  fetchEmployeeSchool: payload => dispatch(apiActions.fetchEmployeeSchool(payload)),
  fetchEmployeeLanguage: payload => dispatch(apiActions.fetchEmployeeLanguage(payload)),
  fetchEmployeePositions: payload => dispatch(apiActions.fetchEmployeePositions(payload)),
  fetchEmployeeRetireDate: id => dispatch(apiActions.fetchEmployeeRetireDate(id)),
  fetchEmployeeLevels: payload => dispatch(apiActions.fetchEmployeeLevels(payload)),
  fetchEmployeePenalties: payload => dispatch(apiActions.fetchEmployeePenalties(payload)),
  fetchEmployeeTrainings: payload => dispatch(apiActions.fetchEmployeeTrainings(payload)),
  fetchEmployeeHonors: payload => dispatch(apiActions.fetchEmployeeHonors(payload)),
  fetchEmployeeAssignments: payload => dispatch(apiActions.fetchEmployeeAssignments(payload)),
  fetchEmployeeSeminars: payload => dispatch(apiActions.fetchEmployeeSeminars(payload)),
  fetchEmployeeLeaves: payload => dispatch(apiActions.fetchEmployeeLeaves(payload)),
  fetchEmployeeJobTrainings: payload => dispatch(apiActions.fetchEmployeeJobTrainings(payload)),
  fetchEmployeeAllowances: payload => dispatch(apiActions.fetchEmployeeAllowances(payload)),
  fetchEmployeeMutations: payload => dispatch(apiActions.fetchEmployeeMutations(payload)),
});

const reduxComponent = connect(mapStateToProps, mapDispatchToProps)(EmployeeDetail);

export default CoreHoC(reduxComponent);
