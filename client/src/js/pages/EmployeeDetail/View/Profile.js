/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import PlaceholderImg from "~/images/avatar-placeholder.png";
import Util from "../../../utils";

const style = {
  nip: {
    borderTop: "1px solid #cecece",
    borderBottom: "1px solid #cecece",
  },
};
class Profile extends React.Component {
  contentComponent = (data) => {
    let imageData = PlaceholderImg;

    if (data.imageFile) {
      imageData = Util.createPathPreview(data.imageFile);
    }

    return (
      <div className="row" style={{ marginRight: "0px" }}>
        <div className="col-sm-3">
          <img src={imageData} style={{ width: "95%" }} />
        </div>
        <div className="col-sm-9">
          {this.employeeInformation(data)}
        </div>
      </div>
    );
  }

  employeeInformation = data => (
    <div className="employee-information">
      <div className="row">
        <div className="col-sm-3"></div>
        <div className="col-sm-9"><h4>{data.fullname}</h4></div>
      </div>
      <div className="row" style={style.nip}>
        <div className="col-sm-3 text-right"><b>NIP</b></div>
        <div className="col-sm-9">{data.nip}</div>
      </div>
      <div className="row">
        <div className="col-sm-3 text-right"><b>Jenis Kelamin</b></div>
        <div className="col-sm-9"><i className="fa fa-intersex" style={{ marginRight: "8px" }} />{data.gender === "L" ? "Laki - Laki" : "Perempuan"}</div>
      </div>
      <div className="row">
        <div className="col-sm-3 text-right"><b>Tempat Tanggal Lahir</b></div>
        <div className="col-sm-9"><i className="fa fa-map-marker" style={{ marginRight: "8px" }} />{`${data.birth_place}, ${moment(data.birth_date).format("DD MMMM YYYY")}`}</div>
      </div>
      <div className="row">
        <div className="col-sm-3 text-right"><b>Umur</b></div>
        <div className="col-sm-9">{this.getAge(data.birth_date)}</div>
      </div>
      <div className="row">
        <div className="col-sm-3 text-right"><b>Golongan Darah</b></div>
        <div className="col-sm-9">{data.blood_type || "-"}</div>
      </div>
      <div className="row">
        <div className="col-sm-3 text-right"><b>Agama</b></div>
        <div className="col-sm-9">{data.religion_name}</div>
      </div>
      <div className="row">
        <div className="col-sm-3 text-right"><b>Status Pernikahan</b></div>
        <div className="col-sm-9">{data.marital_status_name}</div>
      </div>
      <div className="row">
        <div className="col-sm-3 text-right"><b>No. Telp</b></div>
        <div className="col-sm-9"><i className="fa fa-mobile" style={{ marginRight: "8px" }} />{data.phone}</div>
      </div>
      <div className="row">
        <div className="col-sm-3 text-right"><b>Email</b></div>
        <div className="col-sm-9">{data.email || "-"}</div>
      </div>
      <div className="row">
        <div className="col-sm-3 text-right"><b>Alamat</b></div>
        <div className="col-sm-9">{data.address}</div>
      </div>
      <div className="row">
        <div className="col-sm-3 text-right"><b>Status Kepegawaian</b></div>
        <div className="col-sm-9">{data.employee_status}</div>
      </div>
      <div className="row">
        <div className="col-sm-3 text-right"><b>Unit Kerja</b></div>
        <div className="col-sm-9">{data.work_unit_name}</div>
      </div>
    </div>
  )

  getAge = (birthDate) => {
    const a = moment();
    const b = moment(birthDate);
    const age = moment.duration(a.diff(b));
    const years = age.years();
    const months = age.months();
    const days = age.days();

    return `${years} tahun, ${months} bulan, ${days} hari`;
  }

  render() {
    const { apiRedux } = this.props;

    if (apiRedux.isFetching) {
      return (<div>Loading ...</div>);
    }

    if (apiRedux.data === null) {
      return (<div>Data is empty</div>);
    }

    return (<div style={{ padding: "1em" }}>{this.contentComponent(apiRedux.data)}</div>);
  }
}

const mapStateToProps = state => ({
  apiRedux: state.apiRedux.employeeDetail,
});

const reduxComponent = connect(mapStateToProps)(Profile);

export default reduxComponent;
