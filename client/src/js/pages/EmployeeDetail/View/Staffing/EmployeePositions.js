/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import {
  setActiveEmployeePosition, deleteEmployeePosition,
} from "../../../../data";
import Util from "../../../../utils";
import { formatDate } from "./helper";

class EmployeePositions extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: props.apiRedux.data,
    };
  }

  componentWillReceiveProps = (nextProps) => {
    if (!nextProps.apiRedux.isFetching) {
      this.setState({ data: nextProps.apiRedux.data });
    }
  }

  fetchingData = (param) => {
    const {
      fetchEmployeePositions: fetchData,
    } = this.props;

    fetchData(param);
  }

  setActive = async (val) => {
    await setActiveEmployeePosition({ id: val.id, idEmployee: val.id_employee });

    this.fetchingData({ idEmployee: val.id_employee });
  }

  deleteHandler = async (val) => {
    await deleteEmployeePosition({ id: val.id });

    this.fetchingData({ idEmployee: val.id_employee });
  }


  editHandler = (id) => {
    const { history } = this.props;

    history.push(`/kepegawaian-jabatan/detail/edit/${id}`);
  }

  downloadButton = (val) => {
    const parsedVal = JSON.parse(val);

    return (
      <div>
        <a href={Util.createPathPreview(parsedVal.filepath)} target="_blank">Download</a>
      </div>
    );
  }

  tableBodyComponent = (data) => {
    let retval = [
      (
        <tr key="empty-row">
          <td colSpan="7" className="text-center">Tidak ada data</td>
        </tr>
      ),
    ];

    if (data.length > 0) {
      retval = data.map((x, i) => (
        <tr key={x.id}>
          <td>{i + 1}</td>
          <td>-{" "}{x.position_name}<br />-{" "}{x.echelon_name}</td>
          <td>-{" "}{x.sk_number}<br />-{" "}{formatDate(x.sk_date)}</td>
          <td>
            {`${formatDate(x.tmt_position_from)} s/d ${formatDate(x.tmt_position_to)}`}
            {x.file && this.downloadButton(x.file)}
          </td>
          <td>{x.is_active ? "Aktif" : "-"}</td>
          <td>
              <button type="button" className="btn btn-default btn-xs" onClick={() => { this.setActive(x); }}>Set</button>
          </td>
          <td style={{ width: "15%" }} className="text-center">
            <div className="btn-group">
             <button type="button" className="btn btn-default btn-xs" onClick={() => this.editHandler(x.id)}><i className="fa fa-edit" />{" "}Edit</button>
             <button type="button" className="btn btn-default btn-xs" onClick={() => this.deleteHandler(x)} ><i className="fa fa-trash-o" />{" "}Hapus</button>
            </div>
          </td>
        </tr>
      ));
    }

    return (<tbody>{retval}</tbody>);
  }

  render() {
    const { data } = this.state;

    return (
      <table className="table table-bordered table-th-valign-middle">
        <thead>
          <tr>
            <th>No</th>
            <th>Jabatan<br />Eselon</th>
            <th>No. SK<br />Tanggal SK</th>
            <th>TMT / SK</th>
            <th>Status</th>
            <th>Set</th>
            <th width="10%"><center><i className="fa fa-code fa-lg"></i></center></th>
          </tr>
        </thead>
        {this.tableBodyComponent(data)}
      </table>
    );
  }
}

const mapStateToProps = state => ({
  apiRedux: state.apiRedux.employeePositions,
});

const reduxComponent = connect(mapStateToProps)(EmployeePositions);

export default reduxComponent;
