/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import {
  deleteEmployeePenalty,
} from "../../../../data";
import { formatDate } from "./helper";

class EmployeePenalties extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: props.apiRedux.data,
    };
  }

  componentWillReceiveProps = (nextProps) => {
    if (!nextProps.apiRedux.isFetching) {
      this.setState({ data: nextProps.apiRedux.data });
    }
  }

  fetchingData = (param) => {
    const {
      fetchEmployeePenalties: fetchData,
    } = this.props;

    fetchData(param);
  }

  deleteHandler = async (val) => {
    await deleteEmployeePenalty({ id: val.id });

    this.fetchingData({ idEmployee: val.id_employee });
  }

  editHandler = (id) => {
    const { history } = this.props;

    history.push(`/kepegawaian-hukuman/detail/edit/${id}`);
  }

  tableBodyComponent = (data) => {
    let retval = [
      (
        <tr key="empty-row">
          <td colSpan="10" className="text-center">Tidak ada data</td>
        </tr>
      ),
    ];

    if (data && data.length > 0) {
      retval = data.map((x, i) => (
        <tr key={x.id}>
          <td>{i + 1}</td>
          <td>{x.penalty_type_name}</td>
          <td>{x.sk_official_certifier}</td>
          <td>{x.sk_number}</td>
          <td>{formatDate(x.sk_date)}</td>
          <td>{x.penalty_restorer_official}</td>
          <td>{x.restore_penalty_number}</td>
          <td>{formatDate(x.restore_penalty_date)}</td>
          <td style={{ width: "15%" }} className="text-center">
            <div className="btn-group">
             <button type="button" className="btn btn-default btn-xs" onClick={() => this.editHandler(x.id)}><i className="fa fa-edit" />{" "}Edit</button>
             <button type="button" className="btn btn-default btn-xs" onClick={() => this.deleteHandler(x)}><i className="fa fa-trash-o" />{" "}Hapus</button>
            </div>
          </td>
        </tr>
      ));
    }

    return (<tbody>{retval}</tbody>);
  }

  editHandler = (id) => {
    const { history } = this.props;

    history.push(`/kepegawaian-hukuman/detail/edit/${id}`);
  }

  render() {
    const { data } = this.state;

    return (
      <table className="table table-bordered table-th-valign-middle">
        <thead className="thin-border-bottom">
          <tr>
            <th rowSpan="2" width="1%">No<br />&nbsp;</th>
            <th rowSpan="2">Jenis Hukuman<br />&nbsp;</th>
            <th colSpan="3">Surat Keputusan</th>
            <th colSpan="3">Pemulihan</th>
            <th rowSpan="2" width="15%"><center><i className="fa fa-code fa-lg"></i></center></th>
          </tr>
          <tr>
            <th>Pejabat</th>
            <th>No. SK</th>
            <th>Tgl</th>
            <th>Pejabat</th>
            <th>Nomor</th>
            <th>Tgl</th>
          </tr>
        </thead>
        {this.tableBodyComponent(data)}
      </table>
    );
  }
}

const mapStateToProps = state => ({
  apiRedux: state.apiRedux.employeePenalties,
});

const reduxComponent = connect(mapStateToProps)(EmployeePenalties);

export default reduxComponent;
