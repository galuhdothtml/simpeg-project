/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import {
  deleteEmployeeLeave,
} from "../../../../data";
import { formatDate } from "./helper";

class EmployeeLeaves extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.apiRedux.data,
    };
  }

  componentWillReceiveProps = (nextProps) => {
    if (!nextProps.apiRedux.isFetching) {
      this.setState({ data: nextProps.apiRedux.data });
    }
  }

  fetchingData = (param) => {
    const {
      fetchEmployeeLeaves: fetchData,
    } = this.props;

    fetchData(param);
  }

  deleteHandler = async (val) => {
    await deleteEmployeeLeave({ id: val.id });

    this.fetchingData({ idEmployee: val.id_employee });
  }


  editHandler = (id) => {
    const { history } = this.props;

    history.push(`/kepegawaian-cuti/detail/edit/${id}`);
  }

  detailButton = () => (
      <div>
        <a href="#" target="_blank">Detail</a>
      </div>
  )

  tableBodyComponent = (data) => {
    let retval = [
      (
        <tr key="empty-row">
          <td colSpan="7" className="text-center">Tidak ada data</td>
        </tr>
      ),
    ];

    if (data && data.length > 0) {
      retval = data.map((x, i) => (
        <tr key={x.id}>
          <td>{i + 1}</td>
          <td>{x.leave_type}</td>
          <td>{x.leave_number}</td>
          <td>{formatDate(x.leave_letter_date)}</td>
          <td>{`${formatDate(x.start_date)} s/d ${formatDate(x.end_date)}`}</td>
          <td style={{ width: "15%" }} className="text-center">
            <div className="btn-group">
             <button type="button" className="btn btn-default btn-xs" onClick={() => this.editHandler(x.id)}><i className="fa fa-edit" />{" "}Edit</button>
             <button type="button" className="btn btn-default btn-xs" onClick={() => this.deleteHandler(x)} ><i className="fa fa-trash-o" />{" "}Hapus</button>
            </div>
          </td>
          <td>{this.detailButton()}</td>
        </tr>
      ));
    }

    return (<tbody>{retval}</tbody>);
  }

  render() {
    const { data } = this.state;

    return (
      <table className="table table-bordered table-th-valign-middle">
        <thead>
          <tr>
            <th>No</th>
            <th>Jenis Cuti</th>
            <th>No. Surat Cuti</th>
            <th>Tgl. Surat Cuti</th>
            <th>Tanggal Pelaksanaan</th>
            <th width="15%"><center><i className="fa fa-code fa-lg"></i></center></th>
            <th>View</th>
          </tr>
        </thead>
        {this.tableBodyComponent(data)}
      </table>
    );
  }
}

const mapStateToProps = state => ({
  apiRedux: state.apiRedux.employeeLeaves,
});

const reduxComponent = connect(mapStateToProps)(EmployeeLeaves);

export default reduxComponent;
