/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import {
  deleteEmployeeAllowance,
} from "../../../../data";
import { formatDate } from "./helper";

class EmployeeAllowances extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.apiRedux.data,
    };
  }

  componentWillReceiveProps = (nextProps) => {
    if (!nextProps.apiRedux.isFetching) {
      this.setState({ data: nextProps.apiRedux.data });
    }
  }

  fetchingData = (param) => {
    const {
      fetchEmployeeAllowances: fetchData,
    } = this.props;

    fetchData(param);
  }

  deleteHandler = async (val) => {
    await deleteEmployeeAllowance({ id: val.id });

    this.fetchingData({ idEmployee: val.id_employee });
  }


  editHandler = (id) => {
    const { history } = this.props;

    history.push(`/kepegawaian-tunjangan/detail/edit/${id}`);
  }

  detailButton = () => (
      <div>
        <a href="#" target="_blank">Detail</a>
      </div>
  )

  tableBodyComponent = (data) => {
    let retval = [
      (
        <tr key="empty-row">
          <td colSpan="7" className="text-center">Tidak ada data</td>
        </tr>
      ),
    ];

    if (data && data.length > 0) {
      retval = data.map((x, i) => (
        <tr key={x.id}>
          <td>{i + 1}</td>
          <td>{x.allowance_type}</td>
          <td>{x.allowance_number}</td>
          <td>{formatDate(x.allowance_date)}</td>
          <td>{formatDate(x.start_date)}</td>
          <td style={{ width: "15%" }} className="text-center">
            <div className="btn-group">
             <button type="button" className="btn btn-default btn-xs" onClick={() => this.editHandler(x.id)}><i className="fa fa-edit" />{" "}Edit</button>
             <button type="button" className="btn btn-default btn-xs" onClick={() => this.deleteHandler(x)} ><i className="fa fa-trash-o" />{" "}Hapus</button>
            </div>
          </td>
          <td>{this.detailButton()}</td>
        </tr>
      ));
    }

    return (<tbody>{retval}</tbody>);
  }

  render() {
    const { data } = this.state;

    return (
      <table className="table table-bordered table-th-valign-middle">
        <thead>
          <tr>
            <th>No</th>
            <th>Jenis Tunjangan</th>
            <th>Nomor</th>
            <th>Tanggal</th>
            <th>Terhitung Mulai</th>
            <th width="15%"><center><i className="fa fa-code fa-lg"></i></center></th>
            <th>View</th>
          </tr>
        </thead>
        {this.tableBodyComponent(data)}
      </table>
    );
  }
}

const mapStateToProps = state => ({
  apiRedux: state.apiRedux.employeeAllowances,
});

const reduxComponent = connect(mapStateToProps)(EmployeeAllowances);

export default reduxComponent;
