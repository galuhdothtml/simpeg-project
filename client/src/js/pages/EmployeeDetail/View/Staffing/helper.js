export const formatDate = (val) => {
    if (val) {
        return moment(val).format("DD MMMM YYYY");
    }

    return "-";
};