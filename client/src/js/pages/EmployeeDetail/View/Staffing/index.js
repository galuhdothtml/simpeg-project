/* eslint prop-types: 0 */
import React from "react";
import ModalPopup from "../../../../components/form/ModalPopup";
import EmployeePositions from "./EmployeePositions";
import EmployeeRetireDate from "./EmployeeRetireDate";
import EmployeeLevels from "./EmployeeLevels";
import EmployeePenalties from "./EmployeePenalties";
import EmployeeTrainings from "./EmployeeTrainings";
import EmployeeHonors from "./EmployeeHonors";
import EmployeeAssignments from "./EmployeeAssignments";
import EmployeeSeminars from "./EmployeeSeminars";
import EmployeeLeaves from "./EmployeeLeaves";
import EmployeeJobTrainings from "./EmployeeJobTrainings";
import EmployeeAllowances from "./EmployeeAllowances";
import EmployeeMutations from "./EmployeeMutations";

const menuData = [
  {
    id: "1",
    title: "Pensiun",
    icon: "fa fa-calendar",
  },
  {
    id: "2",
    title: "Naik Pangkat",
    icon: "fa fa-calendar",
  },
  {
    id: "3",
    title: "Naik Gaji",
    icon: "fa fa-calendar",
  },
  {
    id: "4",
    title: "Jabatan",
    icon: "fa fa-star",
  },
  {
    id: "5",
    title: "Kepangkatan",
    icon: "fa fa-star",
  },
  {
    id: "6",
    title: "Hukuman",
    icon: "fa fa-gavel",
  },
  {
    id: "7",
    title: "Diklat",
    icon: "fa fa-graduation-cap",
  },
  {
    id: "8",
    title: "Penghargaan",
    icon: "fa fa-certificate",
  },
  {
    id: "9",
    title: "Penugasan LN",
    icon: "fa fa-flag",
  },
  {
    id: "10",
    title: "Seminar",
    icon: "fa fa-desktop",
  },
  {
    id: "11",
    title: "Cuti",
    icon: "fa fa-calendar",
  },
  {
    id: "12",
    title: "Latihan Jabatan",
    icon: "fa fa-book",
  },
  {
    id: "13",
    title: "Mutasi",
    icon: "fa fa-money",
  },
  {
    id: "14",
    title: "Tunjangan",
    icon: "fa fa-book",
  },
];
const style = {
  wrapper: {
    width: "80%",
    position: "relative",
    margin: "10px auto",
  },
  modalBody: {
    padding: "16px",
    paddingBottom: "1px",
    textAlign: "left",
  },
};
class Staffing extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showPopup: false,
      currentId: "",
    };
  }

  showModal = (e, val) => {
    e.preventDefault();
    this.setState({ currentId: val, showPopup: true });
  }

  hideModal = () => {
    this.setState({ showPopup: false });
  }

  createPopupTitle = () => {
    const { currentId } = this.state;
    let retval = "";

    if (currentId === "1") {
      retval = "Schedule Pensiun";
    } else if (currentId === "4") {
      retval = "Riwayat Jabatan";
    } else if (currentId === "5") {
      retval = "Riwayat Pangkat";
    } else if (currentId === "6") {
      retval = "Riwayat Hukuman";
    } else if (currentId === "7") {
      retval = "Riwayat Diklat";
    } else if (currentId === "8") {
      retval = "Riwayat Penghargaan";
    } else if (currentId === "9") {
      retval = "Riwayat Penugasan";
    } else if (currentId === "10") {
      retval = "Riwayat Seminar";
    } else if (currentId === "11") {
      retval = "Riwayat Cuti";
    } else if (currentId === "12") {
      retval = "Riwayat Pelatihan Jabatan";
    } else if (currentId === "13") {
      retval = "Riwayat Mutasi";
    } else if (currentId === "14") {
      retval = "Riwayat Tunjangan";
    }

    return retval;
  }

  modalBodyComponent = () => {
    const { currentId } = this.state;
    let retval = <div />;

    if (currentId === "1") {
      retval = (<EmployeeRetireDate {...this.props} />);
    } else if (currentId === "4") {
      retval = (<EmployeePositions {...this.props} />);
    } else if (currentId === "5") {
      retval = (<EmployeeLevels {...this.props} />);
    } else if (currentId === "6") {
      retval = (<EmployeePenalties {...this.props} />);
    } else if (currentId === "7") {
      retval = (<EmployeeTrainings {...this.props} />);
    } else if (currentId === "8") {
      retval = (<EmployeeHonors {...this.props} />);
    } else if (currentId === "9") {
      retval = (<EmployeeAssignments {...this.props} />);
    } else if (currentId === "10") {
      retval = (<EmployeeSeminars {...this.props} />);
    } else if (currentId === "11") {
      retval = (<EmployeeLeaves {...this.props} />);
    } else if (currentId === "12") {
      retval = (<EmployeeJobTrainings {...this.props} />);
    } else if (currentId === "13") {
      retval = (<EmployeeMutations {...this.props} />);
    } else if (currentId === "14") {
      retval = (<EmployeeAllowances {...this.props} />);
    }

    return retval;
  }

  render() {
    const { showPopup } = this.state;

    return (
      <div>
        <div style={style.wrapper}>
          {menuData.map(x => (<a key={x.id} className="btn btn-app" onClick={e => this.showModal(e, x.id)}><i className={x.icon} />{x.title}</a>))}
        </div>
        {showPopup && (
          <ModalPopup width={1200} title={this.createPopupTitle()} hideModal={this.hideModal}>
            <div style={style.modalBody}>
              {this.modalBodyComponent()}
            </div>
          </ModalPopup>
        )}
      </div>
    );
  }
}

export default Staffing;
