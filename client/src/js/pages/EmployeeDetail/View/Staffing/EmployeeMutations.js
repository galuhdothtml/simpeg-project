/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import {
  deleteEmployeeMutation,
} from "../../../../data";
import { formatDate } from "./helper";

const mutationTypeText = (type) => {
  let retval = "";

  if (type === "1") {
    retval = "Masuk";
  } else if (type === "2") {
    retval = "Keluar";
  } else if (type === "3") {
    retval = "Pindah Antar Instansi";
  } else if (type === "4") {
    retval = "Pensiun";
  } else if (type === "5") {
    retval = "Wafat";
  } else if (type === "6") {
    retval = "Kenaikan Pangkat";
  }

  return retval;
};
class EmployeeMutations extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.apiRedux.data,
    };
  }

  componentWillReceiveProps = (nextProps) => {
    if (!nextProps.apiRedux.isFetching) {
      this.setState({ data: nextProps.apiRedux.data });
    }
  }

  fetchingData = (param) => {
    const {
      fetchEmployeeMutations: fetchData,
    } = this.props;

    fetchData(param);
  }

  deleteHandler = async (val) => {
    await deleteEmployeeMutation({ id: val.id });

    this.fetchingData({ idEmployee: val.id_employee });
  }


  editHandler = (id) => {
    const { history } = this.props;

    history.push(`/mutasi/detail/edit/${id}`);
  }

  tableBodyComponent = (data) => {
    let retval = [
      (
        <tr key="empty-row">
          <td colSpan="6" className="text-center">Tidak ada data</td>
        </tr>
      ),
    ];

    if (data && data.length > 0) {
      retval = data.map((x, i) => (
        <tr key={x.id}>
          <td>{i + 1}</td>
          <td>{mutationTypeText(x.mutation_type)}</td>
          <td>{formatDate(x.mutation_date)}</td>
          <td>{x.sk_number}</td>
          <td style={{ width: "15%" }} className="text-center">
            <div className="btn-group">
             <button type="button" className="btn btn-default btn-xs" onClick={() => this.editHandler(x.id)}><i className="fa fa-edit" />{" "}Edit</button>
             <button type="button" className="btn btn-default btn-xs" onClick={() => this.deleteHandler(x)} ><i className="fa fa-trash-o" />{" "}Hapus</button>
            </div>
          </td>
        </tr>
      ));
    }

    return (<tbody>{retval}</tbody>);
  }

  render() {
    const { data } = this.state;

    return (
      <table className="table table-bordered table-th-valign-middle">
        <thead>
          <tr>
            <th>No</th>
            <th>Jenis Mutasi</th>
            <th>Tanggal</th>
            <th>Nomor SK</th>
            <th width="15%"><center><i className="fa fa-code fa-lg"></i></center></th>
          </tr>
        </thead>
        {this.tableBodyComponent(data)}
      </table>
    );
  }
}

const mapStateToProps = state => ({
  apiRedux: state.apiRedux.employeeMutations,
});

const reduxComponent = connect(mapStateToProps)(EmployeeMutations);

export default reduxComponent;
