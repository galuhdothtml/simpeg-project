/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import {
  deleteEmployeeSeminar,
} from "../../../../data";
import Util from "../../../../utils";
import { formatDate } from "./helper";

class EmployeeSeminars extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.apiRedux.data,
    };
  }

  componentWillReceiveProps = (nextProps) => {
    if (!nextProps.apiRedux.isFetching) {
      this.setState({ data: nextProps.apiRedux.data });
    }
  }

  fetchingData = (param) => {
    const {
      fetchEmployeeSeminars: fetchData,
    } = this.props;

    fetchData(param);
  }

  deleteHandler = async (val) => {
    await deleteEmployeeSeminar({ id: val.id });

    this.fetchingData({ idEmployee: val.id_employee });
  }


  editHandler = (id) => {
    const { history } = this.props;

    history.push(`/kepegawaian-seminar/detail/edit/${id}`);
  }

  downloadButton = (val) => {
    const parsedVal = JSON.parse(val);

    return (
      <div>
        <a href={Util.createPathPreview(parsedVal.filepath)} target="_blank">Download</a>
      </div>
    );
  }

  tableBodyComponent = (data) => {
    let retval = [
      (
        <tr key="empty-row">
          <td colSpan="8" className="text-center">Tidak ada data</td>
        </tr>
      ),
    ];

    if (data && data.length > 0) {
      retval = data.map((x, i) => (
        <tr key={x.id}>
          <td>{i + 1}</td>
          <td>{x.name}</td>
          <td>{x.place}</td>
          <td>{x.organizer}</td>
          <td>{`${formatDate(x.start_date)} s/d ${formatDate(x.end_date)}`}</td>
          <td>-{" "}{x.charter_number}<br />-{" "}{formatDate(x.charter_date)}</td>
          <td>{x.file ? this.downloadButton(x.file) : "-"}</td>
          <td style={{ width: "10%" }} className="text-center">
            <div className="btn-group">
             <button type="button" className="btn btn-default btn-xs" onClick={() => this.editHandler(x.id)}><i className="fa fa-edit" />{" "}Edit</button>
             <button type="button" className="btn btn-default btn-xs" onClick={() => this.deleteHandler(x)} ><i className="fa fa-trash-o" />{" "}Hapus</button>
            </div>
          </td>
        </tr>
      ));
    }

    return (<tbody>{retval}</tbody>);
  }

  render() {
    const { data } = this.state;

    return (
      <table className="table table-bordered table-th-valign-middle">
        <thead>
          <tr>
            <th>No</th>
            <th>Seminar</th>
            <th>Tempat</th>
            <th>Penyelenggara</th>
            <th>Tanggal Pelaksanaan</th>
            <th>No. Piagam< br/>Tgl</th>
            <th>Sertifikat</th>
            <th width="15%"><center><i className="fa fa-code fa-lg"></i></center></th>
          </tr>
        </thead>
        {this.tableBodyComponent(data)}
      </table>
    );
  }
}

const mapStateToProps = state => ({
  apiRedux: state.apiRedux.employeeSeminars,
});

const reduxComponent = connect(mapStateToProps)(EmployeeSeminars);

export default reduxComponent;
