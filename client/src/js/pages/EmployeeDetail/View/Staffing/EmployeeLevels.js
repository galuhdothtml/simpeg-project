/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import {
  setActiveEmployeeLevel, deleteEmployeeLevel,
} from "../../../../data";
import Util from "../../../../utils";
import { formatDate } from "./helper";

class EmployeeLevels extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: props.apiRedux.data,
    };
  }

  componentWillReceiveProps = (nextProps) => {
    if (!nextProps.apiRedux.isFetching) {
      this.setState({ data: nextProps.apiRedux.data });
    }
  }

  fetchingData = (param) => {
    const {
      fetchEmployeeLevels: fetchData,
    } = this.props;

    fetchData(param);
  }

  setActive = async (val) => {
    await setActiveEmployeeLevel({ id: val.id, idEmployee: val.id_employee });

    this.fetchingData({ idEmployee: val.id_employee });
  }

  deleteHandler = async (val) => {
    await deleteEmployeeLevel({ id: val.id });

    this.fetchingData({ idEmployee: val.id_employee });
  }

  editHandler = (id) => {
    const { history } = this.props;

    history.push(`/kepegawaian-pangkat/detail/edit/${id}`);
  }

  downloadButton = (val) => {
    const parsedVal = JSON.parse(val);

    return (
      <div>
        <a href={Util.createPathPreview(parsedVal.filepath)} target="_blank">Download</a>
      </div>
    );
  }

  tableBodyComponent = (data) => {
    let retval = [
      (
        <tr key="empty-row">
          <td colSpan="10" className="text-center">Tidak ada data</td>
        </tr>
      ),
    ];

    if (data && data.length > 0) {
      retval = data.map((x, i) => (
        <tr key={x.id}>
          <td>{i + 1}</td>
          <td>-{" "}{x.level}<br />-{" "}{x.group_name}</td>
          <td>{x.level_type}</td>
          <td>{formatDate(x.tmt_level)}</td>
          <td>{x.sk_official_certifier}</td>
          <td>-{" "}{x.sk_number}<br />-{" "}{formatDate(x.sk_date)}</td>
          <td>{x.file ? this.downloadButton(x.file) : "-"}</td>
          <td>{x.is_active ? "Aktif" : "-"}</td>
          <td style={{ width: "5%" }}>
              <button type="button" className="btn btn-default btn-xs" onClick={() => { this.setActive(x); }}>Set</button>
          </td>
          <td style={{ width: "15%" }} className="text-center">
            <div className="btn-group">
             <button type="button" className="btn btn-default btn-xs" onClick={() => this.editHandler(x.id)}><i className="fa fa-edit" />{" "}Edit</button>
             <button type="button" className="btn btn-default btn-xs" onClick={() => this.deleteHandler(x)}><i className="fa fa-trash-o" />{" "}Hapus</button>
            </div>
          </td>
        </tr>
      ));
    }

    return (<tbody>{retval}</tbody>);
  }

  editHandler = (id) => {
    const { history } = this.props;

    history.push(`/kepegawaian-pangkat/detail/edit/${id}`);
  }

  render() {
    const { data } = this.state;

    return (
      <table className="table table-bordered table-th-valign-middle">
        <thead className="thin-border-bottom">
          <tr>
            <th rowSpan="2" width="1%">No<br />&nbsp;</th>
            <th rowSpan="2">Pangkat<br />Gol</th>
            <th rowSpan="2">Jenis<br />&nbsp;</th>
            <th rowSpan="2">TMT<br />&nbsp;</th>
            <th colSpan="2">Surat Keputusan</th>
            <th rowSpan="2">SK<br />&nbsp;</th>
            <th rowSpan="2">Status<br />&nbsp;</th>
            <th rowSpan="2" width="5%">Set<br />&nbsp;</th>
            <th rowSpan="2" width="15%"><center><i className="fa fa-code fa-lg"></i></center></th>
          </tr>
          <tr>
            <th>Pejabat</th>
            <th>Nomor / TGL</th>
          </tr>
        </thead>
        {this.tableBodyComponent(data)}
      </table>
    );
  }
}

const mapStateToProps = state => ({
  apiRedux: state.apiRedux.employeeLevels,
});

const reduxComponent = connect(mapStateToProps)(EmployeeLevels);

export default reduxComponent;
