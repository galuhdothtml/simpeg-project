/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import { formatDate } from "./helper";

class EmployeeRetireDate extends React.Component {
  tableBodyComponent = (data) => {
    let retval = [];

    retval = (
        <tr key={data.id_employee}>
            <td>{formatDate(data.birth_date)}</td>
            <td>{formatDate(data.retire_date)}</td>
        </tr>
    );

    return (<tbody>{retval}</tbody>);
  }

  render() {
    const { apiRedux } = this.props;

    if (apiRedux.isFetching) {
      return (<div>Loading ...</div>);
    }

    if (apiRedux.data === null) {
      return (<div>Data is empty</div>);
    }

    return (
      <table className="table table-bordered table-th-valign-middle">
        <thead>
          <tr>
            <th>Tanggal Kelahiran</th>
            <th>Tanggal Jatuh Tempo Pensiun</th>
          </tr>
        </thead>
        {this.tableBodyComponent(apiRedux.data)}
      </table>
    );
  }
}

const mapStateToProps = state => ({
  apiRedux: state.apiRedux.employeeRetireDate,
});

const reduxComponent = connect(mapStateToProps)(EmployeeRetireDate);

export default reduxComponent;
