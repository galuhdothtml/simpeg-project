/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import {
  deleteEmployeeAssignment,
} from "../../../../data";

class EmployeeAssignments extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.apiRedux.data,
    };
  }

  componentWillReceiveProps = (nextProps) => {
    if (!nextProps.apiRedux.isFetching) {
      this.setState({ data: nextProps.apiRedux.data });
    }
  }

  fetchingData = (param) => {
    const {
      fetchEmployeeAssignments: fetchData,
    } = this.props;

    fetchData(param);
  }

  deleteHandler = async (val) => {
    await deleteEmployeeAssignment({ id: val.id });

    this.fetchingData({ idEmployee: val.id_employee });
  }


  editHandler = (id) => {
    const { history } = this.props;

    history.push(`/kepegawaian-penugasan/detail/edit/${id}`);
  }

  tableBodyComponent = (data) => {
    let retval = [
      (
        <tr key="empty-row">
          <td colSpan="6" className="text-center">Tidak ada data</td>
        </tr>
      ),
    ];

    if (data && data.length > 0) {
      retval = data.map((x, i) => (
        <tr key={x.id}>
          <td>{i + 1}</td>
          <td>{x.destination}</td>
          <td>{x.year}</td>
          <td>{x.duration}</td>
          <td>{x.reason}</td>
          <td style={{ width: "15%" }} className="text-center">
            <div className="btn-group">
             <button type="button" className="btn btn-default btn-xs" onClick={() => this.editHandler(x.id)}><i className="fa fa-edit" />{" "}Edit</button>
             <button type="button" className="btn btn-default btn-xs" onClick={() => this.deleteHandler(x)} ><i className="fa fa-trash-o" />{" "}Hapus</button>
            </div>
          </td>
        </tr>
      ));
    }

    return (<tbody>{retval}</tbody>);
  }

  render() {
    const { data } = this.state;

    return (
      <table className="table table-bordered table-th-valign-middle">
        <thead>
          <tr>
            <th>No</th>
            <th>Negara Tujuan</th>
            <th>Tahun</th>
            <th>Lama Penugasan (Hari)</th>
            <th>Alasan Penugasan</th>
            <th width="10%"><center><i className="fa fa-code fa-lg"></i></center></th>
          </tr>
        </thead>
        {this.tableBodyComponent(data)}
      </table>
    );
  }
}

const mapStateToProps = state => ({
  apiRedux: state.apiRedux.employeeAssignments,
});

const reduxComponent = connect(mapStateToProps)(EmployeeAssignments);

export default reduxComponent;
