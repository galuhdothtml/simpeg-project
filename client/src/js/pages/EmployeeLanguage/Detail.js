import React from "react";
import update from "immutability-helper";
import PropTypes from "prop-types";
import { FieldFeedbacks, FieldFeedback } from "react-form-with-constraints";
import FormValidation from "~/components/form/FormValidation";
import InputText from "~/components/form/InputText";
import Select from "~/components/form/Select";
import BrowseData from "~/components/form/BrowseData";
import CoreHoC from "../CoreHoC";
import {
  getEmployees, createEmployeeLanguage, updateEmployeeLanguage, getEmployeeLanguage,
} from "../../data";

const employeeColumns = [
  {
    accessor: "fullname",
    Header: "Nama Pegawai",
    width: "100%",
  },
];
const abilityList = [
  {
    id: "", name: "- Pilih Kemampuan Bicara -",
  },
  {
    id: "A", name: "Aktif",
  },
  {
    id: "P", name: "Pasif",
  },
];
class EmployeeLanguageDetail extends React.Component {
  static propTypes = {
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    assignButtons: () => { },
    showNotification: () => { },
  }

  initialButtonActions = [
    {
      id: "1",
      type: null,
      content: (
        <span>
          Batal
        </span>
      ),
      action: () => this.gotoBasePath(),
      isDisabled: false,
    },
    {
      id: "2",
      type: "primary",
      content: (
        <span>
          Simpan
        </span>
      ),
      action: () => this.saveDataHandler(),
      isDisabled: false,
    },
  ];

  constructor(props) {
    super(props);

    this.state = {
      id: "",
      isFormSubmitted: false,
      type: "create",
      form: {
        employee: {
          id: "",
          name: "",
        },
        language_type: "",
        language: "",
        ability: "",
      },
      footerButtons: this.initialButtonActions,
    };
  }

  componentWillMount = () => {
    this.setupData();
  }

  setupData = async () => {
    const { match: { params } } = this.props;

    if (params.type === "edit") {
      await this.setupDetailData(params.id);
    } else {
      this.setupBreadcrumbs("Tambah Data");
    }
  }

  setupBreadcrumbs = (text) => {
    const { assignBreadcrumbs } = this.props;

    assignBreadcrumbs([
      {
        label: "App", link: "#",
      },
      {
        label: "Pegawai", link: "#",
      },
      {
        label: "Riwayat Pendidikan", link: "#",
      },
      {
        label: "Bahasa", link: "/riwayat-pendidikan-bahasa",
      },
      text,
    ]);
  }

  setupDetailData = async (id) => {
    const res = await getEmployeeLanguage(id);

    if (res.status) {
      const { data } = res;
      const newState = {
        id: data.id,
        type: "edit",
        form: {
          employee: {
            id: data.Employee.id,
            name: data.Employee.fullname,
          },
          language_type: data.language_type,
          language: data.language,
          ability: data.ability,
        },
      };

      this.setupBreadcrumbs(data.Employee.fullname);
      this.setState(newState);
    }
  }

  changeValueHandler = async (type, val, e) => {
    const { form } = this.state;

    const { isFormSubmitted } = this.state;

    if (isFormSubmitted && e) {
      await this.onInputChangeValidate(e);
    }

    const newValue = update(form, {
      [type]: { $set: val },
    });

    this.setState({ form: newValue });
  }

  onInputChangeValidate = ({ target }) => {
    this.form.validateInput(target);

    if (this.inputTimeout) {
      clearTimeout(this.inputTimeout);
    }

    this.inputTimeout = setTimeout(() => {
      if (this.form.simpleValidateForm()) {
        this.updateButtonsState();
      } else {
        this.updateButtonsState(false, true);
      }
    }, 300);
  }

  updateButtonsState = (isBtnCancelDisabled = false, isBtnSaveDisabled = false) => {
    const footerButtons = update(this.initialButtonActions, {
      0: { isDisabled: { $set: isBtnCancelDisabled } },
      1: { isDisabled: { $set: isBtnSaveDisabled } },
    });

    this.setState({ footerButtons });
  }

  validateEmployee = async () => {
    const { isFormSubmitted } = this.state;

    if (isFormSubmitted) {
      await this.onInputChangeValidate({ target: this.employeeHidden });
    }
  }

  changeEmployeeHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      employee: { $set: { id: val.id, name: val.fullname } },
    });

    this.setState({ form: newValue }, () => {
      this.validateEmployee();
    });
  }

  onFetchEmployee = async (state) => {
    const payload = {
      limit: 10,
      page: state.page,
      filterText: state.filterText,
      sorted: JSON.stringify({ id: state.fieldToSort, desc: state.sortType === "desc" }),
    };
    const res = await getEmployees(payload);

    return res;
  }

  formComponent = () => {
    const {
      form,
      type,
    } = this.state;
    return (
      <FormValidation ref={(c) => { this.form = c; }}>
        {type === "create" && (
          <div className="row mb-sm">
            <div className="col-sm-12">
              <div className="form-group position-relative mb-0">
                <BrowseData
                  label="Pegawai"
                  placeholder="Pilih Pegawai"
                  columns={employeeColumns}
                  value={form.employee}
                  changeEvent={this.changeEmployeeHandler}
                  onFetch={this.onFetchEmployee}
                />
                <input
                  ref={(c) => { this.employeeHidden = c; }}
                  className="hide-for-validation"
                  name="employeeHidden"
                  type="text"
                  value={form.employee.id}
                  onChange={() => { }}
                  required
                />
              </div>
              <FieldFeedbacks for="employeeHidden">
                <FieldFeedback when="valueMissing">Pegawai wajib diisi</FieldFeedback>
              </FieldFeedbacks>
            </div>
          </div>
        )}
        <div className="row mb-sm">
          <div className="col-sm-4">
            <InputText
              label="Jenis Bahasa"
              changeEvent={(val, e) => this.changeValueHandler("language_type", val, e)}
              value={String(form.language_type)}
              name="language_type"
              required
            />
            <FieldFeedbacks for="language_type">
              <FieldFeedback when="valueMissing">Jenis bahasa wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <InputText
              label="Bahasa"
              changeEvent={(val, e) => this.changeValueHandler("language", val, e)}
              value={String(form.language)}
              name="language"
              required
            />
            <FieldFeedbacks for="language">
              <FieldFeedback when="valueMissing">Bahasa wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
          <div className="col-sm-4">
            <Select
              label="Kemampuan Bicara"
              data={abilityList}
              value={form.ability}
              changeEvent={(val, e) => this.changeValueHandler("ability", val, e)}
              name="ability"
              required
            />
            <FieldFeedbacks for="ability">
              <FieldFeedback when="valueMissing">Kemampuan bicara wajib diisi</FieldFeedback>
            </FieldFeedbacks>
          </div>
        </div>
      </FormValidation>
    );
  }

  gotoBasePath = () => {
    const { history } = this.props;
    history.push("/riwayat-pendidikan-bahasa");
  }

  saveDataHandler = async () => {
    const {
      form, type, id,
    } = this.state;
    this.updateButtonsState(true, true);

    const isFormValid = await this.form.validateForm();

    if (isFormValid) {
      const payload = Object.assign({}, form, {
        id_employee: form.employee.id,
      });
      delete payload.employee;

      if (type === "create") {
        const res = await createEmployeeLanguage(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      } else {
        Object.assign(payload, { id });
        const res = await updateEmployeeLanguage(payload);

        if (res.status) {
          this.gotoBasePath();
          return;
        }
      }
    } else {
      this.updateButtonsState(false, true);
    }

    this.setState({
      isFormSubmitted: true,
    });
  }

  render = () => {
    const {
      footerButtons, type,
    } = this.state;

    return (
      <div style={{ width: "80%", position: "relative", margin: "0px auto" }}>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-12">
                <h3 className="box-title with-title">{type === "create" ? "Tambah" : "Edit"} Data Bahasa</h3>
              </div>
            </div>
          </div>
          <div className="box-body">
            {this.formComponent()}
          </div>
          <div className="box-footer text-right">
            <div>
              {footerButtons.map((x, i) => (<button key={x.id} style={(i === footerButtons.length - 1) ? {} : { marginRight: "10px" }} type="button" className={`btn ${x.type ? `btn-${x.type}` : ""}`} onClick={!x.isDisabled ? x.action : () => { }} disabled={x.isDisabled}>{x.content}</button>))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CoreHoC(EmployeeLanguageDetail);
