/* eslint prop-types: 0 */
import { combineReducers } from "redux";
import apiRedux from "./apiReducers";
import appNavigation from "./appNavigationReducer";

export default combineReducers({
  apiRedux,
  appNavigation,
});
