/* eslint prop-types: 0 */
import { combineReducers } from "redux";
import { ActionTypes } from "../constants";

const activeHeaderId = (state = "", action) => {
  switch (action.type) {
    case ActionTypes.SET_ACTIVE_HEADER_ID:
      return action.value;
    default:
      return state;
  }
};

export default combineReducers({
  activeHeaderId,
});
