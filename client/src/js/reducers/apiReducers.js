/* eslint prop-types: 0 */
import { combineReducers } from "redux";
import { ActionTypes } from "../constants";

const myAccount = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_MY_ACCOUNT:
      return { isFetching: true };
    case ActionTypes.RECEIVE_MY_ACCOUNT:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const appMenu = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_APP_MENU:
      return { isFetching: true };
    case ActionTypes.RECEIVE_APP_MENU:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const employeeDetail = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_EMPLOYEE_DETAIL:
      return { isFetching: true };
    case ActionTypes.RECEIVE_EMPLOYEE_DETAIL:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const employeeSpouse = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_EMPLOYEE_SPOUSE:
      return { isFetching: true };
    case ActionTypes.RECEIVE_EMPLOYEE_SPOUSE:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const employeeKid = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_EMPLOYEE_KID:
      return { isFetching: true };
    case ActionTypes.RECEIVE_EMPLOYEE_KID:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const employeeParent = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_EMPLOYEE_PARENT:
      return { isFetching: true };
    case ActionTypes.RECEIVE_EMPLOYEE_PARENT:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const employeeSchool = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_EMPLOYEE_SCHOOL:
      return { isFetching: true };
    case ActionTypes.RECEIVE_EMPLOYEE_SCHOOL:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const employeeLanguage = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_EMPLOYEE_LANGUAGE:
      return { isFetching: true };
    case ActionTypes.RECEIVE_EMPLOYEE_LANGUAGE:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const employeePositions = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_EMPLOYEE_POSITIONS:
      return { isFetching: true };
    case ActionTypes.RECEIVE_EMPLOYEE_POSITIONS:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const employeeRetireDate = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_EMPLOYEE_RETIRE_DATE:
      return { isFetching: true };
    case ActionTypes.RECEIVE_EMPLOYEE_RETIRE_DATE:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const employeeLevels = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_EMPLOYEE_LEVELS:
      return { isFetching: true };
    case ActionTypes.RECEIVE_EMPLOYEE_LEVELS:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const employeePenalties = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_EMPLOYEE_PENALTIES:
      return { isFetching: true };
    case ActionTypes.RECEIVE_EMPLOYEE_PENALTIES:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const employeeTrainings = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_EMPLOYEE_TRAININGS:
      return { isFetching: true };
    case ActionTypes.RECEIVE_EMPLOYEE_TRAININGS:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const employeeHonors = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_EMPLOYEE_HONORS:
      return { isFetching: true };
    case ActionTypes.RECEIVE_EMPLOYEE_HONORS:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const employeeAssignments = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_EMPLOYEE_ASSIGNMENTS:
      return { isFetching: true };
    case ActionTypes.RECEIVE_EMPLOYEE_ASSIGNMENTS:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const employeeSeminars = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_EMPLOYEE_SEMINARS:
      return { isFetching: true };
    case ActionTypes.RECEIVE_EMPLOYEE_SEMINARS:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const employeeLeaves = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_EMPLOYEE_LEAVES:
      return { isFetching: true };
    case ActionTypes.RECEIVE_EMPLOYEE_LEAVES:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const employeeJobTrainings = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_EMPLOYEE_JOB_TRAININGS:
      return { isFetching: true };
    case ActionTypes.RECEIVE_EMPLOYEE_JOB_TRAININGS:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const employeeAllowances = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_EMPLOYEE_ALLOWANCES:
      return { isFetching: true };
    case ActionTypes.RECEIVE_EMPLOYEE_ALLOWANCES:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

const employeeMutations = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_EMPLOYEE_MUTATIONS:
      return { isFetching: true };
    case ActionTypes.RECEIVE_EMPLOYEE_MUTATIONS:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};


const skpEmployees = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_SKP_EMPLOYEES:
      return { isFetching: true };
    case ActionTypes.RECEIVE_SKP_EMPLOYEES:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};
export default combineReducers({
  myAccount,
  appMenu,
  employeeDetail,
  employeeSpouse,
  employeeKid,
  employeeParent,
  employeeSchool,
  employeeLanguage,
  employeePositions,
  employeeRetireDate,
  employeeLevels,
  employeePenalties,
  employeeTrainings,
  employeeHonors,
  employeeAssignments,
  employeeSeminars,
  employeeLeaves,
  employeeJobTrainings,
  employeeAllowances,
  employeeMutations,
  skpEmployees,
});
