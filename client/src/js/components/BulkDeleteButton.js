/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";

class BulkDeleteButton extends React.Component {
  static propTypes = {
    value: PropTypes.arrayOf(PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ])),
    clickEvent: PropTypes.func,
  }

  static defaultProps = {
    value: [],
    clickEvent: () => {},
  }

  constructor(props) {
    super(props);

    this.state = {
      title: "Halaman Dashboard",
    };
  }

  bulkDeleteHandler = () => {
    const { clickEvent } = this.props;

    clickEvent();
  }


  render() {
    const { value } = this.props;

    if (value.length === 0) {
      return null;
    }

    return (
      <div className="bulk-delete-wrapper">
        <div className="row">
          <div className="col-sm-6">
            <h5 className="text-muted"><b>{value.length} DATA AKAN DIHAPUS ?</b></h5>
          </div>
          <div className="col-sm-6 text-right">
            <button style={{ width: "100px" }} type="button" className="btn btn-danger btn-md btn-flat" onClick={this.bulkDeleteHandler}><i className="fa fa-trash-o" /> Hapus</button>
          </div>
        </div>
      </div>
    );
  }
}

export default BulkDeleteButton;
