/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";

class DatePicker extends React.Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.string,
    errorText: PropTypes.string,
    value: PropTypes.string.isRequired,
  }

  static defaultProps = {
    label: undefined,
    errorText: undefined,
  }

  componentWillReceiveProps = (nextProps) => {
    const { value: pValue } = this.props;

    if (pValue !== nextProps.value) {
      this.setupComponent(nextProps);
    }
  }

  setupComponent = (props) => {
    const { id, value, changeEvent } = props;
    moment.locale("id");
    const $daterangepicker = $(`#${id}`);
    $daterangepicker.daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      drops: "down",
      startDate: moment(value, "YYYY-MM-DD"),
      locale: {
        format: "DD MMMM YYYY",
      },
    });
    $daterangepicker.on("apply.daterangepicker", (ev, picker) => {
      changeEvent(picker.startDate.format("YYYY-MM-DD"));
    });
  }

  componentDidMount = () => {
    this.setupComponent(this.props);
  }

  render() {
    const {
      label, errorText, id,
    } = this.props;

    return (
      <div className={`form-group mb-0 ${(errorText && errorText.length > 0) ? "has-error" : ""}`}>
        {label && <label>{label}</label>}
        <div className="input-group">
          <div className="input-group-addon">
            <i className="fa fa-calendar"></i>
          </div>
          <input type="text" className="form-control pull-right" id={id} />
        </div>
        {(errorText && errorText.length > 0) && <span className="help-block">{errorText}</span>}
      </div>
    );
  }
}

export default DatePicker;
