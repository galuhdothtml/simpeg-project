/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import posed from "react-pose";
import { FormWithConstraints } from "react-form-with-constraints";
import ScrollArea from "react-scrollbar";

const style = {
  modalBody: {
    height: `${(window.innerHeight - 120)}px`,
  },
};
const Box = posed.div({
  hidden: {
    opacity: 0,
    transition: { duration: 300 },
  },
  visible: {
    opacity: 1,
    transition: { duration: 300 },
  },
});
const InnerBox = posed.div({
  hidden: {
    right: "-500px",
    transition: { duration: 300 },
  },
  visible: {
    right: "0px",
    transition: { duration: 300 },
  },
});
class SidePopup extends React.Component {
  static propTypes = {
    onSave: PropTypes.func,
    render: PropTypes.func.isRequired,
    title: PropTypes.string,
  }

  static defaultProps = {
    onSave: () => { },
    title: undefined,
  }

  constructor(props) {
    super(props);

    this.state = {
      isDisplaying: false,
      isVisible: false,
      btnSaveDisabled: false,
      btnSaveClicked: "",
      isSubmitted: false,
    };
  }

  failedCallback = () => {
    this.setState({
      btnSaveClicked: "",
      btnSaveDisabled: false,
    });
  }

  onSubmitHandler = (e) => {
    if (e) {
      e.preventDefault();
    }

    this.doSavingData();
  }

  doSavingData = async () => {
    const { onSave } = this.props;
    const formValidationResults = await this.form.validateForm();
    const formIsValid = this.form.isValid();

    if (formIsValid) {
      const updatedState = {
        btnSaveDisabled: true,
        btnSaveClicked: "Tunggu ...",
      };

      this.setState(updatedState, () => {
        onSave(() => this.failedCallback());
      });
    } else {
      try {
        formValidationResults.forEach(({ name, validations }) => {
          if (validations) {
            validations.forEach(({ type, show }) => {
              if (type === "error" && show) {
                throw name;
              }
            });
          }
        });
      } catch (e) {
        const el = document.getElementsByName(e)[0];

        el.focus();
      }

      this.setState({ btnSaveDisabled: true, isSubmitted: true });
    }
  }

  showPopup = () => {
    this.setState({ isVisible: true, isDisplaying: true });
  }

  hidePopup = () => {
    this.setState({ isVisible: false }, () => {
      setTimeout(() => {
        this.setState({
          isDisplaying: false,
          btnSaveDisabled: false,
          btnSaveClicked: "",
          isSubmitted: false,
        });
      }, 100);
    });
  }

  validateInput = async (target) => {
    const { isSubmitted } = this.state;

    if (isSubmitted) {
      await this.form.validateFields(target);
      this.setState({
        btnSaveDisabled: !this.form.isValid(),
      });
    }
  };

  buttonSaveText = () => {
    const { btnSaveDisabled, btnSaveClicked } = this.state;
    let retval = "Simpan";

    if (btnSaveDisabled && btnSaveClicked) {
      retval = btnSaveClicked;
    }

    return retval;
  }

  render() {
    const { isVisible, btnSaveDisabled, isDisplaying } = this.state;
    const { title, render } = this.props;

    return (
      <div className="sidepopup-container">
        <Box className="modal" pose={isVisible ? "visible" : "hidden"} style={{ display: isDisplaying ? "block" : "none" }}>
          <InnerBox className="modal-dialog sidepopup-container__modal-dialog" pose={isVisible ? "visible" : "hidden"} style={{ width: "500px" }}>
            <div className="modal-content sidepopup-container__modal-content">
              {title && <div className="modal-header">
                <h4 className="modal-title">{title}</h4>
              </div>
              }
              <div className="modal-body" style={style.modalBody}>
                <ScrollArea
                  smoothScrolling={false}
                  horizontal={false}
                  stopScrollPropagation={true}
                  style={{ height: "100%" }}
                >
                  <div style={{ padding: "15px" }}>
                    <FormWithConstraints
                      ref={(c) => { this.form = c; }}
                      onSubmit={this.onSubmitHandler}
                      noValidate
                    >
                      {render && render({ show: isVisible, validateInput: this.validateInput })}
                    </FormWithConstraints>
                  </div>
                </ScrollArea>
              </div>
              <div className="modal-footer">
                <div className="btn pull-left sidepopup-note">
                  <div className="text-muted">Kolom bertanda <span className="text-red">*</span> wajib diisi</div>
                </div>
                <button
                  type="button"
                  className="btn btn-default sidepopup-container__modal-footer-btn"
                  onClick={this.hidePopup}>Batal</button>
                <button
                  type="button"
                  className={`btn btn-primary ${btnSaveDisabled ? "disabled" : ""} sidepopup-container__modal-footer-btn`}
                  onClick={() => this.onSubmitHandler()}
                  disabled={btnSaveDisabled}>{this.buttonSaveText()}</button>
              </div>
            </div>
          </InnerBox>
        </Box>
      </div>
    );
  }
}

export default SidePopup;
