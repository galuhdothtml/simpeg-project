/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";

class EditDeleteComponent extends React.Component {
  static propTypes = {
    id: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]).isRequired,
    editDeleteEvent: PropTypes.func.isRequired,
    extendButtons: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string,
      action: PropTypes.func,
      icon: PropTypes.string,
      label: PropTypes.string,
    })).isRequired,
  }

  editHandler = () => {
    const { editDeleteEvent, id } = this.props;

    editDeleteEvent(id, "edit");
  }

  deleteHandler = () => {
    const { editDeleteEvent, id } = this.props;

    editDeleteEvent(id, "delete");
  }

  extendButtonsComponent = () => {
    const { extendButtons, id } = this.props;

    return extendButtons.map((x) => {
      const actionClick = x.action;

      return (
        <button
          key={x.id}
          onClick={() => { actionClick(id); }}
          type="button"
          className="btn btn-default btn-sm text-center table-option"><i className={x.icon} />{" "}{x.label}</button>
      );
    });
  }

  render() {
    return (
      <div className="text-center table-option">
        <div className="btn-group">
          {this.extendButtonsComponent()}
          <button
            onClick={this.editHandler}
            type="button"
            className="btn btn-default btn-sm text-center table-option"><i className="fa fa-edit" /> Edit</button>
          <button
            onClick={this.deleteHandler}
            type="button"
            className="btn btn-default btn-sm text-center table-option"><i className="fa fa-trash-o" /> Hapus</button>
        </div>
      </div>
    );
  }
}

export default EditDeleteComponent;
