/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import IconPdf from "../../../images/icon-pdf.png";
import ProgressBar from "./ProgressBar";
import {
  uploadFile,
} from "../../../data";
import Util from "../../../utils";

class FileUpload extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.shape({
      fileValue: PropTypes.tring,
      fileType: PropTypes.string,
      fileName: PropTypes.string,
    }),
    changeEvent: PropTypes.func,
    groupDir: PropTypes.shape({
      dirName: PropTypes.string,
      required: PropTypes.bool,
      requiredMsg: PropTypes.string,
    }),
  }

  static defaultProps = {
    label: "",
    value: null,
    changeEvent: () => {},
    groupDir: null,
  }

  constructor(props) {
    super(props);

    let fileValue = "";
    let fileType = "";
    let fileName = "";

    if (props.value) {
      ({ value: { fileValue, fileType, fileName } } = props);
    }

    this.state = {
      fileValue,
      fileType,
      fileName,
      isLoading: false,
    };
  }

  componentWillReceiveProps = (nextProps) => {
    const { value } = this.props;

    if (value !== nextProps.value) {
      let fileValue = "";
      let fileType = "";
      let fileName = "";

      if (nextProps.value) {
        ({ value: { fileValue, fileType, fileName } } = nextProps);
      }

      this.setState({
        fileValue,
        fileType,
        fileName,
      });
    }
  }

  clearState = () => {
    const { changeEvent } = this.props;

    const newState = {
      fileValue: "",
      fileType: "",
      fileName: "",
    };
    this.setState(newState);
    changeEvent(newState);
  }

  openFile = () => {
    const { fileValue } = this.state;

    window.open(Util.createPathPreview(fileValue));
  }

  chooseFile = () => {
    const { groupDir } = this.props;

    if (groupDir && groupDir.required && String(groupDir.dirName).length === 0) {
      toast.error(groupDir.requiredMsg);
    } else {
      this.fileUpload.click();
    }
  }

  changeFileHandler = () => {
    const file = this.fileUpload.files[0];

    this.doUploadingFile(file);
  }

  doUploadingFile = async (file) => {
    const { changeEvent, groupDir } = this.props;

    this.setState({
      isLoading: true,
    }, () => {
      this.progressBar.startLoading();
    });

    let res = null;

    if (groupDir) {
      res = await uploadFile({ photo: file }, groupDir.dirName);
    } else {
      res = await uploadFile({ photo: file });
    }

    if (res.status) {
      const { data } = res;
      const newState = {
        fileValue: data.path,
        fileType: data.mimetype,
        fileName: data.originalname,
        isLoading: false,
      };
      this.progressBar.finishLoading(() => {
        this.setState(newState);
      });
      changeEvent(newState);
    }
  }

  doReadingAsDataUrl = (file) => {
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      this.setState({ fileType: file.type, fileValue: reader.result, fileName: file.name });
    }, false);

    if (file) {
      reader.readAsDataURL(file);
    }
  }

  setupFilePreview = () => {
    const { fileType, fileValue } = this.state;
    const isPdf = (fileType === "application/pdf");

    if (isPdf) {
      return <img className="file-upload__preview-pdf" src={IconPdf} />;
    }

    return <div className="file-upload__preview-img" style={{ backgroundImage: `url(${Util.createPathPreview(fileValue)})` }} />;
  }

  setupComponent = () => {
    const { label } = this.props;
    const { fileValue, fileName, isLoading } = this.state;

    if (isLoading) {
      return (
        <ProgressBar
          ref={(input) => {
            this.progressBar = input;
          }}
        />
      );
    }

    if (fileValue.length > 0) {
      return (
        <div className="file-upload">
          {this.setupFilePreview()}
          <div className="file-upload__information">
            <label>{label}</label>
            <div className="file-upload__filename">{fileName}</div>
          </div>
          <div className="file-upload__upload-button">
            <div onClick={this.openFile} className="file-upload__button-inline">
              <i className="fa fa-eye" />
            </div>
            <div onClick={this.chooseFile} className="file-upload__button-inline">
              <i className="fa fa-edit" />
            </div>
            <div onClick={this.clearState} className="file-upload__button-inline">
              <i className="fa fa-trash-o" />
            </div>
          </div>
        </div>
      );
    }

    return (
      <div className="file-upload">
        <label>{label}</label>
        <div className="file-upload__upload-button" onClick={this.chooseFile}><i className="fa fa-upload" />Upload</div>
      </div>
    );
  }

  render() {
    return (
      <div className={"form-group mb-0"}>
        {this.setupComponent()}
        <input ref={(c) => { this.fileUpload = c; }} onChange={this.changeFileHandler} className="hidden" type="file" />
        <ToastContainer autoClose={3000} />
      </div>
    );
  }
}

export default FileUpload;
