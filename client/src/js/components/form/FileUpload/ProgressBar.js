/* eslint prop-types: 0 */
import React from "react";

class ProgressBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      progressBarWidth: 1,
    };
  }

  startLoading = () => {
    this.loadingInterval = setInterval(this.updateProgressBar, 500);
  }

  cancelLoading = (callback) => {
    clearInterval(this.loadingInterval);
    if (callback) {
      callback();
    }
    this.setState({ progressBarWidth: 1 });
  }

  finishLoading = (callback) => {
    clearInterval(this.loadingInterval);
    this.loadingInterval = setInterval(() => {
      this.updateProgressBar(callback);
    }, 20);
  }

  updateProgressBar = (callback) => {
    const { progressBarWidth } = this.state;

    if (progressBarWidth >= 100) {
      clearInterval(this.loadingInterval);
      if (callback) {
        callback();
      }
    } else {
      this.setState({ progressBarWidth: (progressBarWidth + 1) });
    }
  }

  render() {
    const {
      progressBarWidth,
    } = this.state;

    return (
      <div className="file-upload" style={{ paddingTop: "15px" }}>
        <div className="row">
          <div className="col-sm-6">
            Mohon tunggu sebentar ...
          </div>
          <div className="col-sm-6 text-right">
            {`${progressBarWidth}%`}
          </div>
        </div>
        <div className="file-upload__loading">
          <div className="file-upload__loading-content" style={{ width: `${progressBarWidth}%` }} />
        </div>
      </div>
    );
  }
}

export default ProgressBar;
