/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";

class ModalPopup extends React.Component {
  static propTypes = {
    width: PropTypes.number,
    onHide: PropTypes.func,
    title: PropTypes.string,
    value: PropTypes.string,
  }

  static defaultProps = {
    onHide: () => {},
    width: 1000,
    title: "",
    value: "",
  }

  hideModalHandler = () => {
    const { onHide } = this.props;

    onHide();
  }

  render() {
    const {
      width,
      title,
      value,
    } = this.props;

    return (
      <div className="modal" style={{ display: "block" }}>
        <div className="modal-dialog" style={{ minWidth: `${width}px` }}>
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close disabled" onClick={this.hideModalHandler} aria-label="Close">
                <span aria-hidden="true">×</span></button>
              <h4 className="modal-title text-left">{title}</h4>
            </div>
            <div className="modal-body text-center">
              <div style={{ padding: "10px" }}>
                  <img src={value} style={{ width: "100%" }} />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ModalPopup;
