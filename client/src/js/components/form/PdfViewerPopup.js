/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import Viewer, { Worker } from "@phuocng/react-pdf-viewer";

import "@phuocng/react-pdf-viewer/cjs/react-pdf-viewer.css";

import Util from "../../utils";

class ModalPopup extends React.Component {
  static propTypes = {
    width: PropTypes.number,
    onHide: PropTypes.func,
    title: PropTypes.string,
    value: PropTypes.string,
  }

  static defaultProps = {
    onHide: () => {},
    width: 1000,
    title: "",
    value: "",
  }

  hideModalHandler = () => {
    const { onHide } = this.props;

    onHide();
  }

  render() {
    const {
      width,
      title,
      value,
    } = this.props;

    return (
      <div className="modal" style={{ display: "block" }}>
        <div className="modal-dialog" style={{ width: `${width}px` }}>
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close disabled" onClick={this.hideModalHandler} aria-label="Close">
                <span aria-hidden="true">×</span></button>
              <h4 className="modal-title text-left">{title}</h4>
            </div>
            <div className="modal-body text-center">
              <Worker workerUrl="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.3.200/pdf.worker.min.js">
                  <div style={{ height: "500px" }}>
                      <Viewer fileUrl={Util.createPathPreview(value)} />
                  </div>
              </Worker>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ModalPopup;
