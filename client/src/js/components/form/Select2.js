/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import Util from "../../utils";

const createId = () => (Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15));
class Select2 extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
    url: PropTypes.string.isRequired,
    additionalParam: PropTypes.shape({}),
    changeEvent: PropTypes.func,
    required: PropTypes.bool,
    placeholder: PropTypes.string,
    name: PropTypes.string,
  }

  static defaultProps = {
    label: undefined,
    value: "",
    changeEvent: () => { },
    required: false,
    placeholder: "",
    additionalParam: null,
    name: undefined,
  }

  constructor(props) {
    super(props);

    this.state = {
      id: createId(),
      $eventSelect: null,
      ajaxOptions: null,
      stateValue: "",
    };
  }

  componentDidMount = () => {
    this.createSelect(this.props, () => {
      this.setupComponent(this.props);
    });
  }

  componentWillReceiveProps = (nextProps) => {
    const { $eventSelect } = this.state;
    const { value, additionalParam } = this.props;

    // if (nextProps.additionalParam && JSON.stringify(additionalParam) !== JSON.stringify(nextProps.additionalParam)) {
    //   this.createSelect(nextProps);
    // }

    if (String(value) !== String(nextProps.value)) {
      this.createSelect(nextProps, () => {
        this.setupComponent(nextProps);
        $eventSelect.val(String(nextProps.value));
        $eventSelect.trigger("change");
      });
    }
  }

  createSelect = ({ placeholder, url, additionalParam }, callback) => {
    const {
      id,
    } = this.state;
    const ajaxOptions = Util.createAjax(url, additionalParam);
    const $eventSelect = $(`#${id}.select2`);
    $eventSelect.select2({
      placeholder,
      allowClear: true,
      ajax: ajaxOptions,
      minimumInputLength: 1,
    });

    this.setState({ $eventSelect, ajaxOptions }, () => {
      if (callback) {
        callback();
      }
    });
  }

  setupComponent = (props) => {
    const {
      value, changeEvent,
    } = props;
    const { $eventSelect, ajaxOptions } = this.state;
    $eventSelect.on("change", (e) => {
      setTimeout(() => {
        let newValue = "";

        if (e.target.value) {
          newValue = e.target.value;
        }

        if (newValue) {
          this.setState({ stateValue: newValue }, () => {
            changeEvent(newValue, { target: this.inputHidden });
          });
        }
      });
    });
    $eventSelect.on("select2:unselect", () => {
      setTimeout(() => {
        this.setState({ stateValue: "" }, () => {
          changeEvent("", { target: this.inputHidden });
        });
      });
    });

    if (value && String(value).length > 0) {
      $.ajax({ // make the request for the selected data object
        headers: ajaxOptions.headers,
        type: "GET",
        url: ajaxOptions.url,
        dataType: "json",
        data: {
          page: 1,
          limit: 5,
          orId: value,
        },
      }).then((res) => {
        if (res.status) {
          const { data } = res;
          if (data.length > 0) {
            const newOption = new Option(data[0].name, data[0].id, true, true);
            $eventSelect.append(newOption);
          }
        }
      });
    }
  }

  render() {
    const {
      label, value, required, name,
    } = this.props;
    const { id, stateValue } = this.state;

    return (
      <div className="form-group mb-0">
        {label && <label>{label}</label>}
        <select id={id} value={value} onChange={() => {}} className="form-control select2" />
        <input name={name} ref={(c) => { this.inputHidden = c; }} type="hidden" value={stateValue} onChange={() => {}} required={required} />
      </div>
    );
  }
}

export default Select2;
