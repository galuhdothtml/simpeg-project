/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import update from "immutability-helper";
import InputText from "~/components/InputText";
import Select2 from "~/components/Select2";

class AddressTab extends React.Component {
  static propTypes = {
    value: PropTypes.shape({}).isRequired,
    changeEvent: PropTypes.func,
  }

  static defaultProps = {
    changeEvent: () => {},
  }

  constructor(props) {
    super(props);

    this.state = {
      isValidated: false,
    };
  }

  changeValueHandler = (type, val) => {
    const { value: form } = this.props;
    const newValue = update(form, {
      [type]: { $set: val },
    });

    const { changeEvent } = this.props;

    changeEvent(newValue);
  }

  changeIdProvinceHandler = (val) => {
    const { value: form } = this.props;
    const newValue = update(form, {
      id_province: { $set: val },
      id_city: { $set: "" },
    });

    const { changeEvent } = this.props;

    changeEvent(newValue);
  }

  changeIdCityHandler = (val) => {
    const { value: form } = this.props;
    const newValue = update(form, {
      id_city: { $set: val },
    });
    const { changeEvent } = this.props;

    changeEvent(newValue);
  }

  render() {
    const { value: form } = this.props;

    return (
      <div>
        <div className="row mb-sm">
          <div className="col-sm-4">
            <InputText
              label="Alamat"
              changeEvent={val => this.changeValueHandler("address", val)}
              value={form.address}
            />
          </div>
          <div className="col-sm-4">
            <InputText
              label="RT"
              changeEvent={val => this.changeValueHandler("rt", val)}
              value={form.rt}
            />
          </div>
          <div className="col-sm-4">
            <InputText
              label="RW"
              changeEvent={val => this.changeValueHandler("rw", val)}
              value={form.rw}
            />
          </div>
        </div>
        <div className="row mb-md">
          <div className="col-sm-6">
            <Select2
              label="Provinsi"
              url="/api/provinces"
              placeholder="- Pilih Provinsi -"
              value={form.id_province}
              changeEvent={this.changeIdProvinceHandler}
            />
          </div>
          <div className="col-sm-6">
            <Select2
              label="Kab./Kota"
              url="/api/cities"
              additionalParam={{ idProvince: form.id_province }}
              placeholder="- Pilih Kab./Kota -"
              value={form.id_city}
              changeEvent={this.changeIdCityHandler}
            />
          </div>
        </div>
        <div className="row mb-md">
          <div className="col-sm-4">
            <InputText
              label="Kecamatan"
              changeEvent={val => this.changeValueHandler("kecamatan", val)}
              value={form.kecamatan}
            />
          </div>
          <div className="col-sm-4">
            <InputText
              label="Kelurahan"
              changeEvent={val => this.changeValueHandler("kelurahan", val)}
              value={form.kelurahan}
            />
          </div>
          <div className="col-sm-4">
            <InputText
              label="Kode Pos"
              changeEvent={val => this.changeValueHandler("postal_code", val)}
              value={form.postal_code}
            />
          </div>
        </div>
        <div className="row mb-md">
          <div className="col-sm-6">
            <InputText
              label="Telepon 1"
              changeEvent={val => this.changeValueHandler("phone_1", val)}
              value={form.phone_1}
            />
          </div>
          <div className="col-sm-6">
            <InputText
              label="Telepon 2"
              changeEvent={val => this.changeValueHandler("phone_2", val)}
              value={form.phone_2}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default AddressTab;
