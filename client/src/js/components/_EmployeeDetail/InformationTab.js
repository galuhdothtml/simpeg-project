/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import InputText from "~/components/InputText";
import DatePicker from "~/components/DatePicker";
import Select from "~/components/Select";
import {
  getReligions, getMaritalStatuses,
} from "../../data";

class InformationTab extends React.Component {
  static propTypes = {
    value: PropTypes.shape({}).isRequired,
    changeEvent: PropTypes.func,
  }

  static defaultProps = {
    changeEvent: () => {},
  }

  constructor(props) {
    super(props);

    this.state = {
      genderList: [
        {
          id: "", name: "- Pilih Jenis Kelamin -",
        },
        {
          id: "L", name: "Laki - Laki",
        },
        {
          id: "P", name: "Perempuan",
        },
      ],
      religionList: [],
      maritalStatusList: [],
      isValidated: false,
    };
  }

  componentWillMount = () => {
    this.fetchDropDownData();
  }

  fetchDropDownData = async () => {
    const fetchReligions = getReligions();
    const fetchMaritalStatuses = getMaritalStatuses();

    const resReligions = await fetchReligions;
    const resMaritalStatuses = await fetchMaritalStatuses;

    if (resReligions.status) {
      const { data } = resReligions;
      const newData = [{ id: "", name: "- Pilih Agama -" }, ...data.map(x => ({ id: x.id, name: x.name }))];

      this.setState({ religionList: newData });
    }

    if (resMaritalStatuses.status) {
      const { data } = resMaritalStatuses;
      const newData = [{ id: "", name: "- Pilih Status Pernikahan -" }, ...data.map(x => ({ id: x.id, name: x.name }))];

      this.setState({ maritalStatusList: newData });
    }
  }

  changeValueHandler = (type, val) => {
    const { changeEvent } = this.props;

    changeEvent(type, val);
  }

  render() {
    const {
      genderList, religionList, maritalStatusList,
    } = this.state;
    const {
      value: form, errorMsg,
    } = this.props;

    return (
      <div>
        <div className="row mb-sm">
          <div className="col-sm-6">
            <InputText
              label="NIP *"
              changeEvent={val => this.changeValueHandler("nip", val)}
              value={String(form.nip)}
              errorText={errorMsg.nip}
            />
          </div>
          <div className="col-sm-6">
            <InputText
              label="NIP Lama"
              changeEvent={val => this.changeValueHandler("old_nip", val)}
              value={String(form.old_nip)}
            />
          </div>
        </div>
        <div className="row mb-md">
          <div className="col-sm-12">
            <InputText
              label="Nama Lengkap *"
              changeEvent={val => this.changeValueHandler("fullname", val)}
              value={String(form.fullname)}
              errorText={errorMsg.fullname}
            />
          </div>
        </div>
        <div className="row mb-md">
          <div className="col-sm-6">
            <InputText
              label="Gelar Depan"
              changeEvent={val => this.changeValueHandler("first_degree", val)}
              value={String(form.first_degree)}
            />
          </div>
          <div className="col-sm-6">
            <InputText
              label="Gelar Belakang"
              changeEvent={val => this.changeValueHandler("last_degree", val)}
              value={String(form.last_degree)}
            />
          </div>
        </div>
        <div className="row mb-md">
          <div className="col-sm-4">
            <InputText
              label="Tempat Lahir"
              changeEvent={val => this.changeValueHandler("birth_place", val)}
              value={String(form.birth_place)}
              errorText={errorMsg.birth_place}
            />
          </div>
          <div className="col-sm-4">
            <DatePicker
              label="Tanggal Lahir"
              id="birth-date"
              changeEvent={val => this.changeValueHandler("birth_date", val)}
              value={form.birth_date}
            />
          </div>
          <div className="col-sm-4">
            <Select
              label="Jenis Kelamin"
              data={genderList}
              value={form.gender}
              changeEvent={val => this.changeValueHandler("gender", val)}
              errorText={errorMsg.gender}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-sm-4">
            <InputText
              label="Golongan Darah"
              changeEvent={val => this.changeValueHandler("blood_type", val)}
              value={String(form.blood_type)}
            />
          </div>
          <div className="col-sm-4">
            <Select
              label="Agama"
              data={religionList}
              value={form.id_religion}
              changeEvent={val => this.changeValueHandler("id_religion", val)}
              errorText={errorMsg.id_religion}
            />
          </div>
          <div className="col-sm-4">
            <Select
              label="Status Pernikahan"
              data={maritalStatusList}
              value={form.id_marital_status}
              changeEvent={val => this.changeValueHandler("id_marital_status", val)}
              errorText={errorMsg.id_marital_status}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default InformationTab;
