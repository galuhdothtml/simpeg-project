/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import update from "immutability-helper";
import InputText from "~/components/InputText";
import Select from "~/components/Select";
import {
  getFaceShapes, getHairTypes,
} from "../../data";

class PhysicalTab extends React.Component {
  static propTypes = {
    value: PropTypes.shape({}).isRequired,
    changeEvent: PropTypes.func,
  }

  static defaultProps = {
    changeEvent: () => {},
  }

  constructor(props) {
    super(props);

    this.state = {
      isValidated: false,
      faceShapeList: [],
      hairTypeList: [],
    };
  }

  componentWillMount = () => {
    this.fetchDropDownData();
  }

  fetchDropDownData = async () => {
    const fetchFaceShapes = getFaceShapes();
    const fetchHairTypes = getHairTypes();

    const resFaceShapes = await fetchFaceShapes;
    const resHairTypes = await fetchHairTypes;

    if (resFaceShapes.status) {
      const { data } = resFaceShapes;
      const newData = [{ id: "", name: "- Pilih Bentuk Wajah -" }, ...data.map(x => ({ id: x.id, name: x.name }))];

      this.setState({ faceShapeList: newData });
    }

    if (resHairTypes.status) {
      const { data } = resHairTypes;
      const newData = [{ id: "", name: "- Pilih Jenis Rambut -" }, ...data.map(x => ({ id: x.id, name: x.name }))];

      this.setState({ hairTypeList: newData });
    }
  }

  changeValueHandler = (type, val) => {
    const { value: form } = this.props;
    const newValue = update(form, {
      [type]: { $set: val },
    });

    const { changeEvent } = this.props;

    changeEvent(newValue);
  }

  render() {
    const { hairTypeList, faceShapeList } = this.state;
    const { value: form } = this.props;

    return (
      <div>
        <div className="row mb-sm">
          <div className="col-sm-6">
            <InputText
              label="Tinggi Badan (cm)"
              changeEvent={val => this.changeValueHandler("height", val)}
              value={form.height}
            />
          </div>
          <div className="col-sm-6">
            <InputText
              label="Berat Badan (kg)"
              changeEvent={val => this.changeValueHandler("weight", val)}
              value={form.weight}
            />
          </div>
        </div>
        <div className="row mb-sm">
          <div className="col-sm-4">
            <Select
              label="Jenis Rambut"
              data={hairTypeList}
              value={form.id_hair_type}
              changeEvent={val => this.changeValueHandler("id_hair_type", val)}
            />
          </div>
          <div className="col-sm-4">
            <Select
              label="Bentuk Wajah"
              data={faceShapeList}
              value={form.id_face_shape}
              changeEvent={val => this.changeValueHandler("id_face_shape", val)}
            />
          </div>
          <div className="col-sm-4">
            <InputText
              label="Warna Kulit"
              changeEvent={val => this.changeValueHandler("skin_color", val)}
              value={form.skin_color}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default PhysicalTab;
