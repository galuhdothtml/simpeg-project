/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import update from "immutability-helper";
import InputText from "~/components/InputText";

class PhysicalTab extends React.Component {
  static propTypes = {
    value: PropTypes.shape({}).isRequired,
    changeEvent: PropTypes.func,
  }

  static defaultProps = {
    changeEvent: () => {},
  }

  constructor(props) {
    super(props);

    this.state = {
      isValidated: false,
    };
  }

  changeValueHandler = (type, val) => {
    const { value: form } = this.props;
    const newValue = update(form, {
      [type]: { $set: val },
    });

    const { changeEvent } = this.props;

    changeEvent(newValue);
  }

  render() {
    const { value: form } = this.props;

    return (
      <div>
        <div className="row mb-sm">
          <div className="col-sm-4">
            <InputText
              label="No. KTP"
              changeEvent={val => this.changeValueHandler("ktp", val)}
              value={form.ktp}
            />
          </div>
          <div className="col-sm-4">
            <InputText
              label="No. KARPEG"
              changeEvent={val => this.changeValueHandler("karpeg", val)}
              value={form.karpeg}
            />
          </div>
          <div className="col-sm-4">
            <InputText
              label="No. ASKES"
              changeEvent={val => this.changeValueHandler("askes", val)}
              value={form.askes}
            />
          </div>
        </div>
        <div className="row mb-sm">
          <div className="col-sm-3">
            <InputText
              label="No. TASPEN"
              changeEvent={val => this.changeValueHandler("taspen", val)}
              value={form.taspen}
            />
          </div>
          <div className="col-sm-3">
            <InputText
              label="No. KARIS / KARSU"
              changeEvent={val => this.changeValueHandler("karis_karsu", val)}
              value={form.karis_karsu}
            />
          </div>
          <div className="col-sm-3">
            <InputText
              label="No. NPWP"
              changeEvent={val => this.changeValueHandler("npwp", val)}
              value={form.npwp}
            />
          </div>
          <div className="col-sm-3">
            <InputText
              label="No. KORPRI"
              changeEvent={val => this.changeValueHandler("korpri", val)}
              value={form.korpri}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default PhysicalTab;
