/* eslint prop-types: 0 */
import React from "react";
import update from "immutability-helper";
import {
  createEmployee, updateEmployee, getEmployee,
} from "../../data";
import CoreHoC from "../CoreHoC";
import InformationTab from "./InformationTab";
import AddressTab from "./AddressTab";
import PhysicalTab from "./PhysicalTab";
import LainnyaTab from "./LainnyaTab";
import ImageUpload from "~/components/ImageUpload";

const errorContent = (type, val) => {
  let retval = null;

  if (type === "nip") {
    if (String(val).trim().length === 0) {
      retval = "* NIP wajib diisi";
    }
  } else if (type === "fullname") {
    if (String(val).trim().length === 0) {
      retval = "* Nama lengkap wajib diisi";
    }
  } else if (type === "birth_place") {
    if (String(val).trim().length === 0) {
      retval = "* Tempat lahir wajib diisi";
    }
  } else if (type === "birth_date") {
    if (String(val).trim().length === 0) {
      retval = "* Tanggal lahir wajib diisi";
    }
  } else if (type === "gender") {
    if (String(val).trim().length === 0) {
      retval = "* Jenis kelamin wajib diisi";
    }
  } else if (type === "id_religion") {
    if (String(val).trim().length === 0) {
      retval = "* Agama wajib diisi";
    }
  } else if (type === "id_marital_status") {
    if (String(val).trim().length === 0) {
      retval = "* Status pernikahan wajib diisi";
    }
  }

  return retval;
};
class EmployeeDetail extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentTab: "1",
      fileImage: "",
      id: "",
      isValidated: false,
      type: "create",
      errorMsg: {
        nip: "",
        fullname: "",
        birth_place: "",
        birth_date: "",
        gender: "",
        id_religion: "",
        id_marital_status: "",
      },
      information: {
        nip: "",
        old_nip: "",
        fullname: "",
        first_degree: "",
        last_degree: "",
        birth_place: "",
        birth_date: moment().subtract(18, "years").format("YYYY-MM-DD"),
        gender: "",
        id_religion: "",
        blood_type: "",
        id_marital_status: "",
      },
      address: {
        address: "",
        rt: "",
        rw: "",
        id_province: "",
        id_city: "",
        kecamatan: "",
        kelurahan: "",
        postal_code: "",
        phone_1: "",
        phone_2: "",
      },
      physical: {
        height: "",
        weight: "",
        id_hair_type: "",
        id_face_shape: "",
        skin_color: "",
      },
      lainnya: {
        ktp: "",
        karpeg: "",
        askes: "",
        taspen: "",
        karis_karsu: "",
        npwp: "",
        korpri: "",
      },
    };
  }

  componentWillMount = () => {
    const { match: { params } } = this.props;

    if (params.type === "edit") {
      this.setupEmployeeData(params.id);
    } else {
      this.setState({ type: "create" });
    }
  }

  setupEmployeeData = async (id) => {
    const res = await getEmployee(id);

    if (res.status) {
      const { data } = res;
      const addressData = JSON.parse(data.address_content);
      const physicalData = JSON.parse(data.body_content);
      const lainnyaData = JSON.parse(data.note_content);
      const newState = {
        id: data.id,
        type: "edit",
        information: {
          nip: data.nip,
          old_nip: data.old_nip,
          fullname: data.fullname,
          first_degree: data.first_degree,
          last_degree: data.last_degree,
          birth_place: data.birth_place,
          birth_date: moment(data.birth_date, "YYYY-MM-DD").format("YYYY-MM-DD"),
          gender: data.gender,
          id_religion: String(data.id_religion),
          blood_type: data.blood_type,
          id_marital_status: String(data.id_marital_status),
        },
        address: addressData,
        physical: physicalData,
        lainnya: lainnyaData,
        fileImage: data.imageFile,
      };
      this.setState(newState);
    }
  }

  bulkCreateErrorMessage = () => {
    const { information: form, errorMsg } = this.state;
    const keys = Object.keys(errorMsg);
    let newErrorMsg = errorMsg;
    let isError = false;

    keys.forEach((x) => {
      let error = "";

      const ERROR_TEXT = errorContent(x, form[x]);

      if (ERROR_TEXT) {
        error = ERROR_TEXT;
        isError = true;
      }

      newErrorMsg = update(newErrorMsg, {
        [x]: { $set: error },
      });
    });

    return { msg: newErrorMsg, isError };
  }

  createTabPane = () => {
    const {
      currentTab, information, address, physical, lainnya, errorMsg,
    } = this.state;
    let retval = (<div></div>);

    if (currentTab === "1") {
      retval = (<InformationTab changeEvent={this.changeInformationHandler} value={information} errorMsg={errorMsg} ref={(c) => { this.informationTab = c; }} />);
    } else if (currentTab === "2") {
      retval = (<AddressTab changeEvent={this.changeAddressHandler} value={address} ref={(c) => { this.addressTab = c; }} />);
    } else if (currentTab === "3") {
      retval = (<PhysicalTab changeEvent={this.changePhysicalHandler} value={physical} />);
    } else if (currentTab === "4") {
      retval = (<LainnyaTab changeEvent={this.changeLainnyaHandler} value={lainnya} />);
    }

    return retval;
  }

  changeTab = (e, val) => {
    e.preventDefault();

    this.setState({ currentTab: String(val) });
  }

  changeImageHandler = (val) => {
    this.setState({ fileImage: val });
  }

  changeInformationHandler = (type, val) => {
    const { information: form } = this.state;
    const newValue = update(form, {
      [type]: { $set: val },
    });

    const errorMsg = this.createErrorMessage(type, val);
    this.setState({ information: newValue, errorMsg });
  }

  createErrorMessage = (type, val) => {
    const { errorMsg, isValidated } = this.state;
    let error = "";

    if (!isValidated) {
      return errorMsg;
    }

    const ERROR_TEXT = errorContent(type, val);

    if (ERROR_TEXT) {
      error = ERROR_TEXT;
    }

    const newValue = update(errorMsg, {
      [type]: { $set: error },
    });

    return newValue;
  }

  changeAddressHandler = (val) => {
    this.setState({ address: val });
  }

  changePhysicalHandler = (val) => {
    this.setState({ physical: val });
  }

  changeLainnyaHandler = (val) => {
    this.setState({ lainnya: val });
  }

  dataURLtoFile = (dataurl, filename) => {
    const arr = dataurl.split(",");
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);
    while (n > -1) {
      u8arr[n] = bstr.charCodeAt(n);
      n -= 1;
    }
    return new File([u8arr], filename, { type: mime });
  }

  saveHandler = async () => {
    const { history } = this.props;
    const error = this.bulkCreateErrorMessage();
    const isValid = !error.isError;
    const {
      fileImage,
      information,
      address,
      physical,
      lainnya,
      type,
      id,
    } = this.state;
    let file = null;

    if (String(fileImage).trim().length > 0) {
      file = this.dataURLtoFile(fileImage, "image.png");
    }

    this.setState({ isValidated: true, errorMsg: error.msg });

    if (!isValid) {
      this.setState({ currentTab: "1" });

      return;
    }

    const payload = {
      id,
      information,
      address,
      physical,
      lainnya,
    };

    if (type === "create") {
      const res = await createEmployee(payload, { photo: file });

      if (res.status) {
        history.push("/pegawai");
      }
    } else {
      const res = await updateEmployee(payload, { photo: file });

      if (res.status) {
        history.push("/pegawai");
      }
    }
  }

  render() {
    // const { match: { params } } = this.props;
    const { currentTab, fileImage, type } = this.state;

    return (
      <div>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-12">
                <h3 className="box-title">{type === "create" ? "Tambah" : "Edit"} Pegawai</h3>
              </div>
            </div>
          </div>
          <div className="box-body">
            <div className="row mb-0">
              <div className="col-sm-9">
                <div className="nav-tabs-custom">
                  <ul className="nav nav-tabs">
                    <li className={`${currentTab === "1" && "active"}`}><a href="#" onClick={e => this.changeTab(e, "1")}>Informasi Pegawai</a></li>
                    <li className={`${currentTab === "2" && "active"}`}><a href="#" onClick={e => this.changeTab(e, "2")}>Data Alamat</a></li>
                    <li className={`${currentTab === "3" && "active"}`}><a href="#" onClick={e => this.changeTab(e, "3")}>Keterangan Badan</a></li>
                    <li className={`${currentTab === "4" && "active"}`}><a href="#" onClick={e => this.changeTab(e, "4")}>Keterangan Lainnya</a></li>
                  </ul>
                  <div className="tab-content">
                    <div className="tab-pane active">
                      {this.createTabPane()}
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-3 text-center">
                <ImageUpload value={fileImage} changeEvent={this.changeImageHandler} />
              </div>
            </div>
          </div>
          <div className="box-footer text-right">
            <div>
              <button type="button" className="btn btn-primary" onClick={this.saveHandler}>Simpan</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CoreHoC(EmployeeDetail);
