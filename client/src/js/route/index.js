/* eslint prop-types: 0 */
import React from "react"; // eslint-disable-line no-unused-vars
import {
  BrowserRouter as Router, Route, Switch, Redirect,
} from "react-router-dom";
import Util from "../utils";
import AppNavigation from "./components/AppNavigation";
import asyncComponent from "./components/AsyncComponent";

require("../sass/styles.scss");

const style = {
  container: {
    height: "auto",
    minHeight: "100%",
  },
  wrapper: {
    height: "auto",
    minHeight: "100%",
  },
};
const RedirectToDashboard = () => (
  <Redirect
    to={{
      pathname: "/dashboard",
    }}
  />
);
const isAuthenticated = (Util.getToken() && Util.getToken().length > 0);
// eslint-disable-next-line react/prop-types
const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (isAuthenticated ? (
      <Component {...props} />
    ) : (
        <Redirect
          to={{
            pathname: "/auth/login",
            state: { from: props.location }, // eslint-disable-line react/prop-types
          }}
        />
    ))
    }
  />
);
// eslint-disable-next-line react/prop-types
const AuthRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (!isAuthenticated ? (
      <Component {...props} />
    ) : (
        <Redirect
          to={{
            pathname: "/dashboard",
          }}
        />
    ))
    }
  />
);
class App extends React.Component {
  render = () => (
    <Router>
      <Switch>
        <AuthRoute exact path="/auth/login" component={asyncComponent(() => import("../pages/Login"))} />
        <Route path="/">
          <div className="sidebar-mini fixed skin-purple" style={style.container}>
            <div className="wrapper" style={style.wrapper}>
              <AppNavigation />
              <div>
                <Route exact path="/" component={RedirectToDashboard} />
                <PrivateRoute path="/dashboard" component={asyncComponent(() => import("../pages/Dashboard"))} />
                <PrivateRoute exact path="/pegawai" component={asyncComponent(() => import("../pages/Employee"))} />
                <PrivateRoute exact path="/pegawai/pegawai-detail/:type/:id" component={asyncComponent(() => import("../pages/EmployeeDetail"))} />
                <PrivateRoute exact path="/pegawai/pegawai-detail/:type" component={asyncComponent(() => import("../pages/EmployeeDetail"))} />
                <PrivateRoute path="/umum-agama" component={asyncComponent(() => import("../pages/Religion"))} />
                <PrivateRoute path="/umum-bentuk-wajah" component={asyncComponent(() => import("../pages/FaceShape"))} />
                <PrivateRoute path="/umum-jenis-diklat" component={asyncComponent(() => import("../pages/TrainingType"))} />
                <PrivateRoute path="/umum-jenjang-pendidikan" component={asyncComponent(() => import("../pages/EducationLevel"))} />
                <PrivateRoute path="/umum-jenis-rambut" component={asyncComponent(() => import("../pages/HairType"))} />
                <PrivateRoute path="/umum-status-pernikahan" component={asyncComponent(() => import("../pages/MaritalStatus"))} />
                <PrivateRoute path="/instansi" component={asyncComponent(() => import("../pages/Agency"))} />
                <PrivateRoute path="/unit-kerja" component={asyncComponent(() => import("../pages/WorkUnit"))} />
                <PrivateRoute path="/user" component={asyncComponent(() => import("../pages/User"))} />
                <PrivateRoute path="/bkd" component={asyncComponent(() => import("../pages/BKD"))} />
                <PrivateRoute path="/sekretariat-daerah" component={asyncComponent(() => import("../pages/RegionalSecretariat"))} />
                <PrivateRoute path="/jabatan-jenis-jabatan" component={asyncComponent(() => import("../pages/PositionType"))} />
                <PrivateRoute path="/jabatan" component={asyncComponent(() => import("../pages/Position"))} />
                <PrivateRoute path="/jabatan-jenis-kenaikan-pangkat" component={asyncComponent(() => import("../pages/PromotionType"))} />
                <PrivateRoute path="/jabatan-golongan-ruang" component={asyncComponent(() => import("../pages/Group"))} />
                <PrivateRoute exact path="/keluarga-suami-istri" component={asyncComponent(() => import("../pages/EmployeeSpouse"))} />
                <PrivateRoute exact path="/keluarga-suami-istri/detail/:type/:id" component={asyncComponent(() => import("../pages/EmployeeSpouse/Detail"))} />
                <PrivateRoute exact path="/keluarga-suami-istri/detail/:type" component={asyncComponent(() => import("../pages/EmployeeSpouse/Detail"))} />
                <PrivateRoute exact path="/keluarga-anak" component={asyncComponent(() => import("../pages/EmployeeKid"))} />
                <PrivateRoute exact path="/keluarga-anak/detail/:type/:id" component={asyncComponent(() => import("../pages/EmployeeKid/Detail"))} />
                <PrivateRoute exact path="/keluarga-anak/detail/:type" component={asyncComponent(() => import("../pages/EmployeeKid/Detail"))} />
                <PrivateRoute exact path="/keluarga-orang-tua" component={asyncComponent(() => import("../pages/EmployeeParent"))} />
                <PrivateRoute exact path="/keluarga-orang-tua/detail/:type/:id" component={asyncComponent(() => import("../pages/EmployeeParent/Detail"))} />
                <PrivateRoute exact path="/keluarga-orang-tua/detail/:type" component={asyncComponent(() => import("../pages/EmployeeParent/Detail"))} />
                <PrivateRoute exact path="/riwayat-pendidikan-sekolah" component={asyncComponent(() => import("../pages/EmployeeSchool"))} />
                <PrivateRoute exact path="/riwayat-pendidikan-sekolah/detail/:type/:id" component={asyncComponent(() => import("../pages/EmployeeSchool/Detail"))} />
                <PrivateRoute exact path="/riwayat-pendidikan-sekolah/detail/:type" component={asyncComponent(() => import("../pages/EmployeeSchool/Detail"))} />
                <PrivateRoute exact path="/riwayat-pendidikan-bahasa" component={asyncComponent(() => import("../pages/EmployeeLanguage"))} />
                <PrivateRoute exact path="/riwayat-pendidikan-bahasa/detail/:type/:id" component={asyncComponent(() => import("../pages/EmployeeLanguage/Detail"))} />
                <PrivateRoute exact path="/riwayat-pendidikan-bahasa/detail/:type" component={asyncComponent(() => import("../pages/EmployeeLanguage/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-jabatan" component={asyncComponent(() => import("../pages/EmployeePosition"))} />
                <PrivateRoute exact path="/kepegawaian-jabatan/detail/:type/:id" component={asyncComponent(() => import("../pages/EmployeePosition/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-jabatan/detail/:type" component={asyncComponent(() => import("../pages/EmployeePosition/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-pangkat" component={asyncComponent(() => import("../pages/EmployeeLevel"))} />
                <PrivateRoute exact path="/kepegawaian-pangkat/detail/:type/:id" component={asyncComponent(() => import("../pages/EmployeeLevel/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-pangkat/detail/:type" component={asyncComponent(() => import("../pages/EmployeeLevel/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-hukuman" component={asyncComponent(() => import("../pages/EmployeePenalty"))} />
                <PrivateRoute exact path="/kepegawaian-hukuman/detail/:type/:id" component={asyncComponent(() => import("../pages/EmployeePenalty/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-hukuman/detail/:type" component={asyncComponent(() => import("../pages/EmployeePenalty/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-diklat" component={asyncComponent(() => import("../pages/EmployeeTraining"))} />
                <PrivateRoute exact path="/kepegawaian-diklat/detail/:type/:id" component={asyncComponent(() => import("../pages/EmployeeTraining/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-diklat/detail/:type" component={asyncComponent(() => import("../pages/EmployeeTraining/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-penghargaan" component={asyncComponent(() => import("../pages/EmployeeHonor"))} />
                <PrivateRoute exact path="/kepegawaian-penghargaan/detail/:type/:id" component={asyncComponent(() => import("../pages/EmployeeHonor/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-penghargaan/detail/:type" component={asyncComponent(() => import("../pages/EmployeeHonor/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-penugasan" component={asyncComponent(() => import("../pages/EmployeeAssignment"))} />
                <PrivateRoute exact path="/kepegawaian-penugasan/detail/:type/:id" component={asyncComponent(() => import("../pages/EmployeeAssignment/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-penugasan/detail/:type" component={asyncComponent(() => import("../pages/EmployeeAssignment/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-seminar" component={asyncComponent(() => import("../pages/EmployeeSeminar"))} />
                <PrivateRoute exact path="/kepegawaian-seminar/detail/:type/:id" component={asyncComponent(() => import("../pages/EmployeeSeminar/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-seminar/detail/:type" component={asyncComponent(() => import("../pages/EmployeeSeminar/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-cuti" component={asyncComponent(() => import("../pages/EmployeeLeave"))} />
                <PrivateRoute exact path="/kepegawaian-cuti/detail/:type/:id" component={asyncComponent(() => import("../pages/EmployeeLeave/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-cuti/detail/:type" component={asyncComponent(() => import("../pages/EmployeeLeave/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-latihan-jabatan" component={asyncComponent(() => import("../pages/EmployeeJobTraining"))} />
                <PrivateRoute exact path="/kepegawaian-latihan-jabatan/detail/:type/:id" component={asyncComponent(() => import("../pages/EmployeeJobTraining/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-latihan-jabatan/detail/:type" component={asyncComponent(() => import("../pages/EmployeeJobTraining/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-tunjangan" component={asyncComponent(() => import("../pages/EmployeeAllowance"))} />
                <PrivateRoute exact path="/kepegawaian-tunjangan/detail/:type/:id" component={asyncComponent(() => import("../pages/EmployeeAllowance/Detail"))} />
                <PrivateRoute exact path="/kepegawaian-tunjangan/detail/:type" component={asyncComponent(() => import("../pages/EmployeeAllowance/Detail"))} />
                <PrivateRoute exact path="/mutasi" component={asyncComponent(() => import("../pages/EmployeeMutation"))} />
                <PrivateRoute exact path="/mutasi/detail/:type/:id" component={asyncComponent(() => import("../pages/EmployeeMutation/Detail"))} />
                <PrivateRoute exact path="/mutasi/detail/:type" component={asyncComponent(() => import("../pages/EmployeeMutation/Detail"))} />
                <PrivateRoute exact path="/skp" component={asyncComponent(() => import("../pages/SKP"))} />
                <PrivateRoute exact path="/skp/detail/:type/:id" component={asyncComponent(() => import("../pages/SKP/Detail"))} />
                <PrivateRoute exact path="/skp/detail/:type" component={asyncComponent(() => import("../pages/SKP/Detail"))} />
                <PrivateRoute exact path="/laporan-nominatif" component={asyncComponent(() => import("../pages/Report/Nominative"))} />
                <PrivateRoute exact path="/laporan-duk" component={asyncComponent(() => import("../pages/Report/DUK"))} />
                <PrivateRoute exact path="/laporan-bezetting" component={asyncComponent(() => import("../pages/Report/Bezetting"))} />
                <PrivateRoute exact path="/rekapitulasi-golongan" component={asyncComponent(() => import("../pages/Recapitulation/Group"))} />
                <PrivateRoute exact path="/rekapitulasi-jabatan" component={asyncComponent(() => import("../pages/Recapitulation/Position"))} />
                <PrivateRoute exact path="/rekapitulasi-unit-kerja" component={asyncComponent(() => import("../pages/Recapitulation/WorkUnit"))} />
             </div>
              <footer className="main-footer">
                <div className="pull-right hidden-xs">
                  <b>Version</b> 2.4.0
                </div>
                <b>Created by: </b> <a href="#">@galuhrmdh</a>
              </footer>
            </div>
          </div>
        </Route>
      </Switch>
    </Router>
  )
}

export default App;
