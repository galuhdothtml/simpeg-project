import PropTypes from "prop-types";
import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import ScrollArea from "react-scrollbar";
import _ from "lodash";
import Util from "../../utils/index";

const style = {
  mainSidebar: {
    height: "100%",
  },
  sidebar: {
    height: "100%",
    width: "auto",
  },
};
class Aside extends React.Component {
  static propTypes = {
    activeIdHeader: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
  }

  static defaultProps = {
    activeIdHeader: "",
  }

  constructor(props) {
    super(props);

    this.state = {
      activeId: "",
      menu: [],
      activeDropdown: "",
    };
  }

  componentWillReceiveProps = (nextProps) => {
    const { activeIdHeader } = this.props;

    if (activeIdHeader !== nextProps.activeIdHeader) {
      const { appMenu } = nextProps;

      const menu = appMenu.data.find(x => (String(x.id) === String(nextProps.activeIdHeader)));
      const basePath = Util.getBasePath();
      const activeId = this.findActiveId(basePath, menu.content);

      this.setState({ activeId, menu: menu.content });
    }
  }

  findActiveId = (basePath, menu) => {
    let retval = "";

    const found = menu.find((x) => {
      if (x.content) {
        const childrenFound = _.find(x.content, ["link", basePath]);

        if (childrenFound) {
          return true;
        }

        return false;
      }

      return x.link === basePath;
    });

    if (found) {
      retval = found.id;
      if (found.content) {
        this.setState({ activeDropdown: found.id });
        const childrenFound = _.find(found.content, ["link", basePath]);
        if (childrenFound) {
          retval = childrenFound.id;
        }
      }
    }

    return retval;
  };

  dropdownMenuClickHandler = (e, val) => {
    const { activeDropdown } = this.state;
    e.preventDefault();
    this.setState({ activeDropdown: activeDropdown === val ? "" : val });
  }

  menuClickHandler = (id, isChild = false) => {
    const newState = { activeId: id };

    if (!isChild) {
      Object.assign(newState, { activeDropdown: "" });
    }

    this.setState(newState);
  }

  render() {
    const { menu, activeId, activeDropdown } = this.state;

    return (
      <aside className="main-sidebar" style={style.mainSidebar}>
        <ScrollArea
          smoothScrolling={false}
          className="sidebar"
          style={style.sidebar}
          horizontal={false}
          stopScrollPropagation={true}
        >
          <ul className="sidebar-menu tree">
            {menu.map((x) => {
              if (x.content) {
                return (
                  <li key={x.id} className={`treeview ${(String(x.id) === String(activeDropdown)) && "active menu-open"}`}>
                    <a href="#" onClick={e => this.dropdownMenuClickHandler(e, x.id)}>
                      <i className={x.icon} />
                      <span>{x.title}</span>
                      <span className="pull-right-container">
                        <i className="fa fa-angle-left pull-right" />
                      </span>
                    </a>
                    <ul className="treeview-menu">
                      {x.content.map(childContent => (
                        <li key={childContent.id} className={(String(childContent.id) === String(activeId)) ? "active" : ""}>
                          <Link to={`/${childContent.link}`} onClick={() => this.menuClickHandler(childContent.id, true)}><i className="fa fa-circle-o" />{childContent.title}</Link>
                        </li>
                      ))}
                    </ul>
                  </li>
                );
              }

              return (
                <li key={x.id} className={(String(x.id) === String(activeId)) ? "active" : ""}>
                  <Link to={`/${x.link}`} onClick={() => this.menuClickHandler(x.id)}><i className={x.icon}></i> <span>{x.title}</span></Link>
                </li>
              );
            })}
          </ul>
        </ScrollArea>
      </aside>
    );
  }
}

const mapStateToProps = state => ({
  activeIdHeader: state.appNavigation.activeHeaderId,
  appMenu: state.apiRedux.appMenu,
});

export default connect(mapStateToProps)(Aside);
