/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Header from "./Header";
import Aside from "./Aside";
import { fetchMyAccount, fetchAppMenu } from "../../actions/apiActions";

class AppNavigation extends React.Component {
  static propTypes = {
    doFetchingMyAccount: PropTypes.func.isRequired,
    doFetchingAppMenu: PropTypes.func.isRequired,
  }

  componentDidMount = () => {
    this.fetchData();
  }

  fetchData = () => {
    const { doFetchingMyAccount, doFetchingAppMenu } = this.props;

    doFetchingAppMenu();
    doFetchingMyAccount();
  }

  render = () => (
      <React.Fragment>
        <Header />
        <Aside />
      </React.Fragment>
  )
}

const mapDispatchToProps = dispatch => ({
  doFetchingMyAccount: () => dispatch(fetchMyAccount()),
  doFetchingAppMenu: () => dispatch(fetchAppMenu()),
});

export default connect(null, mapDispatchToProps)(AppNavigation);
