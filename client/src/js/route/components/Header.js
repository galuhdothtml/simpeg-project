import React from "react";
import { connect } from "react-redux";
import _ from "lodash";
import PhotoPlaceholder from "../../../assets/template/img/photo_placeholder.png";
import Logo from "../../images/logo.png";
import Util from "../../utils/index";
import * as appNavigationAction from "../../actions/appNavigationActions";

const style = {
  profile_photo: {
    marginLeft: "10px",
  },
};
const findActiveId = (basePath, menu) => {
  const exist = menu.find((x) => {
    const found = x.content.find((fc) => {
      if (fc.link && fc.link === basePath) {
        return true;
      }

      if (fc.content) {
        const contentFound = _.find(fc.content, ["link", basePath]);
        return !!contentFound;
      }

      return false;
    });

    return found;
  });

  if (exist) {
    const { id } = exist;

    return String(id);
  }

  return "";
};
class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      fullname: "Administrator",
      profilePhoto: PhotoPlaceholder,
      showUserMenu: false,
      menu: [],
    };
  }

  componentWillReceiveProps = (nextProps) => {
    const { myAccount, appMenu } = this.props;

    if (myAccount !== nextProps.myAccount) {
      const { isFetching } = nextProps.myAccount;

      if (!isFetching) {
        const { data } = nextProps.myAccount;
        this.setupAccount(data);
      }
    }

    if (appMenu !== nextProps.appMenu) {
      const { isFetching } = nextProps.appMenu;

      if (!isFetching) {
        const { data } = nextProps.appMenu;

        this.setState({ menu: data }, () => {
          this.setupMenu(data);
        });
      }
    }
  }

  setupMenu = (menu) => {
    const { setActiveHeaderId } = this.props;
    const basePath = Util.getBasePath();
    const activeId = findActiveId(basePath, menu);

    setActiveHeaderId(activeId);
  }

  setupAccount = async (data) => {
    let profilePhoto = PhotoPlaceholder;

    if (data.imageFile) {
      profilePhoto = data.imageFile;
    }

    this.setState({ fullname: data.fullname, profilePhoto });
  }

  changeMenu = (e, id) => {
    const { setActiveHeaderId } = this.props;
    e.preventDefault();

    setActiveHeaderId(id);
  }

  toggleUserMenu = (e) => {
    const { showUserMenu } = this.state;

    e.preventDefault();

    this.setState({ showUserMenu: !showUserMenu });
  }

  logout = (e) => {
    e.preventDefault();

    Util.removeToken();
    location.href = "/auth/login";
  }

  render() {
    const {
      fullname, showUserMenu, profilePhoto, menu,
    } = this.state;
    const { activeId } = this.props;

    return (
      <header className="main-header">
        <a href="#" className="logo">
          <span className="logo-lg"><img src={Logo} /></span>
        </a>
        <nav className="navbar navbar-static-top">
          <div className="navbar-custom-menu" style={{ float: "none" }}>
            <ul className="nav navbar-nav">
              {
                menu.map(x => (
                  <li key={x.id} className={String(activeId) === String(x.id) ? "active" : ""}>
                    <a href="#" onClick={e => this.changeMenu(e, x.id)}>
                      <span>{x.title}</span>
                    </a>
                  </li>
                ))
              }
            </ul>
          </div>
          <div className="navbar-custom-menu">
            <ul className="nav navbar-nav">
              <li className={`dropdown user user-menu ${showUserMenu ? "open" : ""}`}>
                <a onClick={this.toggleUserMenu} href="#" className="dropdown-toggle">
                  <span className="hidden-xs">Hi, {fullname}</span>
                  <span className="pull-right">
                    <i className="fa fa-angle-down" />
                    <img src={profilePhoto} style={style.profile_photo} className="user-image" alt="User Image" />
                  </span>
                </a>
                <ul className="dropdown-menu">
                  <li className="user-body">
                    <div className="item">
                      <a href="#"><i className="fa fa-user" />Profil Saya</a>
                    </div>
                    <div className="item">
                      <a href="#"><i className="fa fa-lock" />Ganti Password</a>
                    </div>
                  </li>
                  <li className="user-footer">
                    <div className="item">
                      <a href="#" onClick={this.logout}><i className="fa fa-sign-out" />Logout</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
    );
  }
}

const mapStateToProps = state => ({
  myAccount: state.apiRedux.myAccount,
  appMenu: state.apiRedux.appMenu,
  activeId: state.appNavigation.activeHeaderId,
});

const mapDispatchToProps = dispatch => ({
  setActiveHeaderId: val => dispatch(appNavigationAction.setActiveHeaderId(val)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
