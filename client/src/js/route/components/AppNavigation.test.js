// eslint-disable-next-line no-unused-vars
import React from "react";
import { shallow } from "enzyme";
import store from "~/store";
import * as api from "../../data";
import AppNavigation from "./AppNavigation";

const fetchApiType = {
  APP_MENU: "APP_MENU",
  MY_ACCOUNT: "MY_ACCOUNT",
};

const fetchApi = (type) => {
  let resolveData = {};

  if (type === fetchApiType.APP_MENU) {
    resolveData = {
      status: true,
      data: [
        {
          id: 1,
          title: "Pegawai",
          content: [
            {
              id: "1.1",
              title: "Dashboard",
              link: "dashboard",
              icon: "fa fa-dashboard",
            },
            {
              id: "1.2",
              title: "Data Pegawai",
              link: "pegawai",
              icon: "fa fa-group",
            },
          ],
          createdAt: "2020-05-10T07:26:13.000Z",
          updatedAt: "2020-05-10T07:26:13.000Z",
        },
        {
          id: 2,
          title: "Laporan",
          createdAt: "2020-05-10T07:26:13.000Z",
          updatedAt: "2020-05-10T07:26:13.000Z",
        },
        {
          id: 3,
          title: "Pengaturan",
          createdAt: "2020-05-10T07:26:13.000Z",
          updatedAt: "2020-05-10T07:26:13.000Z",
        },
      ],
      msg: "success",
    };
  } else if (type === fetchApiType.MY_ACCOUNT) {
    resolveData = {
      status: true,
      data: {
        id: 4,
        username: "superadmin",
        passwd: "$2b$10$R6JCgdc/3yHG/2DAAGxYruSQV4EbJopkiG8khdUzPVg3bPF3TJ0DW",
        email: "superadmin@example.com",
        fullname: "Administrator",
        role: "1",
        last_login: "2020-05-07T12:42:46.000Z",
        createdAt: "2020-05-07T12:42:46.000Z",
        updatedAt: "2020-05-07T12:42:46.000Z",
        imageFile: "",
      },
      msg: "success",
    };
  }

  const promise = new Promise((resolve) => {
    resolve(resolveData);
  });

  return promise.then(data => data)
    .catch((e) => { throw new Error(e); });
};

describe("route/components/AppNavigation", () => {
  test("should dispatch actions", () => {
    api.getAppMenu = jest.fn().mockImplementation(() => fetchApi(fetchApiType.APP_MENU));
    api.getMyAccount = jest.fn().mockImplementation(() => fetchApi(fetchApiType.MY_ACCOUNT));
    const mockStore = store.getMockStore();

    const component = (
        <AppNavigation store={mockStore} />
    );

    shallow(component).dive();

    const expectedActions = [
      { type: "REQUEST_APP_MENU" }, { type: "REQUEST_MY_ACCOUNT" },
    ];

    expect(mockStore.getActions()).toEqual(expectedActions);
  });
});
