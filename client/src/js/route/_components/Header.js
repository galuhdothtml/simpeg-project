import PropTypes from "prop-types";
import React from "react";
import { connect } from "react-redux";
import PhotoPlaceholder from "../../../assets/template/img/photo_placeholder.png";
import Logo from "../../images/logo.png";
import Util from "../../utils/index";
import { fetchMyAccount } from "../../actions/apiActions";

const style = {
  profile_photo: {
    marginLeft: "10px",
  },
};
class Header extends React.Component {
  static propTypes = {
    changeEvent: PropTypes.func,
    menu: PropTypes.arrayOf(PropTypes.shape({})),
    id: PropTypes.string,
  }

  static defaultProps = {
    changeEvent: () => { },
    menu: [],
    id: "",
  }

  constructor(props) {
    super(props);

    this.state = {
      fullname: "Administrator",
      profilePhoto: null,
      showUserMenu: false,
    };
  }

  componentDidMount = () => {
    const { doFetchingMyAccount } = this.props;

    doFetchingMyAccount();
  }

  componentWillReceiveProps = (nextProps) => {
    const { myAccount } = this.props;

    if (myAccount !== nextProps.myAccount) {
      const { isFetching } = nextProps.myAccount;

      if (!isFetching) {
        const { data } = nextProps.myAccount;
        this.setupAccount(data);
      }
    }
  }

  setupAccount = async (data) => {
    let profilePhoto = PhotoPlaceholder;

    if (data.imageFile) {
      profilePhoto = data.imageFile;
    }

    this.setState({ fullname: data.fullname, profilePhoto });
  }

  changeMenu = (e, id) => {
    const { changeEvent } = this.props;

    e.preventDefault();

    changeEvent(id);
  }

  toggleUserMenu = (e) => {
    const { showUserMenu } = this.state;

    e.preventDefault();

    this.setState({ showUserMenu: !showUserMenu });
  }

  logout = (e) => {
    e.preventDefault();

    Util.removeToken();
    location.href = "/auth/login";
  }

  render() {
    const { menu, id } = this.props;
    const { fullname, showUserMenu, profilePhoto } = this.state;

    return (
      <header className="main-header">
        <a href="#" className="logo">
          <span className="logo-lg"><img src={Logo} /></span>
        </a>
        <nav className="navbar navbar-static-top">
          <div className="navbar-custom-menu" style={{ float: "none" }}>
            <ul className="nav navbar-nav">
              {
                menu.map(x => (
                  <li key={x.id} className={String(id) === String(x.id) ? "active" : ""}>
                    <a href="#" onClick={e => this.changeMenu(e, x.id)}>
                      <span>{x.title}</span>
                    </a>
                  </li>
                ))
              }
            </ul>
          </div>
          <div className="navbar-custom-menu">
            <ul className="nav navbar-nav">
              <li className={`dropdown user user-menu ${showUserMenu ? "open" : ""}`}>
                <a onClick={this.toggleUserMenu} href="#" className="dropdown-toggle">
                  <span className="hidden-xs">Hi, {fullname}</span>
                  <span className="pull-right">
                    <i className="fa fa-angle-down" />
                    <img src={profilePhoto} style={style.profile_photo} className="user-image" alt="User Image" />
                  </span>
                </a>
                <ul className="dropdown-menu">
                  <li className="user-body">
                    <div className="item">
                      <a href="#"><i className="fa fa-user" />Profil Saya</a>
                    </div>
                    <div className="item">
                      <a href="#"><i className="fa fa-lock" />Ganti Password</a>
                    </div>
                  </li>
                  <li className="user-footer">
                    <div className="item">
                      <a href="#" onClick={this.logout}><i className="fa fa-sign-out" />Logout</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
    );
  }
}

const mapStateToProps = state => ({
  myAccount: state.apiRedux.myAccount,
});

const mapDispatchToProps = dispatch => ({
  doFetchingMyAccount: () => dispatch(fetchMyAccount()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
