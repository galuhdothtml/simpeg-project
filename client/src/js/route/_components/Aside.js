import PropTypes from "prop-types";
import React from "react";
import { Link } from "react-router-dom";
import ScrollArea from "react-scrollbar";

const style = {
  mainSidebar: {
    height: "100%",
  },
  sidebar: {
    height: "100%",
    width: "auto",
  },
};
class Aside extends React.Component {
  static propTypes = {
    id: PropTypes.shape({
      withContent: "",
      noContent: "",
    }).isRequired,
    childrenId: PropTypes.string.isRequired,
    menu: PropTypes.arrayOf(PropTypes.shape({})),
    changeEvent: PropTypes.func.isRequired,
  }

  static defaultProps = {
    menu: [],
  }

  dropdownMenuClickHandler = (e, val) => {
    e.preventDefault();

    const {
      id: { withContent, noContent }, changeEvent, childrenId,
    } = this.props;
    let newValue = {
      noContent,
      withContent: "",
    };

    if (String(withContent) !== String(val)) {
      newValue = {
        withContent: String(val),
        noContent,
      };
    }

    changeEvent(newValue, childrenId);
  }

  menuClickHandler = (withContent, id, childrenId) => {
    const { changeEvent } = this.props;

    if (withContent) {
      const newValue = {
        withContent: id,
        noContent: "",
      };
      changeEvent(newValue, childrenId);
    } else {
      const newValue = {
        withContent: "",
        noContent: id,
      };
      changeEvent(newValue, childrenId);
    }
  }

  render() {
    const {
      menu: data, id, childrenId,
    } = this.props;

    return (
      <aside className="main-sidebar" style={style.mainSidebar}>
        <ScrollArea
          smoothScrolling={false}
          className="sidebar"
          style={style.sidebar}
          horizontal={false}
          stopScrollPropagation={true}
        >
          <ul className="sidebar-menu tree">
            {data.map((x) => {
              if (x.content) {
                return (
                  <li key={x.id} className={`treeview ${(String(x.id) === String(id.withContent)) && "active menu-open"}`}>
                    <a href="#" onClick={e => this.dropdownMenuClickHandler(e, x.id)}>
                      <i className={x.icon} />
                      <span>{x.title}</span>
                      <span className="pull-right-container">
                        <i className="fa fa-angle-left pull-right" />
                      </span>
                    </a>
                    <ul className="treeview-menu">
                      {x.content.map(xc => (
                        <li key={xc.id} className={(String(xc.id) === String(childrenId)) ? "active" : ""}>
                          <Link to={`/${xc.link}`} onClick={() => this.menuClickHandler(true, x.id, xc.id)}><i className="fa fa-circle-o" />{xc.title}</Link>
                        </li>
                      ))}
                    </ul>
                  </li>
                );
              }

              return (
                <li key={x.id} className={(String(x.id) === String(id.noContent)) ? "active" : ""}>
                  <Link to={`/${x.link}`} onClick={() => this.menuClickHandler(false, x.id, "")}><i className={x.icon}></i> <span>{x.title}</span></Link>
                </li>
              );
            })}
          </ul>
        </ScrollArea>
      </aside>
    );
  }
}

export default Aside;
