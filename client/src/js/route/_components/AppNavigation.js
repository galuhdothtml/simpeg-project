/* eslint prop-types: 0 */
import React from "react";
import Header from "./Header";
import Aside from "./Aside";
import { getAppMenu } from "../../data";
import Util from "../../utils";

class Wrapper extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentHeaderMenuId: "",
      currentSideMenuId: {
        noContent: "",
        withContent: "",
      },
      currentSideMenuChildrenId: "",
      headerMenu: [],
      sideMenu: [],
      rawMenu: [],
    };
  }

  componentWillMount = () => {
    this.fetchData();
  }

  fetchData = async () => {
    const res = await getAppMenu();
    let menu = [];

    if (res.status) {
      menu = res.data;
    }

    let currentLink = String(window.location.pathname).substring(1);

    if (currentLink.split("/").length > 0) {
      [currentLink] = currentLink.split("/");
    }

    const currentHeaderMenuId = Util.findHeaderMenuIdByLink(currentLink, menu);
    const headerMenu = menu.map(x => ({ id: x.id, title: x.title }));
    const currentSideMenuId = Util.findSideMenuIdByLink(currentLink, menu, currentHeaderMenuId);
    const sideMenu = this.sideMenuData(menu, currentHeaderMenuId);

    const currentSideMenuChildrenId = Util.findSideMenuChildrenId(sideMenu, currentSideMenuId.withContent, currentLink);

    this.setState({
      rawMenu: menu, currentHeaderMenuId, currentSideMenuId, headerMenu, sideMenu, currentSideMenuChildrenId,
    });
  }

  sideMenuData = (menu, headerMenuId) => {
    const data = Util.createSideMenuData(headerMenuId, menu);

    return data;
  }

  changeHeaderMenu = (val) => {
    const { rawMenu } = this.state;
    const sideMenu = this.sideMenuData(rawMenu, val);

    this.setState({ sideMenu, currentHeaderMenuId: String(val) });
  }

  changeSideMenu = (val, childrenId) => {
    const { sideMenu } = this.state;
    const data = sideMenu.map(x => (Object.assign(x, { isActive: String(val) === String(x.id) })));
    const newData = val;

    this.setState({ sideMenu: data, currentSideMenuId: newData, currentSideMenuChildrenId: String(childrenId) });
  }

  render() {
    const {
      currentHeaderMenuId, headerMenu, currentSideMenuId, sideMenu, currentSideMenuChildrenId,
      rawMenu,
    } = this.state;

    return (
      <React.Fragment>
        <Header
          changeEvent={this.changeHeaderMenu}
          id={currentHeaderMenuId}
          menu={headerMenu}
          rawMenu={rawMenu}
        />
        <Aside
          changeEvent={this.changeSideMenu}
          id={currentSideMenuId}
          childrenId={currentSideMenuChildrenId}
          menu={sideMenu}
        />
      </React.Fragment>
    );
  }
}

export default Wrapper;
